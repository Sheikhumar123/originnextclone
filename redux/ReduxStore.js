import { configureStore } from "@reduxjs/toolkit";
import ServiceProviderSlice from "./Slices/ServiceProviderSlice/ServiceProviderSlice";
import ProviderdashboardSlice from "./Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import GlobalSlice from "./Slices/GlobalSlice/GlobalSlice";
import ServiceProfileSlice from "./Slices/ProviderdashboardSlice/ServiceProfileSlice/ServiceProfileSlice";
import InvestSlice from "./Slices/InvestorDashboard/InvestSlice/InvestSlice";

// import MyProfileSlice from "./Slices/Profile/MyProfileSlice";

const ReduxStore = configureStore({
  reducer: {
    ServiceProviderSlice: ServiceProviderSlice,
    ProviderdashboardSlice: ProviderdashboardSlice,
    GlobalSlice: GlobalSlice,
    ProviderServiceProfile: ServiceProfileSlice,
    InvestSlice: InvestSlice,
  },
});

export default ReduxStore;
// export const makeStore  = () => {
//     return configureStore({
//       reducer: {
//         ProfileSlice:ProfileSlice
//       },
//     })
//   }
