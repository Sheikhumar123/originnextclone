import { baseUrl } from "@/utils/BaseURL";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import Cookies from "js-cookie";
// import Cookies from "js-cookie";

const initialState = {
  message: null,
  error: null,
  isLogin: true,
  status: "idle", //pending,success,failed
  AllFaq: [],
};

export const GetFAQ = createAsyncThunk(
  "userdata/getFAQsByUserId",
  async ({id}) => {
    let obj = {
      user_id: id,
    };
    var config = {
      method: "post",
      url: `${baseUrl}userdata/getFAQsByUserId`,
      headers: {
        Authorization:`${Cookies.get("orignToken")}`,
        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/json",

      },
      data: obj,
    };
    let res = await axios(config);
    return res.data;
  }
);

const GlobalSlice = createSlice({
  name: "GlobalSlice",
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(GetFAQ.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetFAQ.fulfilled, (state, action) => {
        state.status = "success";
        state.AllFaq = action.payload?.data;
      })
      .addCase(GetFAQ.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.AllFaq = null;
        }
      });
  },
});

export const GetGlobalDetails = (state) => state.GlobalSlice;
export default GlobalSlice.reducer;
