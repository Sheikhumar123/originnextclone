import { baseUrl } from "@/utils/BaseURL";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import Cookies from "js-cookie";
// import Cookies from "js-cookie";

const initialState = {
  message: null,
  error: null,
  isLogin: true,
  status: "idle", //pending,success,failed
  AllInvestment: [],
  AllInvestorDashboardData: [],
  InvestorReferalData: [],
  InvestorWithdrawalData: null,
  QRdetailData: null,
  VendorCodesData: null,
};
export const GetQRdetail = createAsyncThunk(
  "auth/getQRdetail",
  async () => {
    var config = {
      method: "get",
      url: `${baseUrl}auth/getQRdetail`,
      headers: {
        Authorization: `${Cookies.get("orignToken")}`,
        "Content-Type": "application/json",
      },
    };
    let res = await axios(config);
    return res.data;
  }
);
export const GetAllInvestment = createAsyncThunk(
  "investor/getusertraqnsactions",
  async (obj) => {
    var config = {
      method: "post",
      url: `${baseUrl}investor/getusertraqnsactions`,
      headers: {
        Authorization: `${Cookies.get("orignToken")}`,
        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/json",
      },
      data: obj,
    };
    let res = await axios(config);
    return res.data;
  }
);
export const GetInvestorDashboardData = createAsyncThunk(
  "investor/getinvestordashboard",
  async (obj) => {
    var config = {
      method: "post",
      url: `${baseUrl}investor/getinvestordashboard`,
      headers: {
        Authorization: `${Cookies.get("orignToken")}`,
        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/json",
      },
      data: obj,
    };
    let res = await axios(config);
    return res.data;
  }
);
export const GetInvestorRefrelPage = createAsyncThunk(
  "investor/getinvestorreferalinfo",
  async (obj) => {
    var config = {
      method: "post",
      url: `${baseUrl}investor/getinvestorreferalinfo`,
      headers: {
        Authorization: `${Cookies.get("orignToken")}`,
        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/json",
      },
      data: obj,
    };
    let res = await axios(config);
    return res.data;
  }
);
export const GetInvestorWithdrawPage = createAsyncThunk(
  "investor/getuserwithdrawls",
  async (obj) => {
    var config = {
      method: "post",
      url: `${baseUrl}investor/getuserwithdrawls`,
      headers: {
        Authorization: `${Cookies.get("orignToken")}`,
        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/json",
      },
      data: obj,
    };
    let res = await axios(config);
    return res.data;
  }
);
export const GetAllVendorCodes = createAsyncThunk(
  "investor/getvendorcodes",
  async (obj) => {
    var config = {
      method: "post",
      url: `${baseUrl}investor/getvendorcodes`,
      headers: {
        Authorization: `${Cookies.get("orignToken")}`,
        "Content-Type": "application/json",
      },
      data: obj,
    };
    let res = await axios(config);
    return res.data;
  }
);
const InvestSlice = createSlice({
  name: "InvestSlice",
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(GetAllInvestment.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetAllInvestment.fulfilled, (state, action) => {
        state.status = "success";
        state.AllInvestment = action.payload;
      })
      .addCase(GetAllInvestment.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.AllInvestment = null;
        }
      })
      .addCase(GetInvestorDashboardData.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetInvestorDashboardData.fulfilled, (state, action) => {
        state.status = "success";
        state.AllInvestorDashboardData = action.payload;
      })
      .addCase(GetInvestorDashboardData.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.AllInvestorDashboardData = null;
        }
      })
      .addCase(GetInvestorRefrelPage.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetInvestorRefrelPage.fulfilled, (state, action) => {
        state.status = "success";
        state.InvestorReferalData = action.payload;
      })
      .addCase(GetInvestorRefrelPage.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.InvestorReferalData = null;
        }
      })
      .addCase(GetInvestorWithdrawPage.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetInvestorWithdrawPage.fulfilled, (state, action) => {
        state.status = "success";
        state.InvestorWithdrawalData = action.payload;
      })
      .addCase(GetInvestorWithdrawPage.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.InvestorWithdrawalData = null;
        }
      })
      .addCase(GetQRdetail.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetQRdetail.fulfilled, (state, action) => {
        state.status = "success";
        state.QRdetailData = action.payload;
      })
      .addCase(GetQRdetail.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.QRdetailData = null;
        }
      })
      .addCase(GetAllVendorCodes.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetAllVendorCodes.fulfilled, (state, action) => {
        state.status = "success";
        state.VendorCodesData = action.payload;
      })
      .addCase(GetAllVendorCodes.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.VendorCodesData = null;
        }
      });
  },
});

export const GetInvestDetails = (state) => state.InvestSlice;
export default InvestSlice.reducer;
