import axiousInstance from "@/utils/axiousInstance";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import Cookies from "js-cookie";

const initialState = {
  message: null,
  error: null,
  isLogin: true,
  status: "idle", //pending,success,failed
  ServiceProfileDetailData: null,
  ServiceProfileSimilarServicesData: null,
  ServiceProfileFaqsData: null,
  ServiceProfileMoreServicesData: null,
  ServiceProfileMoreReviewsData: null,
};

export const GetServiceProfileDetailData = createAsyncThunk(
  "landingpage/servicesproviderdetaildata",
  async (id) => {
    let res = await axiousInstance.get(
      `landingpage/servicesproviderdetaildata?user_id=${id}`,
      {
        headers: {
          Authorization: `${Cookies.get("orignToken")}`,
          "Content-Type": "application/json",
        },
      }
    );
    return res.data;
  }
);

export const GetServiceProfileSimilarServices = createAsyncThunk(
  "landingpage/getsimilarservices",
  async (id) => {
    let res = await axiousInstance.get(
      `landingpage/getsimilarservices?user_id=${id}`,
      {
        headers: {
          Authorization: `${Cookies.get("orignToken")}`,
          "Content-Type": "application/json",
        },
      }
    );
    return res.data;
  }
);

export const GetServiceProfileFaqs = createAsyncThunk(
  "landingpage/getservicesfaqs",
  async (payload) => {
    let res = await axiousInstance.get(
      `landingpage/getservicesfaqs?user_id=${payload?.user_id}&limit=${payload?.limit}`,
      {
        headers: {
          Authorization: `${Cookies.get("orignToken")}`,
          "Content-Type": "application/json",
        },
      }
    );
    return res.data;
  }
);

export const GetServiceProfileMoreServices = createAsyncThunk(
  "landingpage/providermoreservices",
  async (payload) => {
    let res = await axiousInstance.get(
      `landingpage/providermoreservices?user_id=${payload?.user_id}&limit=${payload?.limit}`,
      {
        headers: {
          Authorization: `${Cookies.get("orignToken")}`,
          "Content-Type": "application/json",
        },
      }
    );
    return res.data;
  }
);

export const GetServiceProfileReviews = createAsyncThunk(
  "landingpage/providerreviews",
  async (payload) => {
    let res = await axiousInstance.get(
      `landingpage/providerreviews?user_id=${payload?.user_id}&page=${payload?.page}`,
      {
        headers: {
          Authorization: `${Cookies.get("orignToken")}`,
          "Content-Type": "application/json",
        },
      }
    );
    return res.data;
  }
);

const ServiceProfileSlice = createSlice({
  name: "ServiceProfileSlice",
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(GetServiceProfileDetailData.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetServiceProfileDetailData.fulfilled, (state, action) => {
        state.status = "success";
        state.ServiceProfileDetailData = action.payload?.data;
      })
      .addCase(GetServiceProfileDetailData.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ServiceProfileDetailData = null;
        }
      })
      .addCase(GetServiceProfileSimilarServices.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetServiceProfileSimilarServices.fulfilled, (state, action) => {
        state.status = "success";
        state.ServiceProfileSimilarServicesData = action.payload;
      })
      .addCase(GetServiceProfileSimilarServices.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ServiceProfileSimilarServicesData = null;
        }
      })
      .addCase(GetServiceProfileFaqs.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetServiceProfileFaqs.fulfilled, (state, action) => {
        state.status = "success";
        state.ServiceProfileFaqsData = action.payload;
      })
      .addCase(GetServiceProfileFaqs.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ServiceProfileFaqsData = null;
        }
      })
      .addCase(GetServiceProfileMoreServices.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetServiceProfileMoreServices.fulfilled, (state, action) => {
        state.status = "success";
        state.ServiceProfileMoreServicesData = action.payload;
      })
      .addCase(GetServiceProfileMoreServices.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ServiceProfileMoreServicesData = null;
        }
      })
      .addCase(GetServiceProfileReviews.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetServiceProfileReviews.fulfilled, (state, action) => {
        state.status = "success";
        state.ServiceProfileMoreReviewsData = action.payload;
      })
      .addCase(GetServiceProfileReviews.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ServiceProfileMoreReviewsData = null;
        }
      });
  },
});

export const GetProviderServiceProfileDetails = (state) =>
  state.ProviderServiceProfile;
export default ServiceProfileSlice.reducer;
