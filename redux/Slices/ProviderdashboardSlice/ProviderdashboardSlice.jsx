import { baseUrl } from "@/utils/BaseURL";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import Cookies from "js-cookie";
// import Cookies from "js-cookie";

const initialState = {
  message: null,
  error: null,
  isLogin: true,
  status: "idle", //pending,success,failed
  ProviderProfileData: null,
  ProviderDashServices: [],
  ProviderDashOffers: [],
  ProvoderDashRating: [],
  ProvoderTrustPointData: null,
};

export const GetProviderProfileData = createAsyncThunk(
  "dashboard/getsingleuserprofile?user_id=",
  async (id) => {
    var config = {
      method: "get",
      url: `${baseUrl}dashboard/getsingleuserprofile?user_id=${id}`,
      headers: {
        Authorization:`${Cookies.get("orignToken")}`,
        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };
    let res = await axios(config);
    return res.data;
  }
);

export const GetProviderDashServices = createAsyncThunk(
  "dashboard/getservicesofsingleprovider",
  async ({ user_id, page }) => {
    var config = {
      method: "post",
      url: `${baseUrl}dashboard/getservicesofsingleprovider`,
      headers: {
        
        Authorization:`${Cookies.get("orignToken")}`,

        "Content-Type": "application/json",
        // Authorization: Cookies.get("orignToken"),
      },
      data: { user_id: user_id, page: page },
    };
    let res = await axios(config);
    return res.data;
  }
);

export const GetServiceProviderOffers = createAsyncThunk(
  "dashboard/getoffersofuser",
  async ({ user_id, page }) => {
    var config = {
      method: "get",
      url: `${baseUrl}dashboard/getoffersofuser?user_id=${user_id}&page=${page}`,
      headers: {
        Authorization:`${Cookies.get("orignToken")}`,

        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/json",
      },
    };
    let res = await axios(config);
    return res.data;
  }
);

export const GetServiceProviderRating = createAsyncThunk(
  "dashboard/userreviewsdata",
  async ({user_id,type,rating,page}) => {
    var config = {
      method: "post",
      url: `${baseUrl}dashboard/userreviewsdata`,
      headers: {
        Authorization:`${Cookies.get("orignToken")}`,

        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/json",
      },
      data:{
        user_id:user_id,
        rate:rating,
        type:type,
        page:page
      }
    };
    let res = await axios(config);
    return res.data;
  }
);

export const GetProviderTrustPoint = createAsyncThunk(
  "dashboard/gettrustpoints",
  async (user_id) => {
    var config = {
      method: "get",
      url: `${baseUrl}dashboard/gettrustpoints?user_id=${user_id}`,
      headers: {
        Authorization:`${Cookies.get("orignToken")}`,
        "Content-Type": "application/json",
      },
    };
    let res = await axios(config);
    return res.data;
  }
);

const ProviderdashboardSlice = createSlice({
  name: "ProviderdashboardSlice",
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(GetProviderProfileData.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetProviderProfileData.fulfilled, (state, action) => {
        state.status = "success";
        state.ProviderProfileData = action.payload?.updateuser;
      })
      .addCase(GetProviderProfileData.rejected, (state, action) => {
        console.log(action);
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ProviderProfileData = null;
        }
      })
      .addCase(GetProviderDashServices.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetProviderDashServices.fulfilled, (state, action) => {
        state.status = "success";
        state.ProviderDashServices = action.payload;
      })
      .addCase(GetProviderDashServices.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ProviderDashServices = null;
        }
      })
      .addCase(GetServiceProviderOffers.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetServiceProviderOffers.fulfilled, (state, action) => {
        state.status = "success";
        state.ProviderDashOffers = action.payload?.Offersdt;
      })
      .addCase(GetServiceProviderOffers.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ProviderDashOffers = null;
        }
      })
      .addCase(GetServiceProviderRating.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetServiceProviderRating.fulfilled, (state, action) => {
        state.status = "success";
        state.ProvoderDashRating = action.payload;
      })
      .addCase(GetServiceProviderRating.rejected, (state, action) => {
        console.log(action);
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ProvoderDashRating = null;
        }
      })
      .addCase(GetProviderTrustPoint.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetProviderTrustPoint.fulfilled, (state, action) => {
        state.status = "success";
        state.ProvoderTrustPointData = action.payload.userdata;
      })
      .addCase(GetProviderTrustPoint.rejected, (state, action) => {
        console.log(action);
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ProvoderTrustPointData = null;
        }
      });
  },
});

export const GetProviderdashboardDetails = (state) =>
  state.ProviderdashboardSlice;
export default ProviderdashboardSlice.reducer;
