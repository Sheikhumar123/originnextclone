import { baseUrl } from "@/utils/BaseURL";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import Cookies from "js-cookie";
// import Cookies from "js-cookie";

const initialState = {
  message: null,
  error: null,
  isLogin: true,
  status: "idle", //pending,success,failed
  ProviderCategories: [],
  ProviderSubCategories: [],
  ProviderSkills: [],
  ProviderServices: [],
};

export const GetProviderCategories = createAsyncThunk(
  "/landingpage/categorypopulateform",
  async () => {
    var config = {
      method: "get",
      url: `${baseUrl}landingpage/categorypopulateform`,
      headers: {
        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };
    let res = await axios(config);
    return res.data;
  }
);
export const GetProviderSubCategories = createAsyncThunk(
  "/landingpage/subcategorypopulate",
  async (data) => {
    var config = {
      method: "post",
      url: `${baseUrl}landingpage/subcategorypopulate`,
      headers: {
        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/json",
      },
      data: { categories: data },
    };
    let res = await axios(config);
    return res.data;
  }
);
export const GetProviderSkills = createAsyncThunk(
  "/landingpage/skillspopulateform",
  async () => {
    var config = {
      method: "get",
      url: `${baseUrl}landingpage/skillspopulateform`,
      headers: {
        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };
    let res = await axios(config);
    return res.data;
  }
);
export const GetProviderServicesPopulate = createAsyncThunk(
  "/dashboard/Offerspopulate",
  async (data) => {
    var config = {
      method: "post",
      url: `${baseUrl}dashboard/Offerspopulate`,
      data: data,
      headers: {
        Authorization:`${Cookies.get("orignToken")}`,
        // authorization: `${Cookies.get("token")}`,
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };
    let res = await axios(config);
    return res.data;
  }
);
const ServiceProviderSlice = createSlice({
  name: "ServiceProviderSlice",
  initialState,
  extraReducers: (builder) => {
    builder
      .addCase(GetProviderCategories.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetProviderCategories.fulfilled, (state, action) => {
        state.status = "success";
        state.ProviderCategories = action.payload?.Categories;
      })
      .addCase(GetProviderCategories.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ProviderCategories = null;
        }
      })
      .addCase(GetProviderSubCategories.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetProviderSubCategories.fulfilled, (state, action) => {
        state.status = "success";
        state.ProviderSubCategories = action.payload?.subcategory;
      })
      .addCase(GetProviderSubCategories.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ProviderSubCategories = null;
        }
      })
      .addCase(GetProviderSkills.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetProviderSkills.fulfilled, (state, action) => {
        state.status = "success";
        state.ProviderSkills = action.payload?.Skillsdata;
      })
      .addCase(GetProviderSkills.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ProviderSkills = null;
        }
      })
      .addCase(GetProviderServicesPopulate.pending, (state) => {
        state.status = "pending";
      })
      .addCase(GetProviderServicesPopulate.fulfilled, (state, action) => {
        state.status = "success";
        state.ProviderServices = action.payload?.data;
      })
      .addCase(GetProviderServicesPopulate.rejected, (state, action) => {
        if (action?.error?.message.includes(401)) {
          state.message = action.error.message;
          state.isLogin = false;
        } else {
          state.error = action.error.message;
          state.status = "rejected";
          state.ProviderServices = null;
        }
      });
  },
});

export const GetServiceProviderDetails = (state) => state.ServiceProviderSlice;
export default ServiceProviderSlice.reducer;
