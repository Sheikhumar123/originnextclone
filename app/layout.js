import { ToastContainer } from "react-toastify";
import "./globals.css";
import "react-toastify/dist/ReactToastify.css";
import "@smastrom/react-rating/style.css";
import StoreProvider from "@/utils/StoreProvider";
export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <ToastContainer
        position="top-right"
        autoClose={3000}
      />
      <StoreProvider>
        <body>{children}</body>
      </StoreProvider>
    </html>
  );
}
