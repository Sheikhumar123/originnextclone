"use client";

import ContactDetails from "@/components/influencer/contact/ContactDetails";
import CredentialsForm from "@/components/influencer/contact/CredentialsForm";
import DescriptionForm from "@/components/influencer/contact/Descriptionform";
import PricingForm from "@/components/influencer/contact/PricingForm";

import { useState } from "react";

const Page = () => {
  const [data, setData] = useState({});
  const [step, setStep] = useState(1);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  return (
    <>
      {step === 1 && (
        <DescriptionForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 2 && (
        <ContactDetails
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
        />
      )}
      {step === 3 && (
        <CredentialsForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 4 && (
        <PricingForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
    </>
  );
};

export default Page;
