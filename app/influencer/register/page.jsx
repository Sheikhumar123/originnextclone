"use client";

import ClientDetailsForm from "@/components/influencer/register/ClientDetailsForm";
import Completion from "@/components/influencer/register/Completion";
import DetailsForm from "@/components/influencer/register/DetailsForm";
import FAQForm from "@/components/influencer/register/FAQForm";
import MoreDetailsForm from "@/components/influencer/register/MoreDetails";
import NotificationsForm from "@/components/influencer/register/NotificationsForm";
import SocialMediaDetails from "@/components/influencer/register/SocialMediaDetails";
import SubtitleForm from "@/components/influencer/register/SubtitleForm";
import UsernameForm from "@/components/influencer/register/UsernameForm";
import { useState } from "react";

const Page = () => {
  const [data, setData] = useState({});
  const [step, setStep] = useState(1);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  return (
    <>
      {step === 1 && (
        <UsernameForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 2 && (
        <DetailsForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 3 && (
        <NotificationsForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 4 && (
        <MoreDetailsForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}{" "}
      {step === 5 && (
        <SubtitleForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 6 && (
        <ClientDetailsForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 7 && (
        <SocialMediaDetails
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 8 && (
        <FAQForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
        />
      )}
      {step === 9 && (
        <Completion
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
        />
      )}
    </>
  );
};

export default Page;
