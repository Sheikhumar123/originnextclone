"use client"
import ProfileForm from "@/components/dashboard/ProfileForm/ProfileForm";
import { manrope } from "@/utils/fontPoppins";
import { IoSettingsOutline } from "react-icons/io5";

const page = () => {
  return (
    <div className={`w-full px-3 ${manrope.className}`}>
      <p className="text-2xl font-semibold text-[#404040] capitalize">
        General Settings
      </p>
      <p className="text-md text-[#6a6a6a]  w-full pt-2">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis dol{" "}
      </p>
      <div className=" p-2 w-[fit-content] flex flex-col gap-2">
        <div className="flex gap-2 items-center px-2">
          <IoSettingsOutline
            className="text-mainpurple"
            size={30}
          />
          <p className="text-xl  text-mainpurple capitalize">My Profile</p>
        </div>
        <div className="w-[100%] border-t-[2px] border-mainpurple"></div>
      </div>
      <div className="pt-2">
        <ProfileForm />
      </div>
    </div>
  );
};

export default page;
