import ContactForm from "@/components/providersDashboard/ServiceProvider/ContactForm/ContactForm";
import { manrope } from "@/utils/fontPoppins";

const Page = () => {
  return (
    <div className={`w-full px-3 ${manrope.className}`}>
      <div className="mb-3">
        <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
          Contact Us
        </h1>
        <p className="text-[#525252">Get Connected To Grow Better Business.</p>
      </div>
      <ContactForm type="ServiceProvider" />
    </div>
  );
};

export default Page;
