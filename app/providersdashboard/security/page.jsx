"use client"

import { manrope } from "@/utils/fontPoppins";
import { IoSettingsOutline } from "react-icons/io5";
import { MdOutlineSecurity } from "react-icons/md";
import Security from "@/components/providersDashboard/generalSetting/Security";
const page = () => {
  return (
    <div className={`w-full px-3 ${manrope.className}`}>
        <p className="text-2xl font-semibold text-[#404040] capitalize">
          General Settings
        </p>
        <p className="text-md text-[#6a6a6a]  w-full pt-2">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis dol{" "}
        </p>
        <div className=" p-2 w-[fit-content] flex flex-col gap-2">
          <div className="flex gap-2 items-center px-2">
            <MdOutlineSecurity  className="text-mainpurple" size={30} />
            <p className="text-xl  text-mainpurple capitalize">
              Security
            </p>
          </div>
          <div className="w-[100%] border-t-[2px] border-mainpurple"></div>
        </div>
        <div className="pt-2">
      <Security />
      </div>

    </div>
  );
};

export default page;
