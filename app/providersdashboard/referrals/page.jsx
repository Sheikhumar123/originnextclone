import RefferalList from "@/components/dashboard/ReferralList";
import { manrope } from "@/utils/fontPoppins";
import { IoShareSocialOutline } from "react-icons/io5";

const page = () => {
  return (
    <>
      <div className={`w-full px-3 ${manrope.className}`}>
        <div className="flex items-center justify-between flex-wrap gap-3 mb-6 ">
          <div>
            <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
              Add more amount to get more profit
            </h1>
            <p className="text-xl font-[400]">
              Only <span className="text-red-600 font-[600]">400</span> slots
              are left that can invest in our project
            </p>
          </div>
          <div className="flex flex-col items-center gap-2 text-nowrap ">
            <p className="text-lg text-gray-500">Total Earned</p>
            <h1 className="text-3xl text-green-500 font-bold">41.5 INR</h1>
          </div>
        </div>
        <div className="mb-3">
          <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
            Your Refferal Link
          </h1>
          <div
            className={`flex items-center justify-between w-full p-4 bg-[#f4f9fd] border border-gray-200 ${manrope.className} rounded-[40px] mt-3 flex-wrap gap-2`}
          >
            <p className="text-[#444BDB]  flex-grow overflow-x-auto md:flex-grow-0  ">
              hdhbfcyu4e8r734erfyuhdcxjchbsjdvbasjskdghv667fkere.....
            </p>
            <div className="flex items-center justify-between gap-4 flex-grow md:flex-grow-0 ">
              <div className="w-[50px] h-[50px] rounded-full bg-blue-100 flex items-center justify-center">
                <IoShareSocialOutline size={25} style={{ color: "#808080" }} />
              </div>
              <button className="bg-[#434CD9] flex-grow md:flex-grow-0  px-12 py-3 rounded-3xl text-white">
                Copy
              </button>
            </div>
          </div>
        </div>
        <div className="mb-3">
          <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
            Your Refferal Code
          </h1>
          <div
            className={`flex items-center justify-between w-full p-4 bg-[#f4f9fd] border border-gray-200 ${manrope.className} rounded-[40px] mt-3 flex-wrap gap-2`}
          >
            <p className="text-black  flex-grow overflow-x-auto md:flex-grow-0  ">
              12345746237846987346597
            </p>
            <div className="flex items-center justify-between gap-4 flex-grow md:flex-grow-0 ">
              <div className="w-[50px] h-[50px] rounded-full bg-blue-100 flex items-center justify-center">
                <IoShareSocialOutline size={25} style={{ color: "#808080" }} />
              </div>
              <button className="bg-[#434CD9] flex-grow md:flex-grow-0  px-12 py-3 rounded-3xl text-white">
                Copy
              </button>
            </div>
          </div>
        </div>
        <RefferalList />
      </div>
    </>
  );
};

export default page;
