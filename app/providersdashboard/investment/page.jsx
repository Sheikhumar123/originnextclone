import InvestmentForm from "@/components/dashboard/InvestmentForm";
import InvestmentList from "@/components/dashboard/InvestmentList";
import { manrope } from "@/utils/fontPoppins";

const InvestmentPage = () => {
  return (
    <>
      <div className={`w-full p-4 ${manrope.className}`}>
        <h1 className="text-2xl font-semibold mb-3">
          Add more amount to get profit
        </h1>
        <InvestmentForm />
        <InvestmentList />
      </div>
    </>
  );
};

export default InvestmentPage;
