"use client";
import BgGradient from "@/components/BgGradient/BgGradient";
import CategoriesSection from "@/components/providersDashboard/help/CategoriesSection";
import QuestionDetail from "@/components/providersDashboard/help/QuestionDetail";
import SelectedCategorySection from "@/components/providersDashboard/help/SelectedCategorySection";

import React, { useState } from "react";

let Categories = [
  {
    title: "Category name here",
    description:
      "  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatibus repellat non sapiente",
    imagePath: "/icons/usericon.png",
    subCategories: [
      {
        title: "Tasker 101",
        questions: [
          {
            question: "Which Skills Should I Add To My Profile?",
            shortDescription:
              "Sed non dui aliquam, ullamcorper est non, aliquet mauris. Quisque lacus ligula, dapibus nec dignissim non, tincidunt vel quam. Etiam porttitor iaculis odio.",
            user: {
              imagePath: "/user.png",
              userName: "Lance Bogrol",
            },
            lastUpdate: "date then convert it into text ",
            twitterLink: "",
            linkedIn: "",
            emailLink: "",
            googleLink: "",
            longDescription: `Suspendisse eu scelerisque tellus. Curabitur non tincidunt nisl, sit amet interdum ex. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Cras congue eros vitae risus tincidunt, nec pulvinar massa bibendum. Nam sit amet convallis mauris. 
            Praesent pulvinar sollicitudin lacus eu luctus. Donec eget ante et mi finibus imperdiet id nec quam. In rutrum, massa non porta malesuada, magna quam ullamcorper nunc, ut placerat erat metus et leo. Fusce sed dapibus libero. Nullam eleifend libero a lectus blandit, elementum euismod arcu elementum. Donec tempus varius mi, vitae aliquam erat luctus et. Aliquam in lacinia nunc.`,
            points: [
              {
                title: "Free Updates",
                description:
                  "Morbi pellentesque finibus libero, in blandit lorem eleifend eget. Praesent egestas hendrerit augue a vestibulum. Nullam fringilla, eros malesuada eleifend placerat, lacus tellus egestas erat, nec varius sem lorem ut mauris. Morbi libero felis.",
              },
              {
                title: "Free Updates",
                description:
                  "Morbi pellentesque finibus libero, in blandit lorem eleifend eget. Praesent egestas hendrerit augue a vestibulum. Nullam fringilla, eros malesuada eleifend placerat, lacus tellus egestas erat, nec varius sem lorem ut mauris. Morbi libero felis.",
              },
            ],
          },
          {
            question: "How Do I Switch To Buying?",
          },
          {
            question: "What Is The Scope For Services?",
          },
        ],
      },
      {
        title: "Account & Profile Settings",
        questions: [
          {
            question: "How To Reset Password?",
          },
          {
            question: "How Can I Change Email Address?",
          },
          {
            question: "How To Add Skills?",
          },
        ],
      },
      {
        title: "What Do I Need To Know?",
        questions: [
          {
            question: "Do I Need To Enter My Phone Number?",
          },
          {
            question: "Password Requirements",
          },
          {
            question: "How Do I Place Bids?",
          },
          {
            question: "How Do I Connect With Client?",
          },
        ],
      },
      {
        title: "Before Registration",
        questions: [
          {
            question: "How Does Tasker Work?",
          },
          {
            question: "Why Should I Become A Tasker?",
          },
        ],
      },
      {
        title: "Gigs Information",
        questions: [
          {
            question: "Where Can I Bid On Jobs?",
          },
          {
            question: "Can Client See My Bids?",
          },
          {
            question: "Maximum Amount For Gigs",
          },
        ],
      },
      {
        title: "Important Announcements ",
        questions: [
          {
            question: "Perks Of Becoming A Tasker",
          },
          {
            question: "Important Benefits",
          },
          {
            question: "Partnerships Information",
          },
        ],
      },
    ],
  },
  {
    title: "Buying",
    description:
      "  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatibus repellat non sapiente",
    imagePath: "/icons/carticon.png",
    subCategories: [],
  },
  {
    title: "Payment & Withdrawals",
    description:
      "  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatibus repellat non sapiente",
    imagePath: "/icons/paymenticon.png",
    subCategories: [],
  },
  {
    title: "Selling",
    description:
      "  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatibus repellat non sapiente",
    imagePath: "/icons/sellingicon.png",
    subCategories: [],
  },
  {
    title: "Quotes",
    description:
      "  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatibus repellat non sapiente",
    imagePath: "/icons/qoutesicon.png",
    subCategories: [],
  },
  {
    title: "Safety & Policies",
    description:
      "  Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptatibus repellat non sapiente",
    imagePath: "/icons/safetyicon.png",
    subCategories: [],
  },
];

const page = () => {
  const [selectedCategory, setselectedCategory] = useState(null);
  const [selectedQuestion, setselectedQuestion] = useState(null);
  const [step, setstep] = useState(1);
  return (
    <div>
      <BgGradient>
        <p className="text-2xl">How Can We Help?</p>
        <div className="relative">
          <div className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
            <svg
              className="w-4 h-4 text-gray-500 "
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 20 20"
            >
              <path
                stroke="currentColor"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
              />
            </svg>
          </div>
          <input
            type="search"
            id="default-search"
            className="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 "
            placeholder="Search anything..."
            required
          />
          <button
            type="submit"
            className="text-white absolute end-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2"
          >
            Search
          </button>
        </div>
        <div className="flex items-center gap-2 text-[12px]">
          <p>Popular help topics:</p>
          <p>account, pricing, payment</p>
        </div>
      </BgGradient>

      {step == 1 ? (
        <CategoriesSection
          Categories={Categories}
          setselectedCategory={setselectedCategory}
          setstep={setstep}
        />
      ) : step == 2 ? (
        <SelectedCategorySection
          setstep={setstep}
          selectedCategory={selectedCategory}
          setselectedQuestion={setselectedQuestion}
        />
      ) : step == 3 ? (
        <QuestionDetail selectedQuestion={selectedQuestion} setstep={setstep} />
      ) : null}
    </div>
  );
};

export default page;
