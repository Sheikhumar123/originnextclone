"use client";

import { useRouter } from "next/navigation";
import { IoSettingsOutline } from "react-icons/io5";
import { MdArrowForwardIos } from "react-icons/md";

const Page = () => {
  const router = useRouter();
  
  return (
    <>
      <div className="p-4">
        <p className="text-2xl font-semibold text-[#404040] capitalize">
          General Settings
        </p>
        <p className="text-md text-[#6a6a6a]  w-full pt-2">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Omnis dol{" "}
        </p>
        <div className=" p-2 w-[fit-content] flex flex-col gap-2">
          <div className="flex gap-2 items-center px-2">
            <IoSettingsOutline className="text-mainpurple" size={30} />
            <p className="text-xl  text-mainpurple capitalize">
              General Setting
            </p>
          </div>
          <div className="w-[100%] border-t-[2px] border-mainpurple"></div>
        </div>
        <div className="pt-10">
          <div className="flex justify-between items-center">
            <div>
              <p className="text-xl font-[500] text-[#404040]">Profile</p>
              <p className="text-md text-[#6a6a6a]  w-full pt-2">
                Edit and manage your profile and personal information{" "}
              </p>
            </div>
            <button
              onClick={() => {
                router.push("/providersdashboard/profile");
              }}
              className="text-xl text-mainpurple  w-[fit-content]"
            >
              Edit
            </button>
          </div>
        </div>

        <div className="pt-10">
          <div className="flex justify-between items-center">
            <div>
              <p className="text-xl font-[500] text-[#404040]">Security</p>
              <p className="text-md text-[#6a6a6a]  w-full pt-2">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus {" "}
              </p>
            </div>
            <button
              onClick={() => {
                router.push("/providersdashboard/security");
              }}
              className="text-xl text-mainpurple  w-[fit-content]"
            >
              <MdArrowForwardIos color="#383838" size={25} />
            </button>
          </div>
        </div>

        <div className="pt-10">
          <div className="flex justify-between items-center">
            <div>
              <p className="text-xl font-[500] text-[#404040]">Notification</p>
              <p className="text-md text-[#6a6a6a]  w-full pt-2">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus {" "}
              </p>
            </div>
            <button
              onClick={() => {
                router.push("/providersdashboard/notification");
              }}
              className="text-xl text-mainpurple  w-[fit-content]"
            >
              <MdArrowForwardIos color="#383838" size={25} />
            </button>
          </div>
        </div>
        <div className="pt-10">
          <div className="flex justify-between items-center">
            <div>
              <p className="text-xl font-[500] text-[#404040]">Verification</p>
              <p className="text-md text-[#6a6a6a]  w-full pt-2">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit ut
                aliquam, purus sit amet luctus{" "}
              </p>
            </div>
            <button
              onClick={() => {
                router.push("verification");
              }}
              className="text-xl text-mainpurple  w-[fit-content]"
            >
              <MdArrowForwardIos color="#383838" size={25} />
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
export default Page;
