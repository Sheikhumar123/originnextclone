import WithdrawalList from "@/components/dashboard/WithdrawalList";
import WithdrawalModal from "@/components/dashboard/WithdrawalModal";
import WithdrawButton from "@/utils/WithdrawButton";
import { manrope } from "@/utils/fontPoppins";

const page = () => {
  return (
    <>
      <div className={`w-full px-3 ${manrope.className}`}>
        <div className="mb-4">
          <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
            Withdrawal
          </h1>
          <p className="text-md text-[#6D6D6D] font-[400]">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur
            accusantium qui cum magnam esse hic dolorem, culpa sint sunt
            consectetur doloribus quas voluptatibus officiis voluptas eum dolore
            similique ratione atque quaerat assumenda et impedit itaque. Minima
            voluptatem nulla amet illum.
          </p>
        </div>
        <div className="flex w-full items-center justify-center gap-2 text-nowrap flex-wrap  mb-4">
          <div className=" min-w-[30%] h-[300px] flex-grow flex flex-col justify-between  bg-white rounded-lg shadow  p-4 md:p-6">
            <div className="p-2">
              <h1 className=" text-[1.3] font-[500]">Balance To Withdraw</h1>
              <h1 className="text-3xl text-green-500 font-bold">$2,567</h1>
            </div>
            <WithdrawButton
              classNameString="bg-[#434CD9] w-full  px-12 py-3 rounded-3xl text-white"
              text={"Withdraw Money"}
            />
          </div>
          <div className=" min-w-[1/4] h-[300px] flex-grow flex flex-col justify-between  bg-white rounded-lg shadow  p-4 md:p-6">
            <div className="p-2">
              <h1 className=" text-[1.3] font-[500]">
                Earning of Inprogress Task
              </h1>
              <h1 className="text-3xl font-bold">$600</h1>
            </div>
          </div>
          <div className=" min-w-[1/4] h-[300px] flex-grow flex flex-col justify-between  bg-white rounded-lg shadow  p-4 md:p-6">
            <div className="p-2">
              <h1 className=" text-[1.3] font-[500]">
                Total Earning this year
              </h1>
              <h1 className="text-3xl font-bold">$7,5867</h1>
            </div>
          </div>
        </div>
        <div>
          <WithdrawalList />
        </div>
      </div>
      <WithdrawalModal />
    </>
  );
};

export default page;
