import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";


export default function Loading() {
    // You can add any UI inside Loading, including a Skeleton.
    return <ScreenLoader/>
  }