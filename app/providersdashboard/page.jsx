import Dashboard from "@/components/providersDashboard/ServiceProvider/Dashboard/Dashboard";
import { dashboardMiniStats } from "@/data/dashboardData";
import { manrope } from "@/utils/fontPoppins";

const page = () => {
  return (
    <>
    <Dashboard/>
    </>
  );
};

export default page;
