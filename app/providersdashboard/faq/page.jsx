"use client";

import Image from "next/image";
import { useState } from "react";
import backgroundImg from "../../../images/dashboard/reviewBg.png";
import faq from "../../../images/dashboard/faq.png";
import AllFaqs from "@/components/providersDashboard/ServiceProvider/ProviderFaq/AllFaqs";
import FaqForm from "@/components/providersDashboard/ServiceProvider/ProviderFaq/FaqForm";
const Page = ({ userId }) => {
  const [addMore, setaddMore] = useState(false);
  const [editData, setEditData] = useState({});
  return (
    <>
      <div
        className="w-full 700px:h-64 300px:h-[200px] bg-gradient-to-r from-[#7d22ed] to-green-400 flex justify-between gap-1 items-center 700px:pl-16 300px:pl-8 rounded-3xl z-[0] relative overflow-hidden"
        style={{
          backgroundImage: `url(${backgroundImg})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      >
        <div className="absolute left-0 z-[1]">
          <Image src={backgroundImg} />
        </div>
        <div className="w-full max-w-[570px] flex flex-col 700px:gap-8 300px:gap-3 justify-center z-[3]">
          <h1 className="font-bold 700px:text-4xl 300px:text-xl text-white">
            {addMore ? "Post your question" : "Frequently Asked Questions"}
          </h1>
          <h2 className="700px:text-lg 300px:text-md text-white">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            x
          </h2>
        </div>
        <div className="pr-6 600px:block 300px:hidden">
          <Image src={faq} />
        </div>
      </div>
      {addMore ? (
        <FaqForm setaddMore={setaddMore} setEditData={setEditData} editData={editData} />
      ) : (
        <AllFaqs setaddMore={setaddMore} setEditData={setEditData} />
      )}
    </>
  );
};

export default Page;
