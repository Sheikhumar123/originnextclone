"use client"
import OffersMain from "@/components/providersDashboard/ServiceProvider/Offers/OffersMain";
import React from "react";

const offers = () => {
  return (
    <div>
      <OffersMain />
    </div>
  );
};

export default offers;
