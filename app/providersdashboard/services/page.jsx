"use client"
import ProviderServicesMain from "@/components/providersDashboard/ServiceProvider/Services/ProviderServicesMain";

const Page = () => {
  return (
    <>

      <ProviderServicesMain />
    </>
  );
};

export default Page;
