"use client";
import { IoMdNotificationsOutline } from "react-icons/io";
import { RxHamburgerMenu } from "react-icons/rx";
import LogoImg from "../../images/logo.png";
import Image from "next/image";
import { dashboardSidebarData } from "@/data/dashboardData";
import Link from "next/link";
import { poppins } from "@/utils/fontPoppins";
import SidebarList from "@/components/providersDashboard/SidebarList/SidebarList";
import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import profile from "../../public/ServiceProvider/profile.png";
import { FaAngleDown } from "react-icons/fa";

const DashboardLayout = ({ children }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [ImageUrl, setImageUrl] = useState(Cookies?.get("profile_image"));
  const router = useRouter();
  const SelectedRole = Cookies.get("selectedRole")?JSON.parse(Cookies.get("selectedRole")):"";
  const toggleDropdown = () => {
    setIsOpen(true);
  };

  const closeDropdown = () => {
    setIsOpen(false);
  };
  useEffect(() => {
    if (SelectedRole?.role_name != "ServiceProvider") {
      router.push("/home");
    }
  }, [SelectedRole?.role_name]);
  const removeCookies = () => {
    // const cookies = Cookies.getJSON();
    Cookies.remove("AllRoles");
    Cookies.remove("email");
    Cookies.remove("orignToken");
    Cookies.remove("profile_image");
    Cookies.remove("selectedRole");
    Cookies.remove("user_id");
    Cookies.remove("username");
  };
  return (
    <div className={`${poppins.className} relative p-4 bg-[#fafafa]`}>
      {/* HEADER */}
      <div className="bg-[#f4f9fd] items-center justify-center p-3 rounded-md border border-gray-300 mb-2">
        <div className="flex  w-[100%] items-center justify-between ">
          <Image src={LogoImg} width={100} height={60} alt="Logo img" />

          <div className="flex items-center justify-center gap-3 500px:gap-6">
            <div className="w-[50px] h-[50px] rounded-full bg-blue-100 flex items-center justify-center">
              <IoMdNotificationsOutline size={25} fill="#000" />
            </div>
            <label
              htmlFor="dashboardDrawer"
              className="w-[50px] h-[50px] rounded-full bg-blue-100 flex items-center justify-center lg:hidden"
            >
              <RxHamburgerMenu color="#000" size={25} />
            </label>
            <div className="dropdown dropdown-bottom dropdown-end">
              <div
                tabIndex={0}
                role="button"
                className="rounded-md flex items-center "
                onClick={toggleDropdown}
              >
                <Image
                  src={
                    ImageUrl
                    // Cookies?.get("profile_image")
                    //   ? Cookies.get("profile_image")
                    //   : "https://images.unsplash.com/photo-1494790108377-be9c29b29330"
                  }
                  onError={() => {
                    setImageUrl(profile);
                  }}
                  style={{
                    width: "50px",
                    height: "50px",
                    objectFit: "cover",
                  }}
                  width={50}
                  height={50}
                  alt="user img"
                  className="rounded-full"
                />
                <FaAngleDown color="grey"/>

              </div>
              {isOpen && (
                <ul
                  tabIndex={0}
                  className="dropdown-content z-[5] menu p-2 shadow bg-base-100 rounded-box w-52"
                >
                  <li onClick={closeDropdown}>
                    <Link href={"/providersdashboard/profile"}>Profile</Link>
                  </li>
                  <li onClick={closeDropdown}>
                    <Link href={"/providersdashboard/generalsettings"}>
                      Settings
                    </Link>
                  </li>
                  <li
                    onClick={() => {
                      removeCookies();
                      closeDropdown();
                      router.push("/home");
                    }}
                  >
                    <span>Logout</span>
                  </li>
                </ul>
              )}
            </div>
          </div>
        </div>
      </div>

      {/* DRAWER  */}
      <div className="drawer lg:drawer-open ">
        <input id="dashboardDrawer" type="checkbox" className="drawer-toggle" />
        <div className="drawer-content flex flex-col p-4 ">{children}</div>
        <div className="drawer-side pt-4">
          <label
            htmlFor="dashboardDrawer"
            aria-label="close sidebar"
            className="drawer-overlay"
          ></label>
          <ul className="menu p-0 w-80  min-h-full !bg-bgBlue text-white rounded-2xl  ">
            <SidebarList />
          </ul>
        </div>
      </div>
    </div>
  );
};
// min-h-full
// [calc(100vh-120px)]
export default DashboardLayout;
