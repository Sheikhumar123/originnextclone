import React from "react";
import Image from "next/image";
import { featuredImgDataorLandingPages } from "@/data/imagesData";
import { IoIosArrowDropright } from "react-icons/io";
import { FaLinkedin } from "react-icons/fa6";
import ratingImg from "@/images/ratingImg.png";
import {
  hiringProcessData,
  webDesignerCatalouge,
  websiteLandingReviewData,
} from "@/data/categoriesLandingPageData";
import { SlFlag } from "react-icons/sl";
import { IoIosArrowRoundForward } from "react-icons/io";

const page = () => {
  return (
    <div className="w-full">
      {/* HERO SECTION  */}
      <div className="w-full bg-bg-designer-img bg-no-repeat   text-white bg-cover flex flex-col justify-between">
        <div className="flex w-full 900px:w-[58%] flex-col gap-4 px-12 pt-12 pb-4">
          <h1 className="uppercase text-[2rem] 500px:text-[4.1rem] font-semibold leading-[1.2]">
            Hire expert Website Designers for any job
          </h1>
          <p className="text-lg w-[90%]">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit.
            Necessitatibus, cum. Dolores, commodi minima esse explicabo,
            corrupti aliquid magni consectetur, blanditiis rerum temporibus qui
            officiis odit.
          </p>
          <button className="500px:px-6 px-3 500px:py-4 py-2 rounded-md bg-[#6366F1] max-w-[300px] text-sm 500px:text-md">
            Hire Top Website Designer Now
          </button>
        </div>
        <div className="hidden 700px:block w-full bg-white/5 px-8 py-2 1200px:mb-8 mt-8">
          <h1 className="mb-2">Trusted by 180,000+ customers worldwide</h1>
          <div className="flex items-center justify-start gap-2 700px:justify-between flex-wrap">
            {featuredImgDataorLandingPages.map((item, index) => (
              <Image
                src={item}
                alt="brand image"
                key={index + 0.03 * 9}
                width={150}
              />
            ))}
          </div>
        </div>
      </div>
      {/* INTRO SECTION  */}
      <div className="w-full p-4 500px:p-12">
        <div className="pb-2 border border-white rounded-[20px] border-b  bg-gradient-to-br from-indigo-500 to-teal-400 max-w-[800px] mx-auto">
          <div className=" bg-white rounded-xl p-8 border border-white">
            <div className="mb-4">
              <h1 className="text-[1.3rem] font-semibold rounded-3xl">
                Types Of Website Design?
              </h1>
              <p className="text-md">
                When considering the term website design more often consumers
                reference the process of creating visual elements of a website
                for distribution to end-users, via the internet. The color
                schemes, typography or fonts, imagery, layout and overall
                graphical appearance are typically forefront of one’s mind as a
                visible demonstration of careful planning and design formation.{" "}
              </p>
            </div>
            <div className="mb-4">
              <h1 className="text-[1.3rem] font-semibold rounded-3xl">
                What Is Website Design?
              </h1>
              <p className="text-sm">
                When considering the term website design more often consumers
                reference the process of creating visual elements of a website
                for distribution to end-users, via the internet. The color
                schemes, typography or fonts, imagery, layout and overall
                graphical appearance are typically forefront of one’s mind as a
                visible demonstration of careful planning and design formation.
              </p>
            </div>
          </div>
        </div>
      </div>
      {/* HIRE WEBSITE DESIGNER SECTION  */}
      <div className="w-full p-4 500px:p-12">
        <h1 className="text-[2rem] font-bold mb-4">Hire Website Designer</h1>
        <div className="w-full flex gap-3 flex-wrap">
          {webDesignerCatalouge.map((item, key) => (
            <div
              key={key + 978 * 2}
              className="p-6 rounded-xl  shadow-xl w-[400px] flex-grow"
            >
              <div className="flex items-center justify-between">
                <div className="mb-2 flex items-center gap-2 w-auto">
                  <div className=" w-[45px] h-[45px] rounded-full overflow-hidden">
                    <Image
                      src={item.image}
                      style={{ width: "100%", height: "100%" }}
                      alt="profile img"
                      className=" object-cover "
                      width={500}
                      height={500}
                    />
                  </div>
                  <div className="flex flex-col ">
                    <h1 className="font-semibold">{item.name}</h1>
                    <p className="text-sm">{item.postition}</p>
                  </div>
                </div>
                <div className="flex items-center gap-2">
                  <FaLinkedin size={25} fill="#0E76A8" />{" "}
                  <h1 className="font-semibold">Verified</h1>
                </div>
              </div>
              <Image src={ratingImg} alt="ratingImg" height={18} />
              <p className="mt-2 text-[#676767] mb-2">{item.review}</p>
              <hr />
              <div className="flex items-center justify-between gap-2 600px:gap-0 mt-4 flex-wrap 600px:flex-nowrap text-nowrap">
                <button className="px-4 py-2 rounded-lg bg-bgBlue text-white font-semibold">
                  View Profile
                </button>
                <span className="text-green-600 font-bold">25$ Per Hour</span>
              </div>
            </div>
          ))}
        </div>
        <button className="flex items-center gap-2 text-bgBlue mt-4 800px:mt-8">
          <h1 className="font-bold">Load More</h1>
          <IoIosArrowDropright size={20} />
        </button>
      </div>
      {/* REVIEWS SECTION  */}
      <div className="w-full bg-bg-gradient-slider bg-no-repeat text-white bg-cover p-8">
        <h1 className="text-[2rem] font-bold text-center mb-8">
          Client Reviews For Our Website Designers
        </h1>
        <div className="overflow-x-auto !scrollbar-none flex items-center gap-12">
          {websiteLandingReviewData.map((item, key) => (
            <div
              key={key + 978 * 2}
              className="p-6 rounded-md  shadow-xl 800px:min-w-[550px] 400px:min-w-[350px] min-w-[300px]  bg-white text-black   "
            >
              <div className="flex 300px:flex-row flex-col items-start 400px:items-center 300px:justify-between">
                <div className="mb-2 flex items-center gap-2  w-auto 400px:flex-row flex-col">
                  <div className=" w-[45px] h-[45px] rounded-full overflow-hidden">
                    <Image
                      src={item.image}
                      style={{ width: "100%", height: "100%" }}
                      alt="profile img"
                      className=" object-cover "
                      width={500}
                      height={500}
                    />
                  </div>
                  <div className="flex flex-col ">
                    <h1 className="font-semibold">{item.name}</h1>
                    <p className="text-sm">{item.postition}</p>
                  </div>
                </div>
                <div className="flex items-center gap-2">
                  <FaLinkedin size={25} fill="#0E76A8" />{" "}
                  <h1 className="font-semibold">Verified</h1>
                </div>
              </div>
              <Image src={ratingImg} alt="ratingImg" height={18} />
              <p className="mt-2 text-[#676767] mb-2">{item.review}</p>
              <hr />
              <div className="flex items-center justify-between gap-2 600px:gap-0 mt-4 flex-wrap 600px:flex-nowrap text-nowrap">
                <div className="flex items-center gap-2 500px:gap-6 font-[600] flex-wrap 600px:flex-nowrap ">
                  <h1>{item.date}</h1>
                  <h1>Helpful (1)</h1>
                  <h1>Share</h1>
                </div>
                <div className=" items-center gap-2 hidden 800px:flex">
                  <SlFlag fill="#f21a1a" size={20} />
                  <h1 className="text-[#f21a1a] font-semibold">Report</h1>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
      {/* DETAILS OF PAGE  */}
      <div className="w-full p-4 500px:p-12">
        <h1 className="text-[2rem] font-bold mb-4 900px:w-1/2 uppercase">
          How to hire a great freelance Website Designer
        </h1>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Mollitia enim
          quam quas. Quaerat, doloribus corporis! Molestias at amet voluptatibus
          unde repellendus explicabo reprehenderit, iste dolor nisi veritatis
          perferendis minima sed vero, eaque quas sint ipsa. Voluptatibus illum
          expedita dolorem quod dolore atque sit, quisquam impedit ex eum
          laborum nisi natus quam eius unde aliquam esse nobis animi eaque odio
          obcaecati? Aut voluptas minima illum odio quae a, molestias tempore
          nostrum nam. Ab qui saepe obcaecati voluptate veritatis, similique
          tempore beatae ut quas magnam quidem deleniti temporibus et aspernatur
          quos incidunt esse possimus illo cum nihil reiciendis magni neque
          officia! Cupiditate asperiores sed aliquid! Cupiditate accusantium
          assumenda, neque laudantium, tempore non excepturi dolorum accusamus
          nam dolores, beatae ratione quaerat sapiente fuga quibusdam ab
          necessitatibus soluta magni quo animi id. Quasi accusantium nostrum
          magnam fugit doloremque pariatur laborum cum provident rem dicta dolor
          minima dolores porro, maxime corporis ea voluptas modi. Est.
        </p>
      </div>
      {/* HIRING PROCESS DATA  */}
      <div className="w-full p-4 500px:p-12">
        <h1 className="text-[2rem] font-bold mb-4  uppercase text-center">
          Process is of hiring
        </h1>
        <div className="flex items-center justify-between flex-wrap w-full">
          {hiringProcessData.map((item, index) => (
            <div
              className={`flex flex-col  mx-auto w-[250px] 400px:p-2 ${
                index !== hiringProcessData.length - 1 &&
                "1100px:[border-image:linear-gradient(to_top_right,#6D48E5,#6D48E55E)_30]  border-r-[3px] border-solid border-transparent "
              } shadow-sm gap-6 p-4 rounded-xl 400px:shadow-none 400px:gap-0  400px:rounded-none`}
              key={index + 0.639 * 2}
            >
              <Image
                src={item.imgUrl}
                alt="hiring process step image"
                width={75}
                height={75}
              />
              <div>
                <h1 className="font-semibold capitalize text-lg">
                  {item.title}
                </h1>
                <p>{item.text}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
      {/* GET STARTED SECTION  */}
      <div className="w-full p-4 500px:p-12">
        <div className="max-w-[900px] mx-auto rounded-3xl bg-bg-gradient-started bg-cover bg-no-repeat p-12 text-white">
          <h1 className="font-semibold text-[2rem]">
            So what are you waiting for?
          </h1>
          <p className="mb-8">
            Post a project today and get bids from talented freelancers
          </p>
          <button className="px-8 py-3 rounded-3xl text-black bg-white flex items-center gap-2">
            <p>Get Started</p>
            <IoIosArrowRoundForward size={25} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default page;
