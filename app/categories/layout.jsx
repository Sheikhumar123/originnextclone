import Footer from "@/components/home/Footer";
import Navbar from "@/components/home/Navbar";
import { manrope } from "@/utils/fontPoppins";

export default function RootLayout({ children }) {
  return (
    <div>
      <Navbar />
      {children}
      <Footer />
    </div>
  );
}
