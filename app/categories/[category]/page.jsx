import CategoryDetailPage from "@/components/CategoryDetailPage/CategoryDetailPage";
import { baseUrl } from "@/utils/BaseURL";
const page = async ({ params }) => {
  console.log("params", params);

  /////fetch category data
  let categoryData = await getCategories(params?.category);
  let categoryUsers = await getCategoriesUser(params?.category);
  let categoryReviews = await getCategoriesReview(params?.category);
console.log("categoryUsers",categoryUsers);
  // const category = await fetch(
  //   `${baseUrl}landingpage/categorylandingpage?id=${params?.category}`,
  //   // ,
  //   {
  //     next: {
  //       revalidate: 0,
  //     },
  //   }
  // );
  // let categoryData = await category.json();

  /////fetch category users data

  // const users = await fetch(
  //   `${baseUrl}landingpage/categoryusers?id=${params?.category}`,
  //   {
  //     next: {
  //       revalidate: 0,
  //     },
  //   }
  // );
  // let categoryUsers = await users.json();

  /////fetch category reviews data

  // const reviews = await fetch(
  //   `${baseUrl}landingpage/categoryreview?id=${params?.category}`,
  //   {
  //     next: {
  //       revalidate: 0,
  //     },
  //   }
  //   // {
  //   //   cache: "no-store",
  //   // }
  // );
  // let categoryReviews = await reviews.json();
  // console.log(categoryReviews?.userreview);
  return (
    <CategoryDetailPage
      categorydata={categoryData?.categorydata}
      categoryUsers={categoryUsers?.allusers}
      categoryUsersCount={categoryUsers?.count}
      categoryUsersLimit={categoryUsers?.limit}
      categoryReviews={categoryReviews?.userreview}
      params={params}
    />
  );
};

export default page;
const getCategories = async (categoryId) => {
  console.log("categoryId", categoryId);
  let categoryData;
  try {
    const category = await fetch(
      `${baseUrl}landingpage/categorylandingpage?id=${categoryId}`,
      {
        next: {
          revalidate: 0,
        },
      }
    );
    if (!category.ok) {
      throw new Error("Network response was not ok");
    }
    categoryData = await category.json();
    return categoryData;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
};
const getCategoriesUser = async (categoryId) => {
  let categoryUsers;
  try {
    const users = await fetch(
      `${baseUrl}landingpage/categoryusers?id=${categoryId}&page=1`,
      {
        next: {
          revalidate: 0,
        },
      }
    );
    if (!users.ok) {
      throw new Error("Network response was not ok");
    }
    categoryUsers = await users.json();
    return categoryUsers;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
};
const getCategoriesReview = async (categoryId) => {
  let categoryReviews;
  try {
    const reviews = await fetch(
      `${baseUrl}landingpage/categoryreview?id=${categoryId}`,
      {
        next: {
          revalidate: 0,
        },
      }
    );
    if (!reviews.ok) {
      throw new Error("Network response was not ok");
    }
    categoryReviews = await reviews.json();
    return categoryReviews;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
};
