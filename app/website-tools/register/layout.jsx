import Image from "next/image";
import logoImg from "@/images/logo.png";
import { manrope } from "@/utils/fontPoppins";

export default function RootLayout({ children }) {
  return (

        <div
          className={`w-full bg-white flex items-center justify-center ${manrope.className}`}
        >
          <div className="flex flex-col items-center justify-center gap-4 500px:p-0 p-4">
            <Image
              src={logoImg}
              priority
              alt="logo img"
              className="w-[200px] mb-4 mt-2"
              width={200}
            />
            {children}
          </div>
        </div>
 
  );
}
