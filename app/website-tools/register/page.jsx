"use client";

import WebsiteDetailsForm from "@/components/website-tools/register/WebsiteDetailsForm";
import WebURLForm from "@/components/website-tools/register/WebURLForm";
import { useState } from "react";
import DetailedFormContinued from "@/components/website-tools/register/DetailsFromContinued";
import ServicesAndPerks from "@/components/website-tools/register/ServicesAndPerks";
import NotificationsFormData from "@/components/website-tools/register/NotificationsFormData";
import VerifiedBadge from "@/components/website-tools/register/VerifiedBadge";
import WebsiteRegisterCompletion from "@/components/website-tools/register/WebsiteRegisterCompletion";

const Page = () => {
  const [data, setData] = useState({});
  const [step, setStep] = useState(1);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  return (
    <>
      {step === 1 && (
        <WebURLForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
        />
      )}
      {step === 2 && (
        <WebsiteDetailsForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 3 && (
        <DetailedFormContinued
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 4 && (
        <ServicesAndPerks
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}{" "}
      {step === 5 && (
        <NotificationsFormData
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 6 && (
        <VerifiedBadge
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 7 && (
        <WebsiteRegisterCompletion
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
    </>
  );
};

export default Page;
