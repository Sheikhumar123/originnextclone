"use client";
import Image from "next/image";
import Logo from "../../../images/logo.png";
import { useEffect, useState } from "react";
import ForgotPassword from "@/components/ForgotPassword/ForgotPassword";
import ChangePassword from "@/components/ForgotPassword/ChangePassword/ChangePassword";
import VerifyOTP from "@/components/ForgotPassword/VerifyOTP/VerifyOTP";

const ResetPassword = () => {
  const [tab, setTab] = useState("forgotPassword");
  const [screenData, setScreenData] = useState({ email: "", duration: 0 });

  useEffect(() => {
    const countdown = setInterval(() => {
      if (screenData?.duration > 0) {
        setScreenData((rest) => ({ ...rest, duration: rest?.duration - 1 }));
      } else {
        clearInterval(countdown);
      }
    }, 1000);

    return () => clearInterval(countdown);
  }, [screenData?.duration]);

  return (
    <div className="flex items-center 900px:items-start justify-center p-6 sm:p-12 900px:w-[50%] relative">
      <div className="w-full mt-8 md:mt-6 lg:mt-0 md:max-h-[800px] p-8 max-w-[700px]">
        <Image
          src={Logo}
          alt={"Login Image"}
          style={{ width: "150px", height: "50px" }}
          className="mb-12"
        />

        {tab === "forgotPassword" && (
          <ForgotPassword
            setTab={setTab}
            setScreenData={setScreenData}
          />
        )}
        {tab === "verifyOTP" && (
          <VerifyOTP
            setTab={setTab}
            screenData={screenData}
            setScreenData={setScreenData}
          />
        )}
        {tab === "changePassword" && (
          <ChangePassword
            setTab={setTab}
            screenData={screenData}
          />
        )}
      </div>
    </div>
  );
};

export default ResetPassword;
