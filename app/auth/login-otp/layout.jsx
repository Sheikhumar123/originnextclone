import RegisterBanner from "../../../images/RegisterBanner.png";
import Image from "next/image";
import { poppins } from "@/utils/fontPoppins";

const layout = ({ children }) => {
  return (
    <div
      className={`flex items-center h-screen bg-gray-50 ${poppins.className}`}
    >
      <div className="flex-1 h-full bg-white rounded-lg">
        <div className="flex flex-col 900px:flex-row gap-[15%]">
          {children}
          <div className="min-h-screen 900px:w-[35%] 900px:block hidden bg-bgBlue relative">
            <Image
              src={RegisterBanner}
              alt="Register Image"
              priority={true}
              width={800}
              height={800}
              className="w-[100%] h-[100%] object-contain object-bottom absolute bottom-0 left-[-15%]"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default layout;
