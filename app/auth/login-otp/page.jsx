"use client";
import Image from "next/image";
import Logo from "../../../images/logo.png";
import { useEffect, useState } from "react";
import TypeOtpGet from "@/components/LoginOtp/TypeOtpGet";
import TypeOtpVerify from "@/components/LoginOtp/TypeOtpVerify";

const LoginOtp = ({ searchParams }) => {
  console.log(searchParams);
  const [tab, setTab] = useState(searchParams?.type || "type");
  const [data, setdata] = useState({
    email: searchParams?.email ? searchParams?.email : "",
    phone: "",
    type: searchParams?.email ? "email" : "",
    dialCode: "",
  });
  const [screenData, setScreenData] = useState({
    email: "",
    duration: searchParams?.email ? 600 : 0,
    phone: "",
    dialCode: "",
    type: searchParams?.email ? "email" : "",
  });

  useEffect(() => {
    const countdown = setInterval(() => {
      if (screenData?.duration > 0) {
        setScreenData((rest) => ({ ...rest, duration: rest?.duration - 1 }));
      } else {
        clearInterval(countdown);
      }
    }, 1000);

    return () => clearInterval(countdown);
  }, [screenData?.duration]);

  return (
    <div className="flex items-center 900px:items-start justify-center p-6 sm:p-12 900px:w-[50%] relative">
      <div className="w-full mt-8 md:mt-6 lg:mt-0 md:max-h-[800px] p-8 max-w-[700px]">
        <Image
          src={Logo}
          alt={"Login Image"}
          style={{ width: "150px", height: "50px" }}
          className="mb-12"
        />

        {tab === "type" && (
          <TypeOtpGet
            setTab={setTab}
            setdata={setdata}
            data={data}
            setScreenData={setScreenData}
          />
        )}
        {tab === "verifyOTP" && (
          <TypeOtpVerify
            setTab={setTab}
            data={data}
            setdata={setdata}
            screenData={screenData}
            setScreenData={setScreenData}
            searchParams={searchParams}
          />
        )}
        {/* {tab === "changePassword" && <ChangePassword setTab={setTab} />} */}
      </div>
    </div>
  );
};

export default LoginOtp;
