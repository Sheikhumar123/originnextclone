import LoginForm from "@/components/login/LoginForm";
import Logo from "../../../images/logo.png";
import LoginImg from "../../../images/LoginImg.png";
import Image from "next/image";
import { Poppins } from "next/font/google";

const poppins = Poppins({
  subsets: ["latin"],
  display: "swap",
  variable: "poppins",
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
});
const Login = () => {
  return (
    <div className={`flex items-center  bg-gray-50 ${poppins.className}`}>
      <div className="flex-1  bg-white rounded-lg ">
        <div className="flex flex-col 900px:flex-row">
          <div className="flex items-center justify-center p-6 sm:p-12 900px:w-1/2 relative">
            <div className="flex flex-col justify-between h-full ">
              <Image
                src={Logo}
                alt={"Login Image"}
                style={{ width: "150px", height: "50px" }}
              />
              <LoginForm />
              <div></div>
              {/* <p>Terms and conditions - Privacy Policy</p> */}
            </div>
          </div>
          <div className=" 900px:h-screen 900px:w-1/2 900px:block hidden">
            <Image
              src={LoginImg}
              alt={"Login Image"}
              className="w-full h-full object-contain object-right"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
