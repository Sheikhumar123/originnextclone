"use client";
import Image from "next/image";
import Logo from "../../../images/logo.png";
import { useEffect, useState } from "react";
import DOB from "@/components/register/DOB";
import RegInfo from "@/components/register/RegInfo";
import Payment from "@/components/register/Payment";
import RegInfo2 from "@/components/register/RegInfo2";
import { useFormik } from "formik";
import * as yup from "yup";
import { registerInvestorApi } from "@/Apis/InvestorApis/InvestorApis";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";

const validationSchema = yup.object().shape({
  dob: yup.string().required("Field Required!"),
  firstname: yup.string().required("Field Required!"),
  lastname: yup.string().required("Field Required!"),
  email: yup.string().email("Invalid Email!").required("Field Required!"),
  phonenum: yup
    .string()
    .min(9, "Invalid phone number")
    .max(12, "Invalid phone number")
    .required("Field Required!"),
  fathername: yup.string().required("Field Required!"),
  address: yup.string().required("Field Required!"),
  password: yup
    .string()
    .min(6, "Password must be at least 6 characters")
    .required("Field Required!"),
  confirmpassword: yup
    .string()
    .oneOf([yup.ref("password"), null], "Passwords must match")
    .required("Field Required!"),
  // payment_mode: yup.string().required("Field Required!"),
  // amount: yup.string().when("payment_mode", {
  //   is: (value) => value !== "Crypto",
  //   then: () => yup.string().required("Field Required!"),
  //   otherwise: () => yup.string(),
  // }),
});

const initialValues = {
  dob: "",
  firstname: "",
  lastname: "",
  email: "",
  country_code: "",
  phonenum: "",
  referal_code: "",
  fathername: "",
  address: "",
  password: "",
  confirmpassword: "",
  amount: "",
  // payment_mode: "",
  emailVerified: false,
  phoneVerified: false,
  duration: "",
  otpFor: "",
};

const SignUp = ({searchParams}) => {
  const [active, setActive] = useState(1);
  const [userId, setuserId] = useState(null);
console.log(searchParams);
  const router = useRouter();

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
useEffect(()=>{
if(searchParams?.referal_code){
formik.setFieldValue("referal_code",searchParams?.referal_code)
}
},[searchParams])
  useEffect(() => {
    const countdown = setInterval(() => {
      if (formik.values.duration > 0) {
        formik.setFieldValue("duration", formik.values.duration - 1);
      } else {
        clearInterval(countdown);
      }
    }, 1000);

    return () => clearInterval(countdown);
  }, [formik.values.duration]);

  const handleSubmit = async (values, resetForm, setSubmitting) => {
    let formData = {
      login_method: "Custom",
      dob: values.dob,
      firstname: values.firstname,
      lastname: values.lastname,
      email: values.email,
      country_code: values.country_code,
      phonenum: values.phonenum,
      fathername: values.fathername,
      address: values.address,
      password: values.password,
      // payment_mode: values.payment_mode,
    };

    // if (values.payment_mode !== "Crypto") {
    //   formData.amount = values.amount;
    // }

    const response = await registerInvestorApi(formData);
    setSubmitting(false);
    if (response?.data?.status === "1") {
      toast.success(response.data.message);
      setuserId(response.data?.user_id)
      setActive(4);
      // resetForm();

      // router.push("/auth/investor-login");
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response?.response?.data.message);
    }
  };

  return (
    <div className="flex items-center 900px:items-start justify-center p-6 sm:p-12 900px:w-[50%] relative">
      <div className="w-full mt-8 md:mt-6 lg:mt-0 md:max-h-[800px] p-8">
        <Image
          src={Logo}
          alt={"Login Image"}
          style={{ width: "150px", height: "50px" }}
          className="mb-12"
        />

        <ol className="flex items-center w-full mb-12">
          <li className="flex w-full  items-center text-blue-600 dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-blue-100 after:border-4 after:inline-block dark:after:border-blue-800">
            <span
              className="flex bg-bgBlue text-white items-center justify-center w-10 h-10  rounded-full lg:h-12 lg:w-12  shrink-0 cursor-pointer"
              onClick={() => {
                setActive(1);
              }}
            >
              1
            </span>
          </li>
          <li
            className={`flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block
            }`}
          >
            <span
              className={`flex items-center justify-center w-10 h-10  rounded-full lg:h-12 lg:w-12  shrink-0 cursor-pointer ${
                active >= 2 ? "bg-bgBlue text-white" : "bg-gray-100"
              }`}
              onClick={() => {
                if (formik.touched?.dob && !formik.errors?.dob) {
                  setActive(2);
                } else {
                  formik.setFieldTouched("dob");
                }
              }}
            >
              2
            </span>
          </li>{" "}
          <li
            className={`flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block`}
          >
            <span
              className={`flex items-center justify-center w-10 h-10 rounded-full lg:h-12 lg:w-12  shrink-0 cursor-pointer ${
                active >= 3 ? "bg-bgBlue text-white" : "bg-gray-100 "
              }`}
              onClick={() => {
                let touched =
                  formik.touched?.firstname &&
                  formik.touched?.lastname &&
                  formik.touched?.email &&
                  formik.touched?.phonenum;
                let error =
                  formik.errors?.firstname ||
                  formik.errors?.lastname ||
                  formik.errors?.email ||
                  formik.errors?.phonenum ||
                  !formik.values.emailVerified ||
                  !formik.values.phoneVerified;

                if (touched && !error) {
                  setActive(3);
                } else {
                  formik.setFieldTouched("firstname");
                  formik.setFieldTouched("lastname");
                  formik.setFieldTouched("email");
                  formik.setFieldTouched("phonenum");
                }
              }}
            >
              3
            </span>
          </li>
          <li className="flex items-center w-full">
            <span
              className={`flex items-center justify-center w-10 h-10  rounded-full lg:h-12 lg:w-12  shrink-0 cursor-pointer ${
                active === 4 ? "bg-bgBlue text-white" : "bg-gray-100"
              }`}
              onClick={() => {
                let touched =
                  formik.touched?.fathername &&
                  formik.touched?.address &&
                  formik.touched?.password &&
                  formik.touched?.confirmpassword;
                let error =
                  formik.errors?.fathername ||
                  formik.errors?.address ||
                  formik.errors?.password ||
                  formik.errors?.confirmpassword;

                if (touched && !error) {
                  setActive(4);
                } else {
                  formik.setFieldTouched("fathername");
                  formik.setFieldTouched("address");
                  formik.setFieldTouched("password");
                  formik.setFieldTouched("confirmpassword");
                }
              }}
            >
              4
            </span>
          </li>
        </ol>

        <h1 className="mb-4 text-2xl font-[600] text-start  text-black">
          Registration
        </h1>

        <form autoComplete="off" onSubmit={formik.handleSubmit}>
          {active === 1 && <DOB formik={formik} setActive={setActive} />}
          {active === 2 && <RegInfo formik={formik} setActive={setActive} />}
          {active === 3 && <RegInfo2 formik={formik} setActive={setActive} />}
        </form>
          {active === 4 && <Payment formikAllValues={formik} userId={userId} />}
      </div>
    </div>
  );
};

export default SignUp;
