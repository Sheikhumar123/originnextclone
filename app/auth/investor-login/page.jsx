import LoginForm from "@/components/login/LoginForm";
import Logo from "../../../images/logo.png";
import LoginImg from "../../../images/LoginImg.png";
import Image from "next/image";
import { Poppins } from "next/font/google";

const poppins = Poppins({
  subsets: ["latin"],
  display: "swap",
  variable: "poppins",
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
});
const Login = () => {
  return (
    <div className={`flex items-center  bg-gray-50 ${poppins.className}`}>
      <div className="flex-1 h-full  bg-white rounded-lg">
        <div className="flex flex-col 900px:flex-row gap-[15%]">
          <div className="flex items-center justify-center p-6 sm:p-12 900px:w-[50%] relative">
            <div className="flex flex-col justify-between h-full">
              <Image
                src={Logo}
                alt={"Login Image"}
                style={{ width: "150px", height: "50px" }}
              />
              <LoginForm />
              <div></div>
            </div>
          </div>

          <div className="min-h-screen 900px:w-[35%] 900px:block hidden bg-[#FAFAFB] relative">
            <Image
              src={LoginImg}
              alt="Register Image"
              priority={true}
              width={800}
              height={800}
              className="w-[100%] h-[100%] object-fill object-bottom absolute bottom-0 left-[-15%]"
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
