"use client";

import CredentialsForm from "@/components/service-provider/contact/CredentialsForm";
import DescriptionForm from "@/components/service-provider/contact/DescriptionForm";
import PreferencesForm from "@/components/service-provider/contact/PreferencesForm";

import { useState } from "react";

const Page = () => {
  const [data, setData] = useState({});
  const [step, setStep] = useState(1);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };
  return (
    <>
      {step === 1 && (
        <DescriptionForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
      {step === 2 && (
        <PreferencesForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
        />
      )}
      {step === 3 && (
        <CredentialsForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
        />
      )}
    </>
  );
};

export default Page;
