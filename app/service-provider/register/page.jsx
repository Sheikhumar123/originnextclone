"use client";
import Completion from "@/components/influencer/register/Completion";
import FAQFormService from "@/components/service-provider/register/FAQFormService";
import FeatureFormService from "@/components/service-provider/register/FeatureForm";
import InitialData from "@/components/service-provider/register/InitialData";
import MoreDetailFormService from "@/components/service-provider/register/MoreDetailsFormService";
import ProfileDetails from "@/components/service-provider/register/ProfileDetails";
import ServiceDetailForm from "@/components/service-provider/register/ServiceDetailForm";
import ServiceDetails from "@/components/service-provider/register/ServiceDetails";
import ServiceNotificationForm from "@/components/service-provider/register/ServiceNotificationForm";
import ServiceNotificationOtp from "@/components/service-provider/register/ServiceNotificationOtp";
import VerifyOtp from "@/components/service-provider/register/VerifyOtp";
import { useEffect, useState } from "react";

const Page = () => {
  const [data, setData] = useState({});
  const [step, setStep] = useState(1);
  const [userId, setuserId] = useState(null);
  const [otp, setOtp] = useState("");
  const [getotp, setgetOtp] = useState("");
  const [screenData, setScreenData] = useState({
    email: "",
    password: "",
    duration: 0,
    dialCode: "",
    phone: "",
    phoneDuration: 0,
    whatsappDialCode: "",
    whatsapp: "",
    whatsappDuration: 0,
  });

  useEffect(() => {
    const countdown = setInterval(() => {
      if (screenData?.duration > 0) {
        setScreenData((rest) => ({
          ...rest,
          duration: rest.duration - 1,
        }));
      } else {
        clearInterval(countdown);
      }
    }, 1000);

    return () => clearInterval(countdown);
  }, [screenData?.duration]);

  useEffect(() => {
    const countdown = setInterval(() => {
      if (screenData?.phoneDuration > 0) {
        setScreenData((rest) => ({
          ...rest,
          phoneDuration: rest.phoneDuration - 1,
        }));
      } else {
        clearInterval(countdown);
      }
    }, 1000);

    return () => clearInterval(countdown);
  }, [screenData?.phoneDuration]);

  useEffect(() => {
    const countdown = setInterval(() => {
      if (screenData?.whatsappDuration > 0) {
        setScreenData((rest) => ({
          ...rest,
          whatsappDuration: rest.whatsappDuration - 1,
        }));
      } else {
        clearInterval(countdown);
      }
    }, 1000);

    return () => clearInterval(countdown);
  }, [screenData?.whatsappDuration]);

  const handleChange = (e) => {
    setData({ ...data, [e.target.name]: e.target.value });
  };

  // console.log("screenData", screenData);
  return (
    <>
      {step === 1 && (
        <InitialData
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
        />
      )}
      {step === 2 && (
        <ServiceDetailForm
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setOtp={setOtp}
          otp={otp}
          setgetOtp={setgetOtp}
          setScreenData={setScreenData}
        />
      )}
      {step === 3 && (
        <VerifyOtp
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
          setOtp={setOtp}
          otp={otp}
          getotp={getotp}
          screenData={screenData}
          setScreenData={setScreenData}
        />
      )}
      {step === 4 && (
        <ServiceNotificationOtp
          setStep={setStep}
          screenData={screenData}
          setScreenData={setScreenData}
        />
      )}
      {step === 5 && (
        <ServiceNotificationForm
          setStep={setStep}
          screenData={screenData}
          setScreenData={setScreenData}
        />
      )}
      {step === 6 && (
        <MoreDetailFormService
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setuserId={setuserId}
          screenData={screenData}
        />
      )}{" "}
      {step === 7 && (
        <ServiceDetails
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
        />
      )}
      {step === 8 && (
        <ProfileDetails
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          userId={userId}
        />
      )}
      {step === 9 && (
        <FeatureFormService
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
          userId={userId}
        />
      )}
      {step === 10 && (
        <FAQFormService
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
          userId={userId}
        />
      )}
      {step === 11 && (
        <Completion
          data={data}
          handleChange={handleChange}
          setStep={setStep}
          setData={setData}
        />
      )}
    </>
  );
};

export default Page;
