"use client"
import VendorForm from "@/components/dashboard/Vendor/VendorForm";
import VendorList from "@/components/dashboard/Vendor/VendorList";
import { manrope } from "@/utils/fontPoppins";
import { useState } from "react";

const page = () => {
  const [showForm, setshowForm] = useState(false);
  const [isEdit, setisEdit] = useState(false);
  const [rowData, setrowData] = useState(null);
  return (
    <>
      <div className={`w-full p-4 ${manrope.className}`}>
        <h1 className="text-2xl font-semibold mb-3">
          Add more amount to get profit
        </h1>
        {showForm ? (
        <VendorForm  setisEdit={setisEdit} isEdit={isEdit} setrowData={setrowData} rowData={rowData} setshowForm={setshowForm} />
      ) : (
        <VendorList setisEdit={setisEdit} setrowData={setrowData} setshowForm={setshowForm} />
      )}
        {/* <InvestmentForm />
        <InvestmentList /> */}
      </div>
    </>
  );
};

export default page;
