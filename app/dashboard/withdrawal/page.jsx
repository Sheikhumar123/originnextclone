"use client";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import RefrelWithdrawModal from "@/components/dashboard/Withdrawal/RefrelWithdrawModal";
import WithdrawalList from "@/components/dashboard/WithdrawalList";
import WithdrawalModal from "@/components/dashboard/WithdrawalModal";
import {
  GetInvestDetails,
  GetInvestorWithdrawPage,
} from "@/redux/Slices/InvestorDashboard/InvestSlice/InvestSlice";
import WithdrawButton from "@/utils/WithdrawButton";
import { manrope } from "@/utils/fontPoppins";
import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

const page = () => {
  const UserId = Cookies.get("user_id");
  const dispatch = useDispatch();
  const { InvestorWithdrawalData, status } = useSelector(GetInvestDetails);
  const [page, setpage] = useState(1);
  const [open, setopen] = useState(false);
  const openModal = () => {
    setopen(true);
  };

  const closeModal = () => {
    setopen(false);
  };
  console.log(InvestorWithdrawalData);
  useEffect(() => {
    let obj = {
      user_id: UserId,
      page: page,
    };
    dispatch(GetInvestorWithdrawPage(obj));
  }, [dispatch, page]);
  return (
    <>
      {status == "pending" && <ScreenLoader />}
      <div className={`w-full px-3 ${manrope.className}`}>
        <div className="mb-4">
          <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
            Withdrawal
          </h1>
          <p className="text-md text-[#6D6D6D] font-[400]">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur
            accusantium qui cum magnam esse hic dolorem, culpa sint sunt
            consectetur doloribus quas voluptatibus officiis voluptas eum dolore
            similique ratione atque quaerat assumenda et impedit itaque. Minima
            voluptatem nulla amet illum.
          </p>
        </div>
        <div className="flex w-full items-center justify-center gap-2 text-nowrap flex-wrap  mb-4">
          <div className=" min-w-[30%] h-[300px] flex-grow flex flex-col justify-between  bg-white rounded-lg shadow  p-4 md:p-6">
            <div className="p-2">
              <h1 className=" text-[1.3] font-[500]">Balance To Withdraw</h1>
              <h1 className="text-3xl text-green-500 font-bold">
                $
                {InvestorWithdrawalData?.Transactions?.balance_to_withdraw
                  ? InvestorWithdrawalData?.Transactions?.balance_to_withdraw?.toLocaleString()
                  : "0"}
              </h1>
            </div>
            <div className="px-2">
              <h1 className=" text-[1.3] font-[500]">Pending Withdrawal</h1>
              <h1 className="text-3xl text-sky-500 font-bold">
                $
                {InvestorWithdrawalData?.Transactions?.pending_withdraw
                  ? InvestorWithdrawalData?.Transactions?.pending_withdraw?.toLocaleString()
                  : "0"}
              </h1>
            </div>
            <button
              className="bg-[#434CD9] w-full  px-12 py-3 rounded-3xl text-white"
              onClick={() => {
                InvestorWithdrawalData?.Transactions?.pending_withdraw >=
                InvestorWithdrawalData?.Transactions?.balance_to_withdraw
                  ? toast.error("Insufficient Balance")
                  : InvestorWithdrawalData?.Transactions?.balance_to_withdraw ||
                    Number(
                      InvestorWithdrawalData?.Transactions?.balance_to_withdraw
                    ) > 0
                  ? openModal()
                  : toast.error("0 Amount for Withdrawal");
              }}
            >
              Withdraw Money
            </button>
            {/* <WithdrawButton
              classNameString="bg-[#434CD9] w-full  px-12 py-3 rounded-3xl text-white"
              text={"Withdraw Money"}
            /> */}
          </div>
          <div className=" min-w-[1/4] h-[300px] flex-grow flex flex-col justify-between  bg-white rounded-lg shadow  p-4 md:p-6">
            <div className="p-2">
              <h1 className=" text-[1.3] font-[500]">
                Earning of Inprogress Task
              </h1>
              <h1 className="text-3xl font-bold">$0</h1>
            </div>
          </div>
          <div className=" min-w-[1/4] h-[300px] flex-grow flex flex-col justify-between  bg-white rounded-lg shadow  p-4 md:p-6">
            <div className="p-2">
              <h1 className=" text-[1.3] font-[500]">
                Total Earning this year
              </h1>
              <h1 className="text-3xl font-bold">$0</h1>
            </div>
          </div>
        </div>
        <div>
          <WithdrawalList
            setpage={setpage}
            page={page}
            InvestorWithdrawalData={InvestorWithdrawalData}
          />
        </div>
      </div>
      {open && (
        <RefrelWithdrawModal
          open={open}
          closeModal={closeModal}
          InvestorWithdrawalData={InvestorWithdrawalData}
        />
      )}
      {/* <WithdrawalModal InvestorWithdrawalData={InvestorWithdrawalData} /> */}
    </>
  );
};

export default page;
