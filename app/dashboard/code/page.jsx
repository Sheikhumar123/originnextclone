"use client";
import { InvestorGenereteCodeApi } from "@/Apis/InvestorApis/InvestorApis";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import CodesList from "@/components/dashboard/Codes/CodesList";
import {
  GetAllVendorCodes,
  GetInvestDetails,
} from "@/redux/Slices/InvestorDashboard/InvestSlice/InvestSlice";
import { manrope } from "@/utils/fontPoppins";
import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { useFormik } from "formik";
import * as Yup from "yup";
import { CircularProgress } from "@mui/material";
const page = () => {
  const { VendorCodesData, status } = useSelector(GetInvestDetails);
  const [page, setpage] = useState(1);
  const UserId = Cookies.get("user_id");

  const dispatch = useDispatch();
  useEffect(() => {
    let obj = {
      user_id: UserId,
      page: page,
    };
    dispatch(GetAllVendorCodes(obj));
  }, [dispatch, page]);
  const ValidationSchema = Yup.object().shape({
    amount: Yup.string()
      .required("Please Enter Amount")
      .max(
        Number(VendorCodesData?.AllCodes?.credit_available),
        `Amount must not exceed ${
          VendorCodesData?.AllCodes?.credit_available
            ? VendorCodesData?.AllCodes?.credit_available
            : 0
        }`
      ),
  });

  const initialValues = {
    amount: "",
  };
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });

  const handleSubmit = async (values, resetForm, setSubmitting) => {
    const response = await InvestorGenereteCodeApi({
      ...values,
      user_id: Cookies.get("user_id"),
    });
    setSubmitting(false);
    console.log(response);

    if (response?.data?.status == "1") {
      toast.success(response.data.message);
      resetForm();
      let obj = {
        user_id: UserId,
        page: page,
      };
      dispatch(GetAllVendorCodes(obj));
    } else if (response?.data?.status === "0") {
      console.log("0", response);

      toast.error(response.data.message);
    } else {
      console.log("errer", response);

      toast.error(response?.response?.data?.message);
    }
  };
  return (
    <>
      {status == "pending" && <ScreenLoader />}

      <div className={`w-full p-4 ${manrope.className}`}>
        <div className="mb-4">
          <h1 className="text-[1.6rem] font-semibold mb-2 text-black">Codes</h1>
          <p className="text-md text-[#6D6D6D] font-[400]">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur
            accusantium qui cum magnam esse hic dolorem, culpa sint sunt
            consectetur doloribus quas voluptatibus officiis voluptas eum dolore
            similique ratione atque quaerat assumenda et impedit itaque. Minima
            voluptatem nulla amet illum.
          </p>
        </div>
        <div className="flex w-full items-center justify-center gap-2 text-nowrap flex-wrap  mb-4">
          <div className=" min-w-[1/4] h-[150px] flex-grow flex flex-col justify-between  bg-white rounded-lg shadow  p-4 md:p-6">
            <div className="p-2 text-center">
              <h1 className=" text-[1.3] font-[500]">Available Credits</h1>
              <h1 className="text-3xl  font-bold">
                $
                {VendorCodesData?.AllCodes?.credit_available
                  ? VendorCodesData?.AllCodes?.credit_available?.toLocaleString()
                  : 0}
              </h1>
            </div>

            {/* <WithdrawButton
              classNameString="bg-[#434CD9] w-full  px-12 py-3 rounded-3xl text-white"
              text={"Withdraw Money"}
            /> */}
          </div>
          <div className=" min-w-[1/4] h-[150px] flex-grow flex flex-col justify-between  bg-white rounded-lg shadow  p-4 md:p-6">
            <div className="p-2 text-center">
              <h1 className=" text-[1.3] font-[500]">Code Purchased</h1>
              <h1 className="text-3xl font-bold">
                $
                {VendorCodesData?.AllCodes?.codepurchased
                  ? VendorCodesData?.AllCodes?.codepurchased?.toLocaleString()
                  : 0}
              </h1>
            </div>
          </div>
          <div className=" min-w-[1/4] h-[150px] flex-grow flex flex-col justify-between  bg-white rounded-lg shadow  p-4 md:p-6">
            <div className="p-2 text-center">
              <h1 className=" text-[1.3] font-[500]">Code Used</h1>
              <h1 className="text-3xl font-bold">
                $
                {VendorCodesData?.AllCodes?.code_used
                  ? VendorCodesData?.AllCodes?.code_used?.toLocaleString()
                  : 0}
              </h1>
            </div>
          </div>
        </div>
        <div className="mb-6">
          <form onSubmit={formik.handleSubmit}>
            <h1 className="text-2xl font-semibold mb-3 text-gray-500">
              Amount:
            </h1>
            <div className="flex 700px:flex-row 300px:flex-col gap-4 700px:items-center 300px:items-start">
              <div className="w-full 700px:max-w-[50%] 300px:max-w-[100%]">
                <input
                  type="number"
                  id="investmentAmount"
                  className="w-full h-[55px]  px-4 py-2 text-sm  border  focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600 bg-white rounded-3xl"
                  placeholder="Enter Amount"
                  name="amount"
                  value={formik.values.amount}
                  onChange={formik.handleChange}
                />
                {formik.errors.amount &&
                  formik.touched.amount &&
                  formik.errors.amount && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.amount}
                    </p>
                  )}
              </div>
              <div>
                {formik.isSubmitting ? (
                  <CircularProgress />
                ) : (
                  <button
                    type="submit"
                    className="bg-[#434CD9]  self-center text-nowrap px-6 py-3 rounded-[999px] text-white"
                  >
                    Generate Code
                  </button>
                )}
              </div>
            </div>
          </form>
        </div>
        <CodesList
          page={page}
          setpage={setpage}
          VendorCodesData={VendorCodesData}
        />

        {/* <InvestmentForm />
        <InvestmentList /> */}
      </div>
    </>
  );
};

export default page;
