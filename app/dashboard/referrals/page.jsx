"use client";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import RefferalList from "@/components/dashboard/ReferralList";
import {
  GetInvestDetails,
  GetInvestorRefrelPage,
} from "@/redux/Slices/InvestorDashboard/InvestSlice/InvestSlice";
import { manrope } from "@/utils/fontPoppins";
import Cookies from "js-cookie";
import { useEffect } from "react";
import { IoShareSocialOutline } from "react-icons/io5";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

const page = () => {
  const UserId = Cookies.get("user_id");
  const dispatch = useDispatch();
  const { InvestorReferalData, status } = useSelector(GetInvestDetails);
  console.log(InvestorReferalData);
  useEffect(() => {
    let obj = {
      user_id: UserId,
    };
    dispatch(GetInvestorRefrelPage(obj));
  }, [dispatch,]);
  const handleCopyClick = async () => {
    try {
      // Use the Clipboard API to copy text
      const url = `http://localhost:3000/auth/investor-register?referal_code=${InvestorReferalData?.transactions?.referal_code}`;
      await navigator.clipboard.writeText(url);
      toast.success("Url is Copied");
    } catch (err) {
      toast.error("Unable to Copy the Url");
    }
  };
  const handleCopyClick2 = async () => {
    try {
      // Use the Clipboard API to copy text
      const url = InvestorReferalData?.transactions?.referal_code;
      await navigator.clipboard.writeText(url);
      toast.success("Code is Copied");
    } catch (err) {
      toast.error("Unable to Copy the Url");
    }
  };
  return (
    <>
      {status == "pending" && <ScreenLoader />}
      <div className={`w-full px-3 ${manrope.className}`}>
        <div className="flex items-center justify-between flex-wrap gap-3 mb-6 ">
          <div>
            <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
              Add more amount to get more profit
            </h1>
            <p className="text-xl font-[400]">
              Only <span className="text-red-600 font-[600]">{InvestorReferalData?.transactions?.referal_amount?InvestorReferalData?.transactions?.referal_amount?.toLocaleString():0}</span> slots
              are left that can invest in our project
            </p>
          </div>
          <div className="flex flex-col items-center gap-2 text-nowrap ">
            <p className="text-lg text-gray-500">Total Earned</p>
            <h1 className="text-3xl text-green-500 font-bold">{InvestorReferalData?.transactions?.referal_amount?InvestorReferalData?.transactions?.referal_amount?.toLocaleString():0} INR</h1>
          </div>
        </div>
        <div className="mb-3">
          <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
            Your Refferal Link
          </h1>
          <div
            className={`flex items-center justify-between w-full p-4 bg-[#f4f9fd] border border-gray-200 ${manrope.className} rounded-[40px] mt-3 flex-wrap gap-2`}
          >
            <p className="text-[#444BDB]  flex-grow overflow-x-auto md:flex-grow-0  ">
              {`http://localhost:3000/auth/investor-register?referal_code=${InvestorReferalData?.transactions?.referal_code}`}
            </p>
            <div className="flex items-center justify-between gap-4 flex-grow md:flex-grow-0 ">
              <div className="w-[50px] h-[50px] rounded-full bg-blue-100 flex items-center justify-center">
                <IoShareSocialOutline size={25} style={{ color: "#808080" }} />
              </div>
              <button
                onClick={handleCopyClick}
                className="bg-[#434CD9] flex-grow md:flex-grow-0  px-12 py-3 rounded-3xl text-white"
              >
                Copy
              </button>
            </div>
          </div>
        </div>
        <div className="mb-3">
          <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
            Your Refferal Code
          </h1>
          <div
            className={`flex items-center justify-between w-full p-4 bg-[#f4f9fd] border border-gray-200 ${manrope.className} rounded-[40px] mt-3 flex-wrap gap-2`}
          >
            <p className="text-black  flex-grow overflow-x-auto md:flex-grow-0  ">
              {InvestorReferalData?.transactions?.referal_code}
            </p>
            <div className="flex items-center justify-between gap-4 flex-grow md:flex-grow-0 ">
              <div className="w-[50px] h-[50px] rounded-full bg-blue-100 flex items-center justify-center">
                <IoShareSocialOutline size={25} style={{ color: "#808080" }} />
              </div>
              <button
                onClick={handleCopyClick2}
                className="bg-[#434CD9] flex-grow md:flex-grow-0  px-12 py-3 rounded-3xl text-white"
              >
                Copy
              </button>
            </div>
          </div>
        </div>
        <RefferalList InvestorReferalData={InvestorReferalData} />
      </div>
    </>
  );
};

export default page;
