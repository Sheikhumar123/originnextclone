import ContactForm from "@/components/dashboard/ContactForm";
import { manrope } from "@/utils/fontPoppins";

const page = () => {
  return (
    <div className={`w-full px-3 ${manrope.className}`}>
      <div className="mb-3">
        <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
          Contact Us
        </h1>
        <p className="text-[#525252">Get Connected To Grow Better business.</p>
      </div>
      <ContactForm type={"Investor"} />
    </div>
  );
};

export default page;
