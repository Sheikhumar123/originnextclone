"use client"
import InvestmentForm from "@/components/dashboard/InvestmentForm";
import InvestmentList from "@/components/dashboard/InvestmentList";
import { manrope } from "@/utils/fontPoppins";
import { useState } from "react";

const InvestmentPage = () => {
  const [showForm, setshowForm] = useState(false);
  const [isEdit, setisEdit] = useState(false);
  const [rowData, setrowData] = useState(null);
  return (
    <>
      <div className={`w-full p-4 ${manrope.className}`}>
        <h1 className="text-2xl font-semibold mb-3">
          Add more amount to get profit
        </h1>
        {showForm ? (
        <InvestmentForm  setisEdit={setisEdit} isEdit={isEdit} setrowData={setrowData} rowData={rowData} setshowForm={setshowForm} />
      ) : (
        <InvestmentList setisEdit={setisEdit} setrowData={setrowData} setshowForm={setshowForm} />
      )}
        {/* <InvestmentForm />
        <InvestmentList /> */}
      </div>
    </>
  );
};

export default InvestmentPage;
