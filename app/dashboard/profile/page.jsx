import ProfileForm from "@/components/dashboard/investor/ProfileForm/ProfileForm";
import { manrope } from "@/utils/fontPoppins";

const page = () => {
  return (
    <div className={`w-full px-3 ${manrope.className}`}>
      <ProfileForm />
    </div>
  );
};

export default page;
