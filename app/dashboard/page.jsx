"use client"
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import { GetInvestDetails, GetInvestorDashboardData } from "@/redux/Slices/InvestorDashboard/InvestSlice/InvestSlice";
import { manrope } from "@/utils/fontPoppins";
import Cookies from "js-cookie";
import dynamic from "next/dynamic";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
const StatsCards = dynamic(
  () => import("../../components/dashboard/StatsCards"),
  {
    ssr: false,
    loading: () => <span className="loading loading-spinner loading-lg"></span>,
  }
);
const StatsCharts = dynamic(
  () => import("../../components/dashboard/StatsChart"),
  {
    ssr: false,
    loading: () => <span className="loading loading-spinner loading-lg"></span>,
  }
);
const page = () => {
  const UserId = Cookies.get("user_id");
  const dispatch = useDispatch();
  const { AllInvestorDashboardData, status } = useSelector(GetInvestDetails);

  useEffect(() => {
    let obj = {
      user_id: UserId,

    };
    dispatch(GetInvestorDashboardData(obj));
  }, [dispatch,UserId]);
   const dashboardMiniStats = [
    {
      title: "Invest",
      value: `${AllInvestorDashboardData?.transactions?.investment_amount?Number(AllInvestorDashboardData?.transactions?.investment_amount)?.toLocaleString() :"0"}`,
      data: {
        chart: {
          height: "100%",
          maxWidth: "100%",
          type: "area",
          fontFamily: "Inter, sans-serif",
          dropShadow: {
            enabled: false,
          },
          toolbar: {
            show: false,
          },
        },
        tooltip: {
          enabled: true,
          x: {
            show: false,
          },
        },
        fill: {
          type: "gradient",
          gradient: {
            opacityFrom: 0.55,
            opacityTo: 0,
            shade: "#00A389",
            gradientToColors: ["#00A389"],
          },
        },
        dataLabels: {
          enabled: false,
        },
        stroke: {
          width: 3,
        },
        grid: {
          show: false,
          strokeDashArray: 4,
          padding: {
            left: 2,
            right: 2,
            top: 0,
          },
        },
  
        xaxis: {
          categories: [
            "01 February",
            "02 February",
            "03 February",
            "04 February",
            "05 February",
            "06 February",
            "07 February",
            "08 February",
            "09 February",
          ],
          labels: {
            show: false,
          },
          axisBorder: {
            show: false,
          },
          axisTicks: {
            show: false,
          },
        },
        yaxis: {
          show: false,
        },
      },
      series: [
        {
          name: "New users",
          data: [0, 200, 400, 250, 600, 700, 800, 1300, 850, 2000],
          color: "#00A389",
        },
      ],
    },
    {
      title: "Profit",
      // title: "Referal Amount",
      value: "0 INR",
      // value: AllInvestorDashboardData?.transactions?.referal_amount?AllInvestorDashboardData?.transactions?.referal_amount:"0",

      data: {
        chart: {
          height: "100%",
          maxWidth: "100%",
          type: "area",
          fontFamily: "Inter, sans-serif",
          dropShadow: {
            enabled: false,
          },
          toolbar: {
            show: false,
          },
        },
        tooltip: {
          enabled: true,
          x: {
            show: false,
          },
        },
        fill: {
          type: "gradient",
          gradient: {
            opacityFrom: 0.55,
            opacityTo: 0,
            shade: "#FF5B5B",
            gradientToColors: ["#FF5B5B"],
          },
        },
        dataLabels: {
          enabled: false,
        },
        stroke: {
          width: 3,
        },
        grid: {
          show: false,
          strokeDashArray: 4,
          padding: {
            left: 2,
            right: 2,
            top: 0,
          },
        },
  
        xaxis: {
          categories: [
            "01 February",
            "02 February",
            "03 February",
            "04 February",
            "05 February",
            "06 February",
            "07 February",
          ],
          labels: {
            show: false,
          },
          axisBorder: {
            show: false,
          },
          axisTicks: {
            show: false,
          },
        },
        yaxis: {
          show: false,
        },
      },
      series: [
        {
          name: "Profit",
          data: [0, 200, 400, 250, 600, 700, 800, 1300, 850, 2000],
          color: "#FF5B5B",
        },
      ],
    },
    {
      title: "Total",
      value: "$0",
      // title: "Purchase Amount",
      // value: AllInvestorDashboardData?.transactions?.purchase_amount?AllInvestorDashboardData?.transactions?.purchase_amount:"0",
      data: {
        chart: {
          height: "100%",
          maxWidth: "100%",
          type: "area",
          fontFamily: "Inter, sans-serif",
          dropShadow: {
            enabled: false,
          },
          toolbar: {
            show: false,
          },
        },
        tooltip: {
          enabled: true,
          x: {
            show: false,
          },
        },
        fill: {
          type: "gradient",
          gradient: {
            opacityFrom: 0.55,
            opacityTo: 0,
            shade: "#AB54DB",
            gradientToColors: ["#AB54DB"],
          },
        },
        dataLabels: {
          enabled: false,
        },
        stroke: {
          width: 3,
        },
        grid: {
          show: false,
          strokeDashArray: 4,
          padding: {
            left: 2,
            right: 2,
            top: 0,
          },
        },
  
        xaxis: {
          categories: [
            "01 February",
            "02 February",
            "03 February",
            "04 February",
            "05 February",
            "06 February",
            "07 February",
          ],
          labels: {
            show: false,
          },
          axisBorder: {
            show: false,
          },
          axisTicks: {
            show: false,
          },
        },
        yaxis: {
          show: false,
        },
      },
      series: [
        {
          name: "New users",
          data: [0, 200, 400, 250, 600, 700, 800, 1300, 850, 2000],
          color: "#AB54DB",
        },
      ],
    },
    {
      title: "Withdrawal Locked",
      value: "0",
      // title: "Credit Amount",
      // value: AllInvestorDashboardData?.transactions?.credit_amount?AllInvestorDashboardData?.transactions?.credit_amount:"0",
      data: {
        chart: {
          height: "100%",
          maxWidth: "100%",
          type: "area",
          fontFamily: "Inter, sans-serif",
          dropShadow: {
            enabled: false,
          },
          toolbar: {
            show: false,
          },
        },
        tooltip: {
          enabled: true,
          x: {
            show: false,
          },
        },
        fill: {
          type: "gradient",
          gradient: {
            opacityFrom: 0.55,
            opacityTo: 0,
            shade: "#FF9937",
            gradientToColors: ["#FF9937"],
          },
        },
        dataLabels: {
          enabled: false,
        },
        stroke: {
          width: 3,
        },
        grid: {
          show: false,
          strokeDashArray: 4,
          padding: {
            left: 2,
            right: 2,
            top: 0,
          },
        },
  
        xaxis: {
          categories: [
            "01 February",
            "02 February",
            "03 February",
            "04 February",
            "05 February",
            "06 February",
            "07 February",
          ],
          labels: {
            show: false,
          },
          axisBorder: {
            show: false,
          },
          axisTicks: {
            show: false,
          },
        },
        yaxis: {
          show: false,
        },
      },
      series: [
        {
          name: "New users",
          data: [0, 200, 400, 250, 600, 700, 800, 1300, 850, 2000],
          color: "#FF9937",
        },
      ],
    },
  ];
  const handleCopyClick = async () => {
    try {
      // Use the Clipboard API to copy text
      const url = `http://localhost:3000/auth/investor-register?referal_code=${AllInvestorDashboardData?.transactions?.referal_code}`
      await navigator.clipboard.writeText(url);
      toast.success("Url is Copied");
    } catch (err) {
      toast.error("Unable to Copy the Url");
    }
  };
  return (
    <div>
      {
        status=="pending" && <ScreenLoader/>
      }
      <div className="w-full px-3">
        <div className="flex items-center justify-center gap-3 flex-wrap w-full mb-12">
          {dashboardMiniStats.map((item, key) => (
            <StatsCards key={item.title} item={item} />
          ))}
        </div>
        <div className={`${manrope.className}`}>
          <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
            Refer to your friends or family
          </h1>
          <p className="text-xl font-[400]">
            Only <span className="text-red-600 font-[600]">{AllInvestorDashboardData?.transactions?.total_slots?.toLocaleString()}</span> slots are
            left that can invest in our project
          </p>
        </div>
        <div
          className={`flex items-center justify-between w-full p-4 bg-[#f4f9fd] border border-gray-200 ${manrope.className} rounded-[40px] mt-3 flex-wrap gap-2`}
        >
          <p className="text-[#444BDB]  flex-grow overflow-x-auto md:flex-grow-0  ">
          {`http://localhost:3000/auth/investor-register?referal_code=${AllInvestorDashboardData?.transactions?.referal_code}`}
          </p>
          <button onClick={handleCopyClick} className="bg-[#434CD9] flex-grow md:flex-grow-0  px-12 py-3 rounded-3xl text-white">
            {" "}
            Copy
          </button>
        </div>
        <StatsCharts />
      </div>
    </div>
  );
};

export default page;
