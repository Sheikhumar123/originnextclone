import AdsImage from "@/components/home/AdsImage";
import { listingData, servicesCardData } from "@/data/homepageData";
import { manrope } from "@/utils/fontPoppins";
import Image from "next/image";
import { BsFillLightningChargeFill } from "react-icons/bs";
import { FiThumbsUp } from "react-icons/fi";
import { HiOutlineUserGroup } from "react-icons/hi2";
import { IoIosHeartEmpty } from "react-icons/io";
import { LiaPhoneVolumeSolid } from "react-icons/lia";
import ratingImgCard from "../../../images/ratingImgCard.png";

const Listings = () => {
  return (
    <div className="w-full ">
      <div className={`w-full ${manrope.className}`}>
        <AdsImage />
        <div className="w-full p-2 700px:p-6 1300px:p-12 bg-gradient-to-r from-[#F2A2DC66] via-[#EFBE8466] to-[#A1F19766]">
          <h1 className="text-[2rem] text-center font-semibold mb-4">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit.
          </h1>
          {/* SEARCH BAR   */}
          <form className="mb-4">
            <label
              htmlFor="default-search"
              className="mb-2 text-sm font-medium text-gray-900 sr-only "
            >
              Search
            </label>
            <div className="relative">
              <input
                type="search"
                id="default-search"
                className="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg  focus:ring-blue-500 focus:border-blue-500 h-[55px] "
                placeholder="Search For Lisitngs"
                required
              />
            </div>
          </form>

          {/* RANDOM CATEGORIES  */}
          <div className="flex items-center justify-center gap-4 flex-wrap  mb-4 w-full">
            {listingData.map((item, index) => (
              <div
                key={index + 67 * 2}
                className={`${
                  index % 2 === 0 ? "bg-[#0479D3]" : "bg-[#7C27EF]"
                } px-6 py-2 rounded-md text-center text-nowrap  text-white`}
              >
                {item}
              </div>
            ))}
          </div>
          {/*  ALL CATEGORIES  */}
          <div className="flex items-center justify-between gap-2 flex-wrap w-full">
            {servicesCardData.map((mainArray, index) => (
              <div
                className="flex flex-col gap-4 rounded-md items-center justify-center p-4 1400px:p-6 border-dashed border border-bgBlue flex-grow "
                key={index + 33 * 8}
              >
                {mainArray.map((item, index) => (
                  <div
                    className={`${
                      item.sideBg ? `bg-${item.sideBg}` : "bg-lime-500"
                    } pl-3 w-full  1200px:w-[500px] 1300px:w-[550px] 1400px:w-[600px] rounded-md shadow-lg `}
                    key={9889 + index * 3}
                  >
                    <div
                      className={`flex flex-col gap-2 p-2 bg-${item.bg} hover:text-white hover:bg-black`}
                    >
                      <div className="flex items-center justify-between gap-2 flex-wrap">
                        <div className="flex gap-2">
                          {item.new && (
                            <p className="px-2 py-1 text-sm rounded-md bg-red-500 text-white">
                              New
                            </p>
                          )}
                          {item.pinned && (
                            <p className="px-2 py-1 text-sm rounded-md bg-black text-yellow-500">
                              Pinned
                            </p>
                          )}
                          {item.featured && (
                            <p className="px-2 py-1 text-sm rounded-md bg-black text-orange-500">
                              Featured
                            </p>
                          )}
                        </div>
                        {item.verified && (
                          <p className="px-2 py-1 text-sm rounded-md bg-blue-500 text-white">
                            Verified
                          </p>
                        )}
                        {item.online && (
                          <p className="px-2 py-1 text-sm rounded-md bg-green-800 text-white">
                            Online
                          </p>
                        )}
                      </div>
                      <div className="w-full flex flex-wrap 500px:flex-nowrap gap-4 relative overflow-hidden ">
                        {item.featured && (
                          <div className="ribbon absolute -bottom-[6.5rem] -right-[5rem] h-[10.5rem] w-40 overflow-hidden before:absolute before:top-0 before:right-0 before:-z-[10000] before:border-4 before:border-blue-500 after:absolute after:left-0 after:bottom-0 after:-z-[1] after:border-4 after:border-blue-500">
                            <div className="absolute -left-[4rem] top-[43px] w-60 -rotate-[40deg] bg-gradient-to-br from-yellow-600 via-yellow-400 to-yellow-500 py-2.5 text-center text-white shadow-md flex items-center justify-center">
                              <BsFillLightningChargeFill className="ml-7 mb-2" />
                            </div>
                          </div>
                        )}
                        <Image src={item.image} alt="img service" />
                        <div>
                          <h1 className="text-xl font-semibold">
                            {item.title}
                          </h1>
                          <p className="text-sm z-50 max-w-[700px]">
                            {item.desc}
                          </p>
                          <span className=" text-green-600 font-semibold">
                            Starting From:{" "}
                          </span>
                          <span className="font-semibold">
                            ${item.price} USD
                          </span>
                        </div>
                      </div>
                      <hr
                        className="border-none h-1  from-black via-black to-transparent bg-repeat-x-6"
                        style={{ height: "1px" }}
                      />
                      <div className="flex items-center justify-between  gap-3 flex-wrap text-gray-500">
                        <div className="flex h-[25px] gap-1 text-sm items-center">
                          <Image
                            src={item.flag}
                            alt="image flag"
                            height={25}
                            width={25}
                          />
                          <p>{item.country}</p>
                        </div>
                        <div className="flex h-[25px] gap-1 text-sm items-center">
                          <HiOutlineUserGroup color="#009999 " size={20} />
                          <p>{item.orders}</p>
                        </div>
                        <div className="flex  gap-1 text-sm items-center">
                          <Image src={ratingImgCard} alt="ratingImg" />
                          <p>{item.rating}</p>
                        </div>
                        <div className="flex  gap-2 text-sm items-center">
                          <IoIosHeartEmpty size={22} color="#d71a1a" />
                          <FiThumbsUp size={22} />
                        </div>
                        <button className="bg-yellow-500 text-black px-6 py-2 rounded-3xl flex items-center gap-1 border-black border">
                          <p> Contact Us </p>
                          <LiaPhoneVolumeSolid size={22} />
                        </button>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Listings;
