import AdsImage from "@/components/home/AdsImage";
// import ListingDetail from "@/components/listing-page/ListingDetail";
import ListingSellerDetails from "@/components/listing-page/ListingSellerDetails";
import { manrope } from "@/utils/fontPoppins";
import Image from "next/image";
import { recommendedServiceData } from "@/data/homepageData";
import { HiOutlineUserGroup } from "react-icons/hi2";
import ratingImgCard from "../../../images/ratingImgCard.png";
import { IoIosHeartEmpty } from "react-icons/io";
import { FiThumbsUp } from "react-icons/fi";
import { BsFillLightningChargeFill } from "react-icons/bs";

import { LiaPhoneVolumeSolid } from "react-icons/lia";

const ListingDetails = () => {
  return (
    <div className="w-full">
      <div className={`w-full  mb-4 ${manrope.className}`}>
        <AdsImage />
        <div className="w-full flex flex-col 1100px:flex-row gap-2 p-8">
          <div className="w-full 1100px:w-[70%]">{/* <ListingDetail /> */}</div>
          <div className="flex flex-col gap-4 items-start w-full 1100px:w-[30%]">
            <div className="1100px:hidden mt-4 p-2">
              <h1 className="text-xl font-semibold">Seller Details</h1>
            </div>
            <ListingSellerDetails />
          </div>
        </div>
        <div className="w-full p-4 600px:p-8">
          <h1 className="text-[2rem] font-semibold">Recommended Services</h1>
          <div className="flex items-center gap-6 flex-wrap">
            {recommendedServiceData.map((item, index) => (
              <div
                className={`text-sm ${
                  item.sideBg ? `bg-${item.sideBg}` : "bg-lime-500"
                } pl-3  w-[400px] rounded-md shadow-lg`}
                key={9889 + index * 3}
              >
                <div className={`flex flex-col p-[2px] bg-${item.bg}`}>
                  <div className="flex items-center justify-between mb-[2px]">
                    <div className="flex gap-2">
                      {item.new && (
                        <p className="px-1 text-sm rounded-md bg-red-500 text-white">
                          New
                        </p>
                      )}
                      {item.pinned && (
                        <p className="px-1 text-sm rounded-md bg-black text-yellow-500">
                          Pinned
                        </p>
                      )}
                      {item.featured && (
                        <p className="px-1 text-sm rounded-md bg-black text-orange-500">
                          Featured
                        </p>
                      )}
                    </div>
                    {item.verified && (
                      <p className="px-1 text-sm rounded-md bg-blue-500 text-white">
                        Verified
                      </p>
                    )}
                    {item.online && (
                      <p className="px-1 text-sm rounded-md bg-green-800 text-white">
                        Online
                      </p>
                    )}
                  </div>
                  <div className="w-full flex  500px:flex-nowrap gap-4 relative overflow-hidden ">
                    {item.featured && (
                      <div className="ribbon absolute -bottom-[6.5rem] -right-[5rem] h-[10.5rem] w-40 overflow-hidden before:absolute before:top-0 before:right-0 before:-z-[10000] before:border-4 before:border-blue-500 after:absolute after:left-0 after:bottom-0 after:-z-[1] after:border-4 after:border-blue-500">
                        <div className="absolute -left-[4rem] top-[43px] w-60 -rotate-[40deg] bg-gradient-to-br from-yellow-600 via-yellow-400 to-yellow-500 py-2.5 text-center text-white shadow-md flex items-center justify-center">
                          <BsFillLightningChargeFill className="ml-7 mb-2" />
                        </div>
                      </div>
                    )}
                    <Image
                      src={item.image}
                      width={100}
                      height={100}
                      alt="img service"
                      className="w-[100px] h-[100px]"
                    />
                    <div className="flex flex-col justify-between">
                      <h1 className="text-sm font-bold">{item.title}</h1>
                      <p className=" text-[8px] 500px:text-sm z-50">
                        {item.desc}
                      </p>
                      <h1>
                        <span className=" text-purple-600 text-[10px] 500px:text-sm  font-bold">
                          Engagement Rate:{" "}
                        </span>
                        <span className="font-semibold text-[10px] 500px:text-sm ">
                          {item.engagementRate}
                        </span>
                      </h1>
                    </div>
                  </div>
                  <hr
                    className="border-none h-1  from-black via-black to-transparent bg-repeat-x-6 mt-[2px] mb-[1px]"
                    style={{ height: "1px" }}
                  />
                  <div className="flex items-center gap-1 justify-between  text-gray-500">
                    <div className="flex h-[25px] gap-[2px] text-sm items-center">
                      <Image
                        src={item.flag}
                        alt="image flag"
                        height={20}
                        width={20}
                      />
                      <p className="text-[6px] 500px:text-[11px] font-semibold">
                        {item.country}
                      </p>
                    </div>
                    <div className="flex h-[25px] gap-[2px] text-sm items-center">
                      <HiOutlineUserGroup color="#009999 " size={14} />
                      <p className="text-[8px] 500px:text-[11px] font-semibold">
                        {item.orders}
                      </p>
                    </div>
                    <div className="flex  gap-1 text-sm items-center">
                      <Image src={ratingImgCard} alt="ratingImg" />
                      <p>{item.rating}</p>
                    </div>
                    <div className="flex  gap-2 text-sm items-center">
                      <IoIosHeartEmpty size={14} color="#d71a1a" />
                      <FiThumbsUp size={14} />
                    </div>
                    <button className="bg-yellow-400 text-black px-2 py-1 rounded-3xl flex items-center gap-1 border-black border text-[11px] font-semibold">
                      <p className="text-[8px] 500px:text-[11px] text-nowrap">
                        {" "}
                        Contact Us{" "}
                      </p>
                      <LiaPhoneVolumeSolid size={10} />
                    </button>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div className="hidden bg-recommendedBlue bg-recommendedRed"></div>
    </div>
  );
};

export default ListingDetails;
