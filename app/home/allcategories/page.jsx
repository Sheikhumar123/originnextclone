import { manrope } from "@/utils/fontPoppins";
import ServicesProvider from "@/components/home/ServicesProvider";
import { baseUrl } from "@/utils/BaseURL";
import AllCategories from "@/components/home/AllCategories/AllCategories";
const Page = async () => {

//   const AllCategories = await fetch(
//     `${baseUrl}landingpage/allcategorieslandingpg?page=1`,
//     { cache: "no-store" }
//   );
  return (
    <div className="w-full bg-gradient-to-b from-blue-50  to-transparent 600px:px-12 ">
      <div className={`w-full  mb-4 ${manrope.className}`}>
        <AllCategories 
        // AllCategoriesData={AllCategories}
         />
      </div>
    </div>
  );
};
export default Page;
