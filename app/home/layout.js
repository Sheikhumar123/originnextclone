import Navbar from "@/components/home/Navbar";
import "../globals.css";
import Footer from "@/components/home/Footer";
export const metadata = {
  title: "Origin",
  description: "Business Directories",
};

export default function RootLayout({ children }) {
  return (
    <div>
      <Navbar />
     
      {children}
    
      <Footer />
    </div>
  );
}
