import AdsImage from "@/components/home/AdsImage";
import influencer1 from "@/images/influencer-img-1.png";
import influencer2 from "@/images/influencer-img-2.png";
import { CiCircleCheck } from "react-icons/ci";
import { FaLongArrowAltRight } from "react-icons/fa";

import {
  benefitsData,
  influencerLandingPageData,
  onBoardData,
  randomInfluencerBg,
  serviceProviderData,
} from "@/data/influencerPageData";
import { manrope } from "@/utils/fontPoppins";
import Image from "next/image";
import React from "react";
import FAQ from "@/components/home/FAQ";

const page = () => {
  return (
    <div className="w-full">
      <div className={`w-full  mb-4 ${manrope.className}`}>
        <AdsImage />
        <div className="w-full p-4 500px:p-12 500px:mt-4">
          <h1 className=" text-[1.8rem] 400px:text-[2.2rem] font-bold">
            Best Influencer Services
          </h1>
          <h1 className="text-[1.5rem] 400px:text-[1.8rem] font=[200] text-[#757575]">
            Youtube Thumbnail Expert{" "}
          </h1>
          <p className="text-[#757575]">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, sed
            consequatur esse magni sit voluptatem doloribus voluptatibus maiores
            blanditiis, soluta officiis aperiam asperiores laudantium totam. Qui
            vero assumenda facilis eligendi quaerat? Veniam ut repellat in
            temporibus similique deleniti. Quo modi aliquid dolor vel ea ipsam
            ut deleniti harum sit dolorem.
          </p>
          {/* SEARCH BAR  */}

          <form className=" mx-auto max-w-3xl mt-4">
            <label
              htmlFor="default-search"
              className="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white"
            >
              Search
            </label>
            <div className="relative">
              <input
                type="search"
                id="default-search"
                className="block w-full p-4 ps-2 text-sm text-blue-500 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 "
                placeholder="Website Developer"
                required
              />
              <button
                type="submit"
                className="text-white absolute end-1 bottom-[0.3rem] bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-8 py-3"
              >
                Search
              </button>
            </div>
          </form>
          {/* POPULAR SECTION  */}
          <div className="flex flex-wrap items-center justify-center max-w-7xl mx-auto mt-4 gap-4 900px:gap-12">
            {influencerLandingPageData.map((item, index) => (
              <div
                className="flex flex-col gap-2  900px:w-[48%] items-center 900px:items-start flex-grow"
                key={index * 89 + 3}
              >
                <h1 className="font-semibold text-gray-500">{item.title}</h1>
                <div className="flex items-center justify-center 900px:justify-start flex-wrap gap-2">
                  {item.categories.map((subItem, subIndex) => (
                    <div
                      className={`flex items-center gap-2 p-2 text-nowrap rounded-md font-semibold 500px:text-sm text-[12px] ${
                        randomInfluencerBg[Math.floor(Math.random() * 3)]
                      }`}
                      key={subIndex * 2.3 + 1.45}
                    >
                      <Image
                        src={subItem.icon}
                        height={25}
                        width={25}
                        alt="icon img"
                      />
                      <h1>{subItem.name}</h1>
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
          <div className="mt-12 w-full">
            {/* CARD SECTION  */}
            <div className="cards-container-div">
              <div className="max-w-2xl mx-auto">
                <h1 className="text-center text-[1.5rem] 400px:text-[1.8rem] font-bold ">
                  Very Easy To onboard On Origin And Start Getting Clients For
                  Free.
                </h1>
              </div>
              <div className="w-full flex flex-col gap-2 justify-center">
                <div className="flex items-center justify-center flex-wrap mt-8 gap-8">
                  {onBoardData.map((item, index) => (
                    <div className="relative text-white" key={index + 6.3 * 2}>
                      <div className="w-[65px] h-[65px] rounded-full bg-gradient-to-r from-[#8C01FA] to-[#19FB9B] flex items-center justify-center absolute z-10 -left-5 -top-5">
                        <h1 className="font-semibold text-white text-sm">
                          Step {index + 1}
                        </h1>
                      </div>
                      <div className="card w-72 bg-base-100 shadow-xl bg-gradient-to-r from-[#3EC1F3] to-[#444BDB]">
                        <div className="overflow-hidden p-1 ">
                          <Image
                            src={item.imageUrl}
                            alt={item.title}
                            width={500}
                            height={500}
                            className="rounded-xl"
                            priority
                          />
                        </div>
                        <div className="card-body p-4">
                          <h2 className="card-title text-[16px] text-nowrap font-bold">
                            {item.title}
                          </h2>
                          <p className="text-sm">{item.text}</p>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
                <button className="mt-6 mx-auto px-12 py-3 bg-bgBlue rounded-md text-white max-w-[300px]">
                  Start Getting Clients
                </button>
              </div>
            </div>
            {/* {BENEFITS OF SERVICE PROVIDER} */}
            <div className="flex flex-col 900px:flex-row items-center justify-between mt-4">
              <div className="900px:w-[48%] w-full flex flex-col gap-2 justify-between 500px:min-h-[350px]">
                <h1 className="text-[#312E81] text-[1.5rem] 900px:text-[2.5rem] 900px:w-[90%] font-semibold leading-none">
                  Benefits to become service provider in our website
                </h1>
                <div>
                  {benefitsData.map((item, index) => (
                    <div
                      className="flex items-center gap-2 mb-2"
                      key={index + 0.365 * 2}
                    >
                      <CiCircleCheck
                        size={30}
                        color="#86D2F1"
                        strokeWidth={"0.5px"}
                      />
                      <h1>{item}</h1>
                    </div>
                  ))}
                </div>
                <button className="bg-[#6366F1] px-6 py-2 w-[160px] rounded-3xl text-white font-semibold flex items-center gap-2">
                  <h1>Get Started</h1> <FaLongArrowAltRight color="#fff" />
                </button>
              </div>
              <div className="900px:w-[48%] w-full">
                <Image src={influencer1} alt="Content Img" priority />
              </div>
            </div>
            {/* WHO ARE SERVICE PROVIDER  */}
            <div className="flex flex-col-reverse 900px:flex-row items-center justify-between mt-4 mb-4">
              <div className="900px:w-[48%] w-full">
                <Image src={influencer2} alt="Content Img" priority />
              </div>
              <div className="900px:w-[48%] w-full flex flex-col gap-2 justify-between 500px:min-h-[350px]">
                <h1 className="text-[#312E81] text-[1.5rem] 900px:text-[2.5rem] 900px:w-[90%] font-semibold leading-none">
                  who are service provider and who can be on our platform{" "}
                </h1>
                <div>
                  {serviceProviderData.map((item, index) => (
                    <div
                      className="flex items-center gap-2 mb-2"
                      key={index + 0.365 * 2}
                    >
                      <CiCircleCheck
                        size={30}
                        color="#86D2F1"
                        strokeWidth={"0.5px"}
                      />
                      <h1>{item}</h1>
                    </div>
                  ))}
                </div>
                <button className="bg-[#6366F1] px-6 py-2 w-[160px] rounded-3xl text-white font-semibold flex items-center gap-2">
                  <h1>Get Started</h1> <FaLongArrowAltRight color="#fff" />
                </button>
              </div>
            </div>
            {/* FAQ  */}
          </div>
        </div>
        <FAQ paddingFalse={true} />
      </div>
      <div className="hidden bg-red-100 bg-blue-100 bg-yellow-100 bg-green-100"></div>
    </div>
  );
};

export default page;
