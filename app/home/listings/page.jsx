import AdsImage from "@/components/home/AdsImage";
import { manrope } from "@/utils/fontPoppins";
import { baseUrl } from "@/utils/BaseURL";
// import Filters from "@/components/home/listing/Filters";
// import CategoryOrServiceData from "@/components/home/listing/CategoryOrService/CategoryOrServiceData";
import dynamic from "next/dynamic";
const CategoryOrServiceData = dynamic(() => import('@/components/home/listing/CategoryOrService/CategoryOrServiceData'), { ssr: false })
const Filters = dynamic(() => import('@/components/home/listing/Filters'), { ssr: false })
const Listings = async ({ searchParams }) => {
  console.log(searchParams);
  const response = await fetch(`${baseUrl}landingpage/categoprywiseservices`, {
    method: "POST",
    body: JSON.stringify(searchParams),
    headers: {
      "Content-Type": "application/json",
    },
    cache: "no-store",
  });
  const categoriesPopulate = await fetch(
    `${baseUrl}landingpage/servicescategorylandingpg`,
    {
      cache: "no-store",
    }
  )
    .then((res) => {
      return res.json();
    })
    .catch((err) => {});
  const { data,count,limit } = await response.json();
console.log("data",limit);
  return (
    <div className="w-full">
      <div className={`w-full  mb-4 ${manrope.className}`}>
        <AdsImage />
        <div className="w-full p-2 700px:p-6 1300px:p-12 1300px:px-[70px] 300px:px-12">
          <h1 className="text-[2rem] text-center font-semibold mb-4">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit.
          </h1>
          {/* SEARCH BAR   */}
          <form className="mb-4">
            <label
              htmlFor="default-search"
              className="mb-2 text-sm font-medium text-gray-900 sr-only "
            >
              Search
            </label>
            <div className="relative">
              <div className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                <svg
                  className="w-4 h-4 text-black"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 20 20"
                >
                  <path
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                  />
                </svg>
              </div>
              <input
                type="search"
                id="default-search"
                className="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg  focus:ring-blue-500 focus:border-blue-500 h-[55px] bg-blue-100 "
                placeholder="Search For Lisitngs"
                required
              />
              <button
                type="submit"
                className="text-white absolute end-2.5 bottom-1.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-6 py-3"
              >
                Search
              </button>
            </div>
          </form>
          {/* FILTER BUTTONS  */}
          <Filters searchParams={searchParams} />
          {/* RANDOM CATEGORIES  */}
          {/* <CategoriesSection
            allCategories={categoriesPopulate?.allcategories}
            searchParams={searchParams}
          /> */}
          {/*  ALL CATEGORIES  */}
          <CategoryOrServiceData
            data={data}
            allCategories={categoriesPopulate}
            searchParams={searchParams}
            totalCount={count}
            Limit={limit}
          />
          {/* <CategoryServices data={data} /> */}
          {/* <button className="btn  mx-auto flex  gap-2">
            <p>Show More</p>{" "}
            <FaChevronDown size={20} className="animate-bounce" />
          </button> */}
        </div>
      </div>
      <div className="hidden bg-lime-400 bg-yellow-400 bg-blue-400 bg-red-400 bg-green-400 bg-orange-400 bg-purple-400 bg-violet-400"></div>
    </div>
  );
};

export default Listings;
