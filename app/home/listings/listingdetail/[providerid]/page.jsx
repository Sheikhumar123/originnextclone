import AdsImage from "@/components/home/AdsImage";
import ListingDetail from "@/components/listing-page/ListingDetail";
import ListingSellerDetails from "@/components/listing-page/ListingSellerDetails";
import { manrope } from "@/utils/fontPoppins";
import { baseUrl } from "@/utils/BaseURL";
// import SimilarServiceSlider from "@/components/listing-page/SimilarServiceSlider";
import dynamic from "next/dynamic";
import SimilarService from "@/components/listing-page/SimilarService";

const SimilarServiceSlider = dynamic(
  () => import("@/components/listing-page/SimilarServiceSlider"),
  { ssr: false }
);

const page = async ({ params }) => {
  let servicesData = await getServices(params?.providerid);
  let servicesfaqData = await getServicesFaq(params?.providerid);
  let servicesReviewData = await getServicesReview(params?.providerid);
  let SimilarServicesData = await getSimilarServices(params?.providerid);

  const providerDetail = await fetch(
    `${baseUrl}landingpage/servicesproviderdetaildata?user_id=${params?.providerid}`,
    {
      cache: "no-store",
    }
  )
    .then((res) => {
      return res.json();
    })
    .catch((err) => {});
  console.log("providerDetail", providerDetail);
  console.log("params", params);
  return (
    <div className="w-full">
      <div className={`w-full  mb-4 ${manrope.className}`}>
        <AdsImage />
        <div className="w-full flex flex-col 1100px:flex-row gap-2 p-8">
          <div className="w-full 1100px:w-[70%]">
            <ListingDetail
              providerDetail={providerDetail?.data}
              servicesData={servicesData}
              servicesfaqData={servicesfaqData}
              servicesReviewData={servicesReviewData}
              params={params}
            />
          </div>
          <div className="flex flex-col gap-4 items-start w-full 1100px:w-[30%]">
            <div className="1100px:hidden mt-4 p-2">
              <h1 className="text-xl font-semibold">Seller Details</h1>
            </div>
            <ListingSellerDetails providerDetail={providerDetail} />
          </div>
        </div>
        <div className="w-full p-4 600px:p-8">
          <h1 className="text-[2rem] font-semibold pb-4">Similar Services</h1>
          <SimilarService SimilarServicesData={SimilarServicesData} />
        </div>
      </div>
      <div className="hidden bg-recommendedBlue "></div>
    </div>
  );
};

export default page;
const getServices = async (categoryId) => {
  let servicesData;
  try {
    const services = await fetch(
      `${baseUrl}landingpage/providermoreservices?user_id=${categoryId}`,
      {
        cache: "no-store",
      }
    );
    if (!services.ok) {
      throw new Error("Network response was not ok");
    }
    servicesData = await services.json();
    return servicesData;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
};
const getServicesFaq = async (categoryId) => {
  let servicesfaqData;
  try {
    const servicesfaq = await fetch(
      `${baseUrl}landingpage/getservicesfaqs?user_id=${categoryId}`,
      {
        cache: "no-store",
      }
    );
    if (!servicesfaq.ok) {
      throw new Error("Network response was not ok");
    }
    servicesfaqData = await servicesfaq.json();
    return servicesfaqData;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
};
const getServicesReview = async (categoryId) => {
  let servicesReviewData;
  try {

    const servicesreview = await fetch(
      `${baseUrl}dashboard/userreviewsdata`,
       {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          // Add any other headers as needed
        },
        cache:"no-store",
        body: JSON.stringify({
          user_id:categoryId,
          page:1
        }),
      }
    );
    if (!servicesreview.ok) {
      throw new Error("Network response was not ok");
    }
    servicesReviewData = await servicesreview.json();
    return servicesReviewData;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
};
// const getServicesReview = async (categoryId) => {
//   let servicesReviewData;
//   try {

//     const servicesreview = await fetch(
//       `${baseUrl}landingpage/providerreviews?user_id=${categoryId}`,
//        {
//         method: 'POST',
//         headers: {
//           'Content-Type': 'application/json',
//           // Add any other headers as needed
//         },
//         cache:"no-store",
//         body: JSON.stringify({
//           user_id:user_id,
//           rate:rating,
//           type:type,
//           page:page
//         }),
//       }
//     );
//     if (!servicesreview.ok) {
//       throw new Error("Network response was not ok");
//     }
//     servicesReviewData = await servicesreview.json();
//     return servicesReviewData;
//   } catch (error) {
//     console.error("Error fetching data:", error);
//   }
// };
const getSimilarServices = async (categoryId) => {
  let SimilarServicesData;
  try {
    const similarservices = await fetch(
      `${baseUrl}landingpage/getsimilarservices?user_id=${categoryId}`,
      {
        cache: "no-store",
      }
    );
    if (!similarservices.ok) {
      throw new Error("Network response was not ok");
    }
    SimilarServicesData = await similarservices.json();
    return SimilarServicesData;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
};
