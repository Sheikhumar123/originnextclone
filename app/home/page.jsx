import { manrope } from "@/utils/fontPoppins";
import AdsImage from "@/components/home/AdsImage";
import CTASection from "@/components/home/CTASection";
import CTASubSection from "@/components/home/CTASubSection";
// import Categories from "@/components/home/Categories";
import ServicesProvider from "@/components/home/ServicesProvider";
import Influencers from "@/components/home/Influencers";
import FAQ from "@/components/home/FAQ";
import WorkCategories from "@/components/home/WorkCategories";
import AboutPeople from "@/components/home/AboutPeople";
import Masonary from "@/components/home/Masonary";
import AboutUs from "@/components/home/AboutUs";
import { baseUrl } from "@/utils/BaseURL";
import Categories from "@/components/home/Categories";

const Home = async () => {
  let AllPlatform=await getAllPlatform()
  let AllFaq=await getAllFaq()
  let AllCategoriesData=await getAllCategories()
  let ProviderCategories=await getProviderCategories()

  // let AllPlatform;
  // let AllFaq;
  // let AllCategoriesData;
  // let ProviderCategories;
  // try {
  //   const platForm = await fetch(`${baseUrl}landingpage/landingpageplatforms`, {
  //     next: {
  //       revalidate: 0,
  //     },
  //   });
  //   if (!platForm.ok) {
  //     throw new Error("Network response was not ok");
  //   }
  //   AllPlatform = await platForm.json();
  // } catch (error) {
  //   console.error("Error fetching data:", error);
  // }

  // try {
  //   const Faq = await fetch(`${baseUrl}landingpage/getFaqsbytype?type=`, {
  //     next: {
  //       revalidate: 0,
  //     },
  //   });
  //   if (!Faq.ok) {
  //     throw new Error("Network response was not ok");
  //   }
  //   AllFaq = await Faq.json();
  // } catch (error) {
  //   console.error("Error fetching data:", error);
  // }


  // try {
  //   const AllCategories = await fetch(
  //     `${baseUrl}landingpage/allcategorieslandingpg?page=1`,
  //     {
  //       next: {
  //         revalidate: 0,
  //       },
  //     }
  //   );
  //   if (!AllCategories.ok) {
  //     throw new Error("Network response was not ok");
  //   }
  //   AllCategoriesData = await AllCategories.json();
  // } catch (error) {
  //   console.error("Error fetching data:", error);
  // }



  // try {
  //   const respons = await fetch(`${baseUrl}landingpage/landingpagecategories`, {
  //     next: {
  //       revalidate: 0,
  //     },
  //   });
  //   if (!respons.ok) {
  //     throw new Error("Network response was not ok");
  //   }
  //   ProviderCategories = await respons.json();
  // } catch (error) {
  //   console.error("Error fetching data:", error);
  // }

  // let data;
  // try {
  //   const response = await fetch(`${baseUrl}landingpage/popularcategories`, {
  //     method: "GET",
  //     next: {
  //       revalidate: 0,
  //     },
  //   });
  //   if (!response.ok) {
  //     throw new Error("Network response was not ok");
  //   }
  //   data = await response.json();

  //   // Use the data here
  //   console.log("datawa", data);
  // } catch (error) {
  //   console.error("Error fetching data:", error);
  // }

  return (
    <div className="w-full bg-gradient-to-b from-blue-50  to-transparent 600px:px-12 ">
      <div className={`w-full  mb-4 ${manrope.className}`}>
        <AdsImage />
        <CTASection />
        <CTASubSection />
        <Categories />
        <ServicesProvider ProviderCategories={ProviderCategories} />
        <Influencers AllPlatform={AllPlatform} />
        <FAQ AllFaq={AllFaq} />
        <WorkCategories AllCategoriesData={AllCategoriesData} />
        <AboutPeople />
        <Masonary />
        <AboutUs />
      </div>
    </div>
  );
};

export default Home;
const getAllPlatform=async()=>{
  let AllPlatform;
  try {
    const platForm = await fetch(
      `${baseUrl}landingpage/landingpageplatforms`,
      {
        next: {
          revalidate: 0,
        },
      }
    );
    if (!platForm.ok) {
      throw new Error("Network response was not ok");
    }
    AllPlatform = await platForm.json();
    return AllPlatform;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}

const getAllFaq=async()=>{
  let AllFaq;
  try {
    const Faq = await fetch(
      `${baseUrl}landingpage/getFaqsbytype?type=`,
      {
        next: {
          revalidate: 0,
        },
      }
    );
    if (!Faq.ok) {
      throw new Error("Network response was not ok");
    }
    AllFaq = await Faq.json();
    return AllFaq;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}
const getAllCategories=async()=>{
  let AllCategoriesData;
  try {
    const AllCategories = await fetch(
      `${baseUrl}landingpage/allcategorieslandingpg?page=1`,
      {
        next: {
          revalidate: 0,
        },
      }
    );
    if (!AllCategories.ok) {
      throw new Error("Network response was not ok");
    }
    AllCategoriesData = await AllCategories.json();
    return AllCategoriesData;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}
const getProviderCategories=async()=>{
  let ProviderCategories;
  try {
    const respons = await fetch(
      `${baseUrl}landingpage/landingpagecategories`,
      {
        next: {
          revalidate: 0,
        },
      }
    );
    if (!respons.ok) {
      throw new Error("Network response was not ok");
    }
    ProviderCategories = await respons.json();
    return ProviderCategories;
  } catch (error) {
    console.error("Error fetching data:", error);
  }
}
