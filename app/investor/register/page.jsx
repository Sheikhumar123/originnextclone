"use client";
import Image from "next/image";
import Logo from "../../../images/logo.png";
import { useState } from "react";
import DOB from "@/components/register/DOB";
import RegInfo from "@/components/register/RegInfo";
import Payment from "@/components/register/Payment";
import RegInfo2 from "@/components/register/RegInfo2";
const SignUp = () => {
  const [tab, setTab] = useState("dob");
  const [active, setActive] = useState(1);

  const [data, setData] = useState({
    dob: "",
    firstName: "",
    lastName: "",
    email: "",
    phone: "",
    otp: "",
    referralNo: "",
    fatherName: "",
    address: "",
    password: "",
    confirmPassword: "",
    amount: "",
  });
  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setData({ ...data, [name]: value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log(data);
  };
  return (
    <div className="flex items-center 900px:items-start justify-center p-6 sm:p-12 900px:w-1/2 relative ">
      <div className="w-full mt-8 md:mt-6 lg:mt-0 md:max-h-[800px] p-8 ">
        <Image
          src={Logo}
          alt={"Login Image"}
          style={{ width: "150px", height: "50px" }}
          className="mb-12"
        />
        <ol className="flex items-center w-full mb-12">
          <li className="flex w-full  items-center text-blue-600 dark:text-blue-500 after:content-[''] after:w-full after:h-1 after:border-b after:border-blue-100 after:border-4 after:inline-block dark:after:border-blue-800">
            <span className="flex bg-bgBlue text-white items-center justify-center w-10 h-10  rounded-full lg:h-12 lg:w-12  shrink-0">
              1
            </span>
          </li>
          <li
            className={`flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block
            }`}
          >
            <span
              className={`flex items-center justify-center w-10 h-10  rounded-full lg:h-12 lg:w-12  shrink-0  ${
                active >= 2 ? "bg-bgBlue text-white" : "bg-gray-100"
              }`}
            >
              2
            </span>
          </li>{" "}
          <li
            className={`flex w-full items-center after:content-[''] after:w-full after:h-1 after:border-b after:border-gray-100 after:border-4 after:inline-block`}
          >
            <span
              className={`flex items-center justify-center w-10 h-10 rounded-full lg:h-12 lg:w-12  shrink-0  ${
                active >= 3 ? "bg-bgBlue text-white" : "bg-gray-100 "
              }`}
            >
              3
            </span>
          </li>
          <li className="flex items-center w-full">
            <span
              className={`flex items-center justify-center w-10 h-10  rounded-full lg:h-12 lg:w-12  shrink-0 ${
                active === 4 ? "bg-bgBlue text-white" : "bg-gray-100"
              }`}
            >
              4
            </span>
          </li>
        </ol>
        <h1 className="mb-4 text-2xl font-[600] text-start  text-black">
          Registration
        </h1>
        <form onSubmit={handleSubmit}>
          {/* {tab === "dob" && (
            <DOB
              setData={setData}
              data={data}
              handleChange={handleChange}
              setTab={setTab}
              setActive={setActive}
            />
          )} */}
          {/* {tab === "info" && (
            <RegInfo
              setData={setData}
              data={data}
              handleChange={handleChange}
              setTab={setTab}
              setActive={setActive}
            />
          )} */}
          {/* {tab === "info-2" && (
            <RegInfo2
              setData={setData}
              data={data}
              handleChange={handleChange}
              setTab={setTab}
              setActive={setActive}
            />
          )} */}
          {/* {tab === "payment" && (
            <Payment
              setData={setData}
              data={data}
              handleChange={handleChange}
              setTab={setTab}
              setActive={setActive}
            />
          )} */}
        </form>
      </div>
    </div>
  );
};

export default SignUp;
