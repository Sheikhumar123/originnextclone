import RegisterBanner from "../../../images/RegisterBanner.png";
import Image from "next/image";
import { poppins } from "@/utils/fontPoppins";

const layout = ({ children }) => {
  return (
    <div
      className={`flex items-center h-screen  bg-gray-50 ${poppins.className}`}
    >
      <div className="flex-1 h-full  bg-white rounded-lg ">
        <div className="flex flex-col 900px:flex-row">
          {children}
          <div className="min-h-screen 900px:w-1/2 900px:block hidden bg-bgBlue relative">
            <Image
              src={RegisterBanner}
              alt="Register Image"
              className="w-[75%] h-[75%] object-contain object-bottom absolute bottom-0 "
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default layout;
