"use client";
import React, { useEffect, useState } from "react";
import { FaChevronDown, FaLinkedin } from "react-icons/fa";
import { SlFlag } from "react-icons/sl";
import { Rating } from "@smastrom/react-rating";
import Image from "next/image";
import ReviewModal from "./ReviewModal";
import Link from "next/link";
import Pagination from "../Pagination/Pagination";
import { useDispatch } from "react-redux";
import { GetServiceProviderRating } from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import axiousInstance from "@/utils/axiousInstance";
import ScreenLoader from "../ScreenLoader/ScreenLoader";
import { toast } from "react-toastify";

function ListingDetailReview({ servicesReviewData, params }) {
  const [allReview, setallReview] = useState([]);
  const [count, setcount] = useState(0);
  const [limit, setlimit] = useState(0);
  const [page, setpage] = useState(1);
  const [rating, setrating] = useState(null);
  const [type, settype] = useState("");
  const [loader, setloader] = useState(false);
  console.log(servicesReviewData);
  useEffect(() => {
    if (servicesReviewData) {
      setallReview(servicesReviewData?.data);
      setcount(servicesReviewData?.count);
      setlimit(servicesReviewData?.limit);
    }
  }, [servicesReviewData]);
  console.log(params?.providerid);
  const handlePagination = async () => {
    let obj = {
      user_id: params?.providerid,
      type: type,
      rate: Number(rating),
      page: page,
    };
    setloader(true);
    try {
      const response = await axiousInstance.post(
        "dashboard/userreviewsdata",
        obj
      );
      setloader(false);
      if (response.data.status === "1") {
        setallReview(response.data?.data);
        setcount(response.data?.count);
        setlimit(response.data?.limit);
      } else {
        setloader(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error.response?.data?.error);
    }
  };
  useEffect(() => {
    handlePagination();
  }, [rating, type, page]);
  return (
    <div>
      {loader && <ScreenLoader />}
      <div className="flex items-center justify-between gap-4  mt-8 flex-wrap mb-4">
        <div className="flex items-center justify-between gap-4 w-full  mt-8 flex-wrap">
          <h1 className="text-3xl font-semibold   ">{count} Reviews</h1>
          {/* <h1 className="text-3xl font-semibold">
            {count} Reviews
          </h1> */}
          <div className="flex items-center gap-4 flex-wrap">
            <div>
              <select
                id="professionalType"
                className="bg-gray-50 border border-gray-300 text-sm rounded-md focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[150px] font-semibold"
                name="professional_type"
                value={type}
                onChange={(e) => {
                  settype(e.target.value);
                }}
              >
                <option value={""}>Type</option>
                <option value={"Most Recent"}>Most Recent</option>
                <option value="Today">Today</option>
                <option value="Last Month">Last Month</option>
              </select>
            </div>
            <div>
              <select
                id="professionalType"
                className="bg-gray-50 border border-gray-300 text-sm rounded-md focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[150px] font-semibold"
                name="professional_type"
                value={rating}
                onChange={(e) => {
                  setrating(e.target.value);
                }}
              >
                <option value={""}>Any Rating</option>
                <option value="1">1 Star</option>
                <option value="2">2 Star</option>
                <option value="3">3 Star</option>
                <option value="4">4 Star</option>
                <option value="5">5 Star</option>
              </select>
            </div>
            <Link href={"/providersdashboard/rating"}>
              <button className="bg-[#434CD9] px-12 py-3 rounded-3xl text-white">
                Reply to reviews
              </button>
            </Link>
          </div>
        </div>
        {/* <div className="flex items-center gap-4 flex-wrap">
          <div className="dropdown dropdown-end   ">
            <div
              tabIndex={0}
              role="button"
              className="btn m-1 bg-white border border-gray-400"
            >
              Most Recent <FaChevronDown size={20} />
            </div>
            <ul
              tabIndex={0}
              className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
            >
              <li>
                <a>Item 1</a>
              </li>
              <li>
                <a>Item 2</a>
              </li>
            </ul>
          </div>
          <div className="dropdown dropdown-end     ">
            <div
              tabIndex={0}
              role="button"
              className="btn m-1 bg-white border border-gray-400"
            >
              Any Rating <FaChevronDown size={20} />
            </div>
            <ul
              tabIndex={0}
              className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
            >
              <li>
                <a>Item 1</a>
              </li>
              <li>
                <a>Item 2</a>
              </li>
            </ul>
          </div>
          <button className="bg-[#434CD9]   px-12 py-3 rounded-3xl text-white">
            Write A Review
          </button>
        </div> */}
      </div>
      <div className="w-full flex flex-col gap-4 ">
        {allReview?.map((item, key) => (
          <div
            key={key + 978 * 2}
            className="p-6 rounded-md  shadow-loginShadow "
          >
            <div className="flex items-center justify-between">
              <div className="mb-2 flex items-center gap-2 w-auto">
                <div className=" w-[45px] h-[45px] rounded-full overflow-hidden">
                  <Image
                    src={item?.image}
                    style={{ width: "100%", height: "100%" }}
                    alt="profile img"
                    className=" object-cover "
                    width={500}
                    height={500}
                  />
                </div>
                <div className="flex flex-col ">
                  <h1 className="font-semibold">{item.review_from}</h1>
                  <p className="text-sm">{item?.title}</p>
                </div>
              </div>
              <div className="flex items-center gap-2">
                <FaLinkedin size={25} fill="#0E76A8" />{" "}
                <h1 className="font-semibold">Verified</h1>
              </div>
            </div>
            <Rating
              style={{ maxWidth: 150 }}
              value={Number(Math.ceil(item?.rating))}
              readonly
              emptySymbol="far fa-star"
              fullSymbol="fas fa-star"
            />
            <p className="mt-2 text-[#676767] mb-2">{item?.review}</p>
            <hr />
            <div className="flex items-center justify-between gap-2 600px:gap-0 mt-4 flex-wrap 600px:flex-nowrap text-nowrap">
              <div className="flex items-center gap-2 500px:gap-6 font-[600] flex-wrap 600px:flex-nowrap ">
                <h1>Helpful</h1>
                <h1>Share</h1>
              </div>
              <div className="flex items-center gap-2">
                <SlFlag fill="#f21a1a" size={20} />
                <h1 className="text-[#f21a1a] font-semibold">Report</h1>
              </div>
            </div>
          </div>
        ))}
      </div>
      {count > allReview?.length ? (
        <div className="pt-4 pb-[130px] flex justify-end px-2">
          <Pagination
            pageCount={page}
            setPageCount={setpage}
            limit={limit}
            totalcount={count}
            showCount={true}
          />
        </div>
      ) : null}
      <ReviewModal />
    </div>
  );
}

export default ListingDetailReview;
