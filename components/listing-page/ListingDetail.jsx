import { manrope } from "@/utils/fontPoppins";
import { FiThumbsUp } from "react-icons/fi";
import { FaChevronDown, FaLinkedin } from "react-icons/fa";
import { SlFlag } from "react-icons/sl";
import React from "react";
import ratingImg from "../../images/ratingImg.png";
import Image from "next/image";
import serviceImg from "../../images/serviceImg.png";
import {
  accordionData,
  listingServiceData,
  reviewListingData,
} from "@/data/homepageData";
import ModalButton from "@/utils/ModalButton";
import ReviewModal from "./ReviewModal";
import { Rating } from "@smastrom/react-rating";
import ListingDetailReview from "./ListingDetailReview";
import ListingButtons from "./ListingButtons";

const ListingDetail = ({
  providerDetail,
  servicesData,
  servicesfaqData,
  servicesReviewData,
  params,
}) => {
  return (
    <div className={`w-full ${manrope.className}`}>
      <h1 className=" text-[1.8rem] 400px:text-[2.5rem] font-semibold">
        {providerDetail?.title || ""}
      </h1>
      <h1 className="text-[1.5rem] 400px:text-[2.2rem] font=[200] text-[#757575]">
        {providerDetail?.username || ""}
      </h1>
      <div className="flex items-center gap-4 p-3 w-full flex-wrap">
        <div className="flex items-center gap-2">
          <Rating
            style={{ maxWidth: 150 }}
            isDisabled={true}
            value={providerDetail?.avgrating || 0}
          />
          <h3 className="text-lg text-black font-semibold">
            {providerDetail?.avgrating || 0}
          </h3>
        </div>
        <span className="p-3  text-black font-semibold">
          {providerDetail?.reviews ? `[${providerDetail?.reviews || ""}]` : ""}
        </span>
        <button
      className="px-4 py-2 border border-blue-500 text-blue-500 rounded-3xl font-semibold"
      // onClick={() => document.getElementById(`${openId}`).showModal()}
    >
     Rate Us Now
    </button>
        {/* <ModalButton
          classNameString={
            "px-4 py-2 border border-blue-500 text-blue-500 rounded-3xl font-semibold"
          }
          text={"Rate Us Now"}
          openId={"reviewModal"}
        /> */}
      </div>
      <div className="flex items-center gap-4 p-3 w-full flex-wrap">
        <button className="px-20 py-2 border bg-bgBlue text-white rounded-3xl font-semibold">
          Follow
        </button>
        <span className="p-3 font-bold">102 Followers</span>
        <span className="p-3  font-bold flex items-center gap-1">
          <div className="p-2 bg-bgBlue rounded-full">
            <FiThumbsUp className="text-white" size={25} />{" "}
          </div>
          <span>102 Recomendations</span>
        </span>
      </div>
      <div className="flex gap-2 justify-between flex-wrap w-full max-w-[800px] items-center mt-1">
        <div className="flex flex-col gap-2 ">
          <div className="flex 500px:gap-12 300px:gap-3 ">
            <p className="w-[150px] text-[#5c5c5c] font-[500]">
              Starting Price
            </p>
            <p className="font-bold">{providerDetail?.starting_price || ""}</p>
          </div>
          <div className="flex 500px:gap-12 300px:gap-3">
            <p className="w-[150px] text-[#5c5c5c] font-[500]">Type</p>
            <p className="font-bold">{providerDetail?.professional_type}</p>
          </div>
        </div>
        <div className="flex flex-col gap-2 ">
          <div className="flex 500px:gap-12 300px:gap-3">
            <p className="w-[150px] text-[#5c5c5c] font-[500]">In services</p>
            <p className="font-bold">{providerDetail?.in_expereince} years</p>
          </div>
          <div className="flex 500px:gap-12 300px:gap-3 ">
            <p className="w-[150px] text-[#5c5c5c] font-[500]">Response time</p>
            <p className="font-bold">40 mints</p>
          </div>
        </div>
      </div>
      <div className="flex flex-col gap-2 mt-2 mb-2">
        <div className="flex 500px:gap-12 300px:gap-3 ">
          <p className="w-[150px] text-[#5c5c5c] font-[500]">
            Languages I know
          </p>
          <p className="font-bold">
            {providerDetail?.language?.join(", ") || ""}
          </p>
        </div>
      </div>
      {/* <div className="flex items-start 600px:items-center gap-8 flex-wrap w-full mb-4 ">
        <p className="flex-1 500px:text-nowrap">
          Starting Price:{" "}
          <span className="font-semibold"> {providerDetail?.starting_price || ""}</span>
        </p>

        <p className="flex-1 500px:text-nowrap">
          In Services: <span className="font-semibold">{providerDetail?.in_expereince || ""}</span>
        </p>

        <p className="flex-1 500px:text-nowrap">
          Language&apos;s I Know:{" "}
          <span className="font-semibold"> {providerDetail?.language?.join(", ") || ""}</span>
        </p>

        <p className="flex-1 500px:text-nowrap">
          Response Time: <span className="font-semibold">40 mints</span>
        </p>

        <p className="flex-1 500px:text-nowrap">
          Type:{" "}
          <span className="font-semibold">
          {providerDetail?.professional_type || ""}
          </span>
        </p>
      </div> */}
      <Image
        src={providerDetail?.display_image || ""}
        alt="service img"
        width={500}
        height={500}
        style={{ width: "100%", maxHeight: "600px", objectFit: "fill" }}
      />
      <ListingButtons />
      {/* <div className="flex items-center gap-4 flex-wrap mt-4">
        <button className="btn flex-grow bg-bgBlue text-white">
          Description
        </button>
        <button className="btn flex-grow bg-bgBlue text-white">Services</button>
        <button className="btn flex-grow bg-bgBlue text-white">Q/A</button>
        <button className="btn flex-grow bg-bgBlue text-white">Reviews</button>
      </div> */}

      <div name="section1" className="element mt-4">
        <h1 className="text-[1.8rem] font-semibold mb-2">Description </h1>
        <p
          className="w-[90%] text-[#636363]"
          dangerouslySetInnerHTML={{ __html: providerDetail?.description }}
        >
          {/* {providerDetail?.description || ""} */}
        </p>
      </div>

      {/* <div className="mt-4">
        <h1 className="text-[1.8rem] font-semibold mb-2">Description </h1>
        <p className="w-[90%] text-[#636363]">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
          commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
          velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
          occaecat cupidatat non proident, sunt in culpa qui officia deserunt
          mollit anim id est laborum.
        </p>
      </div> */}
      <div name="section2" className="element mt-4">
        <h1 className="text-[1.8rem] font-semibold mb-2">Services </h1>
        <div className="p-4 flex flex-col gap-8">
          {servicesData?.provider_services?.map((item, key) => (
            <div
              className="w-full flex gap-4 p-8 shadow-loginShadow rounded-md flex-wrap 800px:flex-nowrap"
              key={key + 98 * 2}
            >
              <div className="min-w-[100px] min-h-[100px]">
                <Image
                  src={item?.image_url}
                  alt="img service"
                  width={100}
                  height={100}
                />
              </div>
              <div>
                <h1 className="text-xl font-semibold">{item?.title}</h1>
                <p
                  className="line-clamp-4 text-[8px] 500px:text-sm"
                  dangerouslySetInnerHTML={{ __html: item?.description }}
                ></p>
                <span className=" text-green-600 font-semibold">
                  Starting From:{" "}
                </span>
                <span className="font-semibold">
                  ${item?.starting_price} USD
                </span>
              </div>
            </div>
          ))}
        </div>
        {/* <div className="p-4 flex flex-col gap-8">
          {listingServiceData.map((item, key) => (
            <div
              className="w-full flex gap-4 p-8 shadow-xl rounded-md flex-wrap 800px:flex-nowrap"
              key={key + 98 * 2}
            >
              <Image src={item.image} alt="img service" />
              <div>
                <h1 className="text-xl font-semibold">{item.title}</h1>
                <p>{item.desc}</p>
                <span className=" text-green-600 font-semibold">
                  Starting From:{" "}
                </span>
                <span className="font-semibold">${item.price} USD</span>
              </div>
            </div>
          ))}
        </div> */}
      </div>
      <div name="section3" className="element mt-4 mb-4">
        <h1 className="text-[1.8rem] font-semibold mb-2">
          Frequently Asked Questions
        </h1>
        <p className="text-[#767676] mb-2">
          Diam Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Vestibulum dui nisi, porttior sit amet diam ut, congue fermentum dui.
          Aenean gravida p
        </p>
        {servicesfaqData?.FAQsdata?.map((item, index) => (
          <div
            key={index + 1 * 7}
            className="collapse collapse-plus bg-[#CCCCCC] text-black border mb-2 rounded-md"
          >
            <input type="radio" name="my-accordion-3" />
            <div className="collapse-title text-lg font-medium">
              {item?.question}
            </div>
            <div className="collapse-content bg-gray-100 text-black">
              <p>{item?.answer}</p>
            </div>
          </div>
        ))}
        {/* {accordionData.map((item, index) => (
          <div
            key={index + 1 * 7}
            className="collapse collapse-plus bg-[#CCCCCC] text-black border mb-2 rounded-md"
          >
            <input type="radio" name="my-accordion-3" />
            <div className="collapse-title text-lg font-medium">
              {item.title}
            </div>
            <div className="collapse-content bg-gray-100 text-black">
              <p>{item.message}</p>
            </div>
          </div>
        ))} */}
      </div>
      <div name="section4" className="element">
        <ListingDetailReview
          params={params}
          servicesReviewData={servicesReviewData}
        />
      </div>
      {/* <div className="flex items-center justify-between gap-4  mt-8 flex-wrap mb-4">
        <h1 className="text-3xl font-semibold   ">{servicesReviewData?.provider_services?.length} Reviews</h1>
        <div className="flex items-center gap-4 flex-wrap">
          <div className="dropdown dropdown-end   ">
            <div
              tabIndex={0}
              role="button"
              className="btn m-1 bg-white border border-gray-400"
            >
              Most Recent <FaChevronDown size={20} />
            </div>
            <ul
              tabIndex={0}
              className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
            >
              <li>
                <a>Item 1</a>
              </li>
              <li>
                <a>Item 2</a>
              </li>
            </ul>
          </div>
          <div className="dropdown dropdown-end     ">
            <div
              tabIndex={0}
              role="button"
              className="btn m-1 bg-white border border-gray-400"
            >
              Any Rating <FaChevronDown size={20} />
            </div>
            <ul
              tabIndex={0}
              className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
            >
              <li>
                <a>Item 1</a>
              </li>
              <li>
                <a>Item 2</a>
              </li>
            </ul>
          </div>
          <button className="bg-[#434CD9]   px-12 py-3 rounded-3xl text-white">
            Write A Review
          </button>
        </div>
      </div>
      <div className="w-full flex flex-col gap-4 ">
        {servicesReviewData?.provider_services?.map((item, key) => (
          <div
            key={key + 978 * 2}
            className="p-6 rounded-md  shadow-loginShadow "
          >
            <div className="flex items-center justify-between">
              <div className="mb-2 flex items-center gap-2 w-auto">
                <div className=" w-[45px] h-[45px] rounded-full overflow-hidden">
                  <Image
                    src={item?.image}
                    style={{ width: "100%", height: "100%" }}
                    alt="profile img"
                    className=" object-cover "
                    width={500}
                    height={500}
                  />
                </div>
                <div className="flex flex-col ">
                  <h1 className="font-semibold">{item.review_from}</h1>
                  <p className="text-sm">{item?.title}</p>
                </div>
              </div>
              <div className="flex items-center gap-2">
                <FaLinkedin
                  size={25}
                  fill="#0E76A8"
                />{" "}
                <h1 className="font-semibold">Verified</h1>
              </div>
            </div>
             <Rating
                  style={{ maxWidth: 150 }}
                  value={Number(Math.ceil(item?.rating))}
                  readonly
                  emptySymbol="far fa-star"
                  fullSymbol="fas fa-star"
                />
            <p className="mt-2 text-[#676767] mb-2">{item?.review}</p>
            <hr />
            <div className="flex items-center justify-between gap-2 600px:gap-0 mt-4 flex-wrap 600px:flex-nowrap text-nowrap">
              <div className="flex items-center gap-2 500px:gap-6 font-[600] flex-wrap 600px:flex-nowrap ">
                <h1>Helpful 
                </h1>
                <h1>Share</h1>
              </div>
              <div className="flex items-center gap-2">
                <SlFlag
                  fill="#f21a1a"
                  size={20}
                />
                <h1 className="text-[#f21a1a] font-semibold">Report</h1>
              </div>
            </div>
          </div>
        ))}
      </div>
      <ReviewModal /> */}
    </div>
  );
};

export default ListingDetail;
