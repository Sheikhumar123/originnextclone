"use client";
import React, { useState } from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import Image from "next/image";
import { HiOutlineUserGroup } from "react-icons/hi2";
import { IoIosHeartEmpty } from "react-icons/io";
import { FiThumbsUp } from "react-icons/fi";
import { BsFillLightningChargeFill } from "react-icons/bs";
import { LiaPhoneVolumeSolid } from "react-icons/lia";
import ratingImgCard from "../../images/ratingImgCard.png";
import { findFlagUrlByCountryName } from "country-flags-svg";

function SimilarServiceSlider({ SimilarServicesData }) {
  const [readmore, setReadmore] = useState(-1);

  console.log(SimilarServicesData);
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow:
      SimilarServicesData?.similar_services?.length <= 3
        ? SimilarServicesData?.similar_services?.length
        : 3,
    slidesToScroll: 1,
    // prevArrow: <SlickLeftArrow />,
    // nextArrow: <SlickRightArrow />,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 800,
        settings: {
          slidesToShow:
            SimilarServicesData?.similar_services?.length < 2
              ? SimilarServicesData?.similar_services?.length
              : 1,
          slidesToScroll: 1,
        },
      }, // 1 item on small screens

      {
        breakpoint: 1400,
        settings: {
          slidesToShow:
            SimilarServicesData?.similar_services?.length < 3
              ? SimilarServicesData?.similar_services?.length
              : 2,
          slidesToScroll: 1,
        },
      }, // 2 items on screens with breakpoint > 500px

      {
        breakpoint: 1800,
        settings: {
          slidesToShow:
            SimilarServicesData?.similar_services?.length < 4
              ? SimilarServicesData?.similar_services?.length
              : 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 2200,
        settings: {
          slidesToShow:
            SimilarServicesData?.similar_services?.length < 5
              ? SimilarServicesData?.similar_services?.length
              : 4,
          slidesToScroll: 1,
        },
      },
    ],
  };
  const randomBackgroundColors2 = [
    "bg-[#d8f8ff]",
    "bg-[#fad8ff]",
    "bg-[#feffd5]",
    "bg-[#fffff]",
  ];
  const randomBackgroundColors3 = [
    "border-[#a100d9]",
    "border-[#0057d9]",
    "border-[#ff6209]",
    "border-[#00d7be]",
    "border-[#e09400]",
  ];
  console.log(SimilarServicesData?.similar_services);
  return (
    <>
     
      <Slider  {...settings}>
        {SimilarServicesData?.similar_services?.map((service, index) => {

          return (
            <div key={service?.service_id} className="700px:px-6 300px:px-1 ">
              <div
                className={`text-sm cursor-pointer ${
                  randomBackgroundColors2[
                    Math.floor(Math.random() * randomBackgroundColors2.length)
                  ]
                } border border-l-[10px] ${
                  randomBackgroundColors3[
                    Math.floor(Math.random() * randomBackgroundColors3.length)
                  ]
                } hover:border-black w-full rounded-md shadow-lg hover:text-white hover:bg-black`}
                // className={`text-sm ${
                //   item?.sideBg ? `bg-${item?.sideBg}` : "bg-lime-500"
                // } pl-3  w-[400px] rounded-md shadow-lg`}
              >
                <div className={`flex flex-col p-[2px] bg-${service?.bg}`}>
                  <div className="flex items-center justify-between mb-[12px]">
                    <div className="flex gap-2">
                      <p className="px-2 text-sm bg-[#4d4d4d] text-white">
                        New
                      </p>

                      {/* {service?.new && (
                        <p className="px-1 text-sm rounded-md bg-red-500 text-white">
                          New
                        </p>
                      )} */}

                      <p className="px-2 text-sm  bg-[#4d4d4d] text-[#FAFF00]">
                        Pinned
                      </p>

                      {/* {service?.pinned && (
                        <p className="px-1 text-sm rounded-md bg-black text-yellow-500">
                          Pinned
                        </p>
                      )} */}

                      <p className="px-2 text-sm  bg-[#4d4d4d] text-orange-500">
                        Featured
                      </p>

                      {/* {service?.featured && (
                        <p className="px-1 text-sm rounded-md bg-black text-orange-500">
                          Featured
                        </p>
                      )} */}
                    </div>

                    <p className="px-2 text-sm  bg-blue-500 text-white">
                      Verified
                    </p>

                    {/* {service?.verified && (
                      <p className="px-1 text-sm rounded-md bg-blue-500 text-white">
                        Verified
                      </p>
                    )} */}

                    <p className="px-2 text-sm  bg-green-800 text-white">
                      Online
                    </p>

                    {/* {service?.online && (
                      <p className="px-1 text-sm rounded-md bg-green-800 text-white">
                        Online
                      </p>
                    )} */}
                  </div>
                  <div className="w-full flex  500px:flex-nowrap gap-4 relative overflow-hidden ">
                    {/* {service?.featured && ( */}
                    <div className="ribbon 1000px:block 800px:hidden 400px:block 300px:hidden absolute -bottom-[6.5rem] -right-[5rem] h-[10.5rem] w-40 overflow-hidden before:absolute before:top-0 before:right-0 before:-z-[10000] before:border-4 before:border-blue-500 after:absolute after:left-0 after:bottom-0 after:-z-[1] after:border-4 after:border-blue-500">
                      <div className="absolute -left-[4rem] top-[43px] w-60 -rotate-[40deg] bg-gradient-to-br from-yellow-600 via-yellow-400 to-yellow-500 py-2.5 text-center text-white shadow-md flex items-center justify-center">
                        <BsFillLightningChargeFill className="ml-7 mb-2" />
                      </div>
                    </div>
                    {/* // )} */}
                    <div className="min-w-[100px] h-[100px]">
                      <Image
                        src={service?.image_url}
                        width={100}
                        height={100}
                        alt="img service"
                        className="w-[100px] h-[100px]"
                      />
                    </div>
                    <div className="flex flex-col justify-between">
                      <h1 className="text-xl  font-bold hover:text-white">
                        {service?.title}
                      </h1>
                      <div className={`${readmore == service?.service_id ?"min-h-[100px]" :  "h-[100px]"}`}>
                      <p
                        className={`text-[8px] 500px:text-sm z-50 ${
                          readmore == service?.service_id ? "" : "line-clamp-4"
                        } `}
                        dangerouslySetInnerHTML={{ __html: service?.description }}
                      >
                        {/* {service?.description} */}
                      </p>
                      {service?.description?.length > 250 &&
                        readmore != service?.service_id && (
                          <div>
                            <span
                              className="text-blue-600 hover:underline hover:text-blue-700 cursor-pointer"
                              onClick={() => setReadmore(service?.service_id)}
                            >
                              {" "}
                              Read More
                            </span>
                          </div>
                        )}
                      {readmore == service?.service_id && (
                        <div>
                          <span
                            className="text-blue-600 hover:underline hover:text-blue-700 cursor-pointer"
                            onClick={() => setReadmore(-1)}
                          >
                            {" "}
                            Read less
                          </span>
                        </div>
                      )}
                      </div>
                      {/* <h1>
                        <span className=" text-purple-600 text-[10px] 500px:text-sm  font-bold">
                          Engagement Rate:{" "}
                        </span>
                        <span className="font-semibold text-[10px] 500px:text-sm ">
                        ${service?.starting_price} USD

                        </span>
                      </h1> */}
                      <h1>
                        <span className=" text-purple-600 text-[10px] 500px:text-sm  font-bold">
                        Starting from:{" "}
                        </span>
                        <span className="font-semibold text-[10px] 500px:text-sm ">
                        ${service?.starting_price}{" "}{service?.duration}
                        </span>
                      </h1>
                    </div>
                  </div>
                  <hr
                    className="border-none h-1  from-black via-black to-transparent bg-repeat-x-6 mt-[2px] mb-[1px]"
                    style={{ height: "1px" }}
                  />
                  <div className="flex items-center gap-1 justify-between  text-gray-500">
                  {
                      service?.country?
                    
                    <div className="flex h-[25px] gap-[3px] text-sm items-center">
                      <Image
                                src={service?.country?findFlagUrlByCountryName(service?.country):""}
                      
                        alt="image flag"
                        height={20}
                        width={20}
                      />
                      <p className="text-[6px] 500px:text-[11px] font-semibold">
                        {service?.country}
                      </p>
                    </div>
                    :
                    null}
                    {/* <div className="flex h-[25px] gap-[2px] text-sm items-center">
                      <Image
                        src={service?.flag}
                        alt="image flag"
                        height={20}
                        width={20}
                      />
                      <p className="text-[6px] 500px:text-[11px] font-semibold">
                        {service?.country}
                      </p>
                    </div> */}
                    <div className="flex h-[25px] gap-[2px] text-sm items-center">
                      <HiOutlineUserGroup color="#009999 " size={14} />
                      <p className="text-[8px] 500px:text-[11px] font-semibold">
                        {service?.orders}
                      </p>
                    </div>
                    <div className="flex  gap-1 text-sm items-center">
                      <Image src={ratingImgCard} alt="ratingImg" />
                      <p>{service?.rating}</p>
                    </div>
                    <div className="flex  gap-2 text-sm items-center">
                      <IoIosHeartEmpty size={14} color="#d71a1a" />
                      <FiThumbsUp size={14} />
                    </div>
                    <button className="bg-yellow-400 text-black px-2 py-1 rounded-3xl flex items-center gap-1 border-black border text-[11px] font-semibold">
                      <p className="text-[8px] 500px:text-[11px] text-nowrap">
                        {" "}
                        Contact Us{" "}
                      </p>
                      <LiaPhoneVolumeSolid size={10} />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </Slider>
    </>
  );
}

export default SimilarServiceSlider;
