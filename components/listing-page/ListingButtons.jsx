"use client";
import React from "react";
import { Link, animateScroll as scroll } from "react-scroll";

function ListingButtons() {
  return (
    <div className="flex w-full items-center gap-4 flex-wrap mt-4">
      <Link
        activeClass="active"
        to="section1"
        spy={true}
        smooth={true}
        offset={-150}
        duration={500}
        delay={100}
        className="w-full max-w-[310px]"
      >
        <button
          // onClick={() => scroll.scrollTo(800)}
          className="btn w-full flex-grow bg-bgBlue text-white"
        >
          Description
        </button>
      </Link>
      <Link
        activeClass="active"
        to="section2"
        spy={true}
        smooth={true}
        offset={-150}
        duration={500}
        delay={100}
        className="w-full max-w-[310px]"

      >
        <button
          // onClick={() => scroll.scrollTo(1000)}
          className="btn w-full flex-grow bg-bgBlue text-white"
        >
          Services
        </button>
      </Link>
      <Link
        activeClass="active"
        to="section3"
        spy={true}
        smooth={true}
        offset={-150}
        duration={500}
        delay={100}
        className="w-full max-w-[310px]"

      >
        <button
          // onClick={() => scroll.scrollTo(1800)}
          className="btn w-full flex-grow bg-bgBlue text-white"
        >
          Q/A
        </button>
      </Link>
      <Link
        activeClass="active"
        to="section4"
        spy={true}
        smooth={true}
        offset={-150}
        duration={500}
        delay={100}
        className="w-full max-w-[310px]"

      >
        <button
          // onClick={() => scroll.scrollTo(2600)}
          className="btn w-full flex-grow bg-bgBlue text-white"
        >
          Reviews
        </button>
      </Link>
    </div>
  );
}

export default ListingButtons;
