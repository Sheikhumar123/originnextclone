"use client";
import React, { useEffect, useState } from "react";

function ListingDetailCategories({ providerDetail }) {
  const [allCategories, setallCategories] = useState([]);
  const [allSubCategories, setallSubCategories] = useState([]);
  useEffect(() => {
    if (providerDetail) {
      let arr = [];
      let arr2 = [];

      providerDetail?.data?.categories?.map((item) => {
        arr.push({
          title: item?.category?.title,
        });
        arr2?.push(...item?.subcategory);
      });
      setallCategories(arr);
      setallSubCategories(arr2);
    }
  }, [providerDetail]);
  console.log(providerDetail);
  return (
    <div>
      {allCategories?.length ? (
        <div className="p-4 w-full flex flex-col items-start  border border-gray-200 rounded-2xl gap-2 mb-4">
          <h1 className="font-semibold text-lg">Categories</h1>
          <div className="flex gap-3 flex-wrap">
            {allCategories?.map((item, key) => (
              <h1
                className="py-2 px-4 border-2 bg-[#CCE0FF] text-[#444BDB] text-sm rounded-2xl text-nowrap"
                key={key + 22 * 8}
              >
                {item?.title}
              </h1>
            ))}
          </div>
        </div>
      ) : null}
      {allSubCategories?.length ? (
        <div className="p-4 w-full flex flex-col items-start  border border-gray-200 rounded-2xl gap-2">
          <h1 className="font-semibold text-lg">Sub Categories</h1>
          <div className="flex gap-3 flex-wrap">
            {allSubCategories?.map((item, key) => (
              <h1
                className="py-2 px-4 border-2 bg-[#CCE0FF] text-[#444BDB] text-sm rounded-2xl text-nowrap"
                key={key + 22 * 8}
              >
                {item?.title}
              </h1>
            ))}
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default ListingDetailCategories;
