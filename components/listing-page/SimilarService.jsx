
import React from "react";
import SimilarServiceSlider from "./SimilarServiceSlider";

function SimilarService({ SimilarServicesData }) {
  return (
    <>
      {/* <div className="w-full p-4 600px:p-8">
        <h1 className="text-[2rem] font-semibold">Recommended Services</h1> */}

        <div className="pb-16 w-full">
          <SimilarServiceSlider SimilarServicesData={SimilarServicesData} />
        </div>
     
      {/* </div> */}
    </>
  );
}

export default SimilarService;
