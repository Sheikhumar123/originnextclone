import { IoClose } from "react-icons/io5";
import PropTypes from "prop-types";

function AppDialog({ title, open, onClose, children }) {
  return open ? (
    <>
      <div className="justify-center px-2 items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-auto mx-auto max-w-4xl xs:min-w-[350px] md:min-w-[550px] max-h-[90%] overflow-y-auto border-0 rounded-lg shadow-lg bg-white outline-none focus:outline-none">
          {/*content*/}
          <div className="flex flex-col w-full">
            {/*header*/}
            <div className="flex justify-between items-center p-5">
              <h3 className="sm:text-xl 300px:text-lg font-semibold">{title}</h3>
              <button
                className="text-black outline-none focus:outline-none hover:text-gray-600 transition-colors"
                onClick={onClose}
              >
                <IoClose size={30} />
              </button>
            </div>

            {/*body*/}
            <div className="px-6 pb-6">{children}</div>
          </div>
        </div>
      </div>
      <div
        className="opacity-25 fixed inset-0 z-40 bg-black"
        onClick={onClose}
      ></div>
    </>
  ) : null;
}

export default AppDialog;
AppDialog.propTypes = {
  title: PropTypes.string,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  children: PropTypes.element,
};
