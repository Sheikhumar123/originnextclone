import { serviceCategories } from "@/data/homepageData";
import { baseUrl } from "@/utils/BaseURL";
import { manrope } from "@/utils/fontPoppins";
import Image from "next/image";
import Link from "next/link";

const ServicesProvider = async ({ ProviderCategories }) => {
  return (
    <div
      className={`p-8 flex flex-col items-center justify-center gap-4 ${manrope.className}`}
    >
      <div className="flex flex-col items-center justify-center gap-2 w-full 800px:w-1/2 mb-8">
        <h1 className="text-[2rem] 800px:text-[2.5rem] font-semibold">
          Service Provider
        </h1>
        <p className="text-xl text-[#5B5B5B]">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Similique
          placeat incidunt dicta iusto at, voluptatum tempora ipsam veniam aut
          laboriosam!
        </p>
      </div>
      <div className="flex w-full justify-center items-center">
        <div
          // className="flex items-center gap-6   flex-wrap w-full max-w-[1500px] "
          className="grid 1100px:grid-cols-4 900px:grid-cols-3 600px:grid-cols-2 300px:grid-cols-1 gap-4 w-full max-w-[1500px]"
        >
          {ProviderCategories?.categories?.map((item, key) => (
            <div
              key={item?.id}
              className="min-h-[337px] rounded-xl hover:rounded-xl w-full max-w-[350px] "
            >
              <div
                className="hover:pb-2 
            rounded-xl hover:border-b hover:rounded-xl bg-gradient-to-br from-indigo-500 to-teal-400"
              >
                {/* min-h-[300px]  */}
                {/* <div className="w-full 400px:w-[220px] 600px:w-[250px] p-6 flex flex-col gap-5 items-center justify-center border shadow-sm rounded-xl bg-white"> */}

                <div className="w-full  p-6 flex flex-col gap-5 items-center justify-center border shadow-sm rounded-xl bg-white">
                  <div className="w-[80px] h-[80px] flex items-center justify-center shadow-lg shadow-gray-300 rounded-full bg-gradient-to-br from-indigo-500 to-teal-400 ">
                    <Image
                      src={item?.icon}
                      alt="icon"
                      style={{ borderRadius: "50%" }}
                      width={80}
                      height={80}
                    />
                  </div>
                  <div className="flex flex-col items-center justify-center gap-2">
                    <h1
                      className="text-lg font-semibold
              "
                    >
                      {item?.title}
                    </h1>
                    <p className="text-center text-sm h-[80px] overflow-hidden">
                      {item?.tagLine}
                    </p>
                  </div>
                  <Link href={`/categories/${item?.id}`}>
                    <button
                      type="button"
                      className="flex-grow  bg-[#434CD9] w-auto  px-12 text-nowrap py-2 rounded-3xl text-white"
                    >
                      Learn More
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ServicesProvider;
