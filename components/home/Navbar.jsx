"use client";
import LogoImg from "../../images/logo.png";
import { RxHamburgerMenu } from "react-icons/rx";
import { useState } from "react";
import Link from "next/link";
import Image from "next/image";
import { subTypesData } from "@/data/homepageData";
const Navbar = () => {
  const [isMobile, setIsMobile] = useState(false);
  const [isOpen, setIsOpen] = useState(false);

  const toggleDropdown = () => {
    setIsOpen(true);
  };

  const closeDropdown = () => {
    setIsOpen(false);
  };
  const handleClose = (e) => {
    if (
      e.target.id === "screen" ||
      e.target.getAttribute("navitem") === "true"
    ) {
      setIsMobile(false);
    }
  };
  return (
    <>
      <div className="sticky top-0 z-[999]">
        <div
          className={`w-full bg-[#f4f9fd]  flex items-center justify-between p-4 600px:px-20 600px:py-2  border-b-4 border-black `}
        >
                   <Link
                  // className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                  href={"/home"}
                >
          <Image src={LogoImg} width={100} height={60} alt="Logo img" />
          </Link>
          <div className="flex items-center justify-between gap-6">
            <div className="hidden 1000px:flex items-center justify-between gap-1 font-semibold text-nowrap ">
              <Link
                className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                href={"/"}
              >
                Home
              </Link>
              <Link
                className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                href={"/find-a-job"}
              >
                Find A Job
              </Link>
              <Link
                className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                href={"/recruiters"}
                navitem="true"
                onClick={handleClose}
              >
                Recruiters
              </Link>
              <Link
                className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                href={"/candidates"}
              >
                Candidates
              </Link>
              <Link
                className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                href={"/home/listings"}
              >
                Services
                {/* Pages */}
              </Link>
            </div>
            <div className="flex gap-3 items-center">
              <div className="flex gap-2 items-center">
                <Link
                  // className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                  href={"/auth/login"}
                >
                  <button className="bg-[#434CD9] w-full   px-3   600px:px-12  py-2   600px:py-3 rounded-3xl text-nowrap text-white">
                    Log In
                  </button>
                </Link>
                <div className="dropdown dropdown-bottom dropdown-end">
              <div
                tabIndex={12}
                role="button"
                className="rounded-md flex items-center gap-2 "
                onClick={toggleDropdown}
              >
          <button className=" w-full 600px:w-auto 600px:block hidden p-[3px]   rounded-3xl text-nowrap text-black  bg-gradient-to-r from-indigo-500 via-indigo-500 to-green-400">
                    <div className="w-full 600px:w-auto h-full px-3   600px:px-12  py-2   600px:py-3  bg-white rounded-3xl">
                      <h1 className="text-bgBlue font-[700]">
                        Join As Professional
                      </h1>
                    </div>
                  </button>
              </div>
              {isOpen && (
                <ul
                  tabIndex={12}
                  className="dropdown-content z-[5] menu p-2 shadow bg-base-100 rounded-box w-52"
                >
                  <li onClick={closeDropdown}>
                    <Link href={"/service-provider/register"}>Service Provider</Link>
                  </li>
                  <li onClick={closeDropdown}>
                    <Link href={"/home"}>Influencer</Link>
                  </li>
                  <li onClick={closeDropdown}>
                    <Link href={"/home"}>Website & Tools</Link>
                  </li>
                  <li onClick={closeDropdown}>
                    <Link href={"/auth/investor-register"}>Investor</Link>
                  </li>
                </ul>
              )}
            </div>
                {/* <Link
                  href={"/service-provider/register"}
                >
                  <button className=" w-full 600px:w-auto 600px:block hidden p-[3px]   rounded-3xl text-nowrap text-black  bg-gradient-to-r from-indigo-500 via-indigo-500 to-green-400">
                    <div className="w-full 600px:w-auto h-full px-3   600px:px-12  py-2   600px:py-3  bg-white rounded-3xl">
                      <h1 className="text-bgBlue font-[700]">
                        Join As Professional
                      </h1>
                    </div>
                  </button>
                </Link> */}
              </div>

              <RxHamburgerMenu
                size={30}
                className=" 1000px:hidden hover:cursor-pointer"
                onClick={() => setIsMobile(true)}
              />
            </div>
            {isMobile && (
              <div
                className="fixed w-full h-screen top-0 left-0 z-[99999] dark:bg-[unset] bg-[#00000024]"
                onClick={handleClose}
                id="screen"
              >
                <div className="fixed w-[70%] z-[99999] h-screen p-8 bg-[#f4f9fd] dark:bg-black dark:bg-opacity-90 top-0 right-0">
                  <div className="flex flex-col gap-2 items-start justify-between">
                    <Image
                      src={LogoImg}
                      width={100}
                      height={60}
                      alt="Logo img"
                    />

                    <Link
                      className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                      href={"/"}
                      navitem="true"
                      onClick={handleClose}
                    >
                      Home
                    </Link>
                    <Link
                      className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                      href={"/find-a-job"}
                      navitem="true"
                      onClick={handleClose}
                    >
                      Find A Job
                    </Link>
                    <Link
                      className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                      href={"/recruiters"}
                      navitem="true"
                      onClick={handleClose}
                    >
                      Recruiters
                    </Link>
                    <Link
                      className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                      href={"/candidate"}
                      navitem="true"
                      onClick={handleClose}
                    >
                      Candidate
                    </Link>
                    <Link
                className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                href={"/home/listings"}
              >
                Services
                {/* Pages */}
              </Link>
                    {/* <Link
                      className="p-2 hover:bg-blue-400 hover:text-black hover:rounded-[8px] "
                      href={"/pages"}
                      navitem="true"
                      onClick={handleClose}
                    >
                      Pages
                    </Link> */}
                  </div>
                  <div className="mx-auto">
                    {subTypesData.map((item, key) => (
                             <Link key={(key*67)+19} href={item?.route}>
                             <div className="flex items-center justify-center gap-1 p-2 hover:bg-gray-300 rounded-md hover:cursor-pointer">
                             <Image
                               src={item.url}
                               alt={item.title}
                               height={20}
                               width={20}
                             />
                             <p>{item.title}</p>
                           </div>
                           </Link>
                      // <div className="dropdown" key={key}>
                      //   <div tabIndex={0} role="button">
                      //     <div className="flex items-center justify-center gap-1 p-2 hover:bg-gray-300 rounded-md hover:cursor-pointer">
                      //       <Image
                      //         src={item.url}
                      //         alt={item.title}
                      //         height={20}
                      //         width={20}
                      //       />
                      //       <p>{item.title}</p>
                      //     </div>
                      //   </div>
                      //   <ul
                      //     tabIndex={0}
                      //     className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-auto"
                      //   >
                      //     <li>
                      //       <a>Item 1</a>
                      //     </li>
                      //     <li>
                      //       <a>Item 2</a>
                      //     </li>
                      //   </ul>
                      // </div>
                    ))}
                  </div>
                  <div className="dropdown dropdown-bottom dropdown-end">
              <div
                tabIndex={12}
                role="button"
                className="rounded-md flex items-center gap-2 "
                onClick={toggleDropdown}
              >
          <button className=" w-full 600px:w-auto 600px:block hidden p-[3px]   rounded-3xl text-nowrap text-black  bg-gradient-to-r from-indigo-500 via-indigo-500 to-green-400">
                    <div className="w-full 600px:w-auto h-full px-3   600px:px-12  py-2   600px:py-3  bg-white rounded-3xl">
                      <h1 className="text-bgBlue font-[700]">
                        Join As Professional
                      </h1>
                    </div>
                  </button>
              </div>
              {isOpen && (
                <ul
                  tabIndex={12}
                  className="dropdown-content z-[5] menu p-2 shadow bg-base-100 rounded-box w-52"
                >
                  <li onClick={closeDropdown}>
                    <Link href={"/service-provider/register"}>Service Provider</Link>
                  </li>
                  <li onClick={closeDropdown}>
                    <Link href={"/home"}>Influencer</Link>
                  </li>
                  <li onClick={closeDropdown}>
                    <Link href={"/home"}>Website & Tools</Link>
                  </li>
                  <li onClick={closeDropdown}>
                    <Link href={"/auth/investor-register"}>Investor</Link>
                  </li>
                </ul>
              )}
            </div>
                  {/* <button className=" w-full 600px:hidden block p-[3px]   rounded-3xl text-nowrap text-black  bg-gradient-to-r from-indigo-500 via-indigo-500 to-green-400">
                    <div className="w-full h-full px-3   600px:px-12  py-2   600px:py-3  bg-white rounded-3xl ">
                      <h1 className="text-bgBlue font-[700]">
                        Join As Professional
                      </h1>
                    </div>
                  </button> */}
                  <br />
                  <div className="flex items-center justify-center">
                    <p className="px-2 text-[16px] pl-5 text-black dark:text-white">
                      Copyright © - Origin
                    </p>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
        <div className=" items-center justify-center w-full bg-[#EEEEEECC] hidden 1000px:flex text-nowrap">
          <div className="w-[80%] h-[50px] flex items-center gap-4 justify-center ">
            {subTypesData.map((item, key) => (
              <Link key={(key*67)+12} href={item?.route}>
                    <div className="flex items-center justify-center gap-1 p-2 hover:bg-gray-300 rounded-md hover:cursor-pointer">
                    <Image
                      src={item.url}
                      alt={item.title}
                      height={20}
                      width={20}
                    />
                    <p>{item.title}</p>
                  </div>
                  </Link>
              // <div className="dropdown" key={key}>
              //   <div tabIndex={0} role="button">
              //     <div className="flex items-center justify-center gap-1 p-2 hover:bg-gray-300 rounded-md hover:cursor-pointer">
              //       <Image
              //         src={item.url}
              //         alt={item.title}
              //         height={20}
              //         width={20}
              //       />
              //       <p>{item.title}</p>
              //     </div>
              //   </div>
              //   <ul
              //     tabIndex={0}
              //     className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
              //   >
              //     <li>
              //       <a>Item 1</a>
              //     </li>
              //     <li>
              //       <a>Item 2</a>
              //     </li>
              //   </ul>
              // </div>
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default Navbar;
