"use client";
import { categoriesData } from "@/data/homepageData";
import axiousInstance from "@/utils/axiousInstance";
import { manrope } from "@/utils/fontPoppins";
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify";
import ScreenLoader from "../ScreenLoader/ScreenLoader";
import Link from "next/link";

const WorkCategories = ({ AllCategoriesData }) => {
  const [allCategoriesData, setallCategoriesData] = useState([]);
  const [loader, setloader] = useState(false);
  const [page, setpage] = useState(1);
  const [count, setcount] = useState(0);
  const [limit, setlimit] = useState(1);
  useEffect(() => {
    if (AllCategoriesData) {
      setallCategoriesData(AllCategoriesData?.categories);
      setcount(AllCategoriesData?.totalCount);
      setlimit(AllCategoriesData?.limit);
    }
  }, [AllCategoriesData]);
  const handleLoadMore = async () => {
    setloader(true);

    try {
      const response = await axiousInstance.get(
        `landingpage/allcategorieslandingpg?page=${page + 1}`
      );
      setloader(false);
      if (response.data.status === "1") {
        setallCategoriesData((prevData) => [
          ...prevData,
          ...response?.data?.categories,
        ]);
        // setallCategoriesData(response?.data?.categories);
        setcount(response?.data?.totalCount);
        setlimit(response?.data?.limit);
      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error?.response?.data?.error);
    }
  };
  return (
    <>
      {loader && <ScreenLoader />}
      <div
        className={`p-8 flex flex-col 700px:flex-row items-start justify-start gap-4 ${manrope.className}`}
      >
        <div className="700px:max-w-[30%] 300px:max-w-[100%] ">
          <h1 className="text-sm text-[#0066FF]">Categories</h1>
          <h1 className="text-[2.5rem] font-semibold">
            Get work done in over 2700 different categories
          </h1>
        </div>
        <div className="700px:max-w-[70%] w-full 300px:max-w-[100%] flex flex-col items-center justify-start">
          <div 
          // className="flex items-center w-full flex-wrap gap-2 "
        className="grid 1200px:grid-cols-4 1000px:grid-cols-3 500px:grid-cols-2 300px:grid-cols-1 gap-2 w-full"

          >
            {allCategoriesData?.map((item, index) => (
                  <Link
                  key={item?.id} href={`/categories/${item?.id}`}>

              <div
                className="p-2 border rounded-md text-nowrap w-full overflow-hidden text-ellipsis hover:cursor-pointer hover:bg-bgBlue hover:text-white "
             
              >
                {item?.title}
              </div>
              </Link>
            ))}
          </div>
          {count > allCategoriesData?.length ? (
            <Link href={"/home/allcategories"}>
            <button
              type="button"
              // onClick={handleLoadMore}
              className=" bg-[#434CD9] w-auto  px-12 text-nowrap py-2 mt-4 rounded-3xl text-white"
            >
              View All
            </button>
            </Link>
          ) : null}
          {/* {count > allCategoriesData?.length ? (
            <button
              type="button"
              onClick={handleLoadMore}
              className=" bg-[#434CD9] w-auto  px-12 text-nowrap py-2 mt-4 rounded-3xl text-white"
            >
              Load More
            </button>
          ) : null} */}
        </div>

        <div className=""></div>
      </div>
    </>
  );
};

export default WorkCategories;
