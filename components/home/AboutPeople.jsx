import { manrope, poppins } from "@/utils/fontPoppins";
import React from "react";

const AboutPeople = () => {
  return (
    <div
      className={`${manrope.className} w-full flex items-center flex-wrap p-4 400px:p-8 gap-4 700px:flex-row flex-col`}
    >
      <div className="700px:w-[49%] 300px::w-[100%]">
      <h1 className=" text-[rgb(49,46,130)] text-[2rem] 400px:text-[3.7rem] font-semibold w-full max-w-[500px]">
        Its All{" "}
        <span className=" mr-1  w-[fit-content] relative">
          About
          <div className="w-[80%] absolute right-1 h-2 rounded-full bg-[#4ADE80]"></div>
        </span>{" "}
        The People{" "}
      </h1>
      </div>
      <div className="700px:w-[49%] 300px::w-[100%] flex-1 flex items-center justify-between gap-4 1000px:flex-nowrap flex-wrap p-2 ">
        <p
          className={`text-[#5F5D83] flex-grow  ${poppins.className} text-[#5F5D83] `}
        >
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat.
        </p>
      </div>
    </div>
  );
};

export default AboutPeople;
