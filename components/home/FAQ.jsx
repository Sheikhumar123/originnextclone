import { accordionData, influencerCategories } from "@/data/homepageData";
import { manrope } from "@/utils/fontPoppins";
import Image from "next/image";

const FAQ = ({ paddingFalse ,AllFaq}) => {
  return (
    <div
      className={`p-4 flex flex-col items-center justify-center gap-4 ${manrope.className} bg-gradient-to-r from-indigo-500 to-teal-400`}
    >
      <div className="flex flex-col items-center justify-center gap-2 w-full 800px:w-1/2 mb-8 text-white">
        <h1
          className={`text-[2rem] 800px:text-[2.5rem] font-semibold text-center ${
            paddingFalse ? "mt-12" : ""
          }`}
        >
          Frequently Asked Questions
        </h1>
        <p className="text-xl text-center font-[200]">
          diam Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Vestibulum dui nisi, porttior sit amet diam ut, congue fermentum dui.
          Aenean gravida p
        </p>
      </div>
      <div className="w-full 900px:w-[70%] p-4 600px:p-12 flex flex-col gap-4  ">
        {AllFaq?.data?.map((item, index) => (
          <div
            key={item?.user_id}
            className="collapse collapse-plus bg-white rounded-none"
          >
            <input type="radio" name="my-accordion-3" />
            <div className="collapse-title text-lg font-medium">
              {item?.question}
            </div>
            <div className="collapse-content">
              <p>{item?.answer}</p>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default FAQ;
