"use client";
import React, { useEffect } from "react";
import categoriesAds from "../../../../images/categoryAds.png";
import Image from "next/image";
import { BsFillLightningChargeFill } from "react-icons/bs";
import { FiThumbsUp } from "react-icons/fi";
import { HiOutlineUserGroup } from "react-icons/hi2";
import { IoIosHeartEmpty } from "react-icons/io";
import { LiaPhoneVolumeSolid } from "react-icons/lia";
import ratingImgCard from "../../../../images/ratingImgCard.png";
import { FaChevronDown } from "react-icons/fa6";
import {
  randomBackgroundColors,
} from "@/data/homepageData";
import { useState } from "react";
import { toast } from "react-toastify";
import Link from "next/link";

import { useRouter } from "next/navigation";
import axiousInstance from "@/utils/axiousInstance";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import { findFlagUrlByCountryName } from "country-flags-svg";

function CategoryOrServiceData({
  data,
  allCategories,
  searchParams,
  totalCount,
  Limit
}) {
  const router = useRouter();
  const [loader, setloader] = useState(false);
  const [selecetdCategories, setselecetdCategories] = useState([]);
  const [allCategorySevices, setallCategorySevices] = useState([]);
  const [allCategoriesData, setallCategoriesData] = useState([]);
  const [readmore, setReadmore] = useState(-1);
  const [page, setpage] = useState(1);
  const [page2, setpage2] = useState(1);
  const [count, setcount] = useState(0);
  const [TotalCount, setTotalCount] = useState(0);
  console.log(allCategorySevices);
  console.log(TotalCount);
  useEffect(() => {
    if (data) {
      setallCategorySevices(data);
      setTotalCount(totalCount);
    }
  }, [data]);
  useEffect(() => {
    if (allCategories?.allcategories) {
      setallCategoriesData(allCategories?.allcategories);
      setcount(allCategories?.totalCount);
    }
  }, [allCategories]);
  console.log(searchParams);
  console.log(selecetdCategories);
  const obj = {
    sort: "Rating",
  };

  const [[firstKey, firstValue]] = Object.entries(obj);

  console.log(`Name (Key): ${firstKey}`);
  console.log(`Value: ${firstValue}`);

  const handelClick = async (categoryId) => {
    setpage(1)
    setpage2(1)
    setselecetdCategories((prev) => {
      const isExist = prev.includes(categoryId);
      if (isExist) {
        return prev.filter((item) => item !== categoryId);
      } else {
  
        return [...prev, categoryId];
      }
    });

  };
  useEffect(() => {
    // if (selecetdCategories?.length > 0) {
    // }
    getSelectedCategoryServices();
  }, [selecetdCategories]);
  //   useEffect(() => {
  //     if (page > 1) {
  //       getMoreCategories();
  //     }
  //   }, [page]);
  const getSelectedCategoryServices = async () => {
    setloader(true);

    if (Object.keys(searchParams)?.length != 0) {
      const [[firstKey, firstValue]] = Object?.entries(searchParams);
    setloader(false);
    setpage(1)
    setpage2(1)
      router.push(
        `/home/listings?${firstKey}=${firstValue}&categories=${JSON.stringify(
          selecetdCategories
        )}`
      );
    } else {
      try {
        const response = await axiousInstance.post(
          "landingpage/categoprywiseservices",
          JSON.stringify({
            ...searchParams,
            page:1,
            categories:
              selecetdCategories?.length > 0 ? selecetdCategories : [],
          })
        );
        setloader(false);
        if (response.data.status === "1") {
          console.log(response.data);
          setallCategorySevices(response.data?.data);
          toast.success(response.data.message);
        } else {
          setloader(false);
          toast.error(response.data.message);
        }
      } catch (error) {
        setloader(false);
        toast.error(error.response?.data?.error);
      }
    }
  };
  const getMoreCategories = async () => {
    setloader(true);

    try {
      const response = await axiousInstance.get(
        `landingpage/servicescategorylandingpg?page=${page}`
      );
      setloader(false);
      if (response.data.status === "1") {
        console.log(response.data);
        setallCategoriesData((prevData) => [
          ...prevData,
          ...response?.data?.allcategories,
        ]);
        setcount(response.data?.totalCount);
        toast.success(response.data.message);
      } else {
        setloader(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error.response?.data?.error);
    }
  };
  const getAllCategoryServices = async (pagecount) => {
    setloader(true);
    try {
      const response = await axiousInstance.post(
        "landingpage/categoprywiseservices",
        JSON.stringify({
          page: 1,
        })
      );
      setloader(false);
      if (response.data.status === "1") {
        setallCategorySevices(response.data?.data);
        setTotalCount(response.data?.count);
        toast.success(response.data.message);
      } else {
        setloader(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error.response?.data?.error);
    }
  };
  const getMoreAllCategoryServices = async (pagecount) => {
    setloader(true);
    try {
      const response = await axiousInstance.post(
        "landingpage/categoprywiseservices",
        JSON.stringify({
          page: page2 ? page2 : 1,
        })
      );
      setloader(false);
      if (response.data.status === "1") {
        setallCategorySevices((prevData) => [
          ...prevData,
          ...response?.data?.data,
        ]);
        // setallCategorySevices(response.data?.data);
        setTotalCount(response.data?.count);
        toast.success(response.data.message);
      } else {
        setloader(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error.response?.data?.error);
    }
  };
  const randomBackgroundColors2 = [
    "bg-[#d8f8ff]",
    "bg-[#fad8ff]",
    "bg-[#feffd5]",
    "bg-[#fffff]",
  ];
  const randomBackgroundColors3 = [
    "border-[#a100d9]",
    "border-[#0057d9]",
    "border-[#ff6209]",
    "border-[#00d7be]",
    "border-[#e09400]",
  ];
  useEffect(()=>{
if(page2>1){
  getMoreAllCategoryServices()
}
  },[page2])
  useEffect(()=>{
if(page>1){
  getMoreCategories()
}
  },[page])
  return (
    <div>
      {loader && <ScreenLoader />}
      <div className="flex items-center justify-center gap-4 flex-wrap  mb-4 w-full">
        <Link href={"/home/listings"}>
          <button
            type="button"
            onClick={() => {
              setselecetdCategories([])
              getAllCategoryServices();
            }}
            className=" px-6 py-2 rounded-md text-center text-nowrap font-[600] border border-gray-800"
          >
            Show All
          </button>
        </Link>
        {allCategoriesData?.map((item, index) => {
          const isSelected = selecetdCategories?.includes(item?.id);
          
          return (
            <button
              key={index + 67 * 2}
              className={`${
                isSelected ? "bg-[black]" :randomBackgroundColors[Math.floor(Math.random() * 8)]
              } px-2  rounded-md text-center text-nowrap font-[600] border ${
                isSelected ? "text-white" :"text-white"
              } border-gray-800 h-9 cursor-pointer `}
              onClick={() => {
                handelClick(item.id);
              }}
            >
              {item?.title}
            </button>
          );
        })}
        {count > allCategoriesData?.length ? (
          <button
            type="button"
            onClick={() => {
              setpage(page+1)
              // getMoreCategories();
            }}
            className="bg-[#e5dddd] px-6 py-2 rounded-md text-center text-nowrap font-[600] border border-gray-800"
          >
            Show More
          </button>
        ) : null}
      </div>

      <div
        //   className="flex gap-2 flex-wrap mb-4 w-auto justify-center"
        className="grid 2000px:grid-cols-4  1100px:grid-cols-3 800px:grid-cols-2 300px:grid-cols-1 gap-4 500px:w-full 300px:w-[fit-content]  "
      >
        {allCategorySevices?.map((category, key) => (
          <div
            key={category?._id}
            className="flex flex-col flex-grow 800px:flex-grow-0 items-center gap-4"
          >
            <h1 className="text-[1.8rem] font-semibold 900px:text-center">
              {category?.category?.title}
            </h1>
            {category?.services.map((service, index) => (
              // w-[325px] 400px:w-[400px]
              <div
              key={service?.service_id}

              onClick={(e)=>{
                e.stopPropagation()
                router.push(`/home/listings/listingdetail/${service?.user_id}`)
              }}
                className={`text-sm cursor-pointer ${
                  randomBackgroundColors2[
                    Math.floor(Math.random() * randomBackgroundColors2.length)
                  ]
                } border border-l-[10px] ${
                  randomBackgroundColors3[
                    Math.floor(Math.random() * randomBackgroundColors3.length)
                  ]
                } hover:border-black w-full rounded-md shadow-lg hover:text-white hover:bg-black`}
              >
                <div className={`flex flex-col p-[2px] bg-${service?.bg}`}>
                  <div className="flex items-center justify-between flex-wrap gap-1 mb-[12px]">
                    <div className="flex gap-2">
                      <p className="px-2 text-sm bg-[#4d4d4d] text-white">
                        New
                      </p>

                      {/* {service?.new && (
                        <p className="px-1 text-sm rounded-md bg-red-500 text-white">
                          New
                        </p>
                      )} */}

                      <p className="px-2 text-sm  bg-[#4d4d4d] text-[#FAFF00]">
                        Pinned
                      </p>

                      {/* {service?.pinned && (
                        <p className="px-1 text-sm rounded-md bg-black text-yellow-500">
                          Pinned
                        </p>
                      )} */}

                      <p className="px-2 text-sm  bg-[#4d4d4d] text-orange-500">
                        Featured
                      </p>

                      {/* {service?.featured && (
                        <p className="px-1 text-sm rounded-md bg-black text-orange-500">
                          Featured
                        </p>
                      )} */}
                    </div>

                    <p className="px-2 text-sm  bg-blue-500 text-white">
                      Verified
                    </p>

                    {/* {service?.verified && (
                      <p className="px-1 text-sm rounded-md bg-blue-500 text-white">
                        Verified
                      </p>
                    )} */}

                    <p className="px-2 text-sm  bg-green-800 text-white">
                      Online
                    </p>

                    {/* {service?.online && (
                      <p className="px-1 text-sm rounded-md bg-green-800 text-white">
                        Online
                      </p>
                    )} */}
                  </div>
                  <div className="w-full flex  500px:flex-nowrap gap-4 relative overflow-hidden ">
                    {/* {service?.featured && ( */}
                    <div className="ribbon 500px:block 300px:hidden absolute -bottom-[6.5rem] -right-[5rem] h-[10.5rem] w-40 overflow-hidden before:absolute before:top-0 before:right-0 before:-z-[10000] before:border-4 before:border-blue-500 after:absolute after:left-0 after:bottom-0 after:-z-[1] after:border-4 after:border-blue-500">
                      <div className="absolute -left-[4rem] top-[43px] w-60 -rotate-[40deg] bg-gradient-to-br from-yellow-600 via-yellow-400 to-yellow-500 py-2.5 text-center text-white shadow-md flex items-center justify-center">
                        <BsFillLightningChargeFill className="ml-7 mb-2" />
                      </div>
                    </div>
                    {/* // )} */}
                    <div className="min-w-[100px] h-[100px]">
                      <Image
                        src={service?.image_url}
                        width={100}
                        height={100}
                        alt="img service"
                        className="w-[100px] h-[100px]"
                      />
                    </div>
                    <div className="flex flex-col justify-between">
                      <h1 className="text-xl  font-bold hover:text-white">
                        {service?.title}
                      </h1>
                      <p
                        className={`text-[8px] 500px:text-sm z-50 ${
                          readmore == service?.service_id ? "" : "line-clamp-4"
                        }`}
                        dangerouslySetInnerHTML={{ __html: service?.description }}
                      >
                        {/* {service?.description} */}
                      </p>
                      {service?.description?.length > 350 &&
                        readmore != service?.service_id && (
                          <div>
                            <span
                              className="text-blue-600 hover:underline hover:text-blue-700 cursor-pointer"
                              onClick={(e) => {
                                e.stopPropagation()
                                setReadmore(service?.service_id)}}
                            >
                              {" "}
                              Read More
                            </span>
                          </div>
                        )}
                      {readmore == service?.service_id && (
                        <div>
                          <span
                            className="text-blue-600 hover:underline hover:text-blue-700 cursor-pointer"
                            onClick={(e) => {
                              e.stopPropagation()
                              setReadmore(-1)}}
                          >
                            {" "}
                            Read less
                          </span>
                        </div>
                      )}
                      <h1>
                        <span className=" text-purple-600 text-[10px] 500px:text-sm  font-bold">
                        Starting from:{" "}
                        </span>
                        <span className="font-semibold text-[10px] 500px:text-sm ">
                        ${service?.starting_price}{" "}{service?.duration}
           
                        </span>
                      </h1>
                    </div>
                  </div>
                  <hr
                    className="border-none h-1  from-black via-black to-transparent bg-repeat-x-6 mt-[2px] mb-[1px]"
                    style={{ height: "1px" }}
                  />
                  <div className="flex items-center gap-1 justify-between  text-gray-500">
                    {
                      service?.country?
                    
                    <div className="flex h-[25px] gap-[3px] text-sm items-center">
                      <Image
                                src={service?.country?findFlagUrlByCountryName(service?.country):""}
                      
                        alt="image flag"
                        height={20}
                        width={20}
                      />
                      <p className="text-[6px] 500px:text-[11px] font-semibold">
                        {service?.country}
                      </p>
                    </div>
                    :
                    null}
                    <div className="flex h-[25px] gap-[2px] text-sm items-center">
                      <HiOutlineUserGroup color="#009999 " size={14} />
                      <p className="text-[8px] 500px:text-[11px] font-semibold">
                        {service?.orders}
                      </p>
                    </div>
                    <div className="flex  gap-1 text-sm items-center">
                      <Image src={ratingImgCard} alt="ratingImg" />
                      <p>{service?.rating}</p>
                    </div>
                    <div className="flex  gap-2 text-sm items-center">
                      <IoIosHeartEmpty size={14} color="#d71a1a" />
                      <FiThumbsUp size={14} />
                    </div>
                    <button className="bg-yellow-400 text-black px-2 py-1 rounded-3xl flex items-center gap-1 border-black border text-[11px] font-semibold">
                      <p className="text-[8px] 500px:text-[11px] text-nowrap">
                        {" "}
                        Contact Us{" "}
                      </p>
                      <LiaPhoneVolumeSolid size={10} />
                    </button>
                  </div>
                </div>
              </div>
            ))}
            {/* Adding "ADS" elements */}
            {[...Array(4 - category?.services?.length)].map((_, idx) => (
              <div
                key={idx}
                className={`bg-lime-500 pl-3   rounded-md shadow-lg`}
              >
                <div className={`flex flex-col gap-2 p-2 bg-white`}>
                  {/* 400px:!max-w-[384px] !w-[325px] */}
                  <Image
                    src={categoriesAds}
                    alt="ads image"
                    width={450}
                    height={180}
                    className="w-[450px]"
                  />
                  {/* Add other elements or styling for ADS as needed */}
                </div>
              </div>
            ))}
          </div>
        ))}
      </div>
      {TotalCount > allCategorySevices?.length  ? (
        <div className="mt-6">
        <button
          type="button"
          onClick={() => {
            setpage2(page2+1)
            // getAllCategoryServices(page2 + 1);
          }}
          className="btn  mx-auto flex  gap-2"
        >
          <p>Show More</p>{" "}
          <FaChevronDown size={20} className="animate-bounce" />
        </button>
        </div>
      ) : null}
    </div>
  );
}

export default CategoryOrServiceData;
