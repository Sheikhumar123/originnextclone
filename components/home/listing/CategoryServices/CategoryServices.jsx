"use client";
import React from "react";
import categoriesAds from "../../../../images/categoryAds.png";
import Image from "next/image";
import { BsFillLightningChargeFill } from "react-icons/bs";
import { FiThumbsUp } from "react-icons/fi";
import { HiOutlineUserGroup } from "react-icons/hi2";
import { IoIosHeartEmpty } from "react-icons/io";
import { LiaPhoneVolumeSolid } from "react-icons/lia";
import ratingImgCard from "../../../../images/ratingImgCard.png";
const CategoryServices = ({ data }) => {
  return (
    <div className="flex gap-2 flex-wrap mb-4 w-auto justify-center">
      {data?.map((category, key) => (
        <div
          key={key + 232 * 7}
          className="flex flex-col flex-grow 800px:flex-grow-0 items-center gap-4"
        >
          <h1 className="text-[1.8rem] font-semibold 900px:text-center">
            {category?.category?.title}
          </h1>
          {category?.services.map((service, index) => (
            <div
              className={`text-sm ${
                service?.sideBg ? `bg-${service?.sideBg}` : "bg-lime-500"
              } pl-3 w-[325px] 400px:w-[400px] rounded-md shadow-lg`}
              key={9889 + index * 3}
            >
              <div className={`flex flex-col p-[2px] bg-${service?.bg}`}>
                <div className="flex items-center justify-between mb-[2px]">
                  <div className="flex gap-2">
                    {service?.new && (
                      <p className="px-1 text-sm rounded-md bg-red-500 text-white">
                        New
                      </p>
                    )}
                    {service?.pinned && (
                      <p className="px-1 text-sm rounded-md bg-black text-yellow-500">
                        Pinned
                      </p>
                    )}
                    {service?.featured && (
                      <p className="px-1 text-sm rounded-md bg-black text-orange-500">
                        Featured
                      </p>
                    )}
                  </div>
                  {service?.verified && (
                    <p className="px-1 text-sm rounded-md bg-blue-500 text-white">
                      Verified
                    </p>
                  )}
                  {service?.online && (
                    <p className="px-1 text-sm rounded-md bg-green-800 text-white">
                      Online
                    </p>
                  )}
                </div>
                <div className="w-full flex  500px:flex-nowrap gap-4 relative overflow-hidden ">
                  {service?.featured && (
                    <div className="ribbon absolute -bottom-[6.5rem] -right-[5rem] h-[10.5rem] w-40 overflow-hidden before:absolute before:top-0 before:right-0 before:-z-[10000] before:border-4 before:border-blue-500 after:absolute after:left-0 after:bottom-0 after:-z-[1] after:border-4 after:border-blue-500">
                      <div className="absolute -left-[4rem] top-[43px] w-60 -rotate-[40deg] bg-gradient-to-br from-yellow-600 via-yellow-400 to-yellow-500 py-2.5 text-center text-white shadow-md flex items-center justify-center">
                        <BsFillLightningChargeFill className="ml-7 mb-2" />
                      </div>
                    </div>
                  )}
                  <Image
                    src={service?.image_url}
                    width={100}
                    height={100}
                    alt="img service"
                    className="w-[100px] h-[100px]"
                  />
                  <div className="flex flex-col justify-between">
                    <h1 className="text-sm font-bold">{service?.title}</h1>
                    <p className=" text-[8px] 500px:text-sm z-50">
                      {service?.desc}
                    </p>
                    <h1>
                      <span className=" text-purple-600 text-[10px] 500px:text-sm  font-bold">
                        Engagement Rate:{" "}
                      </span>
                      <span className="font-semibold text-[10px] 500px:text-sm ">
                        {service?.engagementRate}
                      </span>
                    </h1>
                  </div>
                </div>
                <hr
                  className="border-none h-1  from-black via-black to-transparent bg-repeat-x-6 mt-[2px] mb-[1px]"
                  style={{ height: "1px" }}
                />
                <div className="flex items-center gap-1 justify-between  text-gray-500">
                  <div className="flex h-[25px] gap-[2px] text-sm items-center">
                    <Image
                      src={service?.flag}
                      alt="image flag"
                      height={20}
                      width={20}
                    />
                    <p className="text-[6px] 500px:text-[11px] font-semibold">
                      {service?.country}
                    </p>
                  </div>
                  <div className="flex h-[25px] gap-[2px] text-sm items-center">
                    <HiOutlineUserGroup color="#009999 " size={14} />
                    <p className="text-[8px] 500px:text-[11px] font-semibold">
                      {service?.orders}
                    </p>
                  </div>
                  <div className="flex  gap-1 text-sm items-center">
                    <Image src={ratingImgCard} alt="ratingImg" />
                    <p>{service?.rating}</p>
                  </div>
                  <div className="flex  gap-2 text-sm items-center">
                    <IoIosHeartEmpty size={14} color="#d71a1a" />
                    <FiThumbsUp size={14} />
                  </div>
                  <button className="bg-yellow-400 text-black px-2 py-1 rounded-3xl flex items-center gap-1 border-black border text-[11px] font-semibold">
                    <p className="text-[8px] 500px:text-[11px] text-nowrap">
                      {" "}
                      Contact Us{" "}
                    </p>
                    <LiaPhoneVolumeSolid size={10} />
                  </button>
                </div>
              </div>
            </div>
          ))}
          {/* Adding "ADS" elements */}
          {[...Array(4 - category?.services?.length)].map((_, idx) => (
            <div
              key={idx}
              className={`bg-lime-500 pl-3   rounded-md shadow-lg`}
            >
              <div className={`flex flex-col gap-2 p-2 bg-white`}>
                <Image
                  src={categoriesAds}
                  alt="ads image"
                  width={384}
                  height={180}
                  className="400px:!max-w-[384px] !w-[325px]"
                />
                {/* Add other elements or styling for ADS as needed */}
              </div>
            </div>
          ))}
        </div>
      ))}
    </div>
  );
};

export default CategoryServices;
