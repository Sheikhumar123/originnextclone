"use client";
import React from "react";
import { randomBackgroundColors } from "@/data/homepageData";
import { useState } from "react";
import { baseUrl } from "@/utils/BaseURL";
import { useRouter } from "next/navigation";

const CategoriesSection = ({ allCategories, searchParams }) => {
  const router = useRouter();
  const [isShowAll, setisShowAll] = useState(false);
  const [selecetdCategories, setselecetdCategories] = useState([]);
  // const queryString = Object.entries(searchParams)
  //   .map(
  //     ([key, value]) =>
  //       `${encodeURIComponent(key)}=${encodeURIComponent(value)}`
  //   )
  //   .join("&");
  console.log(searchParams);

  const handelClick = async (categoryId) => {
    setselecetdCategories((prev) => {
      const isExist = prev.includes(categoryId);
      if (isExist) {
        const urlParams = new URLSearchParams(window.location.search);
  
        // Add or update the 'categories' parameter
        urlParams.set('categories', JSON.stringify((prev.filter(
          (item) => item !== categoryId
        ))));
      
        // Construct the new query string
        const queryString = urlParams.toString();
        router.push(`/home/listings?${queryString}`);
        // router.push(
        //   `/home/listings?${{...searchParams}}&categories=${JSON.stringify((prev.filter(
        //     (item) => item !== categoryId
        //   )))}`
        // );

        return prev.filter((item) => item !== categoryId);
      } else {
        router.push(
          `/home/listings?${{...searchParams}}&categories=${JSON.stringify([...prev, categoryId])}`
        );

        return [...prev, categoryId];
      }
    });
    // const response = await fetch(
    //   `${baseUrl}landingpage/categoprywiseservices`,
    //   {
    //     method: "POST",
    //     body: JSON.stringify({
    //       ...searchParams,
    //       categories: selecetdCategories,
    //     }),
    //     headers: {
    //       "Content-Type": "application/json",
    //     },
    //     cache: "no-store",
    //   }
    // );
  };
  // const handelClick = (categoryId) => {
  //   return setselecetdCategories((prev) => {
  //     const isExist = prev.includes(categoryId);
  //     if (isExist) {
  //       return prev.filter((item) => item !== categoryId);
  //     } else {
  //       return [...prev, categoryId];
  //     }
  //   });
  // };
  return (
    <div className="flex items-center justify-center gap-4 flex-wrap  mb-4 w-full">
      {allCategories?.map((item, index) => (
        <button
          key={index + 67 * 2}
          className={`${
            randomBackgroundColors[Math.floor(Math.random() * 8)]
          } px-6 py-2 rounded-md text-center text-nowrap font-[600] border border-gray-800 h-11 cursor-pointer `}
          onClick={() => {
            handelClick(item.id);

            // handelClick(item.id);
            // router.push(
            //   `/home/listings?${queryString}&categories=${selecetdCategories}`
            // );
          }}
        >
          {item?.title}
        </button>
      ))}
      <button className="bg-gray-300 px-6 py-2 rounded-md text-center text-nowrap font-[600] border border-gray-800">
        Show All
      </button>
    </div>
  );
};

export default CategoriesSection;
