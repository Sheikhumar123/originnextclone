"use client";
import { filterDropdownsListing } from "@/data/homepageData";
import { useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";
import { Country } from "country-state-city";
import { AllLanguage } from "@/components/Language/Language";

const Filters = ({ searchParams }) => {
  const router = useRouter();
  const [allcountries, setallcountries] = useState([]);
  console.log(searchParams);
  const handelFilterChange = (name, value) => {
    console.log(name, value);
    router.push(
      `/home/listings?${name}=${value}${
        searchParams?.categories?`&categories=${searchParams?.categories}`:""
      }`
    );
  };

  useEffect(() => {
    let value = Country.getAllCountries()?.map((data) => {
      let obj = {
        label: data?.name,
        value: data,
      };
      return obj;
    });
    setallcountries(value);
  }, []);
  return (
    <div className="flex items-center gap-4 flex-wrap justify-between w-full mb-4 ">
      {/* {filterDropdownsListing.map((item, key) => (
        <div
          className="dropdown dropdown-bottom flex-grow "
          key={key + 6985 * 2}
        >
          <select
            name=""
            id=""
            className="w-full min-w-[200px] bg-[#bdabab66] p-3 rounded-md border border-gray-400"
            onChange={(e) => {
              handelFilterChange(item?.title, e.target.value);
            }}
          >
            <option value="">{item?.title}</option>
            {item.options.map((subItem, key) => (
              <option value={subItem} key={key + 4552 * 98}>
                {subItem}
              </option>
            ))}
          </select>
        </div>
      ))} */}
      <div className="dropdown dropdown-bottom flex-grow ">
        <select
          name=""
          id=""
          className="w-full min-w-[200px] bg-[#bdabab66] p-2 rounded-md border border-gray-400"
          onChange={(e) => {
            handelFilterChange("country", e.target.value);
          }}
          value={searchParams?.country || ""}
        >
          <option value="">Country</option>
          {allcountries.map((country) => {
            return <option value={country?.label}>{country?.label}</option>;
          })}
        </select>
      </div>
      <div className="dropdown dropdown-bottom flex-grow ">
        <select
          name=""
          id=""
          className="w-full min-w-[200px] bg-[#bdabab66] p-2 rounded-md border border-gray-400"
          onChange={(e) => {
            handelFilterChange("language", e.target.value);
          }}
          value={searchParams?.language || ""}
        >
          <option value="">Language</option>
          {AllLanguage.map((language) => {
            return <option value={language}>{language}</option>;
          })}
        </select>
      </div>
      <div className="dropdown dropdown-bottom flex-grow ">
        <select
          name=""
          id=""
          className="w-full min-w-[200px] bg-[#bdabab66] p-2 rounded-md border border-gray-400"
          onChange={(e) => {
            handelFilterChange("rating", e.target.value);
          }}
          value={searchParams?.rating || ""}
        >
          <option value="">Ratings</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
      </div>
      <div className="dropdown dropdown-bottom flex-grow ">
        <select
          name=""
          id=""
          className="w-full min-w-[200px] bg-[#bdabab66] p-2 rounded-md border border-gray-400"
          onChange={(e) => {
            handelFilterChange("budget", e.target.value);
          }}
          value={searchParams?.budget || ""}
        >
          <option value="">Budget</option>
          <option value="[0 , 1000]">less than 1000</option>
          <option value="[1001 , 5000]">1001 to 5000</option>
          <option value="[5001 , 10000]">5001 to 10000</option>
          <option value="[10001 , 15000]">10001 to 15000</option>
          <option value="[15001 , 20000]">15001 to 20000</option>
        </select>
      </div>
      <div className="dropdown dropdown-bottom flex-grow ">
        <select
          name=""
          id=""
          className="w-full min-w-[200px] bg-[#bdabab66] p-2 rounded-md border border-gray-400"
          onChange={(e) => {
            handelFilterChange("status", e.target.value);
          }}
          value={searchParams?.status || ""}
        >
          <option value="">Status</option>
          <option value="Active">Active</option>
          <option value="Inactive">Inactive</option>
        </select>
      </div>
      <div className="dropdown dropdown-bottom flex-grow ">
        <select
          name=""
          id=""
          className="w-full min-w-[200px] bg-[#bdabab66] p-2 rounded-md border border-gray-400"
          onChange={(e) => {
            handelFilterChange("sort", e.target.value);
          }}
          value={searchParams?.sort || ""}
        >
          <option value="">Sort By</option>

          <option value="Rating">Rating</option>
          <option value="Followers">Followers</option>
          <option value="Latest">Latest</option>
        </select>
      </div>
    </div>
  );
};

export default Filters;
