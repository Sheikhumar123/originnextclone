import Image from "next/image";
import React from "react";
import BannerAdsImg from "../../images/BannerAdsImg.png";

const AdsImage = () => {
  return (
    <div className="600px:p-4 rounded-md p-2 flex items-center justify-center overflow-hidden mb-3 ">
      <Image
        src={BannerAdsImg}
        alt="Banner Image"
        placeholder="blur"
        priority={true}
      />
    </div>
  );
};

export default AdsImage;
