import { influencerCategories } from "@/data/homepageData";
import { manrope } from "@/utils/fontPoppins";
import Image from "next/image";

const Influencers = ({ AllPlatform }) => {
  return (
    <div
      className={`p-8 flex flex-col items-center justify-center gap-4 ${manrope.className}`}
    >
      <div className="flex flex-col items-center justify-center gap-2 w-full 800px:w-1/2 mb-8">
        <h1 className="text-[2rem] 800px:text-[2.5rem] font-semibold">
          Influencers{" "}
        </h1>
        <p className="text-xl text-[#5B5B5B]">
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry.
        </p>
      </div>
      <div className="flex w-full justify-center items-center">
        {/* <div className="flex items-center gap-6 flex-wrap justify-center"> */}
        {/* <div className="flex items-center gap-6  justify-start flex-wrap w-full"> */}
        <div 
        // className="flex items-center gap-6   flex-wrap w-full max-w-[1500px] "
        className="grid 1100px:grid-cols-4 900px:grid-cols-3 600px:grid-cols-2 300px:grid-cols-1 gap-4 w-full max-w-[1500px]"

        >
          {AllPlatform?.socialplatforms?.map((item, key) => (
            <div
              key={key * 8 + 1}
              className="min-h-[337px] rounded-xl hover:rounded-xl w-full max-w-[350px] "
            >
              <div className="w-full min-h-[300px] p-6 flex flex-col gap-5 items-center justify-center border shadow-sm rounded-xl bg-white hover:shadow-loadmore">
                
                <div className="w-[80px] h-[80px]">
                  <Image
                    src={item?.icon}
                    alt="icon"
                    // style={{ borderRadius: "50%" }}
                    width={80}
                    height={80}
                    // width={55} height={55}
                  />
                </div>
                <div className="flex flex-col items-center justify-center gap-2">
                  <h1
                    className="text-lg font-semibold
              "
                  >
                    {item?.title}
                  </h1>
                  {/* <p className="text-center text-sm"> */}
                  <p className="text-center text-sm h-[80px] overflow-hidden">
                    {item?.tagLine}
                  </p>
                </div>
                <button className="flex-grow  bg-[#434CD9] w-auto  px-12 text-nowrap py-2 rounded-3xl text-white">
                  Learn More
                </button>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default Influencers;
