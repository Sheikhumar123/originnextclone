import Image from "next/image";
import React from "react";
import masonaryImg from "../../images/masonaryImg.png";
const Masonary = () => {
  return (
    <div className="p-5 sm:p-8">
      <Image src={masonaryImg} alt="masonaryImg" />
    </div>
  );
};

export default Masonary;
