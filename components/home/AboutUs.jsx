import { manrope } from "@/utils/fontPoppins";

const AboutUs = () => {
  return (
    <div
      className={`p-8 flex flex-col items-center justify-center gap-4 ${manrope.className}`}
    >
      <div className="flex flex-col items-center justify-center gap-2 w-full 800px:w-[70%] mb-8">
        <h1 className="text-[2rem] 800px:text-[2.5rem] font-semibold text-[#312E81] mb-8">
          About Us
        </h1>
        <p className="text-md text-[#5B5B5B] text-center">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet,
          consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
          exercitation ullamco laboris <br /> <br />
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet,
          consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
          labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
          exercitation ullamco laboris
        </p>
      </div>
    </div>
  );
};

export default AboutUs;
