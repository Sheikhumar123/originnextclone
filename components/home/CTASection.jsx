import {
  homeBusinessCategories,
  homeServiceCategories,
} from "@/data/homepageData";
const CTASection = () => {
  return (
    <div className="flex items-center p-2 400px:p-8 justify-center gap-4 flex-wrap mb-4">
      {/* <div className="600px:h-[475px] bg-white 600px:py-5 w-full  p-3 border-2 rounded-3xl border-[#0066FF] flex-1 shadow-xl 600px:text-nowrap flex-grow">
        <h1 className="flex text-[#0066FF] text-[2.2rem] font-semibold ">
          <p> Hire the right</p>
          <pre>___</pre>
        </h1>
        <h1 className="h-[50px] 400px:h-[138px] 600px:h-[65px] text-[2.3rem] overflow-hidden 400px:text-[3rem] font-bold  inline-block">
          <span className="animate-spinwords  block h-full bg-gradient-to-r from-indigo-600  to-teal-400 text-transparent bg-clip-text">
            Business Professionals
          </span>
          <span className="animate-spinwords  block h-full bg-gradient-to-r from-indigo-600  to-teal-400 text-transparent bg-clip-text">
            Web Developer
          </span>
          <span className="animate-spinwords  block h-full bg-gradient-to-r from-indigo-600  to-teal-400 text-transparent bg-clip-text">
            Software Engineer
          </span>
          <span className="animate-spinwords  block h-full bg-gradient-to-r from-indigo-600  to-teal-400 text-transparent bg-clip-text">
            Architect Personnel
          </span>
          <span className="animate-spinwords  block h-full bg-gradient-to-r from-indigo-600  to-teal-400 text-transparent bg-clip-text">
            Business Professionals
          </span>
        </h1>
        <h1 className="text-[#0066FF] font-semibold text-[1.2rem]">
          Almost any Online Professionals for your needs
        </h1>
        <ul className="list-disc p-4 font-[500] ml-4">
          <li>Zero Platform Fee</li>
          <li>Get quotes for free within minutes</li>
          <li>No restrictions, contact directly with Business Professionals</li>
          <li>Endless possibilities for any job you imagine</li>
        </ul>
        <div className="search">
          <label
            htmlFor="default-search"
            className="mb-2 text-sm font-medium text-gray-900 sr-only "
          >
            Search
          </label>
          <div className="relative">
            <div className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
              <svg
                className="w-4 h-4 text-gray-500 "
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 20 20"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                />
              </svg>
            </div>
            <input
              type="search"
              id="default-search"
              className="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 "
              placeholder="Search for service you need..."
              required
            />
            <button
              type="submit"
              className="text-white absolute end-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2"
            >
              Search
            </button>
          </div>
        </div>
        <div className="flex items-center gap-2 overflow-x-auto 500px:flex-wrap mt-2">
          <h1>Popular:</h1>
          {homeBusinessCategories.map((item, key) => (
            <h1
              className="500px:py-2 py-1 500px:px-6 px-4 border-2 border-blue-500 text-gray-500 text-sm rounded-3xl text-nowrap"
              key={key}
            >
              {item.title}
            </h1>
          ))}
        </div>
      </div> */}
      <div className="600px:h-[475px] bg-white 600px:py-5 w-full p-3 border-2 rounded-3xl border-[#0066FF] flex-1 shadow-xl 600px:text-nowrap  flex-grow">
        <h1 className="flex text-[#0066FF] text-[2.2rem] font-semibold">
          Are You A <pre>___</pre>
        </h1>
        <h1 className="h-[50px] 400px:h-[138px] 600px:h-[65px] text-[2.3rem] overflow-hidden 400px:text-[3rem] font-bold  inline-block">
          <span className="animate-spinwords animation-delay-3000 block h-full bg-gradient-to-r from-indigo-600  to-teal-400 text-transparent bg-clip-text">
            Service Provider
          </span>
          <span className="animate-spinwords animation-delay-3000 block h-full bg-gradient-to-r from-indigo-600  to-teal-400 text-transparent bg-clip-text">
            Freelancer
          </span>
          <span className="animate-spinwords animation-delay-3000 block h-full bg-gradient-to-r from-indigo-600  to-teal-400 text-transparent bg-clip-text">
            Student
          </span>
          <span className="animate-spinwords animation-delay-3000 block h-full bg-gradient-to-r from-indigo-600  to-teal-400 text-transparent bg-clip-text">
            Recruiter
          </span>{" "}
          <span className="animate-spinwords animation-delay-3000 block h-full bg-gradient-to-r from-indigo-600  to-teal-400 text-transparent bg-clip-text">
            Service Provider
          </span>
        </h1>
        <h1 className="text-[#0066FF] font-semibold text-[1.2rem]">
          List your Business now
        </h1>
        <ul className="list-disc p-4 font-[500] ml-4">
          <li>Zero Platform Fee</li>
          <li>Get quotes for free within minutes</li>
          <li>No restrictions, contact directly with Business Professionals</li>
          <li>Endless possibilities for any job you imagine</li>
        </ul>
        <div className="search">
          <label
            htmlFor="default-search"
            className="mb-2 text-sm font-medium text-gray-900 sr-only "
          >
            Search
          </label>
          <div className="relative">
            <div className="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
              <svg
                className="w-4 h-4 text-gray-500 "
                aria-hidden="true"
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 20 20"
              >
                <path
                  stroke="currentColor"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"
                />
              </svg>
            </div>
            <input
              type="search"
              id="default-search"
              className="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 "
              placeholder="Type your service"
              required
            />
            <button
              type="submit"
              className="text-white absolute end-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2"
            >
              Search
            </button>
          </div>
        </div>
        <div className="flex items-center gap-2 overflow-x-auto flex-wrap mt-2">
          <h1>Popular:</h1>
          {homeServiceCategories.map((item, key) => (
            <h1
              className="500px:py-2 py-1 500px:px-6 px-4 border-2 border-blue-500 text-gray-500 text-sm rounded-3xl text-nowrap"
              key={key}
            >
              {item.title}
            </h1>
          ))}
        </div>
      </div>
    </div>
  );
};

export default CTASection;
