import { manrope, poppins } from "@/utils/fontPoppins";
import React from "react";

const CTASubSection = () => {
  return (
    <div
      className={`${manrope.className} flex items-center justify-between flex-wrap p-4 400px:p-8 gap-4 700px:flex-row flex-col`}
    >
      <h1 className="flex-1 text-[rgb(49,46,130)] text-[2rem] 400px:text-[3.2rem] font-semibold">
        What would you like to work on?
      </h1>
      <div className=" flex-1 flex items-center justify-between gap-4 1000px:flex-nowrap flex-wrap p-2 ">
        <p
          className={`text-[#5F5D83] flex-grow  ${poppins.className} text-[#5F5D83] `}
        >
          o sodales malesuada. Pe risus diam Lorem ipsum dolor sit amet,
          consectetur adipiscing elit. Vestibulum dui nisi, porttior sit amet
          diam ut, congue fermentum dui. Aenean gravida p
        </p>
        <button className="flex-grow  bg-[#434CD9] w-auto  px-12 text-nowrap py-3 rounded-3xl text-white">
          Learn More
        </button>
      </div>
    </div>
  );
};

export default CTASubSection;
