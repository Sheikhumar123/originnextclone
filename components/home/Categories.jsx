
import { workCategories } from "@/data/homepageData";
import { baseUrl } from "@/utils/BaseURL";
import axiousInstance from "@/utils/axiousInstance";
import Image from "next/image";
const Categories = async () => {

  return (
    <div className="w-auto p-8 flex flex-col 800px:grid 1100px:grid-cols-4 800px:grid-cols-3  gap-4 mx-auto ">
      {workCategories.map((item, key) => (
        <div
          className={`flex p-8 shadow-md rounded-lg gap-3 ${
            item.rowspan ? "1100px:row-span-2  items-start flex flex-col" : ""
          } ${item.id === 6 ? "flex-end" : ""}`}
          key={key * 9}
        >
          <div className="w-[70px] h-[70px] rounded-full bg-blue-50 p-3 flex items-center justify-center ">
            <Image
              alt={item.title}
              src={item.link}
              width={50}
              height={50}
              objectFit="cover"
            />
          </div>
          <div>
            <h1 className="font-semibold text-[1.2rem]">{item.title}</h1>
            <p>{item.desc}</p>
            {item.rowspan && (
              <h1 className="mt-4 text-xl">Learn More &#8594;</h1>
            )}
          </div>
        </div>
      ))}
    </div>
  );
};

export default Categories;
