import { GoogleLogin, GoogleOAuthProvider } from '@react-oauth/google';

const GoogleSignIn = () => {
  const responseGoogle = (response) => {
    console.log(response);
    // Handle Google login response
  };

  const onFailure = (error) => {
    console.log(error);
    // Handle error
  };

  return (
    <GoogleOAuthProvider clientId="481742004766-0n7ga9b8knh0jb2vgfjjrs8o7g471dhk.apps.googleusercontent.com">
      <GoogleLogin
        clientId="481742004766-0n7ga9b8knh0jb2vgfjjrs8o7g471dhk.apps.googleusercontent.com"
        buttonText="Login with Google"
        onSuccess={responseGoogle}
        onFailure={onFailure}
        redirectUri="http://localhost:3000/auth/login/callback"
      />
    </GoogleOAuthProvider>
  );
};

export default GoogleSignIn;