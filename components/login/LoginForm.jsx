"use client";
import Link from "next/link";
import React from "react";
import { useState } from "react";
import { MdVisibility } from "react-icons/md";
import { MdVisibilityOff } from "react-icons/md";
import { useFormik } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import { CircularProgress } from "@mui/material";
import { usePathname, useRouter } from "next/navigation";
import axiousInstance from "@/utils/axiousInstance";
import Cookies from "js-cookie";
// import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";
import GoogleSignIn from "./GoogleLogin";
import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";

const ValidationSchema = Yup.object().shape({
  password: Yup.string().required("Please Enter  Password"),
  email: Yup.string()
    .email("Invalid email address")
    .required("Please Enter  Email"),
});

const initialValues = {
  password: "",
  email: "",
};

const LoginForm = () => {
  const [loader, setloader] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const router = useRouter();
  const path = usePathname();

  const handleSubmit = async (values) => {
    setloader(true);
    try {
      const response = await axiousInstance.post("auth/loginuser", values);
      setloader(false);
      if (response.data.status === "1") {
        Cookies.set("orignToken", response.data?.accessToken);
        Cookies.set("user_id", response.data?.user_id);
        Cookies.set("email", response.data?.email);
        Cookies.set("username", response.data?.username);
        Cookies.set("title", response.data?.title);
        Cookies.set("profile_image", response.data?.profile_image);
        Cookies.set("AllRoles", JSON.stringify(response.data?.Roles));
        if (response.data?.two_fa) {
          router.push(`/auth/login-otp?type=verifyOTP&email=${values?.email}`);
        }
        response?.data?.Roles?.forEach((item) => {
          if (item?.role_name == "ServiceProvider") {
            Cookies.set("selectedRole", JSON.stringify(item));
            if (response.data?.two_fa) {
              router.push("/auth/login-otp?type=verifyOTP");
            } else {
              router.push("/providersdashboard");
            }
          } else if (item?.role_name == "Investor") {
            Cookies.set("selectedRole", JSON.stringify(item));
            if (response.data?.two_fa) {
              router.push("/auth/login-otp?type=verifyOTP");
            } else {
              router.push("/dashboard");
            }
          }
        });
        toast.success(response.data.message);
      } else {
        setloader(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error.response?.data?.error);
    }
    // await axios({
    //   url: `${baseUrl}auth/loginuser`,
    //   method: "Post",
    //   headers: {
    //     "content-type": "application/json",
    //   },
    //   data: values,
    // })
    //   .then((res) => {
    //     setloader(false);

    //     if (res.data.status === "1") {
    //       res?.data?.Roles?.map((item) => {
    //         if (item?.role_name == "ServiceProvider") {
    //           router.push("/home/service-provider/get-started");
    //         }
    //       });
    //       toast.success(res.data.message);
    //     } else {
    //       setloader(false);
    //       toast.error(res.data.message);
    //     }
    //   })
    //   .catch((err) => {
    //     setloader(false);
    //     toast.error(err.response?.data?.error);
    //   });
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  const responseGoogle = (response) => {
    console.log("response Social",response);
    const data = {
      social_id: response?.credential,
    };

    try {
      const response = axiousInstance.post("auth/loginuser", data);
      setloader(false);
      if (response.data.status === "1") {
        Cookies.set("orignToken", response.data?.accessToken);
        Cookies.set("user_id", response.data?.user_id);
        Cookies.set("email", response.data?.email);
        Cookies.set("username", response.data?.username);
        Cookies.set("title", response.data?.title);
        Cookies.set("profile_image", response.data?.profile_image);
        Cookies.set("AllRoles", JSON.stringify(response.data?.Roles));
        if (response.data?.two_fa) {
          router.push(`/auth/login-otp?type=verifyOTP&email=${values?.email}`);
        }
        response?.data?.Roles?.forEach((item) => {
          if (item?.role_name == "ServiceProvider") {
            Cookies.set("selectedRole", JSON.stringify(item));
            if (response.data?.two_fa) {
              router.push("/auth/login-otp?type=verifyOTP");
            } else {
              router.push("/providersdashboard");
            }
          } else if (item?.role_name == "Investor") {
            Cookies.set("selectedRole", JSON.stringify(item));
            if (response.data?.two_fa) {
              router.push("/auth/login-otp?type=verifyOTP");
            } else {
              router.push("/dashboard");
            }
          }
        });
        toast.success(response.data.message);
      } else {
        setloader(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error.response?.data?.error);
    }
  };

  const onFailure = (error) => {
    console.log(error);
    toast.error("Something went wrong");

    // Handle error
  };
  // const responseGoogle = (response) => {
  //   const data = {
  //     social_id: response?.googleId,
  //   };

  //   try {
  //     const response = axiousInstance.post("auth/loginuser", data);
  //     setloader(false);
  //     if (response.data.status === "1") {
  //       Cookies.set("orignToken", response.data?.accessToken);
  //       Cookies.set("user_id", response.data?.user_id);
  //       response?.data?.Roles?.map((item) => {
  //         if (item?.role_name == "ServiceProvider") {
  //           router.push("/providersdashboard");
  //         }
  //       });
  //       toast.success(response.data.message);
  //     } else {
  //       setloader(false);
  //       toast.error(response.data.message);
  //     }
  //   } catch (error) {
  //     setloader(false);
  //     toast.error(error.response?.data?.error);
  //   }
  // };

  return (
    <div className="lg:min-w-[450px] mt-14 md:mt-6 lg:mt-0">
      <form onSubmit={formik.handleSubmit}>
        <h1 className="mb-2 text-2xl font-[600] label">Sign In</h1>

        <div className="mb-4">
          <input
            type="email"
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            autoComplete={false}
            readonly="readonly"
            onMouseDown={(e) => {
              e.target.removeAttribute("readonly");
            }}
            className="shadow-loginShadow w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="Email"
          />
          {formik.errors.email &&
            formik.touched.email &&
            formik.errors.email && (
              <p className="text-red-700 text-sm">{formik.errors.email}</p>
            )}
        </div>
        <div className="mb-4 relative">
          <div className="relative">
            <input
              className="shadow-loginShadow w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="Password"
              type={showPassword ? "text" : "password"}
              name="password"
              value={formik.values.password}
              onChange={formik.handleChange}
              autoComplete={false}
              readonly="readonly"
              onMouseDown={(e) => {
                e.target.removeAttribute("readonly");
              }}
            />
            <button
              type="button"
              className="absolute inset-y-0 right-0 flex items-center px-3 text-gray-700"
            >
              {showPassword ? (
                <MdVisibilityOff onClick={handleClickShowPassword} size={25} />
              ) : (
                <MdVisibility onClick={handleClickShowPassword} size={25} />
              )}
            </button>
            {formik.errors.password &&
              formik.touched.password &&
              formik.errors.password && (
                <p className="text-red-700 text-sm">{formik.errors.password}</p>
              )}
          </div>
        </div>

        {path !== "/auth/investor-login" && (
          <div className="flex justify-end">
            <div>
              <Link
                className="text-sm  hover:underline text-headingBlack"
                href={"/auth/forgot-password"}
              >
                Forgot Password?
              </Link>
            </div>
          </div>
        )}

        <div className="flex justify-between gap-1 items-center mb-4">
          {loader ? (
            <CircularProgress />
          ) : (
            <button
              type="submit"
              className="px-5 py-3 text-sm font-small text-center h-[fit-content]  text-white bg-blue-600 rounded-[5px]"
            >
              Log in
            </button>
          )}

          {path !== "/auth/investor-login" && (
            <button
              type="button"
              onClick={(e) => {
                e.stopPropagation();
                router.push("/auth/login-otp");
              }}
              className="text-sm font-bold text-center h-[fit-content]  text-blue-600 rounded-[5px]"
            >
              Log in with OTP
            </button>
          )}
        </div>
        <div className="mb-4 mt-6 flex justify-center text-lightBlack">
          <pre>_______</pre>
          Or continue with
          <pre>_______</pre>
        </div>
        <div className="mb-6 w-full">
          <GoogleOAuthProvider clientId="481742004766-0n7ga9b8knh0jb2vgfjjrs8o7g471dhk.apps.googleusercontent.com">
            <GoogleLogin
              clientId="481742004766-0n7ga9b8knh0jb2vgfjjrs8o7g471dhk.apps.googleusercontent.com"
              buttonText="Login with Google"
              onSuccess={responseGoogle}
              onFailure={onFailure}
              redirectUri="http://localhost:3000/auth/login/callback"
              style={{
                width: "100%",
              }}
            />
          </GoogleOAuthProvider>
          {/* <GoogleSignIn /> */}
          {/* <GoogleOAuthProvider 
          // clientId="703311434451-21glebgpd79cv485h5ukqa8908jtgqot.apps.googleusercontent.com"
          clientId="890551033058-0qg2me2rjrc5rbl5gqabotv7k4hc78v5.apps.googleusercontent.com"
          >
            <div>
      
              <GoogleLogin
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                buttonText="Login with Google"
                cookiePolicy={"single_host_origin"}
                isSignedIn={false}
              />
            </div>
          </GoogleOAuthProvider> */}
          {/* <GoogleLogin
        clientId="703311434451-21glebgpd79cv485h5ukqa8908jtgqot.apps.googleusercontent.com"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        buttonText="Login with Google"
        cookiePolicy={'single_host_origin'}
        isSignedIn={false}
      /> */}
          {/* <GoogleOAuthProvider clientId="703311434451-21glebgpd79cv485h5ukqa8908jtgqot.apps.googleusercontent.com">
            <GoogleLogin clientId="703311434451-21glebgpd79cv485h5ukqa8908jtgqot.apps.googleusercontent.com" onSuccess={responseGoogle} onError={responseGoogle} />
          </GoogleOAuthProvider> */}

          {/* <div
            className={
              "google-btn border px-3 border-gray-400 items-center justify-center flex rounded-md cursor-pointer "
            }
            style={{}}
          >
            <GoogleLogin
              clientId="703311434451-21glebgpd79cv485h5ukqa8908jtgqot.apps.googleusercontent.com"
              buttonText="Sign in with Google"
              style={{ border: "1px solid gray" }}
              onSuccess={responseGoogle}
              onFailure={responseGoogle}
              cookiePolicy={"single_host_origin"}
            />
          </div> */}
        </div>
        {/* <div className="flex items-center justify-center gap-2 mb-6">
          <button className="px-5 py-3 border border-gray-400 flex items-center gap-2 rounded-md">
            <FcGoogle size={25} className="" />
            <p>Sign in with Google</p>
          </button>
  
        </div> */}
        <button
          type="button"
          onClick={(e) => {
            e.stopPropagation();

            router.push(
              path !== "/auth/investor-login"
                ? "/service-provider/register"
                : "/auth/investor-register"
            );
          }}
          className="px-5 py-3 text-sm font-small text-center h-[fit-content] w-[100%]  text-white bg-recommendedBlack rounded-[5px]"
        >
          CREATE NEW ACCOUNT
        </button>
      </form>
    </div>
  );
};

export default LoginForm;
