import { Form, Formik } from "formik";
import React, { useState } from "react";
import PropTypes from "prop-types";
import * as yup from "yup";
import { CircularProgress } from "@mui/material";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";
import { changePasswordApi } from "@/Apis/AuthApis/AuthApis";
import { MdVisibility } from "react-icons/md";
import { MdVisibilityOff } from "react-icons/md";
const validationSchema = yup.object().shape({
  password: yup
    .string()
    .min(6, "Password must be at least 6 characters")
    .required("Field Required!"),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref("password"), null], "Passwords must match")
    .required("Field Required!"),
});

function ChangePassword({ screenData }) {
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const [showPassword2, setShowPassword2] = useState(false);
  const handleClickShowPassword2 = () => setShowPassword2((show) => !show);
  const [loader, setloader] = useState(false);
  const router = useRouter();

  const handleSubmt = async (values, { resetForm }) => {
    setloader(true);
    let obj = {
      email: screenData?.email,
      password: values?.password,
    };

    const response = await changePasswordApi(obj);
    setloader(false);
    if (response?.data?.status === "1") {
      toast.success(response.data.message);
      resetForm();
      router.push("/auth/login");
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };

  return (
    <div>
      <h1 className="mb-4 text-2xl font-[600] text-start  text-black">
        Reset Password
      </h1>

      <Formik
        validateOnChange={true}
        initialValues={{
          password: "",
          confirmPassword: "",
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmt}
      >
        {({ values, handleChange, handleBlur, errors, touched }) => {
          return (
            <Form>
              <div className="mb-2 relative">
                <input
                  type={showPassword ? "text" : "password"}
                  className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md ${
                    touched?.password && errors?.password
                      ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                      : "border focus:border-blue-400 focus:ring-blue-600"
                  } focus:outline-none focus:ring-1 `}
                  placeholder="New Password *"
                  name="password"
                  value={values.password}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <button
                  type="button"
                  className="absolute inset-y-0 right-0 flex items-center px-3 text-gray-700"
                >
                  {showPassword ? (
                    <MdVisibilityOff
                      onClick={handleClickShowPassword}
                      size={25}
                    />
                  ) : (
                    <MdVisibility onClick={handleClickShowPassword} size={25} />
                  )}
                </button>
              </div>
              {touched?.password && errors?.password && (
                <span className="text-xs text-[#cc0000]">
                  {errors?.password}
                </span>
              )}

              <div className="mb-2 relative">
                <input
                  type={showPassword2 ? "text" : "password"}
                  className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md ${
                    touched?.confirmPassword && errors?.confirmPassword
                      ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                      : "border focus:border-blue-400 focus:ring-blue-600"
                  } focus:outline-none focus:ring-1 `}
                  placeholder="Confirm Password *"
                  name="confirmPassword"
                  value={values.confirmPassword}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
                <button
                  type="button"
                  className="absolute inset-y-0 right-0 flex items-center px-3 text-gray-700"
                >
                  {showPassword2 ? (
                    <MdVisibilityOff
                      onClick={handleClickShowPassword2}
                      size={25}
                    />
                  ) : (
                    <MdVisibility
                      onClick={handleClickShowPassword2}
                      size={25}
                    />
                  )}
                </button>
              </div>
                {touched?.confirmPassword && errors?.confirmPassword && (
                  <span className="text-xs text-[#cc0000]">
                    {errors?.confirmPassword}
                  </span>
                )}

              {loader ? (
                <div className="flex justify-center">
                  <CircularProgress />
                </div>
              ) : (
                <button
                  className="block w-full mx-auto px-4 py-2 mt-4 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-md active:bg-btnprimary hover:bg-bgBlue/95 focus:outline-none focus:shadow-outline-blue uppercase"
                  type="submit"
                >
                  Change Password
                </button>
              )}
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default ChangePassword;
ChangePassword.propTypes = {
  screenData: PropTypes.object,
};
