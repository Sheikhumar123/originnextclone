import { Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import * as yup from "yup";
import OTPInput from "react-otp-input";
import { toast } from "react-toastify";
import { CircularProgress } from "@mui/material";
import { forgotPassowordApi, verifyPinApi } from "@/Apis/AuthApis/AuthApis";

const validationSchema = yup.object().shape({
  otp: yup.string().min(4).required("Field Required!"),
});

function VerifyOTP({ setTab, screenData, setScreenData }) {
  const [loader, setloader] = useState(false);

  const formatTime = (time) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
  };

  const resendOTP = async () => {
    setloader(true);
    let obj = {
      email: screenData?.email,
    };

    const response = await forgotPassowordApi(obj);
    setloader(false);
    if (response?.data?.status === "1") {
      setScreenData((data) => ({
        ...data,
        duration: response?.data?.duration * 60,
      }));
      toast.success(response.data.message);
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };

  const handleSubmt = async (values, { resetForm }) => {
    if (screenData?.duration > 0) {
      setloader(true);
      let obj = {
        otp: values?.otp,
        email: screenData?.email,
        type: "email",
      };

      const response = await verifyPinApi(obj);
      setloader(false);
      if (response?.data?.status === "1") {
        toast.success(response.data.message);
        resetForm();
        setTab("changePassword");
      } else if (response?.data?.status === "0") {
        toast.error(response.data.message);
      } else {
        toast.error(response.response?.data.message);
      }
    } else {
      toast.error("OTP Expired!");
    }
  };

  return (
    <div>
      <h1 className="mb-4 text-2xl font-[600] text-start text-black">
        Enter Verification Code
      </h1>
      <p className="font-medium text-[18px] leading-normal text-[#6A6A6A] mb-10">
        Please enter the verification code sent to {screenData?.email || ""}
      </p>

      <Formik
        validateOnChange={true}
        initialValues={{
          otp: "",
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmt}
      >
        {({ values, handleBlur, setFieldValue, errors, touched }) => {
          return (
            <Form>
              <div className="w-full">
                <OTPInput
                  className="w-full"
                  value={values.otp}
                  onChange={(val) => setFieldValue("otp", val)}
                  placeholder={true}
                  numInputs={4}
                  renderInput={(props) => (
                    <input
                      placeholder="--"
                      name="otp"
                      onBlur={handleBlur}
                      {...props}
                    />
                  )}
                  inputStyle={{
                    // Custom styles for the input fields
                    width: "100%",
                    maxWidth: "25%",
                    height: "3rem",
                    margin: "0 0.5rem",
                    fontSize: "2rem",
                    borderRadius: "4px",
                    border: "1px solid rgba(0,0,0,0.3)",
                  }}
                />
                {touched?.otp && errors?.otp && (
                  <span className="text-xs text-[#cc0000]">{errors?.otp}</span>
                )}

                <div className="flex justify-between items-center mt-3">
                  <p className="text-[18px] text-[#6A6A6A] font-semibold">
                    Expires in {formatTime(screenData?.duration)}
                  </p>

                  <div className="flex items-center gap-1">
                    <p className="text-[16px] text-[#363848] font-semibold">
                      Didn’t receive OTP?
                    </p>
                    <p
                      className="text-[16px] text-[#4743E0] font-semibold cursor-pointer"
                      onClick={resendOTP}
                    >
                      Resend
                    </p>
                  </div>
                </div>
              </div>

              {loader ? (
                <div className="flex justify-center">
                  <CircularProgress />
                </div>
              ) : (
                <button
                  className="mt-10 block w-full mx-auto px-4 py-2 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-md active:bg-btnprimary hover:bg-bgBlue/95 focus:outline-none focus:shadow-outline-blue uppercase"
                  type="submit"
                >
                  Verify
                </button>
              )}

              <div className="mt-5 flex justify-center">
                <div className="flex items-center gap-1">
                  <p className="text-[18px] text-[#6A6A6A] font-semibold">
                    Wrong Email?
                  </p>
                  <p
                    className="text-[16px] text-[#4743E0] font-semibold cursor-pointer"
                    onClick={() => setTab("forgotPassword")}
                  >
                    click here
                  </p>
                </div>
              </div>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default VerifyOTP;
VerifyOTP.propTypes = {
  setTab: PropTypes.func,
  screenData: PropTypes.object,
  setScreenData: PropTypes.func,
};
