import { Form, Formik } from "formik";
import React, { useState } from "react";
import PropTypes from "prop-types";
import * as yup from "yup";
import { toast } from "react-toastify";
import { CircularProgress } from "@mui/material";
import { forgotPassowordApi } from "@/Apis/AuthApis/AuthApis";

const validationSchema = yup.object().shape({
  email: yup.string().email("Invalid Email!").required("Field Required!"),
});

function ForgotPassword({ setTab, setScreenData }) {
  const [loader, setloader] = useState(false);

  const handleSubmt = async (values, { resetForm }) => {
    setloader(true);
    let obj = {
      email: values.email,
    };

    const response = await forgotPassowordApi(obj);
    setloader(false);
    if (response?.data?.status === "1") {
      toast.success(response.data.message);
      setScreenData({
        email: values.email,
        duration: response?.data?.duration * 60,
      });
      resetForm();
      setTab("verifyOTP");
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };

  return (
    <div>
      <h1 className="mb-4 text-2xl font-[600] text-start  text-black">
        Forgot Password
      </h1>

      <Formik
        validateOnChange={true}
        initialValues={{
          email: "",
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmt}
      >
        {({ values, handleChange, handleBlur, errors, touched }) => {
          return (
            <Form>
              <div className="mb-2">
                <input
                  type="email"
                  className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md ${
                    touched?.email && errors?.email
                      ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                      : "border focus:border-blue-400 focus:ring-blue-600"
                  } focus:outline-none focus:ring-1 `}
                  placeholder="Email Address *"
                  name="email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />

                {touched?.email && errors?.email && (
                  <span className="text-xs text-[#cc0000]">
                    {errors?.email}
                  </span>
                )}
              </div>
              {loader ? (
                <div className="flex justify-center">
                  <CircularProgress />
                </div>
              ) : (
                <button
                  className="block w-full mx-auto px-4 py-2 mt-4 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-md active:bg-btnprimary hover:bg-bgBlue/95 focus:outline-none focus:shadow-outline-blue uppercase"
                  type="submit"
                >
                  Next
                </button>
              )}
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default ForgotPassword;
ForgotPassword.propTypes = {
  setTab: PropTypes.func,
  setScreenData: PropTypes.object,
};
