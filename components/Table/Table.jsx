import Image from "next/image";

const Table = ({ columns, data, actions, renderBody }) => {
  // const renderBody = (column, item) => {
  //   if (column?.label === "Image") {
  //     return (
  //       <div className="w-[60px] h-[60px] rounded-full">
  //         <Image
  //           width={60}
  //           height={60}
  //           style={{ width: "100%", height: "100%" }}
  //           src={item[column?.value]}
  //         />
  //       </div>
  //     );
  //   }
  //   return item[column?.value];
  // };
  return (
    <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
      <thead className="text-xs text-black capitalize bg-[#e5e7eb]">
        <tr>
        <th className="px-6 py-3 capitalize  text-left">Sr</th>

          {columns?.map((column, index) => (
            <th
              key={index}
              //   className="px-6 py-3 text-left text-xs font-semibold text-gray-500 uppercase tracking-wider"
              className="px-6 py-3 text-left   text-black capitalize tracking-wider"
            >
              {column?.label}
            </th>
          ))}
          {actions && (
            <th className="px-6 py-3 capitalize  text-left">Action</th>
          )}
        </tr>
      </thead>
      <tbody className="bg-white divide-y divide-gray-200">
        {data?.map((item, rowIndex) => (
          <tr
            key={(rowIndex*3)+6}
            className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-100 even:dark:bg-gray-800 border-b dark:border-gray-700"
          >
                    <td  className="px-6 py-4 whitespace-nowrap">
       
                {rowIndex+1}
              </td>
            {columns?.map((column, colIndex) => (
              <td key={(colIndex*8)+8} className="px-6 py-4 whitespace-nowrap">
                {renderBody?renderBody(column, item):item[column?.value]}
                {/* {item[column?.value]} */}
              </td>
            ))}
            {actions && (
              <td
                // className="px-6 flex flex-row  whitespace-nowrap gap-1 py-4 h-full bg-[red] text-left"
                className="px-6 py-4 whitespace-nowrap self-center"
              >
                {actions?.map((action, index) =>
                  action?.type == "icon" ? (
                    <button
                      onClick={() => action.onClick(item)}
                      className="cursor-pointer pl-2"
                    >
                      {action?.icon}
                    </button>
                  ) : action?.type == "toogle" ? (
                    <label
                      className="inline-flex items-center cursor-pointer"
                      data-twe-toggle="tooltip"
                      title={"Change Status"}
                    >
                      <input
                        type="checkbox"
                        defaultValue=""
                        checked={item.status === "Active"}
                        className="sr-only peer"
                        onChange={() => {
                          action.onClick(item);
                        }}
                      />
                      <div className="relative w-10 h-5 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />
                    </label>
                  ) : (
                    <button
                      key={index}
                      className={`${action.buttonClass} mr-2`}
                      onClick={() => action.onClick(item)}
                    >
                      {action.label}
                    </button>
                  )
                )}
              </td>
            )}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default Table;
