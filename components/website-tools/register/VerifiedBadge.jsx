const VerifiedBadge = ({ data, handleChange, setStep }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 700px:!w-[600px] flex flex-col">
      <h1 className=" text-xl">
        Get Verified Badge And Build Confidence For Your User
      </h1>

      <div className="flex items-center gap-3 justify-between mb-6 flex-wrap mt-8">
        <label
          htmlFor="verifiedConnectionType"
          className="text-black text-md font-semibold "
        >
          Connection{" "}
        </label>

        <select
          id="verifiedConnectionType"
          className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[180px]"
          defaultValue={data?.verifiedConnectionType || ""}
          name="verifiedConnectionType"
          onChange={handleChange}
        >
          <option value={""}>Select</option>
          <option value="Analytics">Analytics</option>
          <option value="Firebase">Firebase</option>
        </select>
      </div>

      <div className="flex flex-col 500px:flex-row gap-3 w-full justify-between mt-8">
        {" "}
        <button
          className="px-8 py-2 rounded-md bg-gray-200 text-black 500px:flex-grow-0 flex-grow 500px:w-[150px]"
          onClick={() => {
            setStep(7);
          }}
        >
          Skip
        </button>
        <button
          className="px-8 py-2 rounded-md bg-bgBlue text-white 500px:flex-grow-0 flex-grow 500px:w-[150px]"
          onClick={() => {
            // console.log(data);
            setStep(7);
          }}
        >
          Send
        </button>
      </div>
    </div>
  );
};

export default VerifiedBadge;
