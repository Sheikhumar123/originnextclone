import { useState } from "react";
import { RxCross1 } from "react-icons/rx";

const WebURLForm = ({ data, handleChange, setStep, setData }) => {
  const [showInput, setShowInput] = useState(false);
  const handleRemoveCategory = (indexToRemove) => {
    setData((prevData) => ({
      ...prevData,
      categories: prevData.categories.filter(
        (_, index) => index !== indexToRemove
      ),
    }));
  };
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full flex flex-col items-center justify-center">
      <h1 className="font-bold text-xl mb-6 text-center">
        Website And Online Tools Registration{" "}
      </h1>
      {/* WEB URL  */}
      <div className=" flex flex-col items-start w-full mb-8">
        <label htmlFor="webURL">Web URL</label>
        <div className="flex 600px:flex-nowrap  flex-wrap  items-center gap-2 w-full">
          <input
            type="text"
            name="webURL"
            value={data?.webURL || ""}
            onChange={handleChange}
            className="w-full 600px:flex-grow-0 flex-grow bg-white h-[40px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="Enter your URL here"
          />
          <button className="px-8 py-2 rounded-md bg-bgBlue text-white 600px:flex-grow-0 flex-grow">
            Submit
          </button>
        </div>
      </div>
      {/* WEBSITE NAME  */}
      <div className="flex items-center gap-2 flex-wrap mb-8">
        <h3 className="font-semibold text-md">http://www.origin.com/website</h3>
        <input
          type="text"
          id="websiteName"
          value={data.websiteName || ""}
          name="websiteName"
          onChange={handleChange}
          placeholder="Write Name Here"
          className="border border-gray-200 rounded-md text-black p-1 text-sm max-w-[200px]"
        />
        <h3 className="font-semibold text-md text-green-500">Available</h3>
      </div>
      {/* website title  */}
      <div className=" flex flex-col items-start w-full mb-8">
        <label htmlFor="websiteTitle">Title</label>
        <input
          type="text"
          name="websiteTitle"
          value={data?.websiteTitle || ""}
          onChange={handleChange}
          className="w-full 600px:flex-grow-0 flex-grow bg-white h-[40px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="Enter your title here"
        />
      </div>
      {/* WEBSITE DECRIPTION  */}
      <div className=" flex flex-col items-start w-full mb-8">
        <label htmlFor="websiteDescription">Description</label>
        <textarea
          rows={10}
          cols={20}
          type="websiteDescription"
          name="websiteDescription"
          value={data?.websiteDescription || ""}
          onChange={handleChange}
          className="w-full p-4 border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="Enter your description here"
        />
      </div>
      {/* WEB STATISTICS  */}
      <div className="p-4 border rounded-xl flex items-center justify-between w-full mb-8">
        <h1 className="text-gray-400">Website Age</h1>
        <h1 className="text-gray-400">1 Month 3 Days</h1>
      </div>
      {/* CATEGORIES  */}
      <div className=" flex flex-col items-start w-full mb-8">
        <div className="flex items-center justify-between w-full 500px:flex-nowrap flex-wrap gap-3">
          <label
            htmlFor="websiteCategories"
            className="500px:flex-grow-0 flex-grow"
          >
            Category{" "}
            <span className="text-xs text-gray-400 ml-2">
              Max Four Categories
            </span>
          </label>
          {!data?.categories && (
            <button
              className="px-6 py-2 rounded-3xl bg-bgBlue text-white 500px:flex-grow-0 flex-grow"
              onClick={() => setShowInput(true)}
            >
              Add Category
            </button>
          )}
        </div>
        <div className="flex items-center flex-wrap gap-4 mt-4">
          {data?.categories &&
            data.categories.map((item, index) => (
              <div
                className="flex items-center gap-1 px-4 py-1 rounded-3xl bg-blue-100 text-[14px] text-gray-500"
                key={index * 2.023 + 1}
              >
                <h1>{item}</h1>{" "}
                <RxCross1
                  onClick={() => handleRemoveCategory(index)}
                  className="hover:cursor-pointer"
                />
              </div>
            ))}
        </div>
        {showInput && (
          <div className="flex items-center gap-2 w-full mt-4">
            <input
              type="text"
              name="currentWebsiteCategory"
              disabled={data?.categories && data?.categories.length === 4}
              value={data?.currentWebsiteCategory || ""}
              onChange={handleChange}
              className="w-[80%] 600px:flex-grow-0 flex-grow bg-white h-[40px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="Enter your category"
            />
            {!data?.categories && (
              <button
                className="px-6 py-2 rounded-3xl bg-bgBlue text-white"
                onClick={() =>
                  setData((prevData) => ({
                    ...prevData,

                    categories: [
                      ...(prevData.categories || []),
                      prevData.currentWebsiteCategory,
                    ],
                    currentWebsiteCategory: "",
                  }))
                }
              >
                Add
              </button>
            )}
          </div>
        )}

        {data?.categories && data?.categories.length < 4 && (
          <button
            className="px-6 py-2 rounded-3xl bg-bgBlue text-white flex-grow 500px:flex-grow-0 w-full 500px:w-auto mt-4"
            onClick={() => {
              if (!data?.categories || data?.categories.length < 4) {
                setData((prevData) => ({
                  ...prevData,
                  categories: [
                    ...(prevData.categories || []),
                    prevData.currentWebsiteCategory,
                  ],
                  currentWebsiteCategory: "",
                }));
              }
            }}
          >
            Add More
          </button>
        )}
      </div>
      {/* PRICING MODEL  */}
      <div className=" flex flex-col items-start w-full mb-8">
        <label htmlFor="pricingModel">Pricing Model</label>
        <select
          name="pricingModel"
          value={data?.pricingModel || ""}
          onChange={handleChange}
          className="mt-1 max-w-[150px] 600px:flex-grow-0 flex-grow bg-white h-[40px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
        >
          <option value="">Select</option>
          <option value="Free">Free</option>
          <option value="Paid">Paid</option>
        </select>
      </div>
      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end"
        onClick={() => setStep(2)}
      >
        Next
      </button>
    </div>
  );
};

export default WebURLForm;
