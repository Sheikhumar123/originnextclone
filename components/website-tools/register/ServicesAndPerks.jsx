const ServicesAndPerks = ({ data, handleChange, setStep }) => {
  return (
    <div className="p-8 border border-gray-200 rounded-2xl shadow-md w-full 700px:!w-[600px] flex flex-col">
      <h1 className="mb-2 text-xl font-semibold">Free Services And Perks</h1>
      <div className="flex flex-col gap-4 text-gray-600">
        <label
          className="flex items-center gap-2 p-2 bg-[#F5F7FF] rounded-md border border-gray-200"
          htmlFor="alertsNotifications"
        >
          <input
            type="checkbox"
            className="checkbox checkbox-sm [--chkbg:theme(colors.blue.600)] [--chkfg:white]"
            name="alertsNotifications"
            defaultChecked={data?.alertsNotifications || false}
            onChange={handleChange}
          />
          <p className="leading-none">Do you need alerts / notifications?</p>
        </label>

        <label
          className="flex items-center gap-2 p-2 bg-[#F5F7FF] rounded-md border border-gray-200"
          htmlFor="websiteDownFixIssue"
        >
          <input
            type="checkbox"
            className="checkbox checkbox-sm [--chkbg:theme(colors.blue.600)] [--chkfg:white]"
            name="websiteDownFixIssue"
            defaultChecked={data?.websiteDownFixIssue || false}
            onChange={handleChange}
          />
          <p className="leading-none">
            When your website gets down and not working so, you can fix the
            issue and prevent from business loss.
          </p>
        </label>

        <label
          className="flex items-center gap-2 p-2 bg-[#F5F7FF] rounded-md border border-gray-200"
          htmlFor="freeOffersNotifications"
        >
          <input
            type="checkbox"
            className="checkbox checkbox-sm [--chkbg:theme(colors.blue.600)] [--chkfg:white]"
            name="freeOffersNotifications"
            defaultChecked={data?.freeOffersNotifications || false}
            onChange={handleChange}
          />
          <p className="leading-none">
            {" "}
            Need updates and notifications of free offers, gifts related to your
            business.
          </p>
        </label>

        <label
          className="flex items-center gap-2 p-2 bg-[#F5F7FF] rounded-md border border-gray-200"
          htmlFor="promotionalOffersOrigin"
        >
          <input
            type="checkbox"
            className="checkbox checkbox-sm [--chkbg:theme(colors.blue.600)] [--chkfg:white]"
            name="promotionalOffersOrigin"
            defaultChecked={data?.promotionalOffersOrigin || false}
            onChange={handleChange}
          />
          <p className="leading-none">
            {" "}
            Interested to get promotional and offers from origin.com.
          </p>{" "}
        </label>

        <label
          className="flex items-center gap-2 p-2 bg-[#F5F7FF] rounded-md border border-gray-200"
          htmlFor="educationalNewsletters"
        >
          <input
            type="checkbox"
            className="checkbox checkbox-sm [--chkbg:theme(colors.blue.600)] [--chkfg:white]"
            name="educationalNewsletters"
            defaultChecked={data?.educationalNewsletters || false}
            onChange={handleChange}
          />
          <p className="leading-none">
            Interested to get educational newsletters to help grow your
            knowledge in boosting business.
          </p>
        </label>
      </div>
      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end mt-8"
        onClick={() => {
          setStep(5);
        }}
      >
        Send
      </button>
    </div>
  );
};

export default ServicesAndPerks;
