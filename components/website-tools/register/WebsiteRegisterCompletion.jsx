import Image from "next/image";
import webCompletionBanner from "@/images/webCompletionBanner.png";
const VerifiedBadge = ({ data, handleChange, setStep }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 700px:!w-[600px] flex flex-col">
      <h1 className=" text-xl font-bold text-center mb-8">
        Congratulations 🎊! Your&apos;e Verified✅{" "}
      </h1>
      <Image
        src={webCompletionBanner}
        alt="completion banner"
        width={293}
        height={286}
        className="mx-auto"
        placeholder="blur"
      />
    </div>
  );
};

export default VerifiedBadge;
