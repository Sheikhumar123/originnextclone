const NotificationsFormData = ({ data, handleChange, setStep }) => {
  return (
    <div className="border border-gray-200 rounded-xl p-4 flex flex-col  max-w-[600px]">
      <div className="">
        <h1 className="font-semibold 500px:w-[80%]">
          Get Instant Notifications And Alerts About Your Website Maintenance
          And Server Up-Time
        </h1>
        <p className="mb-2 text-gray-400 text-xs">
          Need Phone Number Verification To Get Verified Batch
        </p>
        <div className="p-2 rounded-md shadow-sm flex flex-col 500px:items-center 500px:justify-center">
          <div className="mb-2 relative w-full">
            <label className="block text-sm text-label mb-2">Phone</label>
            <div className="relative">
              <input
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="+923XXXXXXXXX"
                type="tel"
                name="phone"
                value={data?.phone || ""}
                onChange={handleChange}
              />
            </div>
          </div>
          <button className="px-8 py-2 rounded-md bg-bgBlue text-white self-end">
            Verify
          </button>
          <div className="mb-2 w-full">
            <label className="block text-sm text-label  mb-2">OTP</label>
            <input
              type="number"
              name="phoneOtp"
              value={data?.phoneOtp || ""}
              onChange={handleChange}
              className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="XXXXXX"
            />
            <div className="flex items-center gap-2 ml-1">
              <label className="block text-[15px] text-label text-blue-500">
                Resend
              </label>
              <label className="block text-[15px] text-label ">00:00</label>
            </div>
          </div>
        </div>
        <div className="p-2 rounded-md shadow-sm flex flex-col ">
          <div className="mb-2 relative ">
            <label className="block text-sm text-label mb-2">
              Add Whatsapp (Optional)
            </label>
            <div className="relative">
              <input
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="+923XXXXXXXXX"
                type="tel"
                name="whatsappNumber"
                value={data?.whatsappNumber || ""}
                onChange={handleChange}
              />
            </div>
          </div>
          <button className="px-8 py-2 rounded-md bg-bgBlue text-white self-end">
            Verify
          </button>
          <div className="mb-2 w-full ">
            <label className="block text-sm text-label  mb-2">OTP</label>
            <input
              type="number"
              name="whatsappOtp"
              value={data?.whatsappOtp || ""}
              onChange={handleChange}
              className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="XXXXXX"
            />
            <div className="flex items-center gap-2 ml-1">
              <label className="block text-[15px] text-label text-blue-500">
                Resend
              </label>
              <label className="block text-[15px] text-label ">00:00</label>
            </div>
          </div>
        </div>
        <label
          className="mt-8 flex items-center gap-2 p-2 "
          htmlFor="termsAndPoliciesAccepted"
        >
          <input
            type="checkbox"
            className="checkbox checkbox-sm [--chkbg:theme(colors.blue.600)] [--chkfg:white]"
            name="termsAndPoliciesAccepted"
            defaultChecked={data?.termsAndPoliciesAccepted || false}
            onChange={handleChange}
          />
          <p className="leading-none">
            {" "}
            By signing up, you agree to our terms and privacy policy.
          </p>
        </label>
      </div>
      <div className="w-full flex items-center justify-between 500px:flex-row flex-col 500px:flex-grow-0 flex-grow-0">
        <button
          className="px-8 py-2 rounded-md bg-gray-200 text-black w-full 500px:w-auto mt-4"
          onClick={() => {
            setStep(6);
          }}
        >
          Skip
        </button>
        <button
          className="px-8 py-2 rounded-md bg-bgBlue text-white w-full 500px:w-auto mt-4"
          onClick={() => {
            setStep(6);
          }}
        >
          Next
        </button>
      </div>
    </div>
  );
};

export default NotificationsFormData;
