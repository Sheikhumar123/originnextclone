const DetailedFormContinued = ({ data, handleChange, setStep }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 700px:!w-[600px] flex flex-col">
      <h1 className=" text-xl">Tell Us More About Yourself</h1>
      <p className="mb-2 text-gray-400 text-xs">
        Only For Record - Will Not Be Available Publicly
      </p>
      <div className="mb-2">
        <label htmlFor="name" className="text-black text-md font-semibold ">
          Your Name
        </label>
        <input
          id="name"
          type="text"
          name="name"
          defaultValue={data?.name || ""}
          onChange={handleChange}
          className="w-full bg-white h-[40px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="John Doe"
        />
      </div>
      <div className="flex items-center gap-3 justify-between mb-6 flex-wrap">
        <label
          htmlFor="designation"
          className="text-black text-md font-semibold "
        >
          Your Designation
        </label>

        <select
          id="desigination"
          className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[180px]"
          defaultValue={data?.desigination || ""}
          name="desigination"
          onChange={handleChange}
        >
          <option value={""}>Select</option>
          <option value="Brand Ambassador">Brand Ambassador</option>
          <option value="Sponsor">Sponsor</option>
          <option value="Item">Item</option>
          <option value="Item">Item</option>
        </select>
      </div>

      <div className="flex items-center gap-3 justify-between mb-6 flex-wrap">
        <label htmlFor="country" className="text-black text-md font-semibold ">
          Your Country
        </label>

        <select
          id="country"
          className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[180px]"
          defaultValue={data?.country || ""}
          name="country"
          onChange={handleChange}
        >
          <option value={""}>Select</option>
          <option value="India">India</option>
          <option value="Pakistan">Pakistan</option>
          <option value="United States">United States</option>
          <option value="United Kingdom">United Kingdom</option>
        </select>
      </div>

      <div className="flex items-center gap-3 justify-between mb-6 flex-wrap">
        <label htmlFor="city" className="text-black text-md font-semibold ">
          Your City
        </label>

        <select
          id="city"
          className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[180px]"
          defaultValue={data?.city || ""}
          name="city"
          onChange={handleChange}
        >
          <option value={""}>Select</option>
          <option value="India">India</option>
          <option value="Pakistan">Pakistan</option>
          <option value="US">United States</option>
          <option value="UK">United Kingdom</option>
        </select>
      </div>

      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end"
        onClick={() => {
          setStep(4);
        }}
      >
        Send
      </button>
    </div>
  );
};

export default DetailedFormContinued;
