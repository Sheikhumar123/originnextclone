import Image from "next/image";
import React from "react";
import Logo from "../../images/logo.png";
// ../../../images/logo.png
function ScreenLoader() {
  return (
    <>
     <div
      style={{
        width: "100%",
        height: "100vh",
        position: "absolute",
        top: 0,
        left: 0,
        zIndex: "1455000",
        background: "rgba(250,250,250,0.5)",
        overflow: "hidden",
        position: "fixed",
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      <Image
        src={Logo}
        alt={""}
        style={{ width: "100%", maxWidth: "150px", height: "auto" }}
        className="loader"
      />
    </div>
    </>
  );
}

export default ScreenLoader;
