"use client";
import Table from "../Table/Table";
import { useEffect, useState } from "react";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import {
  GetAllInvestment,
  GetInvestDetails,
} from "@/redux/Slices/InvestorDashboard/InvestSlice/InvestSlice";
import Cookies from "js-cookie";
import Pagination from "../Pagination/Pagination";
const WithdrawalList = ({ InvestorWithdrawalData, page, setpage }) => {
  const [allWithdrawal, setallWithdrawal] = useState([]);
  const [count, setcount] = useState(0);
  const [limit, setlimit] = useState(0);
  useEffect(() => {
    if (InvestorWithdrawalData) {
      setcount(InvestorWithdrawalData?.count);
      setlimit(InvestorWithdrawalData?.limit);
      let arr = [];
      InvestorWithdrawalData?.Transactions?.transactions?.forEach((item) => {
        arr.push({
          ...item,
          date: item?.createdAt
            ? moment(item?.createdAt).format("DD/MM/YYYY")
            : "",
          amount: Number(item?.amount)?.toLocaleString(),
        });
      });
      setallWithdrawal(arr);
    }
  }, [InvestorWithdrawalData]);
  const columns = [
    { label: "Date", value: "date" },
    { label: "Mode", value: "payment_method" },
    { label: "Type", value: "type" },
    { label: "Amount", value: "amount" },
    { label: "Tax", value: "tax" },
    { label: "Total Amount", value: "total_amount" },
    { label: "Status", value: "status" },
  ]; // Define table columns
  const renderBodyContent = (column, item) => {
    if (column?.label === "Status") {
      return (
        <div
          className={`flex gap-1 items-center w-[fit-content] rounded-md ${
            item.status === "Received"
              ? "text-black bg-[#E0FFE9]"
              : item.status === "Decline"
              ? "text-red-600 bg-[#F7B0B069]"
              : "text-black bg-[#dbeafe]"
          } px-3 py-1`}
        >
          <div
            className={`w-[7px] h-[7px] rounded-full ${
              item.status === "Received"
                ? " bg-[#00ad31]"
                : item.status === "Decline"
                ? "bg-red-500"
                : " bg-sky-500"
            }`}
          ></div>
          <div>{item.status}</div>
        </div>
      );
    }
    return item[column?.value];
  };
  return (
    <div className="w-full ">
      <div className="relative overflow-x-auto shadow-md sm:rounded-lg mt-6">
        <Table
          columns={columns}
          data={allWithdrawal}
          // actions={actions}
          renderBody={renderBodyContent}
        />
      </div>
      {count > limit ? (
        <div className="pt-4 pb-[130px] flex justify-end px-2">
          <Pagination
            pageCount={page}
            setPageCount={setpage}
            totalcount={count}
            limit={limit}
            showCount={true}
          />
        </div>
      ) : null}
    </div>
  );
};

export default WithdrawalList;
