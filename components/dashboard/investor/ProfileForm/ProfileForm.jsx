"use client";

import Image from "next/image";
import { useEffect, useState } from "react";
import { IoCameraOutline } from "react-icons/io5";
import { CircularProgress } from "@mui/material";
import { MdDone } from "react-icons/md";
import "react-phone-input-2/lib/style.css";
import {
  GetProviderProfileData,
  GetProviderdashboardDetails,
} from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import { useDispatch, useSelector } from "react-redux";
import profile from "../../../../images/dashboard/profile.png";
import Cookies from "js-cookie";
import { useFormik } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import {
  verifyEmailApi,
  verifyNumberApi,
} from "@/Apis/ServiceProviderApis/ServiceProviderApis";
import {
  ChangePassword,
  EditProfile,
} from "@/Apis/ServiceProviderApis/DashboardApi/Investor/ProfileApis";
import OtpVerificationDialog from "../../ProfileForm/OtpVerificationDialog/OtpVerificationDialog";
import { MdVisibility } from "react-icons/md";
import { MdVisibilityOff } from "react-icons/md";
const ProfileForm = () => {
  const [profileImage, setprofileImage] = useState(null);
  const [loader, setloader] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const [showPassword2, setShowPassword2] = useState(false);
  const handleClickShowPassword2 = () => setShowPassword2((show) => !show);
  const [showOtpVerficationDialog, setShowOtpVerficationDialog] =
    useState(false);
  const [screenData, setScreenData] = useState({
    email: "",
    emailVerified: true,
    duration: 0,
    otpFor: "",
  });

  const dispatch = useDispatch();
  const { ProviderProfileData, status } = useSelector(
    GetProviderdashboardDetails
  );

  useEffect(() => {
    dispatch(GetProviderProfileData(Cookies.get("user_id")));
    if (status == "rejected" && error) {
      toast.error(error);
    }
  }, [dispatch]);

  useEffect(() => {
    const countdown = setInterval(() => {
      if (screenData?.duration > 0) {
        setScreenData((rest) => ({
          ...rest,
          duration: rest.duration - 1,
        }));
      } else {
        clearInterval(countdown);
      }
    }, 1000);

    return () => clearInterval(countdown);
  }, [screenData?.duration]);

  const initialValues = {
    firstname: "",
    lastname: "",
    email: "",
    address: "",
    city: "",
    state: "",
  };
  const initialValuesForPassword = {
    oldpassword: "",
    newpassword: "",
  };

  const ValidationSchema = Yup.object().shape({
    firstname: Yup.string().required("Please Enter First Name"),
    lastname: Yup.string().required("Please Enter Last Name"),
    email: Yup.string()
      .email("Invalid email address")
      .required("Please Enter  Email"),
    city: Yup.string().required("Please Enter City"),
    state: Yup.string().required("Please Enter State"),
    address: Yup.string().required("Please Enter Address"),
  });
  const ValidationSchemaForPassword = Yup.object().shape({
    oldpassword: Yup.string().required("Please Enter Password"),
    newpassword: Yup.string().required("Please Enter New Password"),
  });

  useEffect(() => {
    if (ProviderProfileData) {
      formik.setFieldValue("firstname", ProviderProfileData?.firstname);
      formik.setFieldValue("lastname", ProviderProfileData?.lastname);
      formik.setFieldValue("email", ProviderProfileData?.email);
      formik.setFieldValue("address", ProviderProfileData?.address);
      formik.setFieldValue("city", ProviderProfileData?.city);
      formik.setFieldValue("state", ProviderProfileData?.state);

      setprofileImage(
        ProviderProfileData?.profile_image
          ? {
              file: ProviderProfileData?.profile_image,
              url: ProviderProfileData?.profile_image_url,
            }
          : {
              file: null,
              url: profile,
            }
      );
    }
  }, [ProviderProfileData]);

  const handleSubmit = async (values, setSubmitting, resetForm) => {
    const formData = new FormData();
    formData.append(`user_id`, Cookies.get("user_id"));
    formData.append(`firstname`, values.firstname);
    formData.append(`email`, values.email);
    formData.append(`lastname`, values.lastname);
    formData.append(`city`, values.city);
    formData.append(`state`, values.state);
    formData.append(`address`, values.address);
    formData.append(`profile_image`, profileImage?.file);

    setloader(true);
    const response = await EditProfile(formData);
    setloader(false);
    if (response?.data?.status == "1") {
      toast.success(response.data.message);
      Cookies.set("profile_image", response?.data?.profile_image_url);
      dispatch(GetProviderProfileData(Cookies.get("user_id")));
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data?.message);
    }
  };
  const handlePasswordSubmit = async (values, resetForm, setSubmitting) => {
    const formData = new FormData();
    formData.append(`user_id`, Cookies.get("user_id"));
    formData.append(`oldpassword`, values.oldpassword);
    formData.append(`newpassword`, values.newpassword);

    setSubmitting(true);
    const response = await ChangePassword(formData);
    setSubmitting(false);

    if (response?.data?.status == "1") {
      toast.success(response.data?.message);
      resetForm();
    } else if (response?.data?.status === "0") {
      toast.error(response.data?.message);
    } else {
      toast.error(response?.response?.data?.error);
    }
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  const passwordFormik = useFormik({
    initialValues: initialValuesForPassword,
    validationSchema: ValidationSchemaForPassword,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handlePasswordSubmit(values, resetForm, setSubmitting);
    },
  });

  const handleInputChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
      setprofileImage({
        file: file,
        url: reader.result,
      });
    };
    if (file) {
      reader.readAsDataURL(file);
    }
  };
  console.log(screenData?.emailVerified);

  const resendOTP = async (type) => {
    setloader(true);
    let obj = {};

    if (type === "email") {
      obj.email = formik.values?.email;
    } else if (type === "phone") {
      obj.country_code = formik.values?.pcountry_code;
      obj.phone = formik.values?.phonenum;
    } else {
      obj.country_code = formik.values?.wcountry_code;
      obj.whatsappnum = formik.values?.whatsappnum;
    }

    let response;
    if (type === "email") {
      response = await verifyEmailApi(obj);
    } else {
      response = await verifyNumberApi(obj);
    }

    setloader(false);
    if (response?.data?.status === "1") {
      if (type === "email") {
        setScreenData((data) => ({
          ...data,
          email: formik.values?.email,
          duration: response?.data?.duration * 60,
          otpFor: "email",
        }));
      } else if (type === "phone") {
        setScreenData((data) => ({
          ...data,
          pCCode: formik.values?.pcountry_code,
          phone: formik.values?.phonenum,
          duration: response?.data?.duration * 60,
          otpFor: "phone",
        }));
      } else {
        setScreenData((data) => ({
          ...data,
          wCCode: formik.values?.wcountry_code,
          whatsapp: formik.values?.whatsappnum,
          duration: response?.data?.duration * 60,
          otpFor: "whatsapp",
        }));
      }

      setShowOtpVerficationDialog(true);
      toast.success(response.data.message);
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.data.message);
    }
  };
  return (
    <>
      <div className="w-full  ">
        <form onSubmit={formik.handleSubmit}>
          <div className="flex justify-between items-center">
            <div className="mb-2 flex items-center gap-4 w-auto">
              <div className=" w-[65px] h-[65px]  500px:w-[100px] 500px:h-[100px] rounded-full  relative">
                <label
                  htmlFor={`profileimg`}
                  className=" w-[65px] h-[65px]  500px:w-[100px] 500px:h-[100px] rounded-full cursor-pointer "
                >
                  <Image
                    src={
                      profileImage?.url
                      // "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?q=80&w=1470&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                    }
                    onError={(e) => {
                      setprofileImage({
                        file: null,
                        url: profile,
                      });
                    }}
                    style={{ width: "100%", height: "100%" }}
                    alt="profile img"
                    className=" object-cover rounded-full"
                    width={500}
                    height={500}
                  />
                  <input
                    id={`profileimg`}
                    type="file"
                    accept="image/*"
                    onChange={(event) => handleInputChange(event)}
                    className="mb-2 hidden"
                  />
                  <div className="flex justify-center items-center w-[40px] h-[40px] absolute bg-white bottom-0 right-0 rounded-full">
                    <div className="flex justify-center items-center w-[30px] bg-[#ececec] h-[30px] rounded-full">
                      <IoCameraOutline size={22} />
                    </div>
                  </div>
                </label>
              </div>
              <div className="flex flex-col 500px:gap-2">
                <h1 className="font-semibold text-xl">
                  {ProviderProfileData?.username}
                </h1>
                <p className="text-[#6a6a6a]">{ProviderProfileData?.title}</p>
              </div>
            </div>
          </div>

          <div className="mb-2">
            <div className="flex 700px:flex-row 300px:flex-col items-center gap-2 w-full">
              <div className="700px:w-[49.5%] 300px:w-[100%]">
                <label className="block text-sm text-label mb-2">Name</label>
                <input
                  type="text"
                  name="firstname"
                  value={formik.values.firstname}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="John"
                />
                {formik.errors.firstname &&
                  formik.touched.firstname &&
                  formik.errors.firstname && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.firstname}
                    </p>
                  )}
              </div>

              <div className="700px:w-[49.5%] 300px:w-[100%]">
                <label className="block text-sm text-label mb-2">
                  Last Name
                </label>
                <input
                  type="text"
                  name="lastname"
                  value={formik.values.lastname}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="Doe"
                />
                {formik.errors.lastname &&
                  formik.touched.lastname &&
                  formik.errors.lastname && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.lastname}
                    </p>
                  )}
              </div>
            </div>
          </div>

          <div className="mb-2 ">
            <label className="block text-sm text-label mb-2">Email</label>
            <div className="relative">
              <input
                placeholder="Email"
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                type="email"
                name="email"
                value={formik.values.email}
                onChange={(e) => {
                  setScreenData((prev) => {
                    return {
                      ...prev,
                      emailVerified:
                        e.target.value === ProviderProfileData?.email
                          ? true
                          : false,
                    };
                  });
                  formik.handleChange(e);
                }}
                onBlur={formik.handleBlur}
                autoComplete={false}
                readonly="readonly"
                onMouseDown={(e) => {
                  e.target.removeAttribute("readonly");
                }}
              />
              {!formik.errors?.email && formik.values?.email && (
                <div
                  className={`absolute font-bold inset-y-0 right-0 px-3 flex justify-center items-center`}
                >
                  {formik.values.email === ProviderProfileData?.email ||
                  screenData?.emailVerified ? (
                    <div className={`flex justify-center items-center gap-1`}>
                      <MdDone color="#abcca5" size={22} />
                      <p className="text-[#92d487]">Verified</p>
                    </div>
                  ) : (
                    <button
                      type="button"
                      className={`absolute justify-center font-bold inset-y-0 right-0 flex items-center px-3 text-[#0066ff] cursor-pointer flex-col whitespace-nowrap`}
                      onClick={() => resendOTP("email")}
                    >
                      Get OTP
                    </button>
                  )}
                </div>
              )}
            </div>
            {formik.errors.email &&
              formik.touched.email &&
              formik.errors.email && (
                <p className="text-red-700 text-sm">{formik.errors.email}</p>
              )}
          </div>
          <div className="mb-2 ">
            <label className="block text-sm text-label mb-2">Address</label>
            <div className="relative">
              <textarea
                placeholder="Address"
                rows={4}
                className="w-full bg-white  px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                name="address"
                value={formik.values.address}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                autoComplete={false}
                readonly="readonly"
                onMouseDown={(e) => {
                  e.target.removeAttribute("readonly");
                }}
              />
            </div>
            {formik.errors.address &&
              formik.touched.address &&
              formik.errors.address && (
                <p className="text-red-700 text-sm">{formik.errors.address}</p>
              )}
          </div>
          <div className="mb-2">
            <div className="flex 700px:flex-row 300px:flex-col items-center gap-2 w-full">
              <div className="700px:w-[68.5%] 300px:w-[100%]">
                <label className="block text-sm text-label mb-2">City</label>

                <input
                  type="text"
                  name="city"
                  value={formik.values.city}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="City"
                />
                {formik.errors.city &&
                  formik.touched.city &&
                  formik.errors.city && (
                    <p className="text-red-700 text-sm">{formik.errors.city}</p>
                  )}
              </div>
              <div className="700px:w-[68.5%] 300px:w-[100%]">
                <label className="block text-sm text-label mb-2">State</label>

                <input
                  type="text"
                  name="state"
                  value={formik.values.state}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="State"
                />
                {formik.errors.state &&
                  formik.touched.state &&
                  formik.errors.state && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.state}
                    </p>
                  )}
              </div>
            </div>
          </div>
          <div className="w-[fit-content] pt-6">
            {loader ? (
              <CircularProgress />
            ) : (
              <button
                type="submit"
                disabled={!screenData?.emailVerified}
                className="bg-[#434CD9] flex-grow w-full px-6 py-3 rounded-md text-white"
              >
                Save Changes
              </button>
            )}
          </div>
        </form>

        <div className="py-4">
          <hr />
        </div>
        <form onSubmit={passwordFormik.handleSubmit}>
          <div className="mb-2">
            <div className="flex 700px:flex-row 300px:flex-col items-center gap-2 w-full">
              <div className="700px:w-[49.5%] 300px:w-[100%]">
                <label className="block text-sm text-label mb-2">
                  Password
                </label>
                <div className="relative">
                  <input
                    type={showPassword ? "text" : "password"}
                    name="oldpassword"
                    value={passwordFormik.values.oldpassword}
                    onChange={passwordFormik.handleChange}
                    onBlur={passwordFormik.handleBlur}
                    className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                    placeholder="Enter your old password"
                  />
                  <button
                    type="button"
                    className="absolute inset-y-0 right-0 flex items-center px-3 text-gray-700"
                  >
                    {showPassword ? (
                      <MdVisibilityOff
                        onClick={handleClickShowPassword}
                        size={25}
                      />
                    ) : (
                      <MdVisibility
                        onClick={handleClickShowPassword}
                        size={25}
                      />
                    )}
                  </button>
                </div>
                {passwordFormik.errors.oldpassword &&
                  passwordFormik.touched.oldpassword &&
                  passwordFormik.errors.oldpassword && (
                    <p className="text-red-700 text-sm">
                      {passwordFormik.errors.oldpassword}
                    </p>
                  )}
              </div>

              <div className="700px:w-[49.5%] 300px:w-[100%]">
                <label className="block text-sm text-label mb-2">
                  New Password
                </label>
                <div className="relative">
                <input
                                type={showPassword2 ? "text" : "password"}
                  name="newpassword"
                  value={passwordFormik.values.newpassword}
                  onChange={passwordFormik.handleChange}
                  onBlur={passwordFormik.handleBlur}
                  className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="Enter your new password"
                />
                                  <button
                    type="button"
                    className="absolute inset-y-0 right-0 flex items-center px-3 text-gray-700"
                  >
                    {showPassword2 ? (
                      <MdVisibilityOff
                        onClick={handleClickShowPassword2}
                        size={25}
                      />
                    ) : (
                      <MdVisibility
                        onClick={handleClickShowPassword2}
                        size={25}
                      />
                    )}
                  </button>
                </div>
                {passwordFormik.errors.newpassword &&
                  passwordFormik.touched.newpassword &&
                  passwordFormik.errors.newpassword && (
                    <p className="text-red-700 text-sm">
                      {passwordFormik.errors.newpassword}
                    </p>
                  )}
              </div>
            </div>
          </div>

          <div className="w-[fit-content] pt-6">
            {passwordFormik.isSubmitting ? (
              <CircularProgress />
            ) : (
              <button
                type="submit"
                className="bg-[#434CD9] flex-grow w-full px-6 py-3 rounded-md text-white"
              >
                Change Password
              </button>
            )}
          </div>
        </form>
        <OtpVerificationDialog
          open={showOtpVerficationDialog}
          onClose={() => {
            setShowOtpVerficationDialog(false);
            setScreenData((data) => ({
              ...data,
              duration: 0,
              otpFor: "",
            }));
          }}
          screenData={screenData}
          setScreenData={setScreenData}
        />
      </div>
    </>
  );
};

export default ProfileForm;
