"use client";
import { refferalData } from "@/data/dashboardData";
import Table from "../Table/Table";
import { useEffect, useState } from "react";
import moment from "moment";
const RefferalList = ({InvestorReferalData}) => {
  const [allReferal, setallReferal] = useState([]);
  const [count, setcount] = useState(0);
  const [limit, setlimit] = useState(0);
  useEffect(() => {
    if (InvestorReferalData) {
      setcount(InvestorReferalData?.count)
      setlimit(InvestorReferalData?.limit)
      let arr = [];
      InvestorReferalData?.transactions?.referedusers?.forEach((item) => {
        arr.push({
          ...item,
          date: item?.date
            ? moment(item?.date).format("DD/MM/YYYY")
            : "",
            amount:item?.amount?item?.amount?.toLocaleString():null
        });
      });
      setallReferal(arr);
    }
  }, [InvestorReferalData]);
  const columns = [
    { label: "Name", value: "username" },
    { label: "Earned", value: "amount" },
    { label: "Date", value: "date" },
  ]; // Define table columns
  // const renderBodyContent = (column, item) => {
  //   if (column?.label === "Status") {
  //     return (
  //       <div
  //         className={`flex gap-1 items-center w-[fit-content] rounded-md ${
  //           item.status === "Verified"
  //             ? "text-black bg-[#E0FFE9]"
  //             : "text-red-600 bg-[#F7B0B069] px-3 py-1"
  //         }`}
  //       >
  //         <div
  //           className={`w-[7px] h-[7px] rounded-full ${
  //             item.status === "Verified" ? " bg-[#00ad31]" : " bg-red-500"
  //           }`}
  //         ></div>
  //         <div>{item.status}</div>
  //       </div>
  //     );
  //   }
  //   return item[column?.value];
  // };
  return (
    <div className="w-full p-4">
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg mt-4">
        <Table
          columns={columns}
          data={allReferal}
          // actions={actions}
          // renderBody={renderBodyContent}
        />
      </div>
    </div>
  );
};

export default RefferalList;
