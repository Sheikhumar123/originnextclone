"use client";
import { statsChartData } from "@/data/dashboardData";
import { manrope } from "@/utils/fontPoppins";
import ReactApexChart from "react-apexcharts";
import { FaAngleDown } from "react-icons/fa6";

const StatsChart = () => {
  return (
    <div
      className={`flex items-stretch justify-center gap-4 flex-wrap mt-4 ${manrope.className}`}
    >
      <div className=" w-[60%]  flex-grow bg-white rounded-lg shadow  p-4 md:p-6">
        <div className="flex items-center justify-between">
          <h1 className="text-[1.4rem] font-[500]">Total Revenue </h1>
          <div className="dropdown dropdown-end">
            <div
              tabIndex={0}
              role="button"
              className="px-4 py-2 bg-base-200 rounded-md flex items-center gap-2"
            >
              <p> Month</p>
              <FaAngleDown size={20} />
            </div>
            <ul
              tabIndex={0}
              className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
            >
              <li>
                <a>Day</a>
              </li>
              <li>
                <a>Week</a>
              </li>
            </ul>
          </div>
        </div>
        {typeof window !== "undefined" && (
          <ReactApexChart
            series={statsChartData.series}
            type="area"
            options={statsChartData}
            height={320}
          />
        )}
      </div>
      <div className="  w-[38%]  flex-grow flex flex-col justify-between  bg-white rounded-lg shadow  p-4 md:p-6">
        <div className="p-2">
          <h1 className=" text-[1.3] font-[500]">This Year Profit</h1>
          <h1 className="text-3xl font-bold">$2,567</h1>
        </div>
        <button className="bg-[#434CD9] flex-grow md:flex-grow-0  px-12 py-3 rounded-3xl text-white">
          345 Days Left In Withdrawal
        </button>
      </div>
    </div>
  );
};

export default StatsChart;
