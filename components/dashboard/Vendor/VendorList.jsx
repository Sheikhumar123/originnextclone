"use client";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import {
  GetAllInvestment,
  GetInvestDetails,
} from "@/redux/Slices/InvestorDashboard/InvestSlice/InvestSlice";
import Cookies from "js-cookie";
import moment from "moment";
import Table from "@/components/Table/Table";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import Pagination from "@/components/Pagination/Pagination";
import VendorConfirmation from "./VendorConfirmation";
import { toast } from "react-toastify";
import axiousInstance from "@/utils/axiousInstance";
function VendorList({ setshowForm }) {
  const { AllInvestment, status } = useSelector(GetInvestDetails);
  const [allCredits, setallCredits] = useState([]);
  const [isVendor, setisVendor] = useState(false);
  const [vendorDialoge, setvendorDialoge] = useState(false);
  const [count, setcount] = useState(0);
  const [limit, setlimit] = useState(0);
  const [loader, setloader] = useState(false);
  const UserId = Cookies.get("user_id");
  const dispatch = useDispatch();
  const [page, setpage] = useState(1);
  console.log(allCredits);

  useEffect(() => {
    let obj = {
      user_id: UserId,
      type: "Credit",
      page: page,
    };
    dispatch(GetAllInvestment(obj));
  }, [dispatch, page]);

  useEffect(() => {
    if (AllInvestment) {
      setcount(AllInvestment?.count);
      setlimit(AllInvestment?.limit);
      let arr = [];
      AllInvestment?.Transactions?.forEach((item) => {
        arr.push({
          ...item,
          date: item?.createdAt
            ? moment(item?.createdAt).format("DD/MM/YYYY")
            : "",
          amount: Number(item?.amount)?.toLocaleString(),
          fee: Number(item?.fee)?.toLocaleString(),
        });
      });
      setallCredits(arr);
    }
  }, [AllInvestment]);
  const columns = [
    { label: "Date", value: "date" },
    { label: "Mode", value: "payment_method" },
    { label: "Amount", value: "amount" },
    { label: "Fee", value: "fee" },
    { label: "Total Amount", value: "total_amount" },
    // { label: "Description", value: "description" },
    { label: "Status", value: "status" },
  ]; // Define table columns
  const renderBodyContent = (column, item) => {
    if (column?.label === "Status") {
      return (
        <div
          className={`flex gap-1 items-center w-[fit-content] rounded-md ${
            item.status === "Received"
              ? "text-black bg-[#E0FFE9]"
              : item.status === "Decline"
              ? "text-red-600 bg-[#F7B0B069]"
              : "text-black bg-[#dbeafe]"
          } px-3 py-1`}
        >
          <div
            className={`w-[7px] h-[7px] rounded-full ${
              item.status === "Received"
                ? " bg-[#00ad31]"
                : item.status === "Decline"
                ? "bg-red-500"
                : " bg-sky-500"
            }`}
          ></div>
          <div>{item.status}</div>
        </div>
      );
    }
    return item[column?.value];
  };
  const handleVendor = async () => {
    setloader(true);
    let obj = {
      user_id: Cookies.get("user_id"),
      vendor_status: true,
    };
    try {
      const response = await axiousInstance.post("investor/addvendor", obj);
      setloader(false);
      if (response.data.status === "1") {
        setisVendor(true);
        toast.success(response.data.message);
        let obj = {
          user_id: UserId,
          type: "Credit",
          page: page,
        };
        dispatch(GetAllInvestment(obj));
        setvendorDialoge(false);
      } else {
        setloader(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error.response?.data?.error);
    }
  };
  return (
    <>
      {status == "pending" && <ScreenLoader />}
      {vendorDialoge && (
        <VendorConfirmation
          loader={loader}
          handleVendor={handleVendor}
          setvendorDialoge={setvendorDialoge}
          setisVendor={setisVendor}
        />
      )}
      <div className="w-full flex justify-end">
        {AllInvestment?.vendor_status ? (
          <button
            type="button"
            onClick={() => {
              setshowForm(true);
            }}
            className="bg-[#434CD9] w-[fit-content]  self-center text-nowrap px-4 py-2 rounded-md text-white"
          >
            Add Credits
          </button>
        ) : (
          <div className="flex gap-4 items-center">
            <label className="font-bold text-lg text-blue-600">
              Become a Vendor
            </label>
            <label className="inline-flex items-center cursor-pointer">
              <input
                type="checkbox"
                checked={isVendor}
                value={isVendor}
                className="sr-only peer"
                onChange={(e) => {
                  //   setisVendor(e.target.checked);
                  if (e.target.checked == true) {
                    setvendorDialoge(true);
                  } else {
                    setisVendor(false);
                  }
                }}
              />
              <div className="relative w-14 h-7 bg-gray-200 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full p after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-6 after:w-6 after:transition-all peer-checked:bg-blue-600" />
            </label>
          </div>
        )}
      </div>
      <div className="relative overflow-x-auto shadow-md sm:rounded-lg mt-4">
        <Table
          columns={columns}
          data={allCredits}
          // actions={actions}
          renderBody={renderBodyContent}
        />
      </div>
      {count > limit ? (
        <div className="pt-4 pb-[130px] flex justify-end px-2">
          <Pagination
            pageCount={page}
            setPageCount={setpage}
            totalcount={count}
            limit={limit}
            showCount={true}
          />
        </div>
      ) : null}
    </>
  );
}

export default VendorList;
