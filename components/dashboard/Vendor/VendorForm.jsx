"use client";

import { useEffect, useState } from "react";
import { CircularProgress } from "@mui/material";
// import QRCode from "qrcode.react";
import { IoClose } from "react-icons/io5";
import { toast } from "react-toastify";
import Image from "next/image";
import { useFormik } from "formik";
import * as Yup from "yup";
import { registerInvestmentAddApi } from "@/Apis/InvestorApis/InvestorApis";
import Cookies from "js-cookie";
import { BiLogoPaypal } from "react-icons/bi";
import { FaBitcoin } from "react-icons/fa6";
import { TbCurrencyRupee } from "react-icons/tb";
import { TbBrandCashapp } from "react-icons/tb";
import { useDispatch, useSelector } from "react-redux";
import { GetInvestDetails, GetQRdetail } from "@/redux/Slices/InvestorDashboard/InvestSlice/InvestSlice";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
function VendorForm({ setshowForm }) {
    const [loader, setloader] = useState(false);
  const [investImage, setinvestImage] = useState(null);
  const { QRdetailData, status } = useSelector(GetInvestDetails);
console.log(QRdetailData);
  const dispatch=useDispatch()
  useEffect(()=>{
dispatch(GetQRdetail())
  },[dispatch])
  useEffect(()=>{
if(QRdetailData){
  formik.setFieldValue("bussinessId",QRdetailData?.qrdetails?.bussiness_id)
  formik.setFieldValue("bussinessName",QRdetailData?.qrdetails?.bussiness_name)
}
  },[QRdetailData])
  const ValidationSchema = Yup.object().shape({
    amount: Yup.number()
      .required("Please Enter Amount")
      .min(10000, "Amount must be at least 10000")
      .max(45000, "Amount must not exceed 45000"),
    payment_method: Yup.string().required("Please Enter Payment Method"),
  });

  const initialValues = {
    amount: "",
    payment_method: "",
    fee: "",
    bussinessId: "",
    bussinessName: "",
  };
  const handleSubmit = async (values, resetForm) => {
    console.log(values);
    if (!investImage) {
      return toast.error("Please Enter Screen Shot of Payment");
    }
    setloader(true);
    const formData = new FormData();
    formData.append("user_id", Cookies.get("user_id"));
    formData.append("amount", values?.amount);
    formData.append("fee", values?.fee);
    formData.append("total_amount",Number(values.amount) + Number(values.fee)),

    formData.append("payment_method", values?.payment_method);
    formData.append("type", "Credit");
    formData.append("transaction_ss", investImage?.file);
    const response = await registerInvestmentAddApi(formData);
    setloader(false);
    if (response?.data?.status == "1") {
      toast.success(response.data.message);
      setshowForm(false);
      resetForm();
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response?.response?.data.message);
    }
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  return (
    <>
      {(loader || status=="pending") && <ScreenLoader />}

      <div className="flex flex-col w-full">

        <form onSubmit={formik.handleSubmit}>
          <div className="px-6 pb-6">
            <div className="w-full p-6 flex items-center justify-between border-dashed border-2 border-sky-500 rounded-md bg-[#004551]/15 mb-6">
              <p className="text-xl">Total Amount</p>
              <h1 className="text-xl">
                <span className="font-bold text-2xl">
                  ${Number(formik.values.amount) + Number(formik.values.fee)}
                </span>{" "}
                USD
              </h1>
            </div>
            <div className="flex gap-2 flex-wrap w-full mb-2 ">
              {withdrawalTypeData?.map((item, key) => (
                <label
                  key={key * 3}
                  htmlFor={item.name}
                  className="relative flex-1 cursor-pointer"
                >
                  <input
                    type="radio"
                    disabled={item?.name=="PayPal" || item?.name=="Crypto"}
                    className="peer/payment hidden"
                    id={item.name}
                    name="payment_method"
                    onChange={() => {
                      formik.setFieldValue("payment_method", item?.name);
                    }}
                    value={formik.values.payment_method}
                  />

                  <div className="flex flex-col items-center justify-center p-8 bg-white border-2 border-gray-200 rounded-md transition peer-checked/payment:border-blue-500  peer-checked/payment:shadow-lg peer-checked/payment:-translate-y-1 ">
                    {item.logo}
                    {item.name}
                  </div>
                </label>
              ))}
            </div>
            {formik.errors.payment_method &&
              formik.touched.payment_method &&
              formik.errors.payment_method && (
                <p className="text-red-700 text-sm">
                  {formik.errors.payment_method}
                </p>
              )}
              {
                formik.values.payment_method=="UPI"?
                <>
            <div className="mb-4 mt-2">
              <div className="w-full max-w-[200px]">
                <Image src={QRdetailData?.qrdetails?.qr_code} alt="Qr Code" width={200} height={200} style={{width:"100%",height:"100%"}} />
              </div>
              {/* <QRCode
                id="qr-gen"
                includeMargin={true}
                size={125}
                // value={userId?userId:"usama"}
              /> */}
            </div>
            <div className="mb-2">
              <label className="block  text-lg">Bussines Id</label>
              <input
                type="text"
                name="bussinessId"
                disabled
                value={formik.values.bussinessId}
                className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
   
              />
            </div>
            <div className="mb-2">
              <label className="block  text-lg">Bussines Name</label>
              <input
                type="text"
                name="bussinessName"
                disabled
                value={formik.values.bussinessName}
                className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
        
              />
            </div>
            <div className="mb-2">
              <label className="block  text-lg">Amount</label>
              <input
                type="number"
                name="amount"
                value={formik.values.amount}
                onChange={(e) => {
                  formik.setFieldValue("amount", e.target.value);
                  formik.setFieldValue("fee", (2 / 100) * e.target.value);
                }}
                className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="min 10000 to max 45000"
              />
              {formik.errors.amount &&
                formik.touched.amount &&
                formik.errors.amount && (
                  <p className="text-red-700 text-sm">{formik.errors.amount}</p>
                )}
            </div>
            {/* <div className="mb-2">
              <label className="block  text-lg">Amount</label>
              <input
                type="number"
                name="amount"
                value={formik.values.amount}
                onChange={(e) => {
                  formik.setFieldValue("amount", e.target.value);
                  formik.setFieldValue("fee", (2 / 100) * e.target.value);
                }}
                className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="min 10000 to max 45000"
              />
              {formik.errors.amount &&
                formik.touched.amount &&
                formik.errors.amount && (
                  <p className="text-red-700 text-sm">{formik.errors.amount}</p>
                )}
            </div> */}
            <div className="mb-6">
              <label className="block  text-lg">Fee</label>
              <input
                type="number"
                name="fee"
                disabled
                value={formik.values.fee}
                className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Fee"
              />
              <label className="block text-[12px] text-black mb-2 ">
                <span className="text-red-600 text-lg">*</span> Processing fee
                is 2% of Processing amount
              </label>
            </div>
            <label
              className="cursor-pointer label w-full"
              htmlFor={`investScreenshot`}
            >
              <input
                id={`investScreenshot`}
                type="file"
                accept="image/*"
                name={`investImage`}
                // onChange={(event) => handleInputChange(index, event, "image")}
                // value={item.question}
                onChange={(event) => {
                  const file = event.target.files[0];
                  const reader = new FileReader();
                  reader.onloadend = () => {
                    // formik.values.featureFields[index].image = file;
                    // formik.values.featureFields[index].imageUrl =
                    //   reader.result;
                    setinvestImage({
                      file: file,
                      imageUrl: reader.result,
                    });
                    // formik.setFieldValue("featureFields", newFeatureFields);
                    // setFeatureFields(newFeatureFields);
                  };
                  if (file) {
                    reader.readAsDataURL(file);
                  }
                }}
                className="mb-2 hidden"
              />

              <div className="w-full p-6 border-dashed border-2 border-orange-400 rounded-md bg-[#FF9937]/35 ">
                <p className="text-lg text-center">Uplaod Image</p>
                <p className="text-md text-center">Your Payment ScreenShot</p>
              </div>
            </label>
            {investImage?.imageUrl ? (
              <div className="w-full max-w-[200px] relative mt-2">
                <IoClose
                  size={20}
                  onClick={() => {
                    setinvestImage(null);
                  }}
                  className="absolute right-0 text-[red] cursor-pointer"
                />

                <Image
                  src={investImage.imageUrl}
                  alt="Feature Preview"
                  className="mb-2  cursor-pointer"
                  width={200}
                  height={200}
                  style={{ width: "100%", height: "100%" }}
                />
              </div>
            ) : null}
            </>:null
              }
                      <div className="flex justify-end items-center">
          <div className="flex gap-2 items-center mt-2">
            <button
              type="button"
              className="px-6 py-2 rounded-md bg-red-600 text-white self-end "
              onClick={() => {
                // setrowData(null);
                // setisEdit(null);
                setshowForm(false);
              }}
            >
              Cancel
            </button>
            {loader ? (
                <CircularProgress />
              ) : (
                <button
                  type="submit"
                  // onClick={handleSubmit}
                  className="bg-[#434CD9] w-full md:w-[100px]   px-6 py-2 rounded-md text-white"
                >
                  Save
                </button>
              )}
          </div>
        </div>
            {/* <div className="w-full flex items-center justify-center mt-4">
              {" "}
              {loader ? (
                <CircularProgress />
              ) : (
                <button
                  type="submit"
                  // onClick={handleSubmit}
                  className="bg-[#434CD9] w-full md:w-[100px]   px-6 py-2 rounded-3xl text-white"
                >
                  Save
                </button>
              )}
            </div> */}
          </div>
        </form>
      </div>
    </>
  )
}

export default VendorForm;
export const withdrawalTypeData = [
    {
      id: 1,
      name: "UPI",
      logo: <TbCurrencyRupee size={40} />,
    },
    // {
    //   id: 4,
    //   name: "Cash",
    //   logo: <TbBrandCashapp size={40} />,
    // },
    {
      id: 3,
      name: "Crypto",
      logo: <FaBitcoin color="#F7931A" size={40} />,
    },
  
    {
      id: 2,
      name: "PayPal",
      logo: <BiLogoPaypal color="#253b80" size={40} />,
    },
  ];
  