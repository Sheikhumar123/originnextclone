"use client";
import { useState } from "react";
import { CircularProgress } from "@mui/material";
import { IoClose } from "react-icons/io5";
import { toast } from "react-toastify";
import { useFormik } from "formik";
import * as Yup from "yup";
import { withdrawInvestorRefrelApi } from "@/Apis/InvestorApis/InvestorApis";
import Cookies from "js-cookie";
import { BiLogoPaypal } from "react-icons/bi";
import { FaBitcoin } from "react-icons/fa6";
import { TbCurrencyRupee } from "react-icons/tb";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import { GetInvestorWithdrawPage } from "@/redux/Slices/InvestorDashboard/InvestSlice/InvestSlice";
import { useDispatch } from "react-redux";
function RefrelWithdrawModal({ open, closeModal, InvestorWithdrawalData }) {
  const [loader, setloader] = useState(false);
  const dispatch = useDispatch();
  const ValidationSchema = Yup.object().shape({
    amount: Yup.number()
      .required("Please Enter Amount")
      .max(
        Number(
          InvestorWithdrawalData?.Transactions?.balance_to_withdraw ||
            InvestorWithdrawalData?.Transactions?.balance_to_withdraw -
              InvestorWithdrawalData?.Transactions?.pending_withdraw
        ),
        `Amount must not exceed ${InvestorWithdrawalData?.Transactions?.balance_to_withdraw}`
      ),
    payment_method: Yup.string().required("Please Enter Payment Method"),
  });

  const initialValues = {
    amount: "",
    payment_method: "",
    tax: "",
  };
  const handleSubmit = async (values, resetForm) => {
    console.log(values);
    setloader(true);
    let obj = {
      ...values,
      type: "Referral Income",
      total_amount: Number(values.amount) + Number(values.tax),
      user_id: Cookies.get("user_id"),
    };
    const response = await withdrawInvestorRefrelApi(obj);
    setloader(false);
    if (response?.data?.status == "1") {
      toast.success(response.data.message);
      let obj = {
        user_id: Cookies.get("user_id"),
        page: 1,
      };
      dispatch(GetInvestorWithdrawPage(obj));
      closeModal();
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response?.response?.data.message);
    }
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  console.log(formik.values.amount);
  return open ? (
    <>
      {loader && <ScreenLoader />}
      <div className="justify-center items-center px-4 flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-full max-w-[900px]  max-h-[90%] overflow-y-auto border-0 rounded-lg shadow-lg bg-white outline-none focus:outline-none">
          {/*content*/}
          <div className="flex flex-col w-full">
            {/*header*/}
            <div className="flex justify-between items-center p-5">
              <h3 className="text-xl font-semibold">Withdraw Amount</h3>
              <button
                className="text-black outline-none focus:outline-none hover:text-gray-600 transition-colors"
                onClick={closeModal}
              >
                <IoClose size={30} />
              </button>
            </div>

            {/*body*/}
            <div className="px-6 pb-6">
              <form onSubmit={formik.handleSubmit}>
                <div className="px-6 pb-6">
                  <div className="w-full p-6 flex items-center justify-between border-dashed border-2 border-sky-500 rounded-md bg-[#004551]/15 mb-6">
                    <p className="text-xl">Total Amount</p>
                    <h1 className="text-xl">
                      <span className="font-bold text-2xl">
                        $
                        {Number(formik.values.amount) +
                          Number(formik.values.tax)}
                      </span>{" "}
                      USD
                    </h1>
                  </div>
                  <div className="flex gap-2 flex-wrap w-full mb-2 ">
                    {withdrawalTypeData?.map((item, key) => (
                      <label
                        key={key * 3}
                        htmlFor={item.name}
                        className="relative flex-1 cursor-pointer"
                      >
                        <input
                          type="radio"
                          disabled={
                            item?.name == "PayPal" || item?.name == "Crypto"
                          }
                          className="peer/payment hidden"
                          id={item.name}
                          name="payment_method"
                          onChange={() => {
                            formik.setFieldValue("payment_method", item?.name);
                          }}
                          value={formik.values.payment_method}
                        />

                        <div className="flex flex-col items-center justify-center p-8 bg-white border-2 border-gray-200 rounded-md transition peer-checked/payment:border-blue-500  peer-checked/payment:shadow-lg peer-checked/payment:-translate-y-1 ">
                          {item.logo}
                          {item.name}
                        </div>
                      </label>
                    ))}
                  </div>
                  {formik.errors.payment_method &&
                    formik.touched.payment_method &&
                    formik.errors.payment_method && (
                      <p className="text-red-700 text-sm">
                        {formik.errors.payment_method}
                      </p>
                    )}
                  <div className="mb-2">
                    <label className="block  text-lg">Amount</label>
                    <input
                      type="number"
                      name="amount"
                      value={formik.values.amount}
                      onChange={(e) => {
                        formik.setFieldValue("amount", e.target.value);
                        formik.setFieldValue("tax", (2 / 100) * e.target.value);
                      }}
                      className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                      placeholder="Withdraw Amount"
                    />
                    {formik.errors.amount &&
                      formik.touched.amount &&
                      formik.errors.amount && (
                        <p className="text-red-700 text-sm">
                          {formik.errors.amount}
                        </p>
                      )}
                  </div>
                  <div className="mb-6">
                    <label className="block  text-lg">Tax</label>
                    <input
                      type="number"
                      name="tax"
                      disabled
                      value={formik.values.tax}
                      className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                      placeholder="Fee"
                    />
                    <label className="block text-[12px] text-black mb-2 ">
                      <span className="text-red-600 text-lg">*</span>
                      tax is 2% of Processing amount
                    </label>
                  </div>

                  <div className="flex justify-end items-center">
                    <div className="flex gap-2 items-center mt-2">
                      <button
                        type="button"
                        className="px-6 py-2 rounded-md bg-red-600 text-white self-end "
                        onClick={closeModal}
                      >
                        Cancel
                      </button>
                      {loader ? (
                        <CircularProgress />
                      ) : (
                        <button
                          type="submit"
                          // onClick={handleSubmit}
                          className="bg-[#434CD9] w-full md:w-[100px]   px-6 py-2 rounded-md text-white"
                        >
                          Save
                        </button>
                      )}
                    </div>
                  </div>
                  {/* <div className="w-full flex items-center justify-center mt-4">
              {" "}
              {loader ? (
                <CircularProgress />
              ) : (
                <button
                  type="submit"
                  // onClick={handleSubmit}
                  className="bg-[#434CD9] w-full md:w-[100px]   px-6 py-2 rounded-3xl text-white"
                >
                  Save
                </button>
              )}
            </div> */}
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

      <div
        className="opacity-25 fixed inset-0 z-40 bg-black"
        onClick={closeModal}
      ></div>
    </>
  ) : null;
}

export default RefrelWithdrawModal;
export const withdrawalTypeData = [
  {
    id: 1,
    name: "UPI",
    logo: <TbCurrencyRupee size={40} />,
  },
  {
    id: 3,
    name: "Crypto",
    logo: <FaBitcoin color="#F7931A" size={40} />,
  },

  {
    id: 2,
    name: "PayPal",
    logo: <BiLogoPaypal color="#253b80" size={40} />,
  },
];
