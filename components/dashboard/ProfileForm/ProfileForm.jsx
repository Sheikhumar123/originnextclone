"use client";

import Image from "next/image";
import { useEffect, useState } from "react";
import { IoCameraOutline } from "react-icons/io5";
import { MdVerified } from "react-icons/md";
import { Country, State, City } from "country-state-city";
import { Autocomplete, Chip, CircularProgress, TextField } from "@mui/material";
import { MdDone } from "react-icons/md";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import TimezoneSelect from "react-timezone-select";
import { IconContext } from "react-icons";
import { BiLogoFacebookCircle } from "react-icons/bi";
import { FaLinkedin } from "react-icons/fa";
import {
  GetProviderProfileData,
  GetProviderdashboardDetails,
} from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import { useDispatch, useSelector } from "react-redux";
import profile from "../../../images/dashboard/profile.png";
import Cookies from "js-cookie";
import { useFormik } from "formik";
import * as Yup from "yup";
import { EditProviderProfileApi } from "@/Apis/ServiceProviderApis/DashboardApi/EditProfileApi";
import { toast } from "react-toastify";
import {
  verifyEmailApi,
  verifyNumberApi,
} from "@/Apis/ServiceProviderApis/ServiceProviderApis";
import { AllLanguage } from "@/components/Language/Language";
import OtpVerificationDialog from "./OtpVerificationDialog/OtpVerificationDialog";

import dynamic from "next/dynamic";

const Editor = dynamic(
  () => {
    return import("react-draft-wysiwyg").then((mod) => mod.Editor);
  },
  { ssr: false }
);
// import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { ContentState, EditorState, Modifier } from "draft-js";

const ProfileForm = () => {
  const [allcountries, setallcountries] = useState([]);
  const [selectedTimezone, setSelectedTimezone] = useState({});
  const [profileImage, setprofileImage] = useState(null);
  const [loader, setloader] = useState(false);
  const [showOtpVerficationDialog, setShowOtpVerficationDialog] =
    useState(false);
  const [screenData, setScreenData] = useState({
    email: "",
    emailVerified: false,
    pCCode: "",
    phone: "",
    phoneVerified: false,
    wCCode: "",
    whatsapp: "",
    whatsappVerified: false,
    duration: 0,
    otpFor: "",
  });

  const dispatch = useDispatch();
  const { ProviderProfileData, status } = useSelector(
    GetProviderdashboardDetails
  );

  useEffect(() => {
    dispatch(GetProviderProfileData(Cookies.get("user_id")));
    if (status == "rejected" || status == "error") {
      toast.error("error");
    }
    let value = Country.getAllCountries()?.map((data) => {
      let obj = {
        label: data?.name,
        value: data,
      };
      return obj;
    });
    setallcountries(value);
  }, [dispatch]);

  useEffect(() => {
    const countdown = setInterval(() => {
      if (screenData?.duration > 0) {
        setScreenData((rest) => ({
          ...rest,
          duration: rest.duration - 1,
        }));
      } else {
        clearInterval(countdown);
      }
    }, 1000);

    return () => clearInterval(countdown);
  }, [screenData?.duration]);

  const customStyles = {
    control: (base) => ({
      ...base,
      height: 54,
      minHeight: 54,
    }),
  };

  // const cookieStore = cookies()

  const initialValues = {
    firstname: "",
    lastname: "",
    email: "",
    country: null,
    city: "",
    pcountry_code: "",
    phonenum: "",
    wcountry_code: "",
    whatsappnum: "",
    language: [],
    description: EditorState.createEmpty(),
  };

  const ValidationSchema = Yup.object().shape({
    firstname: Yup.string().required("Please Enter First Name"),
    lastname: Yup.string().required("Please Enter Last Name"),
    country: Yup.object().nullable().required("Please Enter Country"),
    email: Yup.string()
      .email("Invalid email address")
      .required("Please Enter  Email"),
    city: Yup.string().required("Please Enter City"),
    phonenum: Yup.string().required("Please Enter Phone Number"),
    whatsappnum: Yup.string().required("Please Enter Watsapp Number"),
    language: Yup.array()
      .min(1, "Please Select Atleast one Language")
      .required("Please Enter Language"),
    description: Yup.mixed()
      .test("is-string-or-editorstate", "Description is required", (value) => {
        return typeof value === "string" || value instanceof EditorState;
      })
      .required("Description is required"),
  });
  const prefillData = async () => {
    if (ProviderProfileData) {
      formik.setFieldValue("firstname", ProviderProfileData?.firstname);
      formik.setFieldValue("lastname", ProviderProfileData?.lastname);
      formik.setFieldValue("email", ProviderProfileData?.email);
      formik.setFieldValue(
        "country",
        ProviderProfileData?.country
          ? {
              label: ProviderProfileData?.country,
              value: ProviderProfileData?.country,
            }
          : null
      );
      formik.setFieldValue("city", ProviderProfileData?.city);
      formik.setFieldValue("pcountry_code", ProviderProfileData?.pcountry_code);
      formik.setFieldValue("phonenum", ProviderProfileData?.phonenum);
      formik.setFieldValue("whatsappnum", ProviderProfileData?.whatsappnum);
      formik.setFieldValue("wcountry_code", ProviderProfileData?.wcountry_code);
      formik.setFieldValue(
        "language",
        ProviderProfileData?.language ? ProviderProfileData?.language : []
      );
      // formik.setFieldValue(
      //   "description",
      //   ProviderProfileData?.description ? ProviderProfileData?.description : ""
      // );
      const htmlToDraft = (await import("html-to-draftjs")).default;

      let body = htmlToDraft(
        ProviderProfileData?.description ? ProviderProfileData?.description : ""
      );
      console.log(body);
      if (body?.contentBlocks) {
        const contentState = ContentState.createFromBlockArray(
          body?.contentBlocks
        );
        console.log(contentState);
        const editorState = EditorState.createWithContent(contentState);
        console.log(editorState);
        formik.setFieldValue(`description`, editorState);
        // setEditorState(editorState);
      }
      setprofileImage(
        ProviderProfileData?.profile_image
          ? {
              file: ProviderProfileData?.profile_image,
              url: ProviderProfileData?.profile_image_url,
            }
          : {
              file: null,
              url: profile,
            }
      );
      if (ProviderProfileData?.time_zone) {
        setSelectedTimezone(JSON.parse(ProviderProfileData?.time_zone));
      }
    }
  };
  useEffect(() => {
    prefillData();
  }, [ProviderProfileData]);
  // console.log(JSON.parse(ProviderProfileData?.time_zone));
  const handleSubmit = async (values, setSubmitting, resetForm) => {
    const { convertToHTML } = await import("draft-convert");

    if (!selectedTimezone) {
      return toast.error("Please Enter Time Zone");
    } else if (
      screenData?.emailVerified == false &&
      formik.values.email != ProviderProfileData?.email
    ) {
      return toast.error("Please Verify Email");
    } else if (
      screenData?.phoneVerified == false &&
      formik.values.phonenum != ProviderProfileData?.phonenum
    ) {
      return toast.error("Please Verify Phone");
    } else if (
      screenData?.whatsappVerified == false &&
      formik.values.whatsappnum != ProviderProfileData?.whatsappnum
    ) {
      return toast.error("Please Verify Watsapp Phone");
    } else {
      const formData = new FormData();
      formData.append(`user_id`, Cookies.get("user_id"));
      formData.append(`firstname`, values.firstname);
      formData.append(`email`, values.email);
      formData.append(`lastname`, values.lastname);
      formData.append(`country`, values.country?.label);
      formData.append(`city`, values.city);
      formData.append(`pcountry_code`, values.pcountry_code);
      formData.append(`phonenum`, values.phonenum);
      formData.append(`wcountry_code`, values.wcountry_code);
      formData.append(`whatsappnum`, values.whatsappnum);
      formData.append(`language`, JSON.stringify(values.language));
      formData.append(
        `description`,
        convertToHTML(values?.description.getCurrentContent())
      );
      formData.append(`time_zone`, JSON.stringify(selectedTimezone));
      formData.append(`linkedin`, "");
      formData.append(`facebook`, "");
      formData.append(`fb_verified`, false);
      formData.append(`linkedin_verified`, false);
      formData.append(`profile_image`, profileImage?.file);

      setloader(true);
      const response = await EditProviderProfileApi(formData);
      setloader(false);
      if (response?.data?.status == "1") {
        toast.success(response.data.message);
        Cookies.set("profile_image", response?.data?.profile_image_url);
        dispatch(GetProviderProfileData(Cookies.get("user_id")));
      } else if (response?.data?.status === "0") {
        toast.error(response.data.message);
      } else {
        toast.error(response?.response?.data.message);
      }
    }
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });

  const handleInputChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
      setprofileImage({
        file: file,
        url: reader.result,
      });
    };
    if (file) {
      reader.readAsDataURL(file);
    }
  };

  const resendOTP = async (type) => {
    setloader(true);
    let obj = {};

    if (type === "email") {
      obj.email = formik.values?.email;
    } else if (type === "phone") {
      obj.country_code = formik.values?.pcountry_code;
      obj.phone = formik.values?.phonenum;
    } else {
      obj.country_code = formik.values?.wcountry_code;
      obj.whatsappnum = formik.values?.whatsappnum;
    }

    let response;
    if (type === "email") {
      response = await verifyEmailApi(obj);
    } else {
      response = await verifyNumberApi(obj);
    }

    setloader(false);
    if (response?.data?.status === "1") {
      if (type === "email") {
        setScreenData((data) => ({
          ...data,
          email: formik.values?.email,
          duration: response?.data?.duration * 60,
          otpFor: "email",
        }));
      } else if (type === "phone") {
        setScreenData((data) => ({
          ...data,
          pCCode: formik.values?.pcountry_code,
          phone: formik.values?.phonenum,
          duration: response?.data?.duration * 60,
          otpFor: "phone",
        }));
      } else {
        setScreenData((data) => ({
          ...data,
          wCCode: formik.values?.wcountry_code,
          whatsapp: formik.values?.whatsappnum,
          duration: response?.data?.duration * 60,
          otpFor: "whatsapp",
        }));
      }

      setShowOtpVerficationDialog(true);
      toast.success(response.data.message);
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response?.response?.data.message);
    }
  };
  const [errorMessage, setErrorMessage] = useState("");
  const handleOnChange = (editorState) => {
    let contentState = editorState.getCurrentContent();
    let newContentState = contentState;

    // Remove phone numbers and email addresses
    const phoneRegex =
      /\+\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}|\(\d{1,3}\)\s*\d{3,4}[-.\s]??\d{4}|\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}/g;
    const emailRegex = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/gi;

    let match;
    let containsInvalidContent = false;
    let containsInvalidContent2 = false;

    // Remove phone numbers
    contentState.getBlockMap().forEach((contentBlock) => {
      const blockText = contentBlock.getText();
      while ((match = phoneRegex.exec(blockText)) !== null) {
        const start = match.index;
        const end = start + match[0].length;
        newContentState = Modifier.replaceText(
          newContentState,
          editorState
            .getSelection()
            .merge({ anchorOffset: start, focusOffset: end }),
          ""
        );
        containsInvalidContent = true;
      }
    });

    // Remove email addresses
    contentState.getBlockMap().forEach((contentBlock) => {
      const blockText = contentBlock.getText();
      while ((match = emailRegex.exec(blockText)) !== null) {
        const start = match.index;
        const end = start + match[0].length;
        newContentState = Modifier.replaceText(
          newContentState,
          editorState
            .getSelection()
            .merge({ anchorOffset: start, focusOffset: end }),
          ""
        );
        containsInvalidContent2 = true;
      }
    });

    const newEditorState = EditorState.push(
      editorState,
      newContentState,
      "remove-range"
    );

    formik.setFieldValue("description", newEditorState);

    if (containsInvalidContent) {
      setErrorMessage("Please remove phone numbers before entering text.");
    } else if (containsInvalidContent2) {
      setErrorMessage("Please remove email addresses before entering text.");
    } else {
      setErrorMessage("");
    }
  };
  // const handlePastedText = (text, html, editorState) => {
  //   const contentState = editorState.getCurrentContent();
  //   const contentStateWithText = Modifier.insertText(
  //     contentState,
  //     editorState.getSelection(),
  //     text
  //   );

  //   let newEditorState = EditorState.push(
  //     editorState,
  //     contentStateWithText,
  //     "insert-characters"
  //   );

  //   // Remove phone numbers and email addresses
  //   const phoneRegex =
  //     /(\+\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}|\(\d{1,3}\)\s*\d{3,4}[-.\s]??\d{4}|\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4})/g;
  //   const emailBasisRegex = /@[A-Z0-9.-]+\.[A-Z]{2,}/gi;

  //   let match;
  //   let newContent = contentStateWithText;

  //   // Remove phone numbers
  //   while ((match = phoneRegex.exec(text)) !== null) {
  //     const start = match.index;
  //     const end = start + match[0].length;
  //     newContent = Modifier.replaceText(
  //       newContent,
  //       editorState
  //         .getSelection()
  //         .merge({ anchorOffset: start, focusOffset: end }),
  //       ""
  //     );
  //   }

  //   // Remove email addresses at "@" basis
  //   while ((match = emailBasisRegex.exec(text)) !== null) {
  //     const start = match.index;
  //     const end = start + match[0].length;
  //     newContent = Modifier.replaceText(
  //       newContent,
  //       editorState
  //         .getSelection()
  //         .merge({ anchorOffset: start, focusOffset: end }),
  //       ""
  //     );
  //   }

  //   newEditorState = EditorState.push(
  //     newEditorState,
  //     newContent,
  //     "remove-range"
  //   );
  //   formik.setFieldValue("description", newEditorState);
  //   // setEditorState(newEditorState);

  //   return "handled";
  // };
  // console.log(errorMessage);
  // const handleBeforeInput = (chars, editorState) => {
  //   const currentContent = editorState.getCurrentContent();
  //   const selection = editorState.getSelection();

  //   if (selection.isCollapsed()) {
  //     const startKey = selection.getStartKey();
  //     const block = currentContent.getBlockForKey(startKey);
  //     const text = block.getText();

  //     const phoneRegex =
  //       /\+\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}|\(\d{1,3}\)\s*\d{3,4}[-.\s]??\d{4}|\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}/g;
  //     const emailBasisRegex = /@[A-Z0-9.-]+\.[A-Z]{2,}/gi;

  //     let match;
  //     while (
  //       (match = phoneRegex.exec(text)) !== null ||
  //       (match = emailBasisRegex.exec(text)) !== null
  //     ) {
  //       const start = match.index;
  //       const end = start + match[0].length;

  //       if (
  //         selection.getAnchorOffset() >= start &&
  //         selection.getAnchorOffset() <= end
  //       ) {
  //         setErrorMessage(
  //           "Please remove email or phone number before entering text."
  //         );
  //         return "handled";
  //       } else {
  //         setErrorMessage("");
  //       }
  //     }
  //   }

  //   setErrorMessage("");
  //   return "not-handled";
  // };

  console.log(formik.values);
  return (
    <>
      <div className="w-full 700px:max-w-[70%] 300px:max-w-[100%]">
        <form onSubmit={formik.handleSubmit}>
          <div className="flex justify-between items-center">
            <div className="mb-2 flex items-center gap-4 w-auto">
              <div className=" w-[65px] h-[65px]  500px:w-[100px] 500px:h-[100px] rounded-full  relative">
                <label
                  htmlFor={`profileimg`}
                  className=" w-[65px] h-[65px]  500px:w-[100px] 500px:h-[100px] rounded-full cursor-pointer "
                >
                  <Image
                    src={
                      profileImage?.url
                      // "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?q=80&w=1470&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                    }
                    onError={(e) => {
                      setprofileImage({
                        file: null,
                        url: profile,
                      });
                    }}
                    style={{ width: "100%", height: "100%" }}
                    alt="profile img"
                    className=" object-cover rounded-full"
                    width={500}
                    height={500}
                  />
                  <input
                    id={`profileimg`}
                    type="file"
                    accept="image/*"
                    onChange={(event) => handleInputChange(event)}
                    className="mb-2 hidden"
                  />
                  <div className="flex justify-center items-center w-[40px] h-[40px] absolute bg-white bottom-0 right-0 rounded-full">
                    <div className="flex justify-center items-center w-[30px] bg-[#ececec] h-[30px] rounded-full">
                      <IoCameraOutline size={22} />
                    </div>
                  </div>
                </label>
              </div>
              <div className="flex flex-col 500px:gap-2">
                <h1 className="font-semibold text-xl">
                  {ProviderProfileData?.username}
                </h1>
                <p className="text-[#6a6a6a]">{ProviderProfileData?.title}</p>
              </div>
            </div>
            <div className="flex items-center gap-2 p-2 px-4 bg-[#d4d6f7] rounded-lg">
              <MdVerified size={20} className="text-mainpurple" />
              <p className="text-mainpurple font-[500]">Verified</p>
            </div>
          </div>

          <div className="mb-2">
            <div className="flex 700px:flex-row 300px:flex-col items-center gap-2 w-full">
              <div className="700px:w-[49.5%] 300px:w-[100%]">
                <label className="block text-sm text-label mb-2">Name</label>
                <input
                  type="text"
                  name="firstname"
                  value={formik.values.firstname}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="John"
                />
                {formik.errors.firstname &&
                  formik.touched.firstname &&
                  formik.errors.firstname && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.firstname}
                    </p>
                  )}
              </div>

              <div className="700px:w-[49.5%] 300px:w-[100%]">
                <label className="block text-sm text-label mb-2">
                  Last Name
                </label>
                <input
                  type="text"
                  name="lastname"
                  value={formik.values.lastname}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="Doe"
                />
                {formik.errors.lastname &&
                  formik.touched.lastname &&
                  formik.errors.lastname && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.lastname}
                    </p>
                  )}
              </div>
            </div>
          </div>

          <div className="mb-2">
            <div className="flex 700px:flex-row 300px:flex-col items-center gap-2 w-full">
              <div className="700px:w-[49.5%] 300px:w-[100%]">
                <label className="block text-sm text-label mb-2">Country</label>
                <Autocomplete
                  size="small"
                  options={allcountries}
                  id="country"
                  name="country"
                  value={formik.values.country}
                  autoComplete={false}
                  readonly="readonly"
                  onMouseDown={(e) => {
                    e.target.removeAttribute("readonly");
                  }}
                  // className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  getOptionLabel={(option) => option?.label}
                  onChange={(event, value) => {
                    formik.setFieldValue(
                      "country",
                      value
                        ? {
                            value: value?.label,
                            label: value?.label,
                          }
                        : null
                    );
                    // setcountry({
                    //   value: value?.label,
                    //   label: value?.label,
                    // });
                  }}
                  renderOption={(props, option) => {
                    return (
                      <li {...props} key={option}>
                        {option.label}
                      </li>
                    );
                  }}
                  renderInput={(params) => (
                    <TextField
                      style={{ color: "#808080" }}
                      {...params}
                      //   label="Countries"
                      placeholder="Country"
                      sx={{
                        "& .MuiOutlinedInput-root": {
                          borderRadius: "5px",
                          height: "55px",
                        },
                        color: "#9E9E9E",
                        height: "55px",
                        borderRadius: "5px",
                        backgroundColor: "#ffffff",
                      }}
                    />
                  )}
                  sx={{
                    maxWidth: "100%",
                    height: "55px",
                    borderRadius: "8px",
                    backgroundColor: "#ffffff",
                  }}
                />
                {formik.errors.country &&
                  formik.touched.country &&
                  formik.errors.country && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.country}
                    </p>
                  )}
              </div>
              <div className="700px:w-[68.5%] 300px:w-[100%]">
                <label className="block text-sm text-label mb-2">City</label>

                <input
                  type="text"
                  name="city"
                  value={formik.values.city}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="City"
                />
                {formik.errors.city &&
                  formik.touched.city &&
                  formik.errors.city && (
                    <p className="text-red-700 text-sm">{formik.errors.city}</p>
                  )}
              </div>
            </div>
          </div>

          <div className="mb-2 ">
            <label className="block text-sm text-label mb-2">Email</label>
            <div className="relative">
              <input
                placeholder="Email"
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                type="email"
                name="email"
                value={formik.values.email}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                autoComplete={false}
                readonly="readonly"
                onMouseDown={(e) => {
                  e.target.removeAttribute("readonly");
                }}
              />
              {!formik.errors?.email && formik.values?.email && (
                <div
                  className={`absolute font-bold inset-y-0 right-0 px-3 flex justify-center items-center`}
                >
                  {formik.values.email === ProviderProfileData?.email ||
                  screenData?.emailVerified ? (
                    <div className={`flex justify-center items-center gap-1`}>
                      <MdDone color="#abcca5" size={22} />
                      <p className="text-[#92d487]">Verified</p>
                    </div>
                  ) : (
                    <button
                      type="button"
                      className={`absolute justify-center font-bold inset-y-0 right-0 flex items-center px-3 text-[#0066ff] cursor-pointer flex-col whitespace-nowrap`}
                      onClick={() => resendOTP("email")}
                    >
                      Get OTP
                    </button>
                  )}
                </div>
              )}
            </div>
            {formik.errors.email &&
              formik.touched.email &&
              formik.errors.email && (
                <p className="text-red-700 text-sm">{formik.errors.email}</p>
              )}
          </div>

          <div className="mb-2 relative w-full ">
            <label className="block text-sm text-label mb-2 font-[500]">
              Phone Number
            </label>
            <div className="relative">
              <PhoneInput
                inputProps={{
                  maxLength: "16",
                  name: "phonenum",
                  onBlur: formik.handleBlur,
                }}
                inputStyle={{
                  width: "100%",
                  height: "50px",
                }}
                value={formik.values.phonenum}
                onChange={(phone, data) => {
                  formik.setFieldValue("pcountry_code", data.dialCode);
                  formik.setFieldValue("phonenum", phone);
                }}
                containerStyle={{ width: "100%", height: "50px" }}
                country={"pk"}
              />

              {!formik.errors?.phonenum && formik.values?.phonenum && (
                <div
                  className={`absolute font-bold inset-y-0 right-0 px-3 flex justify-center items-center`}
                >
                  {formik.values.phonenum === ProviderProfileData?.phonenum ||
                  screenData?.phoneVerified ? (
                    <div className={`flex justify-center items-center gap-1`}>
                      <MdDone color="#abcca5" size={22} />
                      <p className="text-[#92d487]">Verified</p>
                    </div>
                  ) : (
                    <button
                      type="button"
                      className={`absolute justify-center font-bold inset-y-0 right-0 flex items-center px-3 text-[#0066ff] cursor-pointer flex-col whitespace-nowrap`}
                      onClick={() => resendOTP("phone")}
                    >
                      Get OTP
                    </button>
                  )}
                </div>
              )}
            </div>
            {formik.errors.phonenum &&
              formik.touched.phonenum &&
              formik.errors.phonenum && (
                <p className="text-red-700 text-sm">{formik.errors.phonenum}</p>
              )}
          </div>

          <div className="mb-2 relative w-full ">
            <label className="block text-sm text-label mb-2 font-[500]">
              Whatsapp Number
            </label>
            <div className="relative">
              <PhoneInput
                inputProps={{
                  maxLength: "16",
                  name: "whatsappnum",
                  onBlur: formik.handleBlur,
                }}
                inputStyle={{
                  width: "100%",
                  height: "50px",
                }}
                value={formik.values.whatsappnum}
                onChange={(phone, data) => {
                  formik.setFieldValue("wcountry_code", data.dialCode);
                  formik.setFieldValue("whatsappnum", phone);
                }}
                country={"pk"}
                // value={screenData?.whatsapp}
                // onChange={(phone, data) => {
                //   setScreenData((rest) => ({
                //     ...rest,
                //     whatsappDialCode: data.dialCode,
                //     whatsapp: phone,
                //   }));
                // }}
                containerStyle={{ width: "100%", height: "50px" }}
              />

              {!formik.errors?.whatsappnum && formik.values?.whatsappnum && (
                <div
                  className={`absolute font-bold inset-y-0 right-0 px-3 flex justify-center items-center`}
                >
                  {formik.values.whatsappnum ===
                    ProviderProfileData?.whatsappnum ||
                  screenData?.whatsappVerified ? (
                    <div className={`flex justify-center items-center gap-1`}>
                      <MdDone color="#abcca5" size={22} />
                      <p className="text-[#92d487]">Verified</p>
                    </div>
                  ) : (
                    <button
                      type="button"
                      className={`absolute justify-center font-bold inset-y-0 right-0 flex items-center px-3 text-[#0066ff] cursor-pointer flex-col whitespace-nowrap`}
                      onClick={() => resendOTP("whatsapp")}
                    >
                      Get OTP
                    </button>
                  )}
                </div>
              )}
            </div>
            {formik.errors.whatsappnum &&
              formik.touched.whatsappnum &&
              formik.errors.whatsappnum && (
                <p className="text-red-700 text-sm">
                  {formik.errors.whatsappnum}
                </p>
              )}
          </div>

          <div className="mb-2">
            <div className="flex items-center gap-2 w-full">
              <div className="w-[49.5%]">
                <label className="block text-sm text-label mb-2">
                  Language
                </label>
                <Autocomplete
                  multiple
                  limitTags={2}
                  size="small"
                  fullWidth
                  name="language"
                  autoComplete={false}
                  readonly="readonly"
                  onMouseDown={(e) => {
                    e.target.removeAttribute("readonly");
                  }}
                  // sx={{ maxWidth: "230px" }}
                  id="multiple-limit-tags-subcategory"
                  options={AllLanguage || []}
                  value={formik.values.language}
                  onChange={(event, newValue) => {
                    formik.setFieldValue("language", newValue);
                  }}
                  renderOption={(props, option) => <li {...props}>{option}</li>}
                  renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                      <Chip
                        key={option}
                        label={option}
                        {...getTagProps({ index })}
                        style={{ backgroundColor: "#c7c9f4" }}
                      />
                    ))
                  }
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      sx={{
                        "& .MuiOutlinedInput-root": {
                          // borderRadius: "8px",
                          minHeight: "53px",
                        },
                        color: "#9E9E9E",
                        width: "100%",
                      }}
                    />
                  )}
                />
                {formik.errors.language &&
                  formik.touched.language &&
                  formik.errors.language && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.language}
                    </p>
                  )}
                {/* <select
                  id="language"
                  className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  value={formik.values.language}
                  name="language"
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                >
                  <option value={""}>Select</option>
                  {AllLanguage?.map((item, i) => {
                    return (
                      <option key={i} value={item}>
                        {item}
                      </option>
                    );
                  })}
                </select>
                {formik.errors.language &&
                  formik.touched.language &&
                  formik.errors.language && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.language}
                    </p>
                  )} */}
              </div>

              <div className="w-[49.5%]">
                <label className="block text-sm text-label mb-2">
                  Time zone
                </label>
                <TimezoneSelect
                  styles={customStyles}
                  value={selectedTimezone}
                  onChange={setSelectedTimezone}
                  onBlur={formik.handleBlur}
                />

                {/* {errors && errors?.language ? (
          <p className="text-red-700 text-sm self-end"> Please Enter Language</p>
        ) : null} */}
              </div>
            </div>
          </div>

          <div className="flex 700px:flex-row 300px:flex-col items-center justify-center gap-2 mb-4 mt-4w-full">
            {/* <button className="px-5 py-3 border border-gray-400 flex items-center gap-2 rounded-md flex-grow justify-center">
          <FcGoogle size={25} className="" />
          <p>Google</p>
        </button> */}
            {/* <div className="700px:w-[49.5%] 300px:w-[100%] px-5 py-3 border border-border flex items-center gap-2 rounded-md flex-grow justify-center bg-white">
            <IconContext.Provider value={{ color: "#3b5998" }}>
              <BiLogoFacebookCircle size={25} className="" />
            </IconContext.Provider>
            <p>Facebook</p>
          </div>{" "} */}
            <div className="700px:w-[49.5%] 300px:w-[100%] px-5 py-3 border border-border flex items-center gap-1 justify-between  rounded-md   bg-white">
              <div className="flex items-center gap-2">
                <IconContext.Provider value={{ color: "#3b5998" }}>
                  <BiLogoFacebookCircle size={25} className="" />
                </IconContext.Provider>
                <p>Facebook</p>
              </div>
              <p className="text-[#E81D1D]">disconnect</p>
            </div>
            <div className="700px:w-[49.5%] 300px:w-[100%] px-5 py-3 border border-border flex items-center gap-1 justify-between  rounded-md   bg-white">
              <div className="flex items-center gap-2">
                <IconContext.Provider value={{ color: "#3b5998" }}>
                  <FaLinkedin size={25} className="" />
                </IconContext.Provider>
                <p>LinkedIn</p>
              </div>
              <p className="text-[#E81D1D]">disconnect</p>
            </div>
          </div>

          <div className="mb-2">
            <Editor
              placeholder="Description"
              name={`description`}
              editorState={formik.values.description}
              // value={feature.description}
              // onEditorStateChange={(editorState) => {
              //   formik.setFieldValue("description", editorState);
              //   // onEditorStateChange(index, editorState)
              // }}
              onEditorStateChange={handleOnChange}
              editorStyle={{
                backgroundColor: "#ffffff",
                // height:"100px"
              }}
              // handlePastedText={handlePastedText}
              // handleBeforeInput={handleBeforeInput}
              className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            />
            {/* <textarea
              type="text"
              rows={6}
              name="description"
              value={formik.values.description}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="Description"
            /> */}
            {errorMessage && <div style={{ color: "red" }}>{errorMessage}</div>}
            {formik.errors.description &&
              formik.touched.description &&
              formik.errors.description && (
                <p className="text-red-700 text-sm">
                  {formik.errors.description}
                </p>
              )}
          </div>

          <div className="w-[fit-content] pt-6">
            {loader ? (
              <CircularProgress />
            ) : (
              <button
                type="submit"
                className="bg-[#434CD9] flex-grow w-full px-6 py-3 rounded-md text-white"
              >
                Save Changes
              </button>
            )}
          </div>
        </form>
      </div>

      <OtpVerificationDialog
        open={showOtpVerficationDialog}
        onClose={() => {
          setShowOtpVerficationDialog(false);
          setScreenData((data) => ({
            ...data,
            otpFor: "",
          }));
        }}
        screenData={screenData}
        setScreenData={setScreenData}
      />
    </>
  );
};

export default ProfileForm;
