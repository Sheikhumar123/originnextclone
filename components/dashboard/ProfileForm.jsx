"use client";

import Image from "next/image";
import { useEffect, useState } from "react";
import { IoCameraOutline } from "react-icons/io5";
import { MdVerified } from "react-icons/md";
import { Country, State, City } from "country-state-city";
import { Autocomplete, CircularProgress, TextField } from "@mui/material";
import { MdDone } from "react-icons/md";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import { AllLanguage } from "../Language/Language";
import TimezoneSelect from "react-timezone-select";
import { IconContext } from "react-icons";
import { BiLogoFacebookCircle } from "react-icons/bi";
import { FaLinkedin } from "react-icons/fa";
import {
  GetProviderProfileData,
  GetProviderdashboardDetails,
} from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import { useDispatch, useSelector } from "react-redux";
import profile from "../../images/dashboard/profile.png";
import Cookies from "js-cookie";
import { useFormik } from "formik";
import * as Yup from "yup";
import { EditProviderProfileApi } from "@/Apis/ServiceProviderApis/DashboardApi/EditProfileApi";
import { toast } from "react-toastify";

const ProfileForm = () => {
  const [allcountries, setallcountries] = useState([]);
  const [selectedTimezone, setSelectedTimezone] = useState({});
  const [profileImage, setprofileImage] = useState(null);
  const { ProviderProfileData, status, error } = useSelector(
    GetProviderdashboardDetails
  );
  console.log(ProviderProfileData);
  console.log(status);
  console.log(error);
  const dispatch = useDispatch();
  console.log(selectedTimezone);
  useEffect(() => {
    dispatch(GetProviderProfileData(Cookies.get("user_id")));
    if ( error) {
      toast.error(error);
    }
    let value = Country.getAllCountries()?.map((data) => {
      let obj = {
        label: data?.name,
        value: data,
      };
      return obj;
    });
    setallcountries(value);
  }, [dispatch,]);
  const customStyles = {
    control: (base) => ({
      ...base,
      height: 54,
      minHeight: 54,
    }),
  };
  const [loader, setloader] = useState(false);
  // const cookieStore = cookies()

  const initialValues = {
    firstname: "",
    lastname: "",
    email: "",
    country: null,
    city: "",
    pcountry_code: "",
    phonenum: "",
    wcountry_code: "",
    whatsappnum: "",
    language: "",
    description: "",
  };
  const ValidationSchema = Yup.object().shape({
    firstname: Yup.string().required("Please Enter First Name"),
    lastname: Yup.string().required("Please Enter Last Name"),
    country: Yup.object().nullable().required("Please Enter Country"),
    email: Yup.string()
      .email("Invalid email address")
      .required("Please Enter  Email"),
    city: Yup.string().required("Please Enter City"),
    phonenum: Yup.string().required("Please Enter Phone Number"),
    whatsappnum: Yup.string().required("Please Enter Watsapp Number"),
  });
  useEffect(() => {
    if (ProviderProfileData) {
      formik.setFieldValue("firstname", ProviderProfileData?.firstname);
      formik.setFieldValue("lastname", ProviderProfileData?.lastname);
      formik.setFieldValue("email", ProviderProfileData?.email);
      formik.setFieldValue("country", {
        label: ProviderProfileData?.country,
        value: ProviderProfileData?.country,
      });
      formik.setFieldValue("city", ProviderProfileData?.city);
      formik.setFieldValue("phonenum", ProviderProfileData?.phonenum);
      formik.setFieldValue("whatsappnum", ProviderProfileData?.whatsappnum);
      formik.setFieldValue("language", ProviderProfileData?.language);
      formik.setFieldValue("description", ProviderProfileData?.description);
      setprofileImage(
        ProviderProfileData?.profile_image
          ? {
              file: ProviderProfileData?.profile_image,
              url: ProviderProfileData?.profile_image_url,
            }
          : {
              file: null,
              url: profile,
            }
      );
    }
  }, [ProviderProfileData]);
  const handleSubmit = async (values, setSubmitting, resetForm) => {
    if (!selectedTimezone) {
      return toast.error("Please Enter Time Zone");
    } else {
      const formData = new FormData();
      formData.append(`user_id`, Cookies.get("user_id"));
      formData.append(`firstname`, values.firstname);
      formData.append(`lastname`, values.lastname);
      formData.append(`country`, values.country?.label);
      formData.append(`city`, values.city);
      formData.append(`pcountry_code`, values.pcountry_code);
      formData.append(`phonenum`, values.phonenum);
      formData.append(`wcountry_code`, values.wcountry_code);
      formData.append(`whatsappnum`, values.whatsappnum);
      formData.append(`language`, values.language);
      formData.append(`description`, values.description);
      formData.append(`time_zone`, JSON.stringify(selectedTimezone));
      formData.append(`linkedin`, "");
      formData.append(`facebook`, "");
      formData.append(`fb_verified`, false);
      formData.append(`linkedin_verified`, false);
      formData.append(`profile_image`, profileImage?.file);

      setloader(true);
      const response = await EditProviderProfileApi(formData);
      setloader(false);
      if (response?.data?.status === "1") {
        toast.success(response.data.message);
        dispatch(GetProviderProfileData(Cookies.get("user_id")));
      } else if (response?.data?.status === "0") {
        toast.error(response.data.message);
      } else {
        toast.error(response?.response?.data.message);
      }
    }
  };
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  console.log(formik.values);
  const handleInputChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onloadend = () => {
      setprofileImage({
        file: file,
        url: reader.result,
      });
    };
    if (file) {
      reader.readAsDataURL(file);
    }
  };
  return (
    <div className="w-full 700px:max-w-[70%] 300px:max-w-[100%]">
      <form onSubmit={formik.handleSubmit}>
        <div className="flex justify-between items-center">
          <div className="mb-2 flex items-center gap-4 w-auto">
            <div className=" w-[65px] h-[65px]  500px:w-[100px] 500px:h-[100px] rounded-full  relative">
              <label
                htmlFor={`profileimg`}
                className=" w-[65px] h-[65px]  500px:w-[100px] 500px:h-[100px] rounded-full cursor-pointer "
              >
                <Image
                  src={
                    profileImage?.url
                    // "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?q=80&w=1470&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
                  }
                  onError={(e) => {
                    setprofileImage({
                      file: null,
                      url: profile,
                    });
                  }}
                  style={{ width: "100%", height: "100%" }}
                  alt="profile img"
                  className=" object-cover rounded-full"
                  width={500}
                  height={500}
                />
                <input
                  id={`profileimg`}
                  type="file"
                  accept="image/*"
                  onChange={(event) => handleInputChange(event)}
                  className="mb-2 hidden"
                />
                <div className="flex justify-center items-center w-[40px] h-[40px] absolute bg-white bottom-0 right-0 rounded-full">
                  <div className="flex justify-center items-center w-[30px] bg-[#ececec] h-[30px] rounded-full">
                    <IoCameraOutline size={22} />
                  </div>
                </div>
              </label>
            </div>
            <div className="flex flex-col 500px:gap-2">
              <h1 className="font-semibold text-xl">
                {ProviderProfileData?.username}
              </h1>
              <p className="text-[#6a6a6a]">{ProviderProfileData?.title}</p>
            </div>
          </div>
          <div className="flex items-center gap-2 p-2 px-4 bg-[#d4d6f7] rounded-lg">
            <MdVerified size={20} className="text-mainpurple" />
            <p className="text-mainpurple font-[500]">Verified</p>
          </div>
        </div>
        <div className="mb-2">
          <div className="flex 700px:flex-row 300px:flex-col items-center gap-2 w-full">
            <div className="700px:w-[49.5%] 300px:w-[100%]">
              <label className="block text-sm text-label mb-2">Name</label>
              <input
                type="text"
                name="firstname"
                value={formik.values.firstname}
                onChange={formik.handleChange}
                className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="John"
              />
              {formik.errors.firstname &&
                formik.touched.firstname &&
                formik.errors.firstname && (
                  <p className="text-red-700 text-sm">
                    {formik.errors.firstname}
                  </p>
                )}
            </div>
            <div className="700px:w-[49.5%] 300px:w-[100%]">
              <label className="block text-sm text-label mb-2">Last Name</label>

              <input
                type="text"
                name="lastname"
                value={formik.values.lastname}
                onChange={formik.handleChange}
                className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Doe"
              />
              {formik.errors.lastname &&
                formik.touched.lastname &&
                formik.errors.lastname && (
                  <p className="text-red-700 text-sm">
                    {formik.errors.lastname}
                  </p>
                )}
            </div>
          </div>
        </div>
        <div className="mb-2">
          <div className="flex 700px:flex-row 300px:flex-col items-center gap-2 w-full">
            <div className="700px:w-[49.5%] 300px:w-[100%]">
              <label className="block text-sm text-label mb-2">Country</label>
              <Autocomplete
                size="small"
                options={allcountries}
                id="country"
                name="country"
                value={formik.values.country}
                // className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                getOptionLabel={(option) => option.label}
                onChange={(event, value) => {
                  console.log(value);
                  formik.setFieldValue("country", {
                    value: value?.label,
                    label: value?.label,
                  });
                  // setcountry({
                  //   value: value?.label,
                  //   label: value?.label,
                  // });
                }}
                renderOption={(props, option) => {
                  return (
                    <li {...props} key={option}>
                      {option.label}
                    </li>
                  );
                }}
                renderInput={(params) => (
                  <TextField
                    style={{ color: "#808080" }}
                    {...params}
                    //   label="Countries"
                    placeholder="Country"
                    sx={{
                      "& .MuiOutlinedInput-root": {
                        borderRadius: "5px",
                        height: "55px",
                      },
                      color: "#9E9E9E",
                      height: "55px",
                      borderRadius: "5px",
                      backgroundColor: "#ffffff",
                    }}
                  />
                )}
                sx={{
                  maxWidth: "100%",
                  height: "55px",
                  borderRadius: "8px",
                  backgroundColor: "#ffffff",
                }}
              />
              {formik.errors.country &&
                formik.touched.country &&
                formik.errors.country && (
                  <p className="text-red-700 text-sm">
                    {formik.errors.country}
                  </p>
                )}
            </div>
            <div className="700px:w-[68.5%] 300px:w-[100%]">
              <label className="block text-sm text-label mb-2">City</label>

              <input
                type="text"
                name="city"
                value={formik.values.city}
                onChange={formik.handleChange}
                className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="City"
              />
              {formik.errors.city &&
                formik.touched.city &&
                formik.errors.city && (
                  <p className="text-red-700 text-sm">{formik.errors.city}</p>
                )}
            </div>
          </div>
        </div>
        <div className="mb-2 ">
          <label className="block text-sm text-label mb-2">Email</label>
          <div className="relative">
            <input
              type="email"
              name="email"
              disabled
              value={formik.values.email}
              onChange={formik.handleChange}
              className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="Email"
            />
            {formik.errors.email &&
              formik.touched.email &&
              formik.errors.email && (
                <p className="text-red-700 text-sm">{formik.errors.email}</p>
              )}
            <div
              className={`absolute justify-center font-bold inset-y-0 right-0 flex items-center px-3 gap-1 items-center
               `}
            >
              <MdDone color="#abcca5" size={22} />
              <p className="text-[#92d487]">Verified</p>
            </div>
          </div>
        </div>
        <div className="mb-2 relative w-full ">
          <label className="block text-sm text-label mb-2 font-[500]">
            Phone Number
          </label>
          <div className="relative">
            <PhoneInput
              disabled
              inputProps={{
                maxLength: "16",
                name: "phonenum",
                onBlur: formik.handleBlur,
              }}
              inputStyle={{
                width: "100%",
                height: "50px",
              }}
              value={formik.values.phonenum}
              onChange={(phone, data) => {
                formik.setFieldValue("pcountry_code", data.dialCode);
                formik.setFieldValue("phonenum", phone);
              }}
              containerStyle={{ width: "100%", height: "50px" }}
              country={"pk"}
            />
            {formik.errors.phonenum &&
              formik.touched.phonenum &&
              formik.errors.phonenum && (
                <p className="text-red-700 text-sm">{formik.errors.phonenum}</p>
              )}
            <button
              type="button"
              className={`absolute justify-center font-bold inset-y-0 right-0 flex items-center px-3   flex-col`}
            >
              <MdVerified size={20} className="text-mainpurple" />
            </button>
          </div>
        </div>
        <div className="mb-2 relative w-full ">
          <label className="block text-sm text-label mb-2 font-[500]">
            Whatsapp Number
          </label>
          <div className="relative">
            <PhoneInput
              disabled
              inputProps={{
                maxLength: "16",
                name: "whatsappnum",
                onBlur: formik.handleBlur,
              }}
              inputStyle={{
                width: "100%",
                height: "50px",
              }}
              value={formik.values.whatsappnum}
              onChange={(phone, data) => {
                formik.setFieldValue("wcountry_code", data.dialCode);
                formik.setFieldValue("whatsappnum", phone);
              }}
              country={"pk"}
              // value={screenData?.whatsapp}
              // onChange={(phone, data) => {
              //   setScreenData((rest) => ({
              //     ...rest,
              //     whatsappDialCode: data.dialCode,
              //     whatsapp: phone,
              //   }));
              // }}
              containerStyle={{ width: "100%", height: "50px" }}
            />
            {formik.errors.whatsappnum &&
              formik.touched.whatsappnum &&
              formik.errors.whatsappnum && (
                <p className="text-red-700 text-sm">
                  {formik.errors.whatsappnum}
                </p>
              )}
            <button
              type="button"
              className={`absolute justify-center text-[#0066ff] font-bold inset-y-0 right-0 flex items-center px-3 flex-col`}
              // onClick={() => {
              //   handleVerifyNumber("whatsappnum");
              // }}
            >
              Verify
            </button>
          </div>
        </div>
        <div className="mb-2">
          <div className="flex items-center gap-2 w-full">
            <div className="w-[49.5%]">
              <label className="block text-sm text-label mb-2">Language</label>
              <select
                id="language"
                className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                value={formik.values.language}
                name="language"
                onChange={formik.handleChange}
              >
                <option value={""}>Select</option>
                {AllLanguage?.map((item, i) => {
                  return (
                    <option key={i} value={item}>
                      {item}
                    </option>
                  );
                })}
              </select>
              {formik.errors.language &&
                formik.touched.language &&
                formik.errors.language && (
                  <p className="text-red-700 text-sm">
                    {formik.errors.language}
                  </p>
                )}
            </div>

            <div className="w-[49.5%]">
              <label className="block text-sm text-label mb-2">Time zone</label>
              <TimezoneSelect
                styles={customStyles}
                value={selectedTimezone}
                onChange={setSelectedTimezone}
              />

              {/* {errors && errors?.language ? (
          <p className="text-red-700 text-sm self-end"> Please Enter Language</p>
        ) : null} */}
            </div>
          </div>
        </div>
        <div className="flex 700px:flex-row 300px:flex-col items-center justify-center gap-2 mb-4 mt-4w-full">
          {/* <button className="px-5 py-3 border border-gray-400 flex items-center gap-2 rounded-md flex-grow justify-center">
          <FcGoogle size={25} className="" />
          <p>Google</p>
        </button> */}
          {/* <div className="700px:w-[49.5%] 300px:w-[100%] px-5 py-3 border border-border flex items-center gap-2 rounded-md flex-grow justify-center bg-white">
            <IconContext.Provider value={{ color: "#3b5998" }}>
              <BiLogoFacebookCircle size={25} className="" />
            </IconContext.Provider>
            <p>Facebook</p>
          </div>{" "} */}
          <div className="700px:w-[49.5%] 300px:w-[100%] px-5 py-3 border border-border flex items-center gap-1 justify-between  rounded-md   bg-white">
            <div className="flex items-center gap-2">
              <IconContext.Provider value={{ color: "#3b5998" }}>
                <BiLogoFacebookCircle size={25} className="" />
              </IconContext.Provider>
              <p>Facebook</p>
            </div>
            <p className="text-[#E81D1D]">disconnect</p>
          </div>
          <div className="700px:w-[49.5%] 300px:w-[100%] px-5 py-3 border border-border flex items-center gap-1 justify-between  rounded-md   bg-white">
            <div className="flex items-center gap-2">
              <IconContext.Provider value={{ color: "#3b5998" }}>
                <FaLinkedin size={25} className="" />
              </IconContext.Provider>
              <p>LinkedIn</p>
            </div>
            <p className="text-[#E81D1D]">disconnect</p>
          </div>
        </div>
        <div className="mb-2">
          <input
            type="text"
            name="description"
            value={formik.values.description}
            onChange={formik.handleChange}
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="LOREUM ISPUM"
          />
          {formik.errors.description &&
            formik.touched.description &&
            formik.errors.description && (
              <p className="text-red-700 text-sm">
                {formik.errors.description}
              </p>
            )}
        </div>

        <div className="w-[fit-content] pt-6">
          {loader ? (
            <CircularProgress />
          ) : (
            <button
              type="submit"
              className="bg-[#434CD9] flex-grow w-full px-6 py-3 rounded-md text-white"
            >
              Save Changes
            </button>
          )}
        </div>
      </form>
    </div>
  );
};

export default ProfileForm;
