"use client";
import { addContactApi } from "@/Apis/ServiceProviderApis/DashboardApi/ContactApi";
import { CircularProgress } from "@mui/material";
import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import * as Yup from "yup";
import Cookies from "js-cookie";
import {
  GetProviderProfileData,
  GetProviderdashboardDetails,
} from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import { useDispatch, useSelector } from "react-redux";

const ContactForm = ({ type }) => {
  const [loader, setloader] = useState(false);
  const dispatch = useDispatch();
  const { ProviderProfileData, status } = useSelector(
    GetProviderdashboardDetails
  );

  const initialValues = {
    firstname: "",
    lastname: "",
    email: Cookies.get("email"),
    message: "",
  };
  const ValidationSchema = Yup.object().shape({
    firstname: Yup.string().required("Please Enter First Name"),
    lastname: Yup.string().required("Please Enter Last Name"),
    message: Yup.string().required("Please Enter Message"),
    email: Yup.string()
      .email("Invalid email address")
      .required("Please Enter  Email"),
  });
  useEffect(() => {
    dispatch(GetProviderProfileData(Cookies.get("user_id")));
    if (status == "rejected" && error) {
      toast.error(error);
    }
  }, [dispatch]);

  useEffect(() => {
    if (ProviderProfileData) {
      formik.setFieldValue("firstname", ProviderProfileData?.firstname);
      formik.setFieldValue("lastname", ProviderProfileData?.lastname);
      formik.setFieldValue("email", ProviderProfileData?.email);
    }
  }, [ProviderProfileData]);

  const handleSubmit = async (values, resetForm) => {
    let obj = {
      ...values,
      user_id: Cookies.get("user_id"),
      usertype: type ? type : "ServiceProvider",
    };
    setloader(true);
    const response = await addContactApi(obj);
    setloader(false);
    if (response?.data?.status == "1") {
      resetForm();
      toast.success(response.data.message);
    } else if (response?.data?.status == "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response?.response?.data.message);
    }
  };
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  return (
    <div className="w-full">
      <form onSubmit={formik.handleSubmit}>
        <div className="mb-2">
          <div className="flex 600px:flex-row 300px:flex-col items-center gap-3 w-full">
            <div className="600px:w-[49.5%] 300px:w-[100%]">
              <label className="block text-sm text-label mb-2">Name</label>
              <input
                type="text"
                name="firstname"
                // disabled
                value={formik.values.firstname}
                onChange={formik.handleChange}
                className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="John"
              />
              {formik.errors.firstname &&
                formik.touched.firstname &&
                formik.errors.firstname && (
                  <p className="text-red-700 text-sm">
                    {formik.errors.firstname}
                  </p>
                )}
            </div>
            <div className="600px:w-[49.5%] 300px:w-[100%]">
              <label className="block text-sm text-label mb-2">Last Name</label>

              <input
                type="text"
                name="lastname"
                // disabled
                value={formik.values.lastname}
                onChange={formik.handleChange}
                className="w-full flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Doe"
              />
              {formik.errors.lastname &&
                formik.touched.lastname &&
                formik.errors.lastname && (
                  <p className="text-red-700 text-sm">
                    {formik.errors.lastname}
                  </p>
                )}
            </div>
          </div>
        </div>
        <div className="mb-2">
          <label className="block text-sm text-label mb-2">Email</label>
          <input
            type="email"
            name="email"
            // disabled
            value={formik.values.email}
            onChange={formik.handleChange}
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="John Doe"
          />
          {formik.errors.email &&
            formik.touched.email &&
            formik.errors.email && (
              <p className="text-red-700 text-sm">{formik.errors.email}</p>
            )}
        </div>
        <div className="mb-2">
          <label className="block text-sm text-label mb-2">Message</label>
          <textarea
            name="message"
            value={formik.values.message}
            onChange={formik.handleChange}
            cols="30"
            rows="5"
            placeholder="Your message here..."
            className="w-full border p-2 rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          ></textarea>
          {formik.errors.message &&
            formik.touched.message &&
            formik.errors.message && (
              <p className="text-red-700 text-sm">{formik.errors.message}</p>
            )}
        </div>
        {loader ? (
          <CircularProgress />
        ) : (
          <button
            type="submit"
            className="bg-[#434CD9] flex-grow w-full md:w-auto px-12 py-3 rounded-3xl text-white"
          >
            Send Message
          </button>
        )}
      </form>
    </div>
  );
};

export default ContactForm;
