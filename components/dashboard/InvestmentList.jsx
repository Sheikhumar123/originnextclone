"use client";

import { FaAngleDown } from "react-icons/fa6";
import { SlOptionsVertical } from "react-icons/sl";
import { investmentData } from "@/data/dashboardData";
import Table from "../Table/Table";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import {
  GetAllInvestment,
  GetInvestDetails,
} from "@/redux/Slices/InvestorDashboard/InvestSlice/InvestSlice";
import Cookies from "js-cookie";
import ScreenLoader from "../ScreenLoader/ScreenLoader";
import moment from "moment";
import Pagination from "../Pagination/Pagination";
const InvestmentList = ({ setshowForm }) => {
  const { AllInvestment, status } = useSelector(GetInvestDetails);
  const [allInvestment, setallInvestment] = useState([]);
  const [count, setcount] = useState(0);
  const [limit, setlimit] = useState(0);
  const UserId = Cookies.get("user_id");
  const dispatch = useDispatch();
  const [page, setpage] = useState(1);
  console.log(allInvestment);

  useEffect(() => {
    let obj = {
      user_id: UserId,
      type: "Investment",
      page: page,
    };
    dispatch(GetAllInvestment(obj));
  }, [dispatch, page]);

  useEffect(() => {
    if (AllInvestment) {
      setcount(AllInvestment?.count);
      setlimit(AllInvestment?.limit);
      let arr = [];
      AllInvestment?.Transactions?.forEach((item) => {
        arr.push({
          ...item,
          date: item?.createdAt
            ? moment(item?.createdAt).format("DD/MM/YYYY")
            : "",
          amount: Number(item?.amount)?.toLocaleString(),
          fee: Number(item?.fee)?.toLocaleString(),
        });
      });
      setallInvestment(arr);
    }
  }, [AllInvestment]);
  const columns = [
    { label: "Date", value: "date" },
    { label: "Mode", value: "payment_method" },
    { label: "Amount", value: "amount" },
    { label: "Fee", value: "fee" },
    { label: "Total Amount", value: "total_amount" },
    // { label: "Description", value: "description" },
    { label: "Status", value: "status" },
  ]; // Define table columns
  const renderBodyContent = (column, item) => {
    if (column?.label === "Status") {
      return (
        <div
          className={`flex gap-1 items-center w-[fit-content] rounded-md ${
            item.status === "Received"
              ? "text-black bg-[#E0FFE9]"
              : item.status === "Decline"
              ? "text-red-600 bg-[#F7B0B069]"
              : "text-black bg-[#dbeafe]"
          } px-3 py-1`}
        >
          <div
            className={`w-[7px] h-[7px] rounded-full ${
              item.status === "Received"
                ? " bg-[#00ad31]"
                : item.status === "Decline"
                ? "bg-red-500"
                : " bg-sky-500"
            }`}
          ></div>
          <div>{item.status}</div>
        </div>
      );
    }
    return item[column?.value];
  };
  // const actions = [
  //   // {
  //   //   type: "toogle",
  //   //   onClick: (item) => {
  //   //     console.log(item);
  //   //     // setrowData(item);
  //   //     // setstatusDialoge(true);
  //   //   },
  //   // },
  //   // {
  //   //   type: "icon",
  //   //   label: "View",
  //   //   buttonClass: "bg-red-500 text-white px-4 py-2 rounded",
  //   //   icon: <MdVisibility size={18} />,
  //   //   onClick: (item) => {
  //   //     setshowDetailModal(true);
  //   //     setrowData(item);
  //   //   },
  //   // },
  //   // {
  //   //   type: "icon",
  //   //   label: "Edit",
  //   //   buttonClass: "bg-red-500 text-white px-4 py-2 rounded",
  //   //   icon: <FiEdit size={18} />,
  //   //   onClick: (item) => {
  //   //     setisEdit(true);
  //   //     setrowData(item);
  //   //     setshowForm(true);
  //   //   },
  //   // },
  //   // {
  //   //   type: "icon",
  //   //   label: "Delete",
  //   //   buttonClass: "bg-red-500 text-white px-4 py-2 rounded",
  //   //   icon: <MdDelete size={18} />,
  //   //   onClick: (item) => {
  //   //     setdeleteDialog(true);
  //   //     setrowData(item);
  //   //   },
  //   // },
  // ];
  return (
    <>
      {status == "pending" && <ScreenLoader />}
      <div className="w-full flex justify-end">
        <button
          type="button"
          onClick={() => {
            setshowForm(true);
          }}
          className="bg-[#434CD9] w-[fit-content]  self-center text-nowrap px-4 py-2 rounded-md text-white"
        >
          Invest More
        </button>
      </div>
      <div className="relative overflow-x-auto shadow-md sm:rounded-lg mt-4">
        <Table
          columns={columns}
          data={allInvestment}
          // actions={actions}
          renderBody={renderBodyContent}
        />
      </div>
      {count > limit ? (
        <div className="pt-4 pb-[130px] flex justify-end px-2">
          <Pagination
            pageCount={page}
            setPageCount={setpage}
            totalcount={count}
            limit={limit}
            showCount={true}
          />
        </div>
      ) : null}
      {/* <div className="w-full pt-4">
      <div className="relative overflow-auto scrollbar-thin max-h-[600px] rounded-xl w-full">
        <table className="w-full text-sm text-left rtl:text-right text-gray-500 ">
          <thead className="text-xs text-black uppercase bg-gray-200  ">
            <tr>
              <th scope="col" className="px-6 py-3">
                Sr
              </th>
              <th scope="col" className="px-6 py-3">
                Date
              </th>
              <th scope="col" className="px-6 py-3">
                Amount
              </th>
              <th scope="col" className="px-6 py-3">
                Tax
              </th>
              <th scope="col" className="px-6 py-3">
                Mode
              </th>

              <th scope="col" className="px-6 py-3">
                Status
              </th>
            </tr>
          </thead>
          <tbody>
            {investmentData.map((item, index) => (
              <tr className="bg-white text-black text-nowrap " key={index}>
                <td className="px-6 py-4">{item.sr}</td>
                <td className="px-6 py-4">{item.date}</td>
                <td className="px-6 py-4">{item.amount}</td>
                <td className="px-6 py-4">{item.tax}</td>
                <td className="px-6 py-4">{item.mode}</td>
                <td className="px-3 py-2 ">
                  <span
                    className={`px-3 py-2 font-[500]  rounded-md ${
                      item.status === "Verified"
                        ? "text-black bg-[#E0FFE9]"
                        : "text-red-600 bg-[#F7B0B069]"
                    }`}
                  >
                    {item.status}
                  </span>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div> */}
    </>
  );
};

export default InvestmentList;
