"use client";
import { useEffect, useState } from "react";
import moment from "moment";
import Table from "@/components/Table/Table";
import Pagination from "@/components/Pagination/Pagination";
function CodesList({VendorCodesData,page,setpage}) {
  const [allCredits, setallCredits] = useState([]);
  const [count, setcount] = useState(0);
  const [limit, setlimit] = useState(0);
  const [loader, setloader] = useState(false);
  console.log(allCredits);



  useEffect(() => {
    if (VendorCodesData) {
      setcount(VendorCodesData?.count);
      setlimit(VendorCodesData?.limit);
      let arr = [];
      VendorCodesData?.AllCodes?.Codes?.forEach((item) => {
        console.log(item?.date);
        arr.push({
          ...item,
          date:item?.date
            ? moment(item?.date).format("DD/MM/YYYY")
            : "",
          amount: Number(item?.amount)?.toLocaleString(),
        });
      });
      setallCredits(arr);
    }
  }, [VendorCodesData]);
  const columns = [
    { label: "Date", value: "date" },
    { label: "Name", value: "username" },
    { label: "Amount", value: "amount" },
    { label: "Code", value: "code" },
    // { label: "Description", value: "description" },
    { label: "Status", value: "status" },
  ]; // Define table columns
  const renderBodyContent = (column, item) => {
    if (column?.label === "Status") {
      return (
        <div
          className={`flex gap-1 items-center w-[fit-content] rounded-md ${
            item.status === "Used"
              ? "text-black bg-[#E0FFE9]"
              : item.status === "Decline"
              ? "text-red-600 bg-[#F7B0B069]"
              : "text-black bg-[#dbeafe]"
          } px-3 py-1`}
        >
          <div
            className={`w-[7px] h-[7px] rounded-full ${
              item.status === "Used"
                ? " bg-[#00ad31]"
                : item.status === "Decline"
                ? "bg-red-500"
                : " bg-sky-500"
            }`}
          ></div>
          <div>{item.status}</div>
        </div>
      );
    }
    return item[column?.value];
  };

  return (
    <>



      {/* <div className="mb-4">
        <h1 className="text-2xl font-semibold mb-4 text-gray-600">Modes</h1>
        <div className="flex gap-2 flex-wrap w-full ">
          {paymentTypeData.map((item, key) => (
            <label
              key={key * 3}
              htmlFor={item.name}
              className="relative flex-1 cursor-pointer"
            >
              <input
                type="radio"
                className="peer hidden"
                id={item.name}
                name="paymentType"
                value={item?.name}
              />
              <div className="flex  items-center justify-center p-8 gap-5 h-28   bg-white border-2 border-gray-200 rounded-md transition peer-checked:border-blue-500  peer-checked:shadow-lg peer-checked:-translate-y-1 ">
                {item.name}
              </div>
            </label>
          ))}
          <button className="bg-[#434CD9] flex-1 h-16 self-center text-nowrap px-6 py-3 rounded-[999px] text-white">
            Invest Now
          </button>
        </div>
      </div> */}

      <div className="relative overflow-x-auto shadow-md sm:rounded-lg mt-4">
        <Table
          columns={columns}
          data={allCredits}
          // actions={actions}
          renderBody={renderBodyContent}
        />
      </div>
      {count > limit ? (
        <div className="pt-4 pb-[130px] flex justify-end px-2">
          <Pagination
            pageCount={page}
            setPageCount={setpage}
            totalcount={count}
            limit={limit}
            showCount={true}
          />
        </div>
      ) : null}
    </>
  );
}

export default CodesList;
