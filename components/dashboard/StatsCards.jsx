"use client";
import ReactApexChart from "react-apexcharts";

const StatsCards = ({ item }) => {
  return (
    <div className="flex-grow flex-1 h-[108px] min-w-[250px] flex items-center justify-between p-4 shadow-md rounded-md bg-[#f4f9fd]">
      <div className="flex items-start flex-col justify-center gap-2">
        <h2 className="text-md text-gray-400 font-semibold">{item.title}</h2>
        <h1 className="text-2xl font-bold">{item.value}</h1>
      </div>
      <div className="relative">
        {typeof window !== "undefined" && (
          <ReactApexChart
            options={item.data}
            series={item.series}
            type="area"
            height={90}
            width={80}
          />
        )}
      </div>
    </div>
  );
};

export default StatsCards;
