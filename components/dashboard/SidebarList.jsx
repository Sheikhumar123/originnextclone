"use client";

import { dashboardSidebarData } from "@/data/dashboardData";
import Cookies from "js-cookie";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { useState } from "react";
import profile from "../../public/ServiceProvider/profile.png"

const SidebarList = () => {
  const path = usePathname();
  const [ImageUrl, setImageUrl] = useState(Cookies?.get("profile_image"));

  console.log(path);
  return (
    <>
     <div className="flex items-center gap-4 p-6 border-b-[1px] border-[#d1d5db80] ">
        <div>
          <Image
            className="w-12 h-12 object-contain"
            src={ImageUrl
              // Cookies?.get("profile_image")
              //   ? Cookies.get("profile_image")
              //   : "https://images.unsplash.com/photo-1494790108377-be9c29b29330"
            }
            onError={() => {
              setImageUrl(profile);
            }}
            // src={
            //   Cookies.get("profile_image")
            //     ? Cookies.get("profile_image")
            //     : AvtarImage
            // }
            style={{
              width: "50px",
              height: "50px",
              objectFit: "cover",
            }}
            width={50}
            height={50}
            // src={AvtarImage}
            alt="Avatar"
          />
        </div>

        <div>
          {/* <h5 className="text-[#d1d5db80] uppercase text-base">
            {ProviderProfileData?.title}
          </h5> */}
          <h6 className="text-base">{Cookies.get("username")}</h6>
        </div>
      </div>
        <div className="p-4">
      {dashboardSidebarData.map((item) => (
        <Link
          key={item.id}
          href={item.link}
          scroll={false}
          className={`p-2 flex items-center justify-start gap-2 mb-4 hover:bg-white hover:text-bgBlue rounded-xl hover:font-semibold ${
            path === item.link ? "bg-white text-bgBlue" : ""
          }`}
        >
          {item.icon}
          <p>{item.title}</p>
        </Link>
      ))}
      </div>
    </>
  );
};

export default SidebarList;
