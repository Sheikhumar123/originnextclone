import React, { useState } from "react";
import PropTypes from "prop-types";
import { useFormik } from "formik";
import * as Yup from "yup";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import axios from "axios";
import { baseUrl } from "@/utils/BaseURL";
import { toast } from "react-toastify";
import { CircularProgress } from "@mui/material";

const ValidationSchema = Yup.object().shape({
  type: Yup.string().required("Field Required!"),
  email: Yup.string().when("type", {
    is: (value) => value == "email",
    then: () =>
      Yup.string().email("Invalid Email!").required("Field Required!"),
    otherwise: () => Yup.string(),
  }),
  phone: Yup.string().when("type", {
    is: (value) => value == "phone",
    then: () =>
      Yup.string()
        .min(9, "Invalid phone number")
        .max(12, "Invalid phone number")
        .required("Field Required!"),
    otherwise: () => Yup.string(),
  }),
});
function TypeOtpGet({ setTab, setdata, data, setScreenData }) {
  const [loader, setloader] = useState(false);
  const formik = useFormik({
    initialValues: {
      type: "",
      email: "",
      phone: "",
      dialCode: "",
    },
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });

  const handleSubmit = async (values) => {
    console.log("values", values);
    let obj = null;
    if (values.type == "email") {
      obj = {
        otp_login: true,
        email: values?.email,
      };
    } else {
      obj = {
        otp_login: true,
        phone: values?.phone,
        country_code: values?.dialCode,
      };
    }

    setloader(true);

    await axios({
      url: `${baseUrl}auth/loginuser`,
      method: "Post",
      headers: {
        "content-type": "application/json",
      },
      data: obj,
    })
      .then((res) => {
        setloader(false);

        if (res.data.status === "1") {
          toast.success(res.data.message);
          if (values?.type == "email") {
            setScreenData({
              type: values.type,
              email: values.email,
              duration: res?.data?.duration * 60,
            });
          } else if (values?.type == "phone") {
            setScreenData({
              type: values.type,
              dialCode: values.dialCode,
              phone: values.phone,
              duration: res?.data?.duration * 60,
            });
          }
          // setgetotp(res.data?.otp)
          setTab("verifyOTP");
        } else {
          setloader(false);
          toast.error(res.data.message);
        }
      })
      .catch((err) => {
        setloader(false);
        toast.error(err.response?.data?.error);
      });
  };

  return (
    <div className="mt-[150px]">
      <h1 className="mb-4 text-2xl font-[600] text-start  text-black">
        Sign in with OTP
      </h1>

      <form onSubmit={formik.handleSubmit}>
        <div className="mb-4">
          <label
            htmlFor="Type"
            className=" text-md label"
          >
            Type
          </label>
          <select
            type="text"
            className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:outline-none focus:ring-1 `}
            placeholder="Type"
            name="type"
            value={formik.values.type}
            onChange={(e) => {
              setdata({
                type: e.target.value,
                email: "",
                phone: "",
              });
              formik.setFieldValue("email", "");
              formik.setFieldValue("phone", "");
              formik.handleChange(e);
            }}
            onBlur={formik.handleBlur}
          >
            <option value="">Select</option>
            <option value="email">Email</option>
            <option value="phone">Phone Number</option>
          </select>
          {formik.errors.type && formik.touched.type && formik.errors.type && (
            <p className="text-red-700 text-sm">{formik.errors.type}</p>
          )}
        </div>

        {formik.values?.type == "email" ? (
          <div className="mb-2">
            <label
              htmlFor="Email"
              className=" text-md label"
            >
              Email
            </label>
            <input
              type="email"
              className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md  focus:outline-none focus:ring-1 `}
              placeholder="Email Address *"
              name="email"
              value={formik.values.email}
              onChange={(e) => {
                setdata({
                  ...data,
                  email: e.target.value,
                });
                formik.handleChange(e);
              }}
              onBlur={formik.handleBlur}
            />

            {formik.errors.email &&
              formik.touched.email &&
              formik.errors.email && (
                <p className="text-red-700 text-sm">{formik.errors.email}</p>
              )}
          </div>
        ) : formik.values?.type == "phone" ? (
          <div className="mb-2">
            <label
              htmlFor="Phone Number"
              className=" text-md label"
            >
              Phone Number
            </label>
            <PhoneInput
              inputProps={{
                maxLength: "16",
                name: "phone",
                onBlur: formik.handleBlur,
              }}
              inputStyle={{
                width: "100%",
                height: "40px",
              }}
              country={"pk"}
              value={formik.values.phone}
              onChange={(phone, data) => {
                formik.setFieldValue("dialCode", data.dialCode);
                formik.setFieldValue("phone", phone);
                setdata({
                  ...data,
                  type: formik.values.type,
                  phone: phone,
                  dialCode: data.dialCode,
                });
              }}
              containerStyle={{ width: "100%", height: "40px" }}
            />
            {formik.errors.phone &&
              formik.touched.phone &&
              formik.errors.phone && (
                <p className="text-red-700 text-sm">{formik.errors.phone}</p>
              )}
          </div>
        ) : null}
        {loader ? (
          <div className="flex justify-center">
            <CircularProgress />
          </div>
        ) : (
          <button
            className="block w-full mx-auto px-4 py-2 mt-4 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-md active:bg-btnprimary hover:bg-bgBlue/95 focus:outline-none focus:shadow-outline-blue uppercase"
            type="submit"
          >
            Next
          </button>
        )}
      </form>
    </div>
  );
}

export default TypeOtpGet;
TypeOtpGet.propTypes = {
  setTab: PropTypes.func,
};
