import { Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import * as yup from "yup";
import OTPInput from "react-otp-input";
import axios from "axios";
import { baseUrl } from "@/utils/BaseURL";
import { toast } from "react-toastify";
import { CircularProgress } from "@mui/material";
import { useRouter } from "next/navigation";
import Cookies from "js-cookie";

const validationSchema = yup.object().shape({
  otp: yup.string().min(4).required("Field Required!"),
});

function TypeOtpVerify({
  setTab,
  data,
  screenData,
  setScreenData,
  searchParams,
}) {
  const router = useRouter();
  console.log(data);
  console.log(screenData);
  const [loader, setloader] = useState(false);
  const [disbtn, setdisbtn] = useState(false);
  const [disablebtn, setdisablebtn] = useState(
    searchParams?.email ? true : false
  );
  const formatTime = (time) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
  };
  console.log(disablebtn);
  const handleSubmt = async (values) => {
    console.log("values", values);
    let obj = null;
    if (data?.type == "email") {
      obj = {
        type: data?.type,
        otp: values.otp,
        email: data?.email,
      };
    } else {
      obj = {
        type: data?.type,
        otp: values.otp,
        phone: data?.phone,
        country_code: data?.dialCode,
      };
    }

    setloader(true);

    await axios({
      url: `${baseUrl}auth/verifypin`,
      method: "Post",
      headers: {
        "content-type": "application/json",
      },
      data: obj,
    })
      .then((res) => {
        setloader(false);

        if (res.data.status === "1") {
          toast.success(res.data.message);
          Cookies.set("orignToken", res.data?.accessToken);
          Cookies.set("user_id", res.data?.user_id);
          Cookies.set("email", res.data?.email);
          Cookies.set("username", res.data?.username);
        Cookies.set("title", res.data?.title);

          Cookies.set("profile_image", res.data?.profile_image);
          Cookies.set("AllRoles", JSON.stringify(res.data?.Roles));
          router.push("/providersdashboard");
          // router.push("/home/service-provider/get-started");
          res?.data?.Roles?.forEach((item) => {
            if (item?.role_name == "ServiceProvider") {
              Cookies.set("selectedRole", JSON.stringify(item));
              router.push("/providersdashboard");
            } else if (item?.role_name == "Investor") {
              Cookies.set("selectedRole", JSON.stringify(item));
              router.push("/dashboard");
            }
          });
        } else {
          setloader(false);
          toast.error(res.data.message);
        }
      })
      .catch((err) => {
        setloader(false);
        toast.error(err.response?.data?.error);
      });
  };
  useEffect(() => {
    let timer1 = setTimeout(() => {
      setdisbtn(false);
      setdisablebtn(false);
    }, screenData?.duration * 1000);
    return () => {
      clearTimeout(timer1);
    };
  }, [disablebtn, disbtn, searchParams?.email]);

  const resendOTP = async () => {
    let obj = null;
    if (data?.type == "email") {
      obj = {
        otp_login: true,
        email: data?.email,
      };
    } else if (data?.type == "phone") {
      obj = {
        otp_login: true,
        phone: data?.phone,
        country_code: data?.dialCode,
      };
    }
    console.log(disablebtn);
    setdisbtn(true);
    await axios({
      url: `${baseUrl}auth/loginuser`,
      method: "Post",
      headers: {
        "content-type": "application/json",
      },
      data: obj,
    })
      .then((res) => {
        if (res.data.status === "1") {
          setdisablebtn((prev) => !prev);
          toast.success(res.data.message);
          if (screenData?.type == "email") {
            setScreenData({
              type: data?.type,
              email: data?.email,
              duration: res?.data?.duration * 60,
            });
          } else if (screenData?.type == "phone") {
            setScreenData({
              type: data?.type,

              dialCode: data?.dialCode,
              phone: data?.phone,
              duration: res?.data?.duration * 60,
            });
          }
        } else {
          toast.error(res.data.message);
        }
      })
      .catch((err) => {
        setdisbtn(false);

        toast.error(err.response?.data?.error);
      });
  };
  // const handleSubmt = (values) => {
  //     console.log("values", values);
  //     setTab("changePassword");
  //   };

  return (
    <div>
      <h1 className="mb-4 text-2xl font-[600] text-start text-black">
        Enter Verification Code
      </h1>
      <p className="font-medium text-[18px] leading-normal text-[#6A6A6A] mb-10">
        Please enter the verification code sent to{" "}
        {data?.type == "email"
          ? data?.email
          : data?.type == "phone"
          ? data?.phone
          : null}
      </p>

      <Formik
        validateOnChange={true}
        initialValues={{
          otp: "",
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmt}
      >
        {({ values, handleBlur, setFieldValue, errors, touched }) => {
          return (
            <Form>
              <div className="w-full">
                <OTPInput
                  className="w-full"
                  value={values.otp}
                  onChange={(val) => setFieldValue("otp", val)}
                  placeholder={true}
                  numInputs={4}
                  renderInput={(props) => (
                    <input
                      placeholder="--"
                      name="otp"
                      onBlur={handleBlur}
                      {...props}
                    />
                  )}
                  inputStyle={{
                    // Custom styles for the input fields
                    width: "100%",
                    maxWidth: "25%",

                    height: "3rem",
                    margin: "0 0.5rem",
                    fontSize: "2rem",
                    borderRadius: "4px",
                    border: "1px solid rgba(0,0,0,0.3)",
                  }}
                />
                {touched?.otp && errors?.otp && (
                  <span className="text-xs text-[#cc0000]">{errors?.otp}</span>
                )}

                <div className="flex justify-between items-center mt-3">
                  <p className="text-[18px] text-[#6A6A6A] font-semibold">
                    Expires in {formatTime(screenData?.duration)}
                  </p>

                  <div className="flex items-center gap-1">
                    <p className="text-[16px] text-[#363848] font-semibold">
                      Didn’t receive OTP?
                    </p>
                    <button
                      disabled={disbtn || disablebtn}
                      type="button"
                      onClick={resendOTP}
                      className="text-[16px] text-[#4743E0] font-semibold cursor-pointer"
                    >
                      Resend
                    </button>
                  </div>
                </div>
              </div>
              {loader ? (
                <div className="flex justify-center">
                  <CircularProgress />
                </div>
              ) : (
                <button
                  className="mt-10 block w-full mx-auto px-4 py-2 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-md active:bg-btnprimary hover:bg-bgBlue/95 focus:outline-none focus:shadow-outline-blue uppercase"
                  type="submit"
                >
                  Verify
                </button>
              )}
              <div className="mt-5 flex justify-center">
                <div className="flex items-center gap-1">
                  <p className="text-[18px] text-[#6A6A6A] font-semibold">
                    Wrong {data?.type == "email" ? "Email" : "Phone Number"}?
                  </p>
                  <p
                    className="text-[16px] text-[#4743E0] font-semibold cursor-pointer"
                    onClick={() => setTab("type")}
                  >
                    click here
                  </p>
                </div>
              </div>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}

export default TypeOtpVerify;
TypeOtpVerify.propTypes = {
  setTab: PropTypes.func,
};
