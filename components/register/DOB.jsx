import moment from "moment";

const DOB = ({ formik, setActive }) => {
  return (
    <div>
      <div className="mb-2">
        <label className="block text-sm text-label mb-2">Date Of Birth</label>
        <input
          type="date"
          className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md  focus:outline-none focus:ring-1 ${
            formik.touched?.dob && formik.errors?.dob
              ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
              : "border focus:border-blue-400 focus:ring-blue-600"
          }`}
          max={moment().format("YYYY-MM-DD")}
          name="dob"
          value={formik.values.dob}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
        {formik.touched?.dob && formik.errors?.dob && (
          <span className="text-xs text-[#cc0000]">{formik.errors?.dob}</span>
        )}
      </div>
      <button
        type="button"
        className="block w-full mx-auto px-4 py-2 mt-4 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150
         bg-blue-600 border border-transparent rounded-md active:bg-btnprimary hover:bg-bgBlue/95 focus:outline-none focus:shadow-outline-blue"
        onClick={() => {
          if (formik.touched?.dob && !formik.errors?.dob) {
            setActive(2);
          } else {
            formik.setFieldTouched("dob");
          }
        }}
      >
        Next
      </button>
    </div>
  );
};

export default DOB;
