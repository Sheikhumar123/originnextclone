import { CircularProgress } from "@mui/material";
import { useFormik } from "formik";
import { toast } from "react-toastify";
import * as Yup from "yup";
import UpiPaymentModal from "./UpiPaymentModal";
import { useRef, useState } from "react";
import Cookies from "js-cookie";
import ScreenLoader from "../ScreenLoader/ScreenLoader";
import {
  registerInvestmentAddApi,
  registerInvestorRefrelApi,
} from "@/Apis/InvestorApis/InvestorApis";
import { useRouter } from "next/navigation";
import axiousInstance from "@/utils/axiousInstance";

const Payment = ({ formikAllValues, userId }) => {
  const [open, setopen] = useState(false);
  const [loader, setloader] = useState(false);
  const [loader2, setloader2] = useState(false);
  const [verifyCode, setverifyCode] = useState(false);
  const [codeAmount, setcodeAmount] = useState(false);
  const router = useRouter();

  const dialogRef = useRef(null);
  const openModal = () => {
    setopen(true);
    dialogRef?.current?.showModal();
  };

  const closeModal = () => {
    setopen(false);
    dialogRef?.current?.close();
  };
  const initialValues = {
    amount: "",
    payment_mode: "",
    code: "",
  };
  const ValidationSchema = Yup.object().shape({
    payment_mode: Yup.string().required("Please Enter Payment Method"),
    amount: Yup.number().when("payment_mode", {
      is: (value) => value == "UPI",
      then: () =>
        Yup.number()
          .required("Please Enter Amount")
          .min(10000, "Amount must be at least 10000")
          .max(45000, "Amount must not exceed 45000"),
      otherwise: () => Yup.string(),
    }),
    code: Yup.string().when("payment_mode", {
      is: (value) => value == "Cash",
      then: () => Yup.string().required("Please Enter Code"),
      otherwise: () => Yup.string(),
    }),
    // payment_mode: yup.string().required("Field Required!"),
    // amount: yup
    //   .number()
    //   .required("Field Required!")
    //   .min(10000, "Amount must be at least 10000")
    //   .max(45000, "Amount must not exceed 45000"),
    // amount: yup.string().when("payment_mode", {
    //   is: (value) => value !== "Crypto",
    //   then: () => yup.string().required("Field Required!"),
    //   otherwise: () => yup.string(),
    // }),
  });
  const formik = useFormik({
    initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  console.log(formik.values);

  const handleSubmit = async (values, resetForm, setSubmitting) => {
    console.log(values);
    const formData = new FormData();

    if (values.payment_mode == "UPI") {
      openModal();
    } else if (values.payment_mode == "Cash") {
      setloader(true);
      formData.append("user_id", userId);
      formData.append("payment_method", values?.payment_mode);
      formData.append("type", "Investment");
      formData.append("code", values?.code);
      const response = await registerInvestmentAddApi(formData);
      setloader(false);
      setSubmitting(false);
      if (response?.data?.status == "1") {
        toast.success(response.data.message);
        let obj = {
          user_id: userId,
          reference_code: formikAllValues?.values?.referal_code,
          amount: codeAmount,
        };
        const response2 = await registerInvestorRefrelApi(obj);
        console.log(response2);

        if (response2?.data?.status == "1") {
          toast.success(response2.data.message);
          router.push("/auth/investor-login");
        } else if (response2?.data?.status == "0") {
          console.log(response2);
          toast.error(response2.data.message);
        } else {
          toast.error(response2?.response?.data?.message);
        }
        // router.push("/auth/investor-login");
      } else if (response?.data?.status == "0") {
        toast.error(response.data.message);
      } else {
        console.log(response);
        toast.error(response?.response?.data?.message);
      }
    }
  };
  const handleVerifyCode = async () => {
    try {
      const response = await axiousInstance.get(
        `auth/getcodeamount?code=${formik.values.code}`
      );
      // setloader(false);
      if (response.data.status == "1") {
        toast.success(response.data.message);
        setverifyCode(true);
        setcodeAmount(response.data?.amount);
      } else {
        // setloader(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      // setloader(false);
      toast.error(error.response?.data?.message);
    }
  };
  return (
    <div>
      {loader && <ScreenLoader />}
      <form onSubmit={formik.handleSubmit}>
        <div>
          <div className="mb-2">
            <label className="block text-sm text-label mb-2">
              Payment Type
            </label>

            <div className="flex items-center gap-2 mb-1">
              <input
                type="radio"
                className={`radio ${
                  formik.touched?.payment_mode && formik.errors?.payment_mode
                    ? "radio-error"
                    : "radio-primary"
                }`}
                name="payment_mode"
                value={"UPI"}
                checked={formik.values.payment_mode === "UPI"}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              <p className="text-sm">UPI</p>
            </div>

            <div className="flex items-center gap-2 mb-1">
              <input
                type="radio"
                className={`radio ${
                  formik.touched?.payment_mode && formik.errors?.payment_mode
                    ? "radio-error"
                    : "radio-primary"
                }`}
                name="payment_mode"
                value={"Cash"}
                checked={formik.values.payment_mode === "Cash"}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              <p className="text-sm">Cash</p>
            </div>

            <div className="flex items-center gap-2 mb-1">
              <input
                type="radio"
                disabled
                className={`radio ${
                  formik.touched?.payment_mode && formik.errors?.payment_mode
                    ? "radio-error"
                    : "radio-primary"
                }`}
                name="payment_mode"
                value={"Credit"}
                checked={formik.values.payment_mode === "Credit"}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              <p className="text-sm">Credit</p>
            </div>

            <div className="flex items-center gap-2 mb-1">
              <input
                type="radio"
                disabled
                className={`radio ${
                  formik.touched?.payment_mode && formik.errors?.payment_mode
                    ? "radio-error"
                    : "radio-primary"
                }`}
                name="payment_mode"
                value={"Crypto"}
                checked={formik.values.payment_mode === "Crypto"}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
              />
              <p className="text-sm">Crypto</p>
            </div>

            {formik.touched?.payment_mode && formik.errors?.payment_mode && (
              <span className="text-xs text-[#cc0000]">
                {formik.errors?.payment_mode}
              </span>
            )}
          </div>
          {formik.values.payment_mode === "Cash" ? (
            <>
              <div className="mb-2">
                <label className="block text-sm text-label mb-2">Code</label>
                <div className="flex gap-2 items-center">
                  <input
                    className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:outline-none focus:ring-1 ${
                      formik.touched?.amount && formik.errors?.amount
                        ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                        : "border focus:border-blue-400 focus:ring-blue-600"
                    }`}
                    type="text"
                    name="code"
                    value={formik.values.code}
                    onChange={formik.handleChange}
                    placeholder="Code"
                  />
                  <div>
                    <button
                      onClick={handleVerifyCode}
                      type="button"
                      className="px-6 py-2 rounded-md bg-[#434CD9] text-white self-end "
                    >
                      Verify
                    </button>
                  </div>
                </div>
                {formik.touched?.code && formik.errors?.code && (
                  <span className="text-xs text-[#cc0000]">
                    {formik.errors?.code}
                  </span>
                )}
              </div>
              {formik.values.payment_mode == "Cash" && verifyCode ? (
                <div className="mb-2">
                  <label className="block  text-lg">Amount</label>
                  <input
                    type="text"
                    value={codeAmount}
                    disabled
                    className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                    placeholder="Amount"
                  />
                </div>
              ) : null}
            </>
          ) : (
            <div className="mb-2">
              <label className="block text-sm text-label mb-2">Amount</label>
              <input
                type="number"
                className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:outline-none focus:ring-1 ${
                  formik.touched?.amount && formik.errors?.amount
                    ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                    : "border focus:border-blue-400 focus:ring-blue-600"
                }`}
                placeholder="min 10000 to max 45000"
                name="amount"
                value={formik.values.amount}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                autoComplete="off"
                readonly="readonly"
                onMouseDown={(e) => {
                  e.target.removeAttribute("readonly");
                }}
              />
              {formik.touched?.amount && formik.errors?.amount && (
                <span className="text-xs text-[#cc0000]">
                  {formik.errors?.amount}
                </span>
              )}
              <label className="block text-[12px] text-black mb-2 ">
                <span className="text-red-600 text-lg">*</span> This Limit is
                only for credit/debit/upi
              </label>
            </div>
          )}

          {loader ? (
            <CircularProgress />
          ) : (
            <button
              disabled={formik.values.payment_mode == "Cash" && !verifyCode}
              type="submit"
              className="block w-full px-4 py-2 mt-4 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-md active:bg-btnprimary hover:bg-bgBlue/95 focus:outline-none focus:shadow-outline-blue"
            >
              Next
            </button>
          )}
        </div>
      </form>
      {open && (
        <UpiPaymentModal
          formikAllValues={formikAllValues}
          paymentValue={formik.values}
          open={open}
          closeModal={closeModal}
          userId={userId}
        />
      )}
    </div>
  );
};

export default Payment;
