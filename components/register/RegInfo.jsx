import { MdDone } from "react-icons/md";
import { CircularProgress } from "@mui/material";
import { useState } from "react";
import {
  verifyEmailApi,
  verifyNumberApi,
} from "@/Apis/ServiceProviderApis/ServiceProviderApis";
import { toast } from "react-toastify";
import OtpVerificationDialog from "./OtpVerificationDialog/OtpVerificationDialog";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";

const RegInfo = ({ formik, setActive }) => {
  const [loader, setLoader] = useState(false);
  const [showOtpVerficationDialog, setShowOtpVerficationDialog] =
    useState(false);

  const resendOTP = async (type) => {
    setLoader(true);
    let obj = {};

    if (type === "email") {
      obj.email = formik.values.email;
    } else if (type === "phone") {
      obj.country_code = formik.values.country_code;
      obj.phone = formik.values.phonenum;
    }

    let response;
    if (type === "email") {
      response = await verifyEmailApi(obj);
    } else {
      response = await verifyNumberApi(obj);
    }

    setLoader(false);
    if (response?.data?.status === "1") {
      if (type === "email") {
        formik.setFieldValue("otpFor", "email");
      } else {
        formik.setFieldValue("otpFor", "phone");
      }

      formik.setFieldValue("duration", response?.data?.duration * 60);
      setShowOtpVerficationDialog(true);
      toast.success(response.data.message);
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.data.message);
    }
  };

  return (
    <>
      <div>
        <div className="mb-2 flex 900px:gap-8 900px:flex-nowrap flex-wrap gap-2">
          <div className="flex-1">
            <label className="block text-sm text-label mb-2">First Name</label>
            <input
              type="text"
              className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:outline-none focus:ring-1 ${
                formik.touched?.firstname && formik.errors?.firstname
                  ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                  : "border focus:border-blue-400 focus:ring-blue-600"
              }`}
              placeholder="Jhon"
              name="firstname"
              value={formik.values.firstname}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.touched?.firstname && formik.errors?.firstname && (
              <span className="text-xs text-[#cc0000]">
                {formik.errors?.firstname}
              </span>
            )}
          </div>

          <div className="flex-1">
            <label className="block text-sm text-label mb-2">Last Name</label>
            <input
              type="text"
              className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:outline-none focus:ring-1 ${
                formik.touched?.lastname && formik.errors?.lastname
                  ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                  : "border focus:border-blue-400 focus:ring-blue-600"
              }`}
              placeholder="Smith"
              name="lastname"
              value={formik.values.lastname}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
            />
            {formik.touched?.lastname && formik.errors?.lastname && (
              <span className="text-xs text-[#cc0000]">
                {formik.errors?.lastname}
              </span>
            )}
          </div>
        </div>

        <div className="mb-2 ">
          <label className="block text-sm text-label mb-2">Email</label>
          <div className="relative">
            <input
              className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:outline-none focus:ring-1 ${
                formik.touched?.email && formik.errors?.email
                  ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                  : "border focus:border-blue-400 focus:ring-blue-600"
              }`}
              type="email"
              placeholder="johndoe@example.com"
              name="email"
              value={formik.values.email}
              onChange={(e) => {
                formik.setFieldValue("email", e.target.value);
                formik.setFieldValue("emailVerified", false);
              }}
              onBlur={formik.handleBlur}
            />
            {!formik.errors?.email && formik.values?.email && (
              <div
                className={`absolute font-bold inset-y-0 right-0 px-3 flex justify-center items-center`}
              >
                {formik.values.emailVerified ? (
                  <div className={`flex justify-center items-center gap-1`}>
                    <MdDone
                      color="#abcca5"
                      size={22}
                    />
                    <p className="text-[#92d487]">Verified</p>
                  </div>
                ) : (
                  <button
                    type="button"
                    className={`absolute justify-center font-bold inset-y-0 right-0 flex items-center px-3 text-[#0066ff] cursor-pointer flex-col whitespace-nowrap`}
                    onClick={() => resendOTP("email")}
                  >
                    Get OTP
                  </button>
                )}
              </div>
            )}
          </div>
          {formik.touched?.email && formik.errors?.email && (
            <span className="text-xs text-[#cc0000]">
              {formik.errors?.email}
            </span>
          )}
        </div>

        <div className="mb-2 relative w-full">
          <label className="block text-sm text-label mb-2">Phone</label>
          <div className="relative">
            <PhoneInput
              inputProps={{
                maxLength: "16",
                name: "phonenum",
                onBlur: formik.handleBlur,
              }}
              inputStyle={{
                width: "100%",
                height: "50px",
                borderColor:
                  formik.errors?.phonenum && formik.touched?.phonenum
                    ? "#cc0000"
                    : "inherit",
              }}
              value={formik.values.phonenum}
              onChange={(phone, data) => {
                formik.setFieldValue("country_code", data.dialCode);
                formik.setFieldValue("phonenum", phone);
                formik.setFieldValue("phoneVerified", false);
              }}
              containerStyle={{ width: "100%", height: "50px" }}
              country={"pk"}
            />

            {!formik.errors?.phonenum && formik.values?.phonenum && (
              <div
                className={`absolute font-bold inset-y-0 right-0 px-3 flex justify-center items-center`}
              >
                {formik.values.phoneVerified ? (
                  <div className={`flex justify-center items-center gap-1`}>
                    <MdDone
                      color="#abcca5"
                      size={22}
                    />
                    <p className="text-[#92d487]">Verified</p>
                  </div>
                ) : (
                  <button
                    type="button"
                    className={`absolute justify-center font-bold inset-y-0 right-0 flex items-center px-3 text-[#0066ff] cursor-pointer flex-col whitespace-nowrap`}
                    onClick={() => resendOTP("phone")}
                  >
                    Get OTP
                  </button>
                )}
              </div>
            )}
          </div>
          {formik.touched?.phonenum && formik.errors?.phonenum && (
            <span className="text-xs text-[#cc0000]">
              {formik.errors?.phonenum}
            </span>
          )}
        </div>

        <div className="mb-2 w-full">
          <label className="block text-sm text-label mb-2">Refferal Code</label>
          <input
            type="number"
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="eg. *******"
            name="referal_code"
            value={formik.values.referal_code}
            onChange={formik.handleChange}
          />
        </div>

        {loader ? (
          <CircularProgress />
        ) : (
          <button
            type="button"
            className="block w-full px-4 py-2 mt-4 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-md active:bg-btnprimary hover:bg-bgBlue/95 focus:outline-none focus:shadow-outline-blue"
            onClick={() => {
              let touched =
                formik.touched?.firstname &&
                formik.touched?.lastname &&
                formik.touched?.email &&
                formik.touched?.phonenum;
              let error =
                formik.errors?.firstname ||
                formik.errors?.lastname ||
                formik.errors?.email ||
                formik.errors?.phonenum ||
                !formik.values.emailVerified ||
                !formik.values.phoneVerified;

              if (touched && !error) {
                setActive(3);
              } else {
                formik.setFieldTouched("firstname");
                formik.setFieldTouched("lastname");
                formik.setFieldTouched("email");
                formik.setFieldTouched("phonenum");
              }
            }}
          >
            Next
          </button>
        )}
      </div>

      <OtpVerificationDialog
        open={showOtpVerficationDialog}
        onClose={() => {
          formik.setFieldValue("duration", 0);
          formik.setFieldValue("otpFor", "");
          setShowOtpVerficationDialog(false);
        }}
        formik={formik}
      />
    </>
  );
};

export default RegInfo;
