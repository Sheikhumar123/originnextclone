import { CircularProgress } from "@mui/material";
import { useState } from "react";
import { MdVisibility } from "react-icons/md";
import { MdVisibilityOff } from "react-icons/md";
const RegInfo2 = ({ formik, setActive }) => {
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const [showPassword2, setShowPassword2] = useState(false);
  const handleClickShowPassword2 = () => setShowPassword2((show) => !show);
  return (
    <div>
      <div className="mb-2">
        <label className="block text-sm text-label mb-2">Father Name</label>
        <input
          type="text"
          className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:outline-none focus:ring-1 ${
            formik.touched?.fathername && formik.errors?.fathername
              ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
              : "border focus:border-blue-400 focus:ring-blue-600"
          }`}
          placeholder="John Doe"
          name="fathername"
          value={formik.values.fathername}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
        {formik.touched?.fathername && formik.errors?.fathername && (
          <span className="text-xs text-[#cc0000]">
            {formik.errors?.fathername}
          </span>
        )}
      </div>

      <div className="mb-2">
        <label className="block text-sm text-label mb-2">Address</label>
        <input
          type="text"
          className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:outline-none focus:ring-1 ${
            formik.touched?.address && formik.errors?.address
              ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
              : "border focus:border-blue-400 focus:ring-blue-600"
          }`}
          placeholder="Manchester - UK"
          name="address"
          value={formik.values.address}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          autoComplete="off"
          readonly="readonly"
          onMouseDown={(e) => {
            e.target.removeAttribute("readonly");
          }}
        />
        {formik.touched?.address && formik.errors?.address && (
          <span className="text-xs text-[#cc0000]">
            {formik.errors?.address}
          </span>
        )}
      </div>

      <div className="mb-2">
        <label className="block text-sm text-label mb-2">Password</label>
        <div className="relative">
        <input
              type={showPassword ? "text" : "password"}
          className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:outline-none focus:ring-1 ${
            formik.touched?.password && formik.errors?.password
              ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
              : "border focus:border-blue-400 focus:ring-blue-600"
          }`}
          placeholder="*******"
          name="password"
          value={formik.values.password}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          autoComplete="off"
          readonly="readonly"
          onMouseDown={(e) => {
            e.target.removeAttribute("readonly");
          }}
        />
                   <button
              type="button"
              className="absolute inset-y-0 right-0 flex items-center px-3 text-gray-700"
            >
              {showPassword ? (
                <MdVisibilityOff onClick={handleClickShowPassword} size={25} />
              ) : (
                <MdVisibility onClick={handleClickShowPassword} size={25} />
              )}
            </button>
            </div>
        {formik.touched?.password && formik.errors?.password && (
          <span className="text-xs text-[#cc0000]">
            {formik.errors?.password}
          </span>
        )}
      </div>

      <div className="mb-2">
        <label className="block text-sm text-label mb-2">
          Confirm Password
        </label>
        <div className="relative">
        <input
                   type={showPassword2 ? "text" : "password"}

          className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:outline-none focus:ring-1 ${
            formik.touched?.confirmpassword && formik.errors?.confirmpassword
              ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
              : "border focus:border-blue-400 focus:ring-blue-600"
          }`}
          placeholder="*******"
          name="confirmpassword"
          value={formik.values.confirmpassword}
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          autoComplete="off"
          readonly="readonly"
          onMouseDown={(e) => {
            e.target.removeAttribute("readonly");
          }}
        />
               <button
              type="button"
              className="absolute inset-y-0 right-0 flex items-center px-3 text-gray-700"
            >
              {showPassword2 ? (
                <MdVisibilityOff onClick={handleClickShowPassword2} size={25} />
              ) : (
                <MdVisibility onClick={handleClickShowPassword2} size={25} />
              )}
            </button>
            </div>
        {formik.touched?.confirmpassword && formik.errors?.confirmpassword && (
          <span className="text-xs text-[#cc0000]">
            {formik.errors?.confirmpassword}
          </span>
        )}
      </div>
      {formik.isSubmitting ? (
        <CircularProgress />
      ) : (
      <button
        type="submit"
        className="block w-full px-4 py-2 mt-4 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-md active:bg-btnprimary hover:bg-bgBlue/95 focus:outline-none focus:shadow-outline-blue"
        onClick={() => {
          // let touched =
          //   formik.touched?.fathername &&
          //   formik.touched?.address &&
          //   formik.touched?.password &&
          //   formik.touched?.confirmpassword;
          // let error =
          //   formik.errors?.fathername ||
          //   formik.errors?.address ||
          //   formik.errors?.password ||
          //   formik.errors?.confirmpassword;

          // if (touched && !error) {
          //   setActive(4);
          // } else {
          //   formik.setFieldTouched("fathername");
          //   formik.setFieldTouched("address");
          //   formik.setFieldTouched("password");
          //   formik.setFieldTouched("confirmpassword");
          // }
        }
      }
      >
        Done
        {/* Next */}
      </button>)}
    </div>
  );
};

export default RegInfo2;
