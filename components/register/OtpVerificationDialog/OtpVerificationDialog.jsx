"use client";
import { useState } from "react";
import { toast } from "react-toastify";
import OTPInput from "react-otp-input";
import { CircularProgress } from "@mui/material";
import {
  verifyEmailApi,
  verifyNumberApi,
} from "@/Apis/ServiceProviderApis/ServiceProviderApis";
import { verifyPinApi } from "@/Apis/AuthApis/AuthApis";
import AppDialog from "@/components/CustomComponents/AppDialog/AppDialog";

function OtpVerificationDialog({ open, onClose, formik }) {
  const [otp, setOtp] = useState("");
  const [loader, setloader] = useState(false);

  const formatTime = (time) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
  };

  const resendOTP = async () => {
    setloader(true);
    let obj = {};

    if (formik.values.otpFor === "email") {
      obj.email = formik.values.email;
    } else {
      obj.country_code = formik.values.country_code;
      obj.phone = formik.values.phonenum;
    }

    let response;
    if (formik.values.otpFor === "email") {
      response = await verifyEmailApi(obj);
    } else {
      response = await verifyNumberApi(obj);
    }

    setloader(false);
    if (response?.data?.status === "1") {
      setOtp("");
      formik.setFieldValue("duration", response?.data?.duration * 60);
      toast.success(response.data.message);
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.data.message);
    }
  };

  const handleVerify = async () => {
    if (formik.values.duration > 0) {
      setloader(true);
      let obj = {
        otp: otp,
      };

      if (formik.values.otpFor == "email") {
        obj.type = "email";
        obj.email = formik.values.email;
      } else {
        obj.type = "phone";
        obj.phone = formik.values.phonenum;
      }

      const response = await verifyPinApi(obj);
      setloader(false);
      if (response?.data?.status === "1") {
        if (formik.values.otpFor == "email") {
          formik.setFieldValue("emailVerified", true);
        } else {
          formik.setFieldValue("phoneVerified", true);
        }

        setOtp("");
        onClose();
        toast.success(response.data.message);
      } else if (response?.data?.status === "0") {
        toast.error(response.data.message);
      } else {
        toast.error(response.response?.data.message);
      }
    } else {
      toast.error("OTP Expired!");
    }
  };

  return (
    <AppDialog
      open={open}
      onClose={() => {
        setOtp("");
        onClose();
      }}
      title="Verify OTP"
    >
      <div>
        <div className="w-full">
          <p className="text-[18px] text-[#6A6A6A] font-semibold mb-2">OTP</p>
          <OTPInput
            className="w-full"
            value={otp}
            onChange={setOtp}
            placeholder={true}
            numInputs={4}
            renderInput={(props) => (
              <input
                placeholder="--"
                {...props}
              />
            )}
            inputStyle={{
              // Custom styles for the input fields
              width: "100%",
              maxWidth: "80px",
              height: "3rem",
              margin: "0 0.5rem",
              fontSize: "2rem",
              borderRadius: "4px",
              border: "1px solid rgba(0,0,0,0.3)",
            }}
          />
          {!otp ||
            (otp?.length < 4 && (
              <span className="text-xs text-[#cc0000]">
                Please Enter Valid OTP
              </span>
            ))}

          <div className="flex justify-between items-center mt-3">
            <p className="text-[18px] text-[#6A6A6A] font-semibold">
              Expires in {formatTime(formik.values.duration)}
            </p>

            <div className="flex items-center gap-1">
              <p className="text-[16px] text-[#363848] font-semibold">
                Didn’t receive OTP?
              </p>
              <p
                className="text-[16px] text-[#4743E0] font-semibold cursor-pointer"
                onClick={resendOTP}
              >
                Resend
              </p>
            </div>
          </div>
        </div>

        <div className="mt-6 flex justify-end">
          {loader ? (
            <CircularProgress />
          ) : (
            <button
              className="px-8 py-2 rounded-md bg-bgBlue text-white"
              disabled={otp?.length < 4 ? true : false}
              onClick={handleVerify}
            >
              Verify
            </button>
          )}
        </div>
      </div>
    </AppDialog>
  );
}

export default OtpVerificationDialog;
