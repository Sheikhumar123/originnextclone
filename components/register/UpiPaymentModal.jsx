"use client";
import Image from "next/image";
import { useRouter } from "next/navigation";
import React, { useEffect, useState } from "react";
import { IoClose } from "react-icons/io5";
import { toast } from "react-toastify";
import ScreenLoader from "../ScreenLoader/ScreenLoader";
import {
  registerInvestmentAddApi,
  registerInvestorRefrelApi,
} from "@/Apis/InvestorApis/InvestorApis";
import { CircularProgress } from "@mui/material";
import QRCode from "qrcode.react";
import { useDispatch, useSelector } from "react-redux";
import {
  GetInvestDetails,
  GetQRdetail,
} from "@/redux/Slices/InvestorDashboard/InvestSlice/InvestSlice";

function UpiPaymentModal({
  open,
  closeModal,
  paymentValue,
  userId,
  formikAllValues,
}) {
  const [investImage, setinvestImage] = useState(null);
  const [loader, setloader] = useState(false);
  const [loader2, setloader2] = useState(false);
  const router = useRouter();
  const amount = paymentValue?.amount; // Example amount
  const percentage = 2;
  const Fee = (percentage / 100) * amount;
  const { QRdetailData, status } = useSelector(GetInvestDetails);
  console.log(QRdetailData);
  const dispatch = useDispatch();
  useEffect(() => {
    if (open) {
      dispatch(GetQRdetail());
    }
  }, [dispatch]);
  const handleSubmit = async () => {
    if (!investImage) {
      return toast.error("Please Enter Screen Shot of Payment");
    }
    setloader(true);
    setloader2(true);
    const formData = new FormData();
    formData.append("user_id", userId);
    formData.append("amount", paymentValue?.amount);
    formData.append("fee", Fee);
    formData.append("total_amount",Number(paymentValue.amount) + Number(Fee)),
    formData.append("payment_method", paymentValue?.payment_mode);
    formData.append("type", "Investment");
    formData.append("transaction_ss", investImage?.file);
    const response = await registerInvestmentAddApi(formData);
    setloader(false);
    setloader2(false);
    if (response?.data?.status == "1") {
      toast.success(response.data.message);
      let obj = {
        user_id: userId,
        reference_code: formikAllValues?.values?.referal_code,
        amount: paymentValue?.amount,
      };
      const response2 = await registerInvestorRefrelApi(obj);
      if (response2?.data?.status == "1") {
        closeModal();
        router.push("/auth/investor-login");
      } else if (response2?.data?.status === "0") {
        toast.error(response2.data.message);
      }
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };
  return open ? (
    <>
      {(loader || status == "pending") && <ScreenLoader />}
      <div className="justify-center items-center px-4 flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
        <div className="relative w-full max-w-[900px]  max-h-[96%] overflow-y-auto border-0 rounded-lg shadow-lg bg-white outline-none focus:outline-none">
          {/*content*/}
          <div className="flex flex-col w-full">
            {/*header*/}
            <div className="flex justify-between items-center p-5">
              <h3 className="text-xl font-semibold">Invest Amount</h3>
              <button
                className="text-black outline-none focus:outline-none hover:text-gray-600 transition-colors"
                onClick={closeModal}
              >
                <IoClose size={30} />
              </button>
            </div>

            {/*body*/}
            <div className="px-6 pb-6">
              <div className="w-full p-6 flex items-center justify-between border-dashed border-2 border-sky-500 rounded-md bg-[#004551]/15 mb-6">
                <p className="text-xl">Total Amount</p>
                <h1 className="text-xl">
                  <span className="font-bold text-2xl">
                    ${Number(paymentValue?.amount) + Number(Fee)}
                  </span>{" "}
                  USD
                </h1>
              </div>
              <div className="mb-4 mt-2">
                <div className="w-full max-w-[200px]">
                  <Image
                    src={QRdetailData?.qrdetails?.qr_code}
                    alt="Qr Code"
                    width={200}
                    height={200}
                    style={{ width: "100%", height: "100%" }}
                  />
                </div>
                {/* <QRCode
                id="qr-gen"
                includeMargin={true}
                size={125}
                // value={userId?userId:"usama"}
              /> */}
              </div>
              <div className="mb-2">
                <label className="block  text-lg">Bussines Id</label>
                <input
                  type="text"
                  name="bussinessId"
                  disabled
                  value={QRdetailData?.qrdetails?.bussiness_id}
                  className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                />
              </div>
              <div className="mb-2">
                <label className="block  text-lg">Bussines Name</label>
                <input
                  type="text"
                  name="bussinessName"
                  disabled
                  value={QRdetailData?.qrdetails?.bussiness_name}
                  className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                />
              </div>
              <div className="mb-2">
                <label className="block  text-lg">Amount</label>
                <input
                  type="number"
                  name="amount"
                  disabled
                  value={paymentValue?.amount ? paymentValue?.amount : 0}
                  className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="Amount"
                />
              </div>
              <div className="mb-6">
                <label className="block  text-lg">Fee</label>
                <input
                  type="number"
                  name="amount"
                  disabled
                  value={Fee}
                  className="w-full bg-white h-[45px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="Fee"
                />
                <label className="block text-[12px] text-black mb-2 ">
                  <span className="text-red-600 text-lg">*</span> Processing fee
                  is 2% of Processing amount
                </label>
              </div>
              <label
                className="cursor-pointer label w-full"
                htmlFor={`investScreenshot`}
              >
                <input
                  id={`investScreenshot`}
                  type="file"
                  accept="image/*"
                  name={`investImage`}
                  // onChange={(event) => handleInputChange(index, event, "image")}
                  // value={item.question}
                  onChange={(event) => {
                    const file = event.target.files[0];
                    const reader = new FileReader();
                    reader.onloadend = () => {
                      // formik.values.featureFields[index].image = file;
                      // formik.values.featureFields[index].imageUrl =
                      //   reader.result;
                      setinvestImage({
                        file: file,
                        imageUrl: reader.result,
                      });
                      // formik.setFieldValue("featureFields", newFeatureFields);
                      // setFeatureFields(newFeatureFields);
                    };
                    if (file) {
                      reader.readAsDataURL(file);
                    }
                  }}
                  className="mb-2 hidden"
                />

                <div className="w-full p-6 border-dashed border-2 border-orange-400 rounded-md bg-[#FF9937]/35 ">
                  <p className="text-lg text-center">Uplaod Image</p>
                  <p className="text-md text-center">Your Payment ScreenShot</p>
                </div>
              </label>
              {investImage?.imageUrl ? (
                <div className="w-full max-w-[200px] relative mt-2">
                  <IoClose
                    size={20}
                    onClick={() => {
                      setinvestImage(null);
                    }}
                    className="absolute right-0 text-[red] cursor-pointer"
                  />

                  <Image
                    src={investImage.imageUrl}
                    alt="Feature Preview"
                    className="mb-2  cursor-pointer"
                    width={200}
                    height={200}
                    style={{ width: "100%", height: "100%" }}
                  />
                </div>
              ) : null}
              <div className="w-full flex items-center justify-center mt-4">
                {" "}
                {loader2 ? (
                  <CircularProgress />
                ) : (
                  <button
                    type="button"
                    onClick={handleSubmit}
                    className="bg-[#434CD9] w-full md:w-[100px]   px-6 py-2 rounded-3xl text-white"
                  >
                    Done
                  </button>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div
        className="opacity-25 fixed inset-0 z-40 bg-black"
        onClick={closeModal}
      ></div>
    </>
  ) : null;
}

export default UpiPaymentModal;
