import {
  countriesList,
  mostSpokenLanguages,
  skillsDataAutoComplete,
} from "@/data/autocompleteData";
import { Autocomplete, Chip, TextField } from "@mui/material";
import { CircleFlag } from "react-circle-flags";

const PreferencesForm = ({ data, handleChange, setStep, setData }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px] flex flex-col">
      <h1 className=" text-lg font-semibold mb-2 capitalize">
        do you want more questions from similar experts our AI will match and
        find similar experts for you.{" "}
      </h1>{" "}
      <label
        className="flex mb-2 items-center gap-2 p-2 bg-[#F5F7FF] rounded-md border border-gray-200"
        htmlFor="alertsNotifications"
      >
        <input
          type="checkbox"
          className="checkbox checkbox-sm [--chkbg:theme(colors.blue.600)] [--chkfg:white]"
          name="recommenationAcceptance"
          defaultChecked={data?.recommenationAcceptance || false}
          onChange={handleChange}
        />
        <p className="leading-none">Yes Please</p>
      </label>
      {data?.recommenationAcceptance && (
        <div className="w-full">
          <div className="flex flex-col 500px:flex-row 500px:items-center 500px:justify-between mb-2">
            <label htmlFor="country" className="text-black text-md font-[500]">
              Your Country
            </label>
            <Autocomplete
              multiple
              limitTags={1}
              size="small"
              id="country"
              options={countriesList}
              getOptionLabel={(option) => option.name}
              onChange={(event, value) =>
                setData({ ...data, countries: value })
              }
              renderOption={(props, option) => {
                return (
                  <li
                    {...props}
                    key={option.name}
                    className="p-2 flex items-center gap-2 cursor-pointer hover:bg-gray-100"
                  >
                    <CircleFlag
                      countryCode={option.code.toLowerCase()}
                      height="20px"
                      width={"20px"}
                    />
                    <h1>{option.name}</h1>
                  </li>
                );
              }}
              renderTags={(tagValue, getTagProps) => {
                return tagValue.map((option, index) => (
                  <Chip
                    style={{ backgroundColor: "#c7c9f4" }}
                    {...getTagProps({ index })}
                    key={option.name}
                    label={option.name}
                  />
                ));
              }}
              renderInput={(params) => (
                <TextField
                  style={{ color: "#808080" }}
                  {...params}
                  //   label="Countries"
                  placeholder="Country"
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      borderRadius: "8px",
                    },
                    color: "#9E9E9E",
                  }}
                />
              )}
              sx={{
                maxWidth: "280px",
                minWidth: "180px",
                borderRadius: "8px",
              }}
            />
          </div>
          <div className="flex flex-col 500px:flex-row 500px:items-center 500px:justify-between mb-2">
            <label
              htmlFor="languages"
              className="text-black text-md font-[500]"
            >
              Your Languages
            </label>
            <Autocomplete
              multiple
              limitTags={1}
              size="small"
              id="languages"
              options={mostSpokenLanguages}
              getOptionLabel={(option) => option}
              onChange={(event, value) =>
                setData({ ...data, languages: value })
              }
              renderOption={(props, option) => {
                return (
                  <li {...props} key={option}>
                    {option}
                  </li>
                );
              }}
              renderTags={(tagValue, getTagProps) => {
                return tagValue.map((option, index) => (
                  <Chip
                    style={{ backgroundColor: "#c7c9f4" }}
                    {...getTagProps({ index })}
                    key={option}
                    label={option}
                  />
                ));
              }}
              renderInput={(params) => (
                <TextField
                  style={{ color: "#808080" }}
                  {...params}
                  //   label="Countries"
                  placeholder="Language"
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      borderRadius: "8px",
                    },
                    color: "#9E9E9E",
                  }}
                />
              )}
              sx={{
                maxWidth: "280px",
                minWidth: "180px",
                borderRadius: "8px",
              }}
            />
          </div>
          <div className="flex flex-col  mb-2">
            <label htmlFor="skills" className="text-black text-md font-[500]">
              Your Skills
            </label>
            <Autocomplete
              multiple
              limitTags={1}
              size="small"
              id="skills"
              options={skillsDataAutoComplete}
              getOptionLabel={(option) => option}
              onChange={(event, value) => setData({ ...data, skills: value })}
              renderOption={(props, option) => {
                return (
                  <li {...props} key={option}>
                    {option}
                  </li>
                );
              }}
              renderTags={(tagValue, getTagProps) => {
                return tagValue.map((option, index) => (
                  <Chip
                    style={{ backgroundColor: "#c7c9f4" }}
                    {...getTagProps({ index })}
                    key={option}
                    label={option}
                  />
                ));
              }}
              renderInput={(params) => (
                <TextField
                  style={{ color: "#808080" }}
                  {...params}
                  //   label="Countries"
                  placeholder="Skills"
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      borderRadius: "8px",
                    },
                    color: "#9E9E9E",
                  }}
                />
              )}
              sx={{
                borderRadius: "8px",
              }}
            />
          </div>
        </div>
      )}
      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end mt-6"
        onClick={() => {
          setStep(3);
        }}
      >
        Next
      </button>
    </div>
  );
};

export default PreferencesForm;
