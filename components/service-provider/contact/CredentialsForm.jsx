import { RiShieldCheckFill } from "react-icons/ri";

const CredentialsForm = ({ data, handleChange, setStep, setData }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px] flex flex-col">
      <h1 className=" text-lg font-semibold mb-2 capitalize">
        Looking for Website Designers ?Login to get connect with services
        Providers.
      </h1>
      <input
        id="name"
        type="text"
        name="name"
        defaultValue={data?.name || ""}
        onChange={handleChange}
        className="w-full bg-white h-[40px] mb-2 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
        placeholder="John Doe"
      />
      <div className="relative mb-2">
        <input
          className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="johndoe@example.com"
          type="email"
          name="email"
          value={data?.email || ""}
          onChange={handleChange}
        />
        <button
          type="button"
          className="absolute justify-center inset-y-0 right-0 flex items-center px-3 text-gray-700  flex-col"
        >
          <RiShieldCheckFill size={25} />
          <p className="text-[8px] font-bold">Verify</p>
        </button>
      </div>
      <div className="relative mb-2">
        <input
          className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="+923XXXXXXXXX"
          type="tel"
          name="phoneNumber"
          value={data?.phoneNumber || ""}
          onChange={handleChange}
        />
        <button
          type="button"
          className="absolute justify-center inset-y-0 right-0 flex items-center px-3 text-gray-700  flex-col"
        >
          <RiShieldCheckFill size={25} />
          <p className="text-[8px] font-bold">Verify</p>
        </button>
      </div>
      <label
        className="flex mb-2 items-center gap-2 p-2  rounded-md "
        htmlFor="whatsAppSame"
      >
        <input
          type="checkbox"
          className="checkbox checkbox-sm [--chkbg:theme(colors.blue.600)] [--chkfg:white]"
          name="whatsAppSame"
          defaultChecked={data?.recommenationAcceptance || false}
          onChange={handleChange}
        />
        <p className="leading-none font-semibold">
          If Whatsapp number is same and wants you to contact on whatsapp then ,
          please check it
        </p>
      </label>
      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end mt-6"
        onClick={() => {
          //   setStep(3);
          console.log(data);
        }}
      >
        Next
      </button>
    </div>
  );
};

export default CredentialsForm;
