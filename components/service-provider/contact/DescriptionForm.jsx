const DescriptionForm = ({ data, handleChange, setStep }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px] flex flex-col">
      <h1 className=" text-lg font-semibold mb-2 capitalize">
        what you need , we will help you to get free quotes.{" "}
      </h1>{" "}
      <textarea
        name="description"
        cols="30"
        onChange={handleChange}
        value={data?.description || ""}
        rows="5"
        placeholder="Your message here..."
        className="w-full border p-2 rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
      ></textarea>
      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end mt-6"
        onClick={() => {
          setStep(2);
        }}
      >
        Next
      </button>
    </div>
  );
};

export default DescriptionForm;
