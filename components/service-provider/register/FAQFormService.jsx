import axiousInstance from "@/utils/axiousInstance";
import { CircularProgress } from "@mui/material";
import { useFormik } from "formik";
import React, { useState } from "react";
import { FaCirclePlus } from "react-icons/fa6";
import { toast } from "react-toastify";
import * as Yup from "yup";
const FAQFormService = ({ userId, setStep }) => {
  const [render, setrender] = useState(false);
  const [loader, setloader] = useState(false);
  // const handleInputChange = (index, event, field) => {
  //   const newFaqFields = [...faqFields];
  //   newFaqFields[index][field] = event.target.value;
  //   setFaqFields(newFaqFields);
  // };

  // const handleAddFAQ = () => {
  //   setFaqFields([...faqFields, { question: "", answer: "" }]);
  // };

  const handleSubmit = async (values) => {
    setloader(true);
    let obj = {
      faqs: values?.faqs,
      user_id: userId,
      type: "ServiceProvider",
      // user_id:"65fbe408df2dca09c5cf1551",
      // formData.append(`id`, "65fbe408df2dca09c5cf1551");
    };
    try {
      const response = await axiousInstance.post(
        "userdata/addproviderfaqs",
        obj
      );
      setloader(false);
      if (response.data.status === "1") {
        setStep(11);
        toast.success(response.data.message);
      } else {
        setloader(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error.response?.data?.error);
    }
    // setData((prevData) => ({
    //   ...prevData,
    //   faq: faqFields,
    // }));
    // setStep(11);
  };
  const validationSchema = Yup.object().shape({
    faqs: Yup.array().of(
      Yup.object().shape({
        question: Yup.string().required("Question is required"),
        answer: Yup.string().required("Answer is required"),
      })
    ),
  });
  const formik = useFormik({
    initialValues: {
      faqs: [{ question: "", answer: "" }],
    },
    validationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  console.log(formik.values.faqs);
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 600px:max-w-[600px] flex flex-col">
      <h1 className="text-xl label mb-1 capitalize text-center">
        Add FAQ for your client to clear their questions
      </h1>
      <form onSubmit={formik.handleSubmit}>
        <div className="overflow-y-auto h-[350px] 800px:max-h-[270px]">
          {formik.values.faqs?.map((item, index) => (
            <div key={index} className="mb-2">
              <label className="label text-md block">Question</label>
              <input
                name={`faqs.${index}.question`}
                type="text"
                value={item.question}
                onChange={formik.handleChange}
                className="w-full bg-white h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Enter your question"
              />

              {formik.touched.faqs &&
                formik.errors.faqs &&
                formik.errors.faqs[index] && (
                  <p className="text-red-700 text-sm">
                    {" "}
                    {formik.errors.faqs[index].question}
                  </p>
                )}
              <label className="label text-md  block">Answer</label>
              <textarea
                name={`faqs.${index}.answer`}
                rows="3"
                value={item.answer}
                onChange={formik.handleChange}
                placeholder="Enter the answer to the question"
                className="w-full border p-2 rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              ></textarea>

              {formik.touched.faqs &&
                formik.errors.faqs &&
                formik.errors.faqs[index] && (
                  <p className="text-red-700 text-sm">
                    {" "}
                    {formik.errors.faqs[index].answer}
                  </p>
                )}
            </div>
          ))}
        </div>
        <button
          type="button"
          onClick={() => {
            setrender(!render);
            formik.values.faqs?.push({ question: "", answer: "" });
          }}
          className="px-4 py-2 rounded-md mt-2  text-black self-start flex items-center gap-1"
        >
          <FaCirclePlus />
          <h1> Add More</h1>
        </button>
        <div className="flex justify-between items-center">
          <button
            type="button"
            className="px-4 py-2 rounded-md  bg-[#dedede] self-end mt-6 "
            onClick={() => setStep(11)}
          >
            Skip for now
          </button>
          {loader ? (
            <CircularProgress className="self-end" />
          ) : (
            <button
              type="submit"
              className="px-8 py-2 rounded-md bg-bgBlue text-white self-end mt-4"
            >
              Next
            </button>
          )}
        </div>
      </form>
    </div>
  );
};

export default FAQFormService;
