import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import { CircularProgress } from "@mui/material";
import { verifyNumberApi } from "@/Apis/ServiceProviderApis/ServiceProviderApis";
import { useState } from "react";
import { toast } from "react-toastify";

function ServiceNotificationOtp({ setStep, screenData, setScreenData }) {
  const [loader, setloader] = useState(false);
  const [otpSend, setOtpSend] = useState({
    phone: false,
    whatsappnum: false,
  });

  const handleVerifyNumber = async (type) => {
    setloader(true);
    let obj = {};

    if (type == "phone") {
      obj.country_code = screenData?.dialCode;
      obj.phone = screenData?.phone;
    } else {
      obj.country_code = screenData?.whatsappDialCode;
      obj.whatsappnum = screenData?.whatsapp;
    }

    const response = await verifyNumberApi(obj);
    setloader(false);
    if (response?.data?.status === "1") {
      if (type == "phone") {
        setScreenData((rest) => ({
          ...rest,
          phoneDuration: response?.data?.duration * 60,
        }));
      } else {
        setScreenData((rest) => ({
          ...rest,
          whatsappDuration: response?.data?.duration * 60,
        }));
      }
      setOtpSend((rest) => ({
        ...rest,
        [type]: true,
      }));
      toast.success(response.data.message);
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };

  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px] flex flex-col">
      <h1 className=" text-xl font-semibold mb-2 capitalize">
        don’t miss clients orders notifications.
      </h1>{" "}
      <h1 className=" text-xl font-semibold mb-2 capitalize">
        get it instantly{" "}
      </h1>
      <div className="mb-2 relative w-full ">
        <label className="block text-sm text-label mb-2 font-[500]">
          Phone Number
        </label>
        <div className="relative">
          <PhoneInput
            inputProps={{
              maxLength: "16",
              name: "phone",
            }}
            inputStyle={{
              width: "100%",
              height: "50px",
            }}
            containerStyle={{ width: "100%", height: "50px" }}
            country={"pk"}
            value={screenData?.phone}
            onChange={(phone, data) => {
              setScreenData((rest) => ({
                ...rest,
                dialCode: data.dialCode,
                phone,
              }));
            }}
          />
          <button
            type="button"
            disabled={screenData?.phone?.length < 9 || otpSend?.phone}
            className={`absolute justify-center font-bold inset-y-0 right-0 flex items-center px-3 ${
              screenData?.phone?.length > 9 && !otpSend?.phone
                ? "text-[#0066ff]"
                : "text-[#cce0ff]"
            } ${
              screenData?.phone?.length > 9 && !otpSend?.phone
                ? "cursor-pointer"
                : "cursor-not-allowed"
            } flex-col`}
            onClick={() => {
              handleVerifyNumber("phone");
            }}
          >
            Get OTP
          </button>
        </div>
      </div>
      <div className="mb-2 relative w-full ">
        <label className="block text-sm text-label mb-2 font-[500]">
          Whatsapp Number
        </label>
        <div className="relative">
          <PhoneInput
            inputProps={{
              maxLength: "16",
            }}
            inputStyle={{
              width: "100%",
              height: "50px",
            }}
            country={"pk"}
            value={screenData?.whatsapp}
            onChange={(phone, data) => {
              setScreenData((rest) => ({
                ...rest,
                whatsappDialCode: data.dialCode,
                whatsapp: phone,
              }));
            }}
            containerStyle={{ width: "100%", height: "50px" }}
          />
          <button
            type="button"
            disabled={screenData?.whatsapp?.length < 9 || otpSend?.whatsappnum}
            className={`absolute justify-center font-bold inset-y-0 right-0 flex items-center px-3 ${
              screenData?.whatsapp?.length > 9 && !otpSend?.whatsappnum
                ? "text-[#0066ff]"
                : "text-[#cce0ff]"
            } ${
              screenData?.whatsapp?.length > 9 && !otpSend?.whatsappnum
                ? "cursor-pointer"
                : "cursor-not-allowed"
            } flex-col`}
            onClick={() => {
              handleVerifyNumber("whatsappnum");
            }}
          >
            Get OTP
          </button>
        </div>
      </div>
      <div className="mt-6 flex justify-end">
        {loader ? (
          <CircularProgress />
        ) : (
          <button
          // disabled={!otpSend?.phone || !otpSend?.whatsappnum}
          // className={`px-8 py-2 rounded-md ${
          //   otpSend?.phone && otpSend?.whatsappnum
          //     ? "bg-[#0066ff]"
          //     : "bg-[#cce0ff]"
          // }  text-white`}
            disabled={(!otpSend?.phone ) && ( !otpSend?.whatsappnum )}
            className={`px-8 py-2 rounded-md ${
              (otpSend?.phone ) || ( otpSend?.whatsappnum )
                ? "bg-[#0066ff]"
                : "bg-[#cce0ff]"
            }  text-white`}
            onClick={() => {
              setStep(5);
            }}
          >
            Next
          </button>
        )}
      </div>
    </div>
  );
}

export default ServiceNotificationOtp;
