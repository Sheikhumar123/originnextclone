import { verifyNumberApi } from "@/Apis/ServiceProviderApis/ServiceProviderApis";
import { useState } from "react";
import { RiShieldCheckFill } from "react-icons/ri";
import OtpInput from "react-otp-input";
import { toast } from "react-toastify";
import { CircularProgress } from "@mui/material";
import { verifyPinApi } from "@/Apis/AuthApis/AuthApis";

const ServiceNotificationForm = ({ setStep, screenData, setScreenData }) => {
  const [loader, setloader] = useState(false);
  const [phoneOtp, setPhoneOtp] = useState("");
  const [whatsappOtp, setWhatsappOtp] = useState("");
  const [otpVerified, setOtpVerified] = useState({
    phone: false,
    whatsappnum: false,
  });
  console.log(otpVerified);

  const formatTime = (time) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
  };

  const resendOTP = async (type) => {
    setloader(true);
    let obj = {};

    if (type == "phone") {
      obj.country_code = screenData?.dialCode;
      obj.phone = screenData?.phone;
    } else {
      obj.country_code = screenData?.whatsappDialCode;
      obj.whatsappnum = screenData?.whatsapp;
    }

    const response = await verifyNumberApi(obj);
    setloader(false);
    if (response?.data?.status === "1") {
      if (type == "phone") {
        setScreenData((rest) => ({
          ...rest,
          phoneDuration: response?.data?.duration * 60,
        }));
      } else {
        setScreenData((rest) => ({
          ...rest,
          whatsappDuration: response?.data?.duration * 60,
        }));
      }
      toast.success(response.data.message);
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };

  const verifyOTP = async (type, otp) => {
    setloader(true);
    let obj = {
      otp: otp,
    };

    if (type == "phone") {
      obj.phone = screenData?.phone;
      obj.type = "phone";
    } else {
      obj.phone = screenData?.whatsapp;
      obj.type = "whatsapp";
    }

    const response = await verifyPinApi(obj);
    setloader(false);
    if (response?.data?.status === "1") {
      setOtpVerified((rest) => ({
        ...rest,
        [type]: true,
      }));

      toast.success(response.data.message);
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px] flex flex-col">
      <h1 className=" text-xl font-semibold mb-2 capitalize">
        don’t miss clients orders notifications.
      </h1>{" "}
      <h1 className=" text-xl font-semibold mb-2 capitalize">
        get it instantly{" "}
      </h1>
      <div className="mb-2 relative w-full ">
        <label className="block text-sm text-label mb-2 font-[500]">
          Phone Number
        </label>
        <div className="flex items-center gap-[10px]">
          <OtpInput
            // renderSeparator={<span>-</span>}
            renderInput={(props) => (
              <input
                placeholder="--"
                {...props}
              />
            )}
            inputStyle={{
              // Custom styles for the input fields
              width: "100%",
              maxWidth: "4rem",
              height: "3rem",
              margin: "0 0.5rem",
              fontSize: "2rem",
              borderRadius: "4px",
              border: "1px solid rgba(0,0,0,0.3)",
            }}
            numInputs={4}
            placeholder={true}
            value={phoneOtp}
            onChange={(otp) => {
              setPhoneOtp(otp);

              if (otp?.length == 4 && !otpVerified?.phone) {
                if (screenData?.phoneDuration > 0) {
                  verifyOTP("phone", otp);
                } else {
                  toast.error("Phone Number OTP Expired!");
                }
              }
            }}
          />
          {otpVerified?.phone && (
            <button
              type="button"
              className="justify-center inset-y-0 flex items-center px-3 flex-col gap-[5px] text-[#0066ff] "
            >
              <RiShieldCheckFill size={20} />
              <p className="text-[10px] font-bold">Verified</p>
            </button>
          )}
        </div>
        <div className="flex items-center gap-2 ml-1 pt-1">
          <label
            className="block text-[15px] text-label text-blue-500 cursor-pointer"
            onClick={() => {
              resendOTP("phone");
            }}
          >
            Resend
          </label>
          <label className="block text-[15px] text-label ">
            {formatTime(screenData?.phoneDuration)}
          </label>
        </div>
      </div>
      <div className="mb-2 relative w-full ">
        <label className="block text-sm text-label mb-2 font-[500]">
          Whatsapp Number
        </label>
        <div className="flex items-center gap-[10px]">
          <OtpInput
            // renderSeparator={<span>-</span>}
            renderInput={(props) => (
              <input
                placeholder="--"
                {...props}
              />
            )}
            inputStyle={{
              // Custom styles for the input fields
              width: "100%",
              maxWidth: "4rem",
              height: "3rem",
              margin: "0 0.5rem",
              fontSize: "2rem",
              borderRadius: "4px",
              border: "1px solid rgba(0,0,0,0.3)",
            }}
            numInputs={4}
            placeholder={true}
            value={whatsappOtp}
            onChange={(otp) => {
              setWhatsappOtp(otp);

              if (otp?.length == 4 && !otpVerified?.whatsappnum) {
                if (screenData?.whatsappDuration > 0) {
                  verifyOTP("whatsappnum", otp);
                } else {
                  toast.error("Whatsapp Number OTP Expired!");
                }
              }
            }}
          />
          {otpVerified?.whatsappnum && (
            <button
              type="button"
              className="justify-center inset-y-0 flex items-center px-3 flex-col gap-[5px] text-[#0066ff] "
            >
              <RiShieldCheckFill size={20} />
              <p className="text-[10px] font-bold">Verified</p>
            </button>
          )}
        </div>
        <div className="flex items-center gap-2 ml-1 pt-1">
          <label
            className="block text-[15px] text-label text-blue-500 cursor-pointer"
            onClick={() => {
              resendOTP("whatsappnum");
            }}
          >
            Resend
          </label>
          <label className="block text-[15px] text-label ">
            {formatTime(screenData?.whatsappDuration)}
          </label>
        </div>
      </div>
      <div className="mt-6 flex justify-end">
        {loader ? (
          <CircularProgress />
        ) : (
          <button
            disabled={!otpVerified?.phone && !otpVerified?.whatsappnum}
            className={`px-8 py-2 rounded-md ${
              otpVerified?.phone && otpVerified?.whatsappnum
                ? "bg-[#0066ff]"
                : "bg-[#cce0ff]"
            } bg-bgBlue text-white`}
            onClick={() => {
              setStep(6);
            }}
          >
            Next
          </button>
        )}
      </div>
    </div>
  );
};

export default ServiceNotificationForm;
