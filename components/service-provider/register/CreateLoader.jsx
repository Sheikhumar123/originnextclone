import { CircularProgress } from "@mui/material";
import React from "react";

function CreateLoader() {
  return (
    <div className="flex justify-center items-center w-[100%] py-16">
      <div className="flex gap-2 items-center">
        <CircularProgress sx={{
            color:"#4c5edb"
        }}  size={100} />
        <span className="font-bold text-2xl text-[#3E3E3E]">Account Creating</span>
      </div>

    </div>
  );
}

export default CreateLoader;
