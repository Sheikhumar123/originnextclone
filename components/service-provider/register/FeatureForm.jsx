"use client";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import { FaCirclePlus } from "react-icons/fa6";
import { RxCrossCircled } from "react-icons/rx";
import { baseUrl } from "@/utils/BaseURL";
import { Autocomplete, Chip, CircularProgress, TextField } from "@mui/material";
import { toast } from "react-toastify";
import axios from "axios";
import { FaUpload } from "react-icons/fa6";
import { useDispatch, useSelector } from "react-redux";
import {
  GetProviderCategories,
  GetProviderSkills,
  GetServiceProviderDetails,
} from "@/redux/Slices/ServiceProviderSlice/ServiceProviderSlice";
import axiousInstance from "@/utils/axiousInstance";
import { AddServicesofProviderApi } from "@/Apis/ServiceProviderApis/DashboardApi/ServicesApis/AddServicesofProviderApi";
import { useFormik } from "formik";
import * as Yup from "yup";

import dynamic from "next/dynamic";
const Editor = dynamic(
  () => {
    return import("react-draft-wysiwyg").then((mod) => mod.Editor);
  },
  { ssr: false }
);

// import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { EditorState, Modifier } from "draft-js";

const FeatureFormService = ({ setStep, userId }) => {
  const [loader, setloader] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const { ProviderCategories, ProviderSkills } = useSelector(
    GetServiceProviderDetails
  );
  const dispatch = useDispatch();
  const validationSchema = Yup.object().shape({
    featureFields: Yup.array().of(
      Yup.object().shape({
        imageUrl: Yup.string().required("Image is required"),
        categories: Yup.object().required("Categories is required"),
        subcategories: Yup.array()
          .min(1, "At least 1 Category is required")
          .max(3, "Please Select maximum 3 Categories")
          .required("test"),
        skills: Yup.array()
          .min(1, "At least 1 Skill is required")
          .max(3, "Please Select maximum 3 Skills")
          .required("test"),
        title: Yup.string().required("Title is required"),
        description: Yup.mixed()
          .test(
            "is-string-or-editorstate",
            "Description is required",
            (value) => {
              return typeof value === "string" || value instanceof EditorState;
            }
          )
          .required("Description is required"),
        // description: Yup.string().required("Description is required"),
        price: Yup.string().required("Price is required"),
        priceFrequency: Yup.string().required("Duration is required"),
      })
    ),
  });
  const formik = useFormik({
    initialValues: {
      featureFields: [
        {
          image: null,
          imageUrl: "",
          selectedskills: [],
          selectedsubcategories: [],
          categories: null,
          subcategories: [],
          Allsubcategories: [],
          skills: [],
          title: "",
          description: EditorState.createEmpty(),
          price: "",
          priceFrequency: "Hourly",
        },
      ],
    },
    validationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  useEffect(() => {
    dispatch(GetProviderCategories());
    dispatch(GetProviderSkills());
  }, [dispatch]);
  const handleAddFeature = () => {
    if (formik.values.featureFields?.length > 5) {
      return toast.error("You cannot add more than 5 Services");
    } else {
      formik.setFieldValue("featureFields", [
        ...formik.values.featureFields,
        {
          image: null,
          imageUrl: "",
          selectedskills: [],
          selectedsubcategories: [],
          categories: null,
          subcategories: [],
          Allsubcategories: [],
          skills: [],
          title: "",
          description: "",
          price: "",
          priceFrequency: "Hourly",
        },
      ]);
    }
  };
  const handleSubmit = async (values) => {
    const { convertToHTML } = await import("draft-convert");

    let arr1 = [];
    let error = false;
    values?.featureFields?.map((item) => {
      if (
        convertToHTML(item?.description.getCurrentContent()) == "" ||
        convertToHTML(item?.description.getCurrentContent()) == "<p></p>"
      ) {
        error = true;
      }
    });
    values?.featureFields?.map((item) => {
      arr1.push({
        category: item?.categories?.id,
        subcategory: item?.selectedsubcategories,
        skills: item?.selectedskills,
        title: item?.title,
        description: convertToHTML(item?.description.getCurrentContent()),
        starting_price: item?.price,
        duration: item?.priceFrequency,
      });
    });
    if (error) {
      return toast.error("Please Enter Description");
    } else if (errorMessage) {
      return toast.error("Please Enter valid Description");
    } else {
      setloader(true);

      const formData = new FormData();
      formData.append(`user_id`, userId);
      values.featureFields?.map((item, i) => {
        formData.append(`image`, item?.image);
      });
      formData.append("services", JSON.stringify(arr1));
      const response = await AddServicesofProviderApi(formData);
      setloader(false);
      if (response?.data?.status == "1") {
        toast.success(response.data.message);
        setStep(10);
      } else if (response?.data?.status == "0") {
        toast.error(response.data.message);
      } else {
        setloader(false);
        toast.error(response.response?.data.message);
      }
    }
  };
  const removeItem = (itemToRemove) => {
    const updatedItems = formik.values?.featureFields.filter(
      (item) => item !== itemToRemove
    );
    formik.setFieldValue("featureFields", updatedItems);
  };
  const getSubCat = async (id, index, field) => {
    try {
      const response = await axiousInstance.post(
        "landingpage/subcategorypopulate",
        { categories: id }
      );
      // setloader(false);
      if (response.data.status === "1") {
        console.log(response.data?.subcategory);
        formik.setFieldValue(
          `featureFields.${index}.Allsubcategories`,
          response.data?.subcategory
        );
        toast.success(response.data.message);
      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      toast.error(error.response?.data?.error);
    }
  };
  const handleOnChange = (editorState, index) => {
    let contentState = editorState.getCurrentContent();
    let newContentState = contentState;

    // Remove phone numbers and email addresses
    const phoneRegex =
      /\+\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}|\(\d{1,3}\)\s*\d{3,4}[-.\s]??\d{4}|\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}/g;
    const emailRegex = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/gi;

    let match;
    let containsInvalidContent = false;
    let containsInvalidContent2 = false;

    // Remove phone numbers
    contentState.getBlockMap().forEach((contentBlock) => {
      const blockText = contentBlock.getText();
      while ((match = phoneRegex.exec(blockText)) !== null) {
        const start = match.index;
        const end = start + match[0].length;
        newContentState = Modifier.replaceText(
          newContentState,
          editorState
            .getSelection()
            .merge({ anchorOffset: start, focusOffset: end }),
          ""
        );
        containsInvalidContent = true;
      }
    });

    // Remove email addresses
    contentState.getBlockMap().forEach((contentBlock) => {
      const blockText = contentBlock.getText();
      while ((match = emailRegex.exec(blockText)) !== null) {
        const start = match.index;
        const end = start + match[0].length;
        newContentState = Modifier.replaceText(
          newContentState,
          editorState
            .getSelection()
            .merge({ anchorOffset: start, focusOffset: end }),
          ""
        );
        containsInvalidContent2 = true;
      }
    });

    const newEditorState = EditorState.push(
      editorState,
      newContentState,
      "remove-range"
    );
    const { featureFields } = formik.values;
    featureFields[index].description = editorState;
    // && convertToHTML(editorState.getCurrentContent());
    formik.setFieldValue("featureFields", featureFields);
    formik.setFieldValue("description", newEditorState);

    if (containsInvalidContent) {
      setErrorMessage("Please remove phone numbers before entering text.");
    } else if (containsInvalidContent2) {
      setErrorMessage("Please remove email addresses before entering text.");
    } else {
      setErrorMessage("");
    }
  };
  console.log(formik.values.featureFields);
  // const onEditorStateChange = (index, editorState) => {
  //   // const newFeatureFields = [...formik.values.featureFields];
  //   // newFeatureFields[index].description = editorState;
  //   // formik.setFieldValue(`featureFields.${index}.description`, editorState);
  //   const { featureFields } = formik.values;
  //   featureFields[index].description = editorState;

  //   formik.setFieldValue("featureFields", featureFields);
  // };
  return (
    <div className="p-4 border border-gray-100  rounded-2xl shadow-md w-full 800px:min-w-[600px] 1600px:max-w-full 300px:max-w-[800px] flex flex-col">
      <h1 className="text-xl label font-semibold capitalize">
        Add Services That you Provide{" "}
      </h1>
      <form onSubmit={formik.handleSubmit}>
        <div className="overflow-y-auto h-[37rem] p-4">
          {formik.values.featureFields?.map((feature, index) => (
            <div key={index} className="mb-8 p-2 rounded-xl shadow-md relative">
              {index == 0 ? null : (
                <RxCrossCircled
                  onClick={() => removeItem(feature)}
                  size={20}
                  className="absolute right-0 cursor-pointer text-red-700"
                />
              )}
              <div>
                <label
                  className="cursor-pointer label w-[fit-content]"
                  htmlFor={`featureImage-${index}`}
                >
                  Upload Image here
                  <input
                    id={`featureImage-${index}`}
                    type="file"
                    accept="image/*"
                    name={`featureFields.${index}.imageUrl`}
                    // onChange={(event) => handleInputChange(index, event, "image")}
                    // value={item.question}
                    onChange={(event) => {
                      const newFeatureFields = [...formik.values.featureFields];
                      const file = event.target.files[0];
                      const reader = new FileReader();
                      reader.onloadend = () => {
                        formik.values.featureFields[index].image = file;
                        formik.values.featureFields[index].imageUrl =
                          reader.result;
                        formik.setFieldValue("featureFields", newFeatureFields);
                        // setFeatureFields(newFeatureFields);
                      };
                      if (file) {
                        reader.readAsDataURL(file);
                      }
                    }}
                    className="mb-2 hidden"
                  />
                </label>
              </div>
              <div className="mb-2 w-[150px] h-[110px] cursor-pointer">
                {feature.imageUrl ? (
                  <label htmlFor={`featureImage-${index}`}>
                    <Image
                      src={feature.imageUrl}
                      alt="Feature Preview"
                      className="mb-2  cursor-pointer"
                      width={150}
                      height={150}
                      style={{ width: "100%", height: "100%" }}
                    />
                    <input
                      id={`featureImage-${index}`}
                      type="file"
                      accept="image/*"
                      name={`featureFields.${index}.imageUrl`}
                      onChange={(event) => {
                        const newFeatureFields = [
                          ...formik.values.featureFields,
                        ];
                        const file = event.target.files[0];
                        const reader = new FileReader();
                        reader.onloadend = () => {
                          formik.values.featureFields[index].image = file;
                          formik.values.featureFields[index].imageUrl =
                            reader.result;
                          formik.setFieldValue(
                            "featureFields",
                            newFeatureFields
                          );
                          // setFeatureFields(newFeatureFields);
                        };
                        if (file) {
                          reader.readAsDataURL(file);
                        }
                      }}
                      // onChange={(event) =>
                      //   handleInputChange(index, event, "image")
                      // }
                      className="mb-2 hidden"
                    />
                  </label>
                ) : (
                  <label
                    className="border w-[150px] h-[110px] flex justify-center items-center border-gray-300"
                    htmlFor={`featureImage-${index}`}
                  >
                    <div className="flex justify-center items-center flex-col gap-1">
                      <FaUpload color="grey" className="text-red" size={26} />
                      <p className="text-[grey] font-semibold">Upload</p>
                    </div>

                    <input
                      id={`featureImage-${index}`}
                      type="file"
                      accept="image/*"
                      name={`featureFields.${index}.imageUrl`}
                      onChange={(event) => {
                        const newFeatureFields = [
                          ...formik.values.featureFields,
                        ];
                        const file = event.target.files[0];
                        const reader = new FileReader();
                        reader.onloadend = () => {
                          formik.values.featureFields[index].image = file;
                          formik.values.featureFields[index].imageUrl =
                            reader.result;
                          formik.setFieldValue(
                            "featureFields",
                            newFeatureFields
                          );
                          // setFeatureFields(newFeatureFields);
                        };
                        if (file) {
                          reader.readAsDataURL(file);
                        }
                      }}
                      // onChange={(event) =>
                      //   handleInputChange(index, event, "image")
                      // }
                      className="mb-2 hidden"
                    />
                  </label>
                )}
              </div>
              {formik.touched.featureFields &&
                formik.errors.featureFields &&
                formik.errors.featureFields[index] && (
                  <p className="text-red-700 text-sm">
                    {" "}
                    {formik.errors.featureFields[index].imageUrl}
                  </p>
                )}
              <div className="flex flex-col gap-2 mb-2">
                <label htmlFor="category" className="text-md label">
                  Select Category
                </label>
                <Autocomplete
                  options={ProviderCategories || []}
                  sx={{
                    "& .MuiAutocomplete-inputRoot": {},
                    width: "100%",
                  }}
                  size="small"
                  name={`featureFields.${index}.categories`}
                  value={feature.categories || null}
                  onChange={(event, newValue) => {
                    formik.setFieldValue(
                      `featureFields.${index}.categories`,
                      newValue
                    );
                    formik.setFieldValue(
                      `featureFields.${index}.subcategories`,
                      []
                    );
                    getSubCat([newValue?.id], index, "Allsubcategories");
                  }}
                  getOptionLabel={(option) => option?.title}
                  isOptionEqualToValue={(option, value) =>
                    option?.id === value?.id
                  }
                  fullWidth
                  renderInput={(params) => (
                    <TextField size="small" {...params} />
                  )}
                />
                {/* <Autocomplete
                  options={ProviderCategories || []}
                  sx={{
                    "& .MuiAutocomplete-inputRoot": {},
                    width: "100%",
                  }}
                  size="small"
                  value={feature.categories || null}
                  onChange={(event, newValue) => {
                    handleInputChange(index, newValue, "categories");
                    getSubCat([newValue?.id], index, "Allsubcategories");
                    const newFeatureFields = [...featureFields];
                    newFeatureFields[index]["subcategories"] = [];
                    setFeatureFields(newFeatureFields);
                  }}
                  getOptionLabel={(option) => option?.title}
                  isOptionEqualToValue={(option, value) =>
                    option?.id === value?.id
                  }
                  fullWidth
                  renderInput={(params) => (
                    <TextField size="small" {...params} />
                  )}
                /> */}
                {formik.touched.featureFields &&
                  formik.errors.featureFields &&
                  formik.errors.featureFields[index] && (
                    <p className="text-red-700 text-sm">
                      {" "}
                      {formik.errors.featureFields[index].categories}
                    </p>
                  )}
              </div>
              <div className="flex flex-col gap-2 mb-2">
                <label htmlFor="subcategory" className="text-md label">
                  Select Sub-Category
                </label>
                <Autocomplete
                  multiple
                  limitTags={2}
                  size="small"
                  id="multiple-limit-tags-subcategory"
                  options={feature?.Allsubcategories || []}
                  name={`featureFields.${index}.subcategories`}
                  value={feature.subcategories || []}
                  onChange={(event, newValue) => {
                    if (newValue?.length > 3) {
                      toast.error("Please Enter Maximum 3 Sub Categories");
                    } else {
                      formik.setFieldValue(
                        `featureFields.${index}.selectedsubcategories`,
                        newValue?.map((category) => category?._id)
                      );

                      formik.setFieldValue(
                        `featureFields.${index}.subcategories`,
                        newValue
                      );
                    }
                  }}
                  // value={feature?.subcategories}
                  // onChange={(event, newValue) =>
                  //   handleInputChange(index, newValue, "subcategories")
                  // }
                  getOptionLabel={(option) => option?.title}
                  renderOption={(props, option) => (
                    <li {...props}>{option?.title}</li>
                  )}
                  renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                      <Chip
                        key={option}
                        label={option?.title}
                        {...getTagProps({ index })}
                        style={{ backgroundColor: "#c7c9f4" }}
                      />
                    ))
                  }
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      sx={{
                        "& .MuiOutlinedInput-root": {
                          borderRadius: "8px",
                        },
                        color: "#9E9E9E",
                        width: "100%",
                      }}
                    />
                  )}
                  isOptionEqualToValue={(option, value) =>
                    option?._id === value?._id
                  }
                />
                {formik.touched.featureFields &&
                  formik.errors.featureFields &&
                  formik.errors.featureFields[index] && (
                    <p className="text-red-700 text-sm">
                      {" "}
                      {formik.errors.featureFields[index].subcategories}
                    </p>
                  )}
              </div>
              <div className="flex flex-col gap-2 mb-2">
                <label htmlFor="skills" className="text-md label">
                  Select Skills
                </label>
                <Autocomplete
                  multiple
                  limitTags={2}
                  size="small"
                  id="multiple-limit-tags-skills"
                  options={ProviderSkills || []}
                  name={`featureFields.${index}.skills`}
                  value={feature.skills || []}
                  onChange={(event, newValue) => {
                    if (newValue?.length > 3) {
                      toast.error("Please Enter Maximum 3 Skills");
                    } else {
                      formik.setFieldValue(
                        `featureFields.${index}.selectedskills`,
                        newValue?.map((skill) => skill?.id)
                      );

                      formik.setFieldValue(
                        `featureFields.${index}.skills`,
                        newValue
                      );
                    }
                  }}
                  getOptionLabel={(option) => option?.title}
                  renderOption={(props, option) => (
                    <li {...props}>{option?.title}</li>
                  )}
                  renderTags={(value, getTagProps) =>
                    value.map((option, index) => (
                      <Chip
                        key={option}
                        label={option?.title}
                        {...getTagProps({ index })}
                        style={{ backgroundColor: "#c7c9f4" }}
                      />
                    ))
                  }
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      sx={{
                        "& .MuiOutlinedInput-root": {
                          borderRadius: "8px",
                        },
                        color: "#9E9E9E",
                        width: "100%",
                      }}
                    />
                  )}
                  isOptionEqualToValue={(option, value) =>
                    option?.id === value?.id
                  }
                />
                {formik.touched.featureFields &&
                  formik.errors.featureFields &&
                  formik.errors.featureFields[index] && (
                    <p className="text-red-700 text-sm">
                      {" "}
                      {formik.errors.featureFields[index].skills}
                    </p>
                  )}
              </div>
              <label
                htmlFor={`featureTitle-${index}`}
                className="label text-md  block"
              >
                Title
              </label>
              <input
                id={`featureTitle-${index}`}
                type="text"
                name={`featureFields.${index}.title`}
                value={feature.title}
                onChange={formik.handleChange}
                className="w-full h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Enter title"
              />
              {formik.touched.featureFields &&
                formik.errors.featureFields &&
                formik.errors.featureFields[index] && (
                  <p className="text-red-700 text-sm">
                    {" "}
                    {formik.errors.featureFields[index].title}
                  </p>
                )}
              <label
                htmlFor={`featureDescription-${index}`}
                className="label text-md block"
              >
                Description
              </label>
              <Editor
                placeholder="Enter description"
                name={`featureFields.${index}.description`}
                editorState={feature.description}
                // value={feature.description}
                onEditorStateChange={(editorState) => {
                  handleOnChange(editorState, index);
                }}
                // onEditorStateChange={(editorState) =>
                //   onEditorStateChange(index, editorState)
                // }
                editorStyle={{
                  backgroundColor: "#ffffff",
                  // height:"100px"
                }}
                className="w-full px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              />
              {/* <textarea
                id={`featureDescription-${index}`}
                rows={8}
                cols={20}
                name={`featureFields.${index}.description`}
                value={feature.description}
                onChange={formik.handleChange}
                className="w-full px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Enter description"
              /> */}
              {errorMessage && (
                <div style={{ color: "red" }}>{errorMessage}</div>
              )}

              {formik.touched.featureFields &&
                formik.errors.featureFields &&
                formik.errors.featureFields[index] && (
                  <p className="text-red-700 text-sm">
                    {" "}
                    {formik.errors.featureFields[index].description}
                  </p>
                )}
              <div className="mb-2">
                <div className="flex 600px:items-center justify-between flex-col 600px:flex-row">
                  {" "}
                  <label
                    htmlFor={`featurePrice-${index}`}
                    className="label text-md  block"
                  >
                    Starting Price
                  </label>
                  <div className=" flex items-center gap-2 flex-grow 600px:flex-grow-0">
                    <div className="w-[100%]">
                      <input
                        id={`featurePrice-${index}`}
                        type="number"
                        name={`featureFields.${index}.price`}
                        value={feature.price}
                        onChange={formik.handleChange}
                        className="w-full h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                        placeholder="Enter price"
                      />
                      {formik.touched.featureFields &&
                        formik.errors.featureFields &&
                        formik.errors.featureFields[index] && (
                          <p className="text-red-700 text-sm">
                            {" "}
                            {formik.errors.featureFields[index].price}
                          </p>
                        )}
                    </div>
                    <div className="w-[100%]">
                      <select
                        id={`featurePriceFrequency-${index}`}
                        name={`featureFields.${index}.priceFrequency`}
                        value={feature.priceFrequency}
                        onChange={formik.handleChange}
                        className="w-full h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                      >
                        <option value="Hourly">Hourly</option>
                        <option value="Daily">Daily</option>
                        <option value="Weekly">Weekly</option>
                        <option value="Monthly">Monthly</option>
                      </select>
                      {formik.touched.featureFields &&
                        formik.errors.featureFields &&
                        formik.errors.featureFields[index] && (
                          <p className="text-red-700 text-sm">
                            {" "}
                            {formik.errors.featureFields[index].description}
                          </p>
                        )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
        <button
          className="px-4 py-2 rounded-md mt-2 text-black self-start flex items-center gap-1"
          onClick={handleAddFeature}
          type="button"
        >
          <FaCirclePlus />
          <span>Add More</span>
        </button>
        <div className="flex justify-between items-center">
          <button
            type="button"
            className="px-4 py-2 rounded-md  bg-[#dedede] self-end mt-6 "
            onClick={() => setStep(10)}
          >
            Skip for now
          </button>
          {loader ? (
            <div className="self-end">
              <CircularProgress />
            </div>
          ) : (
            <button
              type="submit"
              className="px-8 py-2 rounded-md bg-blue-500 text-white self-end mt-4"
              // onClick={handleSubmit}
            >
              Next
            </button>
          )}
        </div>
      </form>
    </div>
  );
};

export default FeatureFormService;
