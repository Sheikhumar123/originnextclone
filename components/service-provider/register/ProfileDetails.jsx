"use client";

import { addServiceProviderCategoryData } from "@/Apis/ServiceProviderApis/ServiceProviderApis";
import { CircularProgress } from "@mui/material";
import { useFormik } from "formik";
import { useEffect, useState } from "react";
import { IoChevronBackSharp } from "react-icons/io5";
import { toast } from "react-toastify";
import * as Yup from "yup";
import dynamic from "next/dynamic";

// import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { EditorState, Modifier } from "draft-js";
const Editor = dynamic(
  () => {
    return import("react-draft-wysiwyg").then((mod) => mod.Editor);
  },
  { ssr: false }
);

const ProfileDetails = ({ data, handleChange, setStep, userId }) => {
  const [loader, setloader] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const initialValues = {
    title: "",
    description: EditorState.createEmpty(),
  };
  const ValidationSchema = Yup.object().shape({
    title: Yup.string().required("Please Enter Title"),
    description: Yup.mixed()
      .test("is-string-or-editorstate", "Description is required", (value) => {
        return typeof value === "string" || value instanceof EditorState;
      })
      .required("Description is required"),
    // description: Yup.string().required("Please Enter Description"),
  });
  // const handleSubmit = async (values) => {
  //   setStep(3);
  // };
  console.log(data);
  const handleSubmit = async (values, setSubmitting, resetForm) => {
    const { convertToHTML } = await import("draft-convert");

    let arr = [];
    let arr3 = [];

    data?.providingDetails?.categories?.map((item) => {
      let arr2 = [];
      data?.providingDetails?.subCategories?.map((item2) => {
        if (
          item?.id == item2?.categoryid &&
          item?.title == item2?.categorytype
        ) {
          arr2.push(item2?._id);
        }
      });
      arr.push({ category: item?.id, subcategory: arr2 });
    });
    data?.providingDetails?.skills?.map((item) => {
      arr3.push(item?.id);
    });
    let error = false;
    if (
      convertToHTML(values?.description.getCurrentContent()) == "" ||
      convertToHTML(values?.description.getCurrentContent()) == "<p></p>"
    ) {
      error = true;
    }
    if (error) {
      return toast.error("Please Enter Description");
    } else {
      setloader(true);
      let obj = {
        categories: arr,
        skills: arr3,
        ...values,
        description: convertToHTML(values?.description.getCurrentContent()),
        user_id: userId,
        language: data?.providingDetails?.language,
      };

      const response = await addServiceProviderCategoryData(obj);
      setloader(false);
      if (response?.data?.status === "1") {
        toast.success(response.data.message);
        setStep(9);
      } else if (response?.data?.status === "0") {
        toast.error(response.data.message);
      } else {
        toast.error(response.response?.data.message);
      }
    }
    // setStep(9);
  };
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  const handleOnChange = (editorState) => {
    let contentState = editorState.getCurrentContent();
    let newContentState = contentState;

    // Remove phone numbers and email addresses
    const phoneRegex =
      /\+\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}|\(\d{1,3}\)\s*\d{3,4}[-.\s]??\d{4}|\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}/g;
    const emailRegex = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/gi;

    let match;
    let containsInvalidContent = false;
    let containsInvalidContent2 = false;

    // Remove phone numbers
    contentState.getBlockMap().forEach((contentBlock) => {
      const blockText = contentBlock.getText();
      while ((match = phoneRegex.exec(blockText)) !== null) {
        const start = match.index;
        const end = start + match[0].length;
        newContentState = Modifier.replaceText(
          newContentState,
          editorState
            .getSelection()
            .merge({ anchorOffset: start, focusOffset: end }),
          ""
        );
        containsInvalidContent = true;
      }
    });

    // Remove email addresses
    contentState.getBlockMap().forEach((contentBlock) => {
      const blockText = contentBlock.getText();
      while ((match = emailRegex.exec(blockText)) !== null) {
        const start = match.index;
        const end = start + match[0].length;
        newContentState = Modifier.replaceText(
          newContentState,
          editorState
            .getSelection()
            .merge({ anchorOffset: start, focusOffset: end }),
          ""
        );
        containsInvalidContent2 = true;
      }
    });

    const newEditorState = EditorState.push(
      editorState,
      newContentState,
      "remove-range"
    );

    formik.setFieldValue("description", newEditorState);

    if (containsInvalidContent) {
      setErrorMessage("Please remove phone numbers before entering text.");
    } else if (containsInvalidContent2) {
      setErrorMessage("Please remove email addresses before entering text.");
    } else {
      setErrorMessage("");
    }
  };
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px]">
      <h1 className=" text-xl mb-2 label capitalize">
        Write details that client get interested in your Profile{" "}
      </h1>
      <form onSubmit={formik.handleSubmit}>
        <div className="mb-2">
          <label htmlFor="name" className="label">
            Catchy Title
          </label>
          <input
            type="text"
            name="title"
            value={formik.values.title}
            onChange={formik.handleChange}
            className="w-full bg-white h-[30px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="Write Text here"
          />
          {formik.errors.title &&
            formik.touched.title &&
            formik.errors.title && (
              <p className="text-red-700 text-sm">{formik.errors.title}</p>
            )}
        </div>
        <div className="mb-5">
          <label
            htmlFor="clientDetails"
            className="label text-md  font-semibold"
          >
            Describe about you and your business{" "}
          </label>
          <Editor
            placeholder="Enter description"
            name="description"
            editorState={formik.values.description}
            // value={feature.description}
            // onEditorStateChange={(editorState) => {
            //   formik.setFieldValue("description", editorState);
            //   // onEditorStateChange(index, editorState)
            // }}
            onEditorStateChange={handleOnChange}

            editorStyle={{
              backgroundColor: "#ffffff",
              // height:"100px"
            }}
            className="w-full px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          />
          {/* <textarea
            id="serviceProviderDetails"
            rows={8}
            name="description"
            value={formik.values.description}
            onChange={formik.handleChange}
            className="w-full bg-white  px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="Write Details"
          /> */}
            {errorMessage && <div style={{ color: "red" }}>{errorMessage}</div>}

          {formik.errors.description &&
            formik.touched.description &&
            formik.errors.description && (
              <p className="text-red-700 text-sm">
                {formik.errors.description}
              </p>
            )}
        </div>
        <div className="flex justify-between items-center">
          <button
            type="button"
            className=" py-2 rounded-md  text-bgBlue self-end mt-6 "
            onClick={() => {
              setStep(7);
            }}
          >
            <div className="px-1 flex gap-1 items-center border-b border-bgBlue">
              <IoChevronBackSharp />
              <div>Previous</div>
            </div>
          </button>
          {loader ? (
            <CircularProgress className="self-end" />
          ) : (
            <button
              type="submit"
              className="px-8 py-2 rounded-md bg-bgBlue text-white self-end"
              // onClick={() => {
              //   setStep(9);
              // }}
            >
              Next
            </button>
          )}
        </div>
      </form>
    </div>
  );
};

export default ProfileDetails;
