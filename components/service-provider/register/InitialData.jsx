import { Autocomplete, Chip, TextField } from "@mui/material";
import { Country, State, City } from "country-state-city";
import { useEffect, useState } from "react";

const InitialData = ({ data, handleChange, setStep, setData }) => {
  const [allcountries, setallcountries] = useState([]);
  const [allstates, setallstates] = useState([]);
  const [allcities, setallcities] = useState([]);
  const [country, setcountry] = useState(null);
  const [states, setstates] = useState(null);
  const [city, setcity] = useState(null);
  console.log(data);
  useEffect(() => {
    let value = Country.getAllCountries()?.map((data) => {
      let obj = {
        label: data?.name,
        value: data,
      };
      return obj;
    });
    setallcountries(value);
  }, []);
  useEffect(() => {
    let value = State.getStatesOfCountry(country?.isoCode);
    value = value?.map((data) => {
      let obj = {
        label: data?.name,
        value: data,
      };
      return obj;
    });
    setallstates(value);
  }, [country]);
  useEffect(() => {
    let value = City.getCitiesOfState(states?.countryCode, states?.isoCode);
    value = value?.map((data) => {
      let obj = {
        label: data?.name,
        value: data?.name,
      };
      return obj;
    });
    setallcities(value);
  }, [states]);
  const countries = [
    "United States",
    "United Kingdom",
    "Canada",
    "Australia",
    "Germany",
  ];
  // console.log(data);
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full ">
      <h1 className="font-bold label text-xl">
        Where Would You Like To Get Leads From?
      </h1>
      <p className="mb-4">
        Tell us the area you cover so we can show you leads for your location.
      </p>
      {/* LEADS LOCATION  */}
      <div className="flex flex-col gap-2 mb-2">
        <div className="form-control">
          <label className="flex items-center gap-3 cursor-pointer">
            <input
              type="radio"
              name="leadsLocation"
              className="radio checked:bg-bgBlue"
              value={"worldwide"}
              onChange={handleChange}
            // defaultChecked
            />
            <span className="label-text">
              I provide services world wide (All Available Countries)
            </span>
          </label>
        </div>
        <div className="form-control">
          <label className="flex items-center gap-3 cursor-pointer">
            <input
              type="radio"
              name="leadsLocation"
              onChange={handleChange}
              value={"region"}
              className="radio checked:bg-bgBlue"
            />{" "}
            <span className="label-text">
              I provide service only below region..
            </span>
          </label>
        </div>
      </div>
      {data.leadsLocation && data.leadsLocation === "region" && (
        <div className="w-full">
          <div className="flex flex-col 500px:flex-row 500px:items-center 500px:justify-between">
            <label htmlFor="designation" className="label text-md ">
              Your Country
            </label>
            <Autocomplete
              size="small"
              options={allcountries}
              id="country"
              getOptionLabel={(option) => option.label}
              onChange={(event, value) => {
                console.log(value);
                setcountry({
                  value: value?.label,
                  label: value?.label,
                  isoCode: value?.value?.isoCode,
                });

                setstates(null);
                setcity(null);
                setData({ ...data, country: value?.label });
              }}
              renderOption={(props, option) => {
                return (
                  <li {...props} key={option}>
                    {option.label}
                  </li>
                );
              }}
              renderInput={(params) => (
                <TextField
                  style={{ color: "#808080" }}
                  {...params}
                  //   label="Countries"
                  placeholder="Country"
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      borderRadius: "8px",
                    },
                    color: "#9E9E9E",
                  }}
                />
              )}
              sx={{
                maxWidth: "280px",
                minWidth: "250px",
                borderRadius: "8px",
              }}
            />
          </div>
          {country
            ? !country && (
              <p className="text-red-700 text-sm text-right">
                Please Enter Country
              </p>
            )
            : null}
          <div className="flex items-center justify-between mt-2">
            <label htmlFor="designation" className="label text-md">
              State
            </label>
            <Autocomplete
              size="small"
              options={allstates}
              id="states"
              isDisabled={!country ? true : false}
              getOptionLabel={(option) => option.label}
              onChange={(event, value) => {
                console.log(value);
                setstates({
                  label: value?.label,
                  value: value?.label,
                  isoCode: value?.value?.isoCode,
                  countryCode: value?.value?.countryCode,
                });
                setcity(null);
                setData({ ...data, state: value?.label });
              }}
              renderOption={(props, option) => {
                return (
                  <li {...props} key={option}>
                    {option.label}
                  </li>
                );
              }}
              renderInput={(params) => (
                <TextField
                  style={{ color: "#808080" }}
                  {...params}
                  //   label="Countries"
                  placeholder="States"
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      borderRadius: "8px",
                    },
                    color: "#9E9E9E",
                  }}
                />
              )}
              sx={{
                maxWidth: "280px",
                minWidth: "250px",
                borderRadius: "8px",
              }}
            />
            {/* <select
              id="experience"
              className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[150px]"
              defaultValue={data?.experience || ""}
              name="experience"
              onChange={handleChange}
              >
              <option value={""}>Select</option>
              <option value="Anatola">Anatola</option>
              <option value="Abc">ABC</option>
            </select> */}
          </div>
          {states
            ? !states && (
              <p className="text-red-700 text-sm text-right">
                Please Enter State
              </p>
            )
            : null}
          <div className="flex items-center justify-between mt-2">
            <label htmlFor="city" className="label text-md ">
              Your City
            </label>
            <Autocomplete
              size="small"
              id="city"
              options={allcities}
              isDisabled={!states ? true : false}
              getOptionLabel={(option) => option.label}
              onChange={(event, value) => {
                console.log(value);
                setcity({
                  label: value?.label,
                  value: value?.label,
                  isoCode: value?.value?.isoCode,
                  countryCode: value?.value?.countryCode,
                });
                setData({ ...data, city: value?.label });
              }}
              renderOption={(props, option) => {
                return (
                  <li {...props} key={option}>
                    {option.label}
                  </li>
                );
              }}
              renderInput={(params) => (
                <TextField
                  style={{ color: "#808080" }}
                  {...params}
                  //   label="Countries"
                  placeholder="City"
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      borderRadius: "8px",
                    },
                    color: "#9E9E9E",
                  }}
                />
              )}
              sx={{
                maxWidth: "280px",
                minWidth: "250px",
                borderRadius: "8px",
              }}
            />
          </div>
          {city
            ? !city && (
              <p className="text-red-700 text-sm text-right">
                Please Enter City
              </p>
            )
            : null}
        </div>
      )}
      <label htmlFor="" className="text-gray-400 block mb-4">
        *You can change this later also
      </label>
      <button
      type="button"
        disabled={data?.leadsLocation == "" || !data?.leadsLocation}
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end"
        onClick={() => {
          if (data?.leadsLocation == "region" && !country) {
            return;
          } else if (data?.leadsLocation == "region" && !states) {
            return;
          } else if (data?.leadsLocation == "region" && !city) {
            return;
          } else {
            setStep(2);
          }
        }}
      >
        Next
      </button>
    </div>
  );
};

export default InitialData;
