import { verifyPinApi } from "@/Apis/AuthApis/AuthApis";
import { verifyEmailApi } from "@/Apis/ServiceProviderApis/ServiceProviderApis";
import { CircularProgress } from "@mui/material";
import React, { useEffect, useState } from "react";
import OtpInput from "react-otp-input";
import { toast } from "react-toastify";

function VerifyOtp({ getotp, setStep, data, screenData, setScreenData }) {
  console.log(data);
  const [otp, setOtp] = useState("");
  const [loader, setloader] = useState(false);

  const formatTime = (time) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
  };

  const resendOTP = async () => {
    setloader(true);
    let obj = {
      email: screenData?.email,
      password: screenData?.password,
    };

    const response = await verifyEmailApi(obj);
    setloader(false);
    if (response?.data?.status === "1") {
      setScreenData((data) => ({
        ...data,
        duration: response?.data?.duration * 60,
      }));
      toast.success(response.data.message);
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };

  const handleSubmt = async () => {
    if (screenData?.duration > 0) {
      setloader(true);
      let obj = {
        otp: otp,
        email: screenData?.email,
        type: "email",
      };
      const response = await verifyPinApi(obj);
      setloader(false);
      if (response?.data?.status === "1") {
        toast.success(response.data.message);
        setStep(4);
      } else if (response?.data?.status === "0") {
        toast.error(response.data.message);
      } else {
        toast.error(response.response?.data.message);
      }
    } else {
      toast.error("OTP Expired!");
    }
  };
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px] flex flex-col">
      <h1 className=" text-xl font-semibold capitalize label">Verify OTP</h1>{" "}
      <h1 className=" text-xl  mb-2 capitalize">
        Go to Your Email for the verification proccess we will send 4 digits
        code to your email.
      </h1>
      <div className="mb-2 w-full 900px:w-[80%]">
        <label className="block text-sm text-label  mb-2">OTP</label>
        <OtpInput
          value={otp}
          onChange={setOtp}
          placeholder={true}
          numInputs={4}
          // renderSeparator={<span>-</span>}
          renderInput={(props) => <input placeholder="--" {...props} />}
          inputStyle={{
            // Custom styles for the input fields
            width: "100%",
            maxWidth: "4rem",
            height: "3rem",
            margin: "0 0.5rem",
            fontSize: "2rem",
            borderRadius: "4px",
            border: "1px solid rgba(0,0,0,0.3)",
          }}
        />
        {otp
          ? (otp == "" || !otp || otp?.length < 4) && (
              <p className="text-red-700 text-sm ">Please Enter OTP</p>
            )
          : null}
        {/* <input
          type="number"
          name="emailOtp"
          value={data?.emailOtp || ""}
          onChange={handleChange}
          className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="XXXXXX"
        /> */}
        <div className="flex items-center gap-2 ml-1 pt-1">
          <label
            className="block text-[15px] text-label text-blue-500 cursor-pointer"
            onClick={resendOTP}
          >
            Resend
          </label>
          <label className="block text-[15px] text-label ">
            {formatTime(screenData?.duration)}
          </label>
        </div>
      </div>
      <div className="mt-6 flex justify-end">
        {loader ? (
          <CircularProgress />
        ) : (
          <button
            className="px-8 py-2 rounded-md bg-bgBlue text-white"
            disabled={otp?.length < 4 ? true : false}
            onClick={handleSubmt}
          >
            Next
          </button>
        )}
      </div>
    </div>
  );
}

export default VerifyOtp;
