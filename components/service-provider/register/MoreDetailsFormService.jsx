import { useFormik } from "formik";
import * as Yup from "yup";
import CreateLoader from "./CreateLoader";
import { baseUrl } from "@/utils/BaseURL";
import axios from "axios";
import { toast } from "react-toastify";
import { useState } from "react";

const MoreDetailFormService = ({ data, setStep, setuserId, screenData }) => {
  const [loader, setloader] = useState(false);
  console.log(data);
  console.log(screenData);
  const initialValues = {
    name: "",
    company_name: "",
    professional_type: "",
    experience: "",
    starting_price: "",
    website_url: "",
  };
  const ValidationSchema = Yup.object().shape({
    name: Yup.string().required("Please Enter Name"),
    professional_type: Yup.string().required("Please Enter Professional Type"),
    experience: Yup.string().required("Please Enter Experience"),
    starting_price: Yup.string().required("Please Enter Starting Price"),
  });
  const handleSubmit = async (values, setSubmitting, resetForm) => {
    setloader(true);
    let obj = {
      ...values,
      email: data?.email,
      password: data?.password,
      phonenum: screenData?.phone,
      pcountry_code: screenData?.dialCode,
      whatsappnum: screenData?.whatsapp,
      wcountry_code: screenData?.whatsappDialCode,
      login_method: "Custom",
      worldwide: data?.leadsLocation == "worldwide" ? true : false,
      region: data?.leadsLocation == "region" ? true : false,
      country: data?.country ? data?.country : "",
      state: data?.state ? data?.state : "",
      city: data?.city ? data?.city : "",
    };
    console.log(values);
    await axios({
      url: `${baseUrl}userdata/registerserviceprovider`,
      method: "Post",
      headers: {
        "content-type": "application/json",
      },
      data: obj,
    })
      .then((res) => {
        setloader(false);

        if (res.data.status === "1") {
          toast.success(res.data.message);
          setuserId(res.data?.user_id);
          setStep(7);
        } else {
          toast.error(res.data.message);
        }
      })
      .catch((err) => {
        setloader(false);
        toast.error(err.response?.data?.error);
      });
  };

  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  return (
    <>
      {loader ? (
        <CreateLoader />
      ) : (
        <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px] flex flex-col">
          <h1 className=" text-xl label font-semibold capitalize">
            Some Details About you{" "}
          </h1>{" "}
          <h1
            className="text-lg font-semibold label
      "
          >
            You are just few steps away from viewing the potential leads.{" "}
          </h1>
          <form onSubmit={formik.handleSubmit}>
            <div className="mb-2">
              <label className="label" htmlFor="name">
                Your Name
              </label>
              <input
                type="text"
                name="name"
                value={formik.values.name}
                onChange={formik.handleChange}
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="John Doe"
              />
              {formik.errors.name &&
                formik.touched.name &&
                formik.errors.name && (
                  <p className="text-red-700 text-sm">{formik.errors.name}</p>
                )}
            </div>
            <div className="mb-2">
              <label className="label" htmlFor="companyName">
                Company Name
              </label>
              <input
                type="text"
                name="company_name"
                value={formik.values.company_name}
                onChange={formik.handleChange}
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="John Doe"
              />
            </div>
            <p className=" text-[#585858] text-sm ">
              If you are not a business or don’t have information you can leave
              this blank{" "}
            </p>
            <div className="flex 500px:items-center 300px:items-start 500px:flex-row 300px:flex-col  justify-between mt-2">
              <label htmlFor="professionalType" className=" text-md label">
                Professional Type
              </label>
              <select
                id="professionalType"
                className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 500px:max-w-[150px] 300px:max-w-full"
                value={formik.values.professional_type}
                onChange={formik.handleChange}
                name="professional_type"
              >
                <option value={""}>Select</option>
                <option value="Dev">Dev</option>
                <option value="Programmer">Programmer</option>
              </select>
            </div>
            {formik.errors.professional_type &&
              formik.touched.professional_type &&
              formik.errors.professional_type && (
                <p className="text-red-700 text-sm text-end">
                  {formik.errors.professional_type}
                </p>
              )}
            <div className="flex 500px:flex-row 300px:flex-col 500px:items-center 300px:items-start justify-between mt-2">
              <label htmlFor="servicesDuration" className=" text-md label">
                From how many years you providing Services{" "}
              </label>
              <select
                id="servicesDuration"
                className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 500px:max-w-[150px] 300px:max-w-full"
                value={formik.values.experience}
                onChange={formik.handleChange}
                name="experience"
              >
                <option value={""}>Select</option>
                <option value="1-2 Year">1-2 Year</option>
                <option value="2-3 Year">2-3 Year</option>
                <option value="3-4 Year">3-4 Year</option>
                <option value="4-5 Year">4-5 Year</option>
                <option value="5-6 Year">5-6 Year</option>
                <option value="Above 6 Year">Above 6 Year</option>
              </select>
            </div>
            {formik.errors.experience &&
              formik.touched.experience &&
              formik.errors.experience && (
                <p className="text-red-700 text-sm text-end">
                  {formik.errors.experience}
                </p>
              )}
                          <div className="flex 500px:flex-row 300px:flex-col gap-1 500px:items-center 300px:items-start justify-between mt-2">
              <label  className="text-start text-md label">
               Starting Price Per Hour{" "}
              </label>
              <input
                type="number"
                name="starting_price"
                value={formik.values.starting_price}
                onChange={formik.handleChange}
                className="w-full 500px:max-w-[230px] 300px:max-w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Starting Price"
              />
            </div>
            {formik.errors.starting_price &&
              formik.touched.starting_price &&
              formik.errors.starting_price && (
                <p className="text-red-700 text-sm text-end">
                  {formik.errors.starting_price}
                </p>
              )}
            <div className="mb-2">
              <label className="label" htmlFor="website">
                Your Website
              </label>
              <input
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="https:// Enter Websit URL"
                type="url"
                name="website_url"
                value={formik.values.website_url}
                onChange={formik.handleChange}
              />
            </div>
            <button
              type="submit"
              className="px-8 py-2 rounded-md bg-bgBlue text-white self-end mt-6"
              // onClick={() => {
              //   setStep(7);
              // }}
            >
              Submit
            </button>
          </form>
        </div>
      )}
    </>
  );
};

export default MoreDetailFormService;
