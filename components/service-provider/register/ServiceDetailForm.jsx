import { IconContext } from "react-icons";
import { BiLogoFacebookCircle } from "react-icons/bi";
import { FaLinkedin } from "react-icons/fa";
import Link from "next/link";
// import { GoogleLogin } from "react-google-login";
import { MdVisibility } from "react-icons/md";
import { MdVisibilityOff } from "react-icons/md";
import { useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import axios from "axios";
import { baseUrl } from "@/utils/BaseURL";
import { toast } from "react-toastify";
import { CircularProgress } from "@mui/material";
import axiousInstance from "@/utils/axiousInstance";
import { GoogleLogin, GoogleOAuthProvider } from "@react-oauth/google";

const ServiceDetailForm = ({ handleChange, setStep, setScreenData }) => {
  const [showPassword, setShowPassword] = useState(false);
  const [loader, setloader] = useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const initialValues = {
    password: "",
    email: "",
    termPolicy: true,
  };
  const ValidationSchema = Yup.object().shape({
    password: Yup.string().required("Please Enter  Password"),
    termPolicy: Yup.boolean()
      .oneOf([true], "You must accept the terms and conditions")
      .required("You must accept the terms and conditions"),
    email: Yup.string()
      .email("Invalid email address")
      .required("Please Enter  Email"),
  });
  // const handleSubmit = async (values) => {
  //   setStep(3);
  // };
  const handleSubmit = async (values, setSubmitting, resetForm) => {
    setloader(true);
    let obj = {
      password: values.password,
      email: values.email,
    };
    await axios({
      url: `${baseUrl}auth/verifyemail`,
      method: "Post",
      headers: {
        "content-type": "application/json",
      },
      data: obj,
    })
      .then((res) => {
        setloader(false);

        if (res.data.status === "1") {
          toast.success(res.data.message);
          setScreenData((data) => ({
            ...data,
            email: values.email,
            password: values.password,
            duration: res?.data?.duration * 60,
          }));
          setStep(3);
        } else {
          toast.error(res.data.message);
        }
      })
      .catch((err) => {
        setloader(false);
        toast.error(err.response?.data?.error);
      });
  };
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  // const responseGoogle = async (response) => {
  //   console.log(response);
  //   if (response?.error) {
  //     return toast.error("Something went wrong");
  //   }else{
  //   const data = {
  //     login_method: "Google",
  //     social_id: response?.googleId,
  //     email: response?.profileObj?.email,
  //     social_name: "google",
  //   };

  //   try {
  //     const response = await axiousInstance.post(
  //       "userdata/registerserviceprovider",
  //       data
  //     );
  //     setloader(false);
  //     if (response.data.status === "1") {
  //       toast.success(response.data.message);
  //       setStep(7);
  //     } else {
  //       setloader(false);
  //       toast.error(response.data.message);
  //     }
  //   } catch (error) {
  //     setloader(false);
  //     toast.error(error.response?.data?.error);
  //   }
  // }
  // };
  console.log(formik.values);
  const responseGoogle = async (response) => {
    console.log("response Social", response);
    const data = {
      social_id: response?.credential,
    };

    await axios({
      url: `${baseUrl}userdata/registerserviceprovider`,
      method: "Post",
      headers: {
        "content-type": "application/json",
      },
      data: data,
    })
      .then((res) => {
        // setloader(false);

        if (res.data.status === "1") {
          toast.success(res.data.message);
          setuserId(res.data?.user_id);
          setStep(7);
        } else {
          toast.error(res.data.message);
        }
      })
      .catch((err) => {
        // setloader(false);
        toast.error(err.response?.data?.error);
      });
  };

  const onFailure = (error) => {
    console.log(error);
    toast.error("Something went wrong");

    // Handle error
  };
  return (
    <div className="lg:min-w-[450px] md:mt-6 lg:mt-0">
      <h1 className="mb-2 text-2xl label font-[600]">Create Your Account</h1>
      <div className="flex items-center justify-center gap-2 mb-4 flex-wrap">
        <div>
          <GoogleOAuthProvider clientId="481742004766-0n7ga9b8knh0jb2vgfjjrs8o7g471dhk.apps.googleusercontent.com">
            <GoogleLogin
              clientId="481742004766-0n7ga9b8knh0jb2vgfjjrs8o7g471dhk.apps.googleusercontent.com"
              buttonText="Google"
              onSuccess={responseGoogle}
              onFailure={onFailure}
              redirectUri="http://localhost:3000/auth/login/callback"
              scope="email profile"
            
              style={{
                width: "100%",
              }}
            />
          </GoogleOAuthProvider>
        </div>
        {/* <button className="px-5 py-3 border border-gray-400 flex items-center gap-2 rounded-md flex-grow justify-center">
          <FcGoogle size={25} className="" />
          <p>Google</p>
        </button> */}
        {/* border-gray-400  */}
        <button
          className="px-5 py-[6px] border
   
         flex items-center gap-2 rounded-md flex-grow justify-center"
        >
          <IconContext.Provider value={{ color: "#3b5998" }}>
            <BiLogoFacebookCircle size={25} className="" />
          </IconContext.Provider>
          <p>Facebook</p>
        </button>{" "}
        <button
          className="px-5 py-[6px] border
   
         flex items-center gap-2 rounded-md flex-grow justify-center"
        >
          <IconContext.Provider value={{ color: "#3b5998" }}>
            <FaLinkedin size={25} className="" />
          </IconContext.Provider>
          <p>LinkedIn</p>
        </button>{" "}
      </div>
      <div className="mb-4 flex justify-center text-gray-500">
        <pre>_______</pre>
        Or continue with
        <pre>_______</pre>
      </div>
      <form onSubmit={formik.handleSubmit}>
        <div className="mb-2">
          <label htmlFor="email" className="label">
            Enter Your Email
          </label>
          <input
            id="email"
            type="email"
            name="email"
            value={formik.values.email}
            // onChange={handleChange}
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="Email"
            autoComplete={false}
            readonly="readonly"
            onMouseDown={(e) => {
              e.target.removeAttribute("readonly");
            }}
            onChange={(e) => {
              handleChange(e);
              formik.handleChange(e);
            }}
          />
          {formik.errors.email &&
            formik.touched.email &&
            formik.errors.email && (
              <p className="text-red-700 text-sm">{formik.errors.email}</p>
            )}
        </div>
        <div className="mb-2">
          <label htmlFor="email" className="label">
            Enter Your Password
          </label>
          <div className="relative">
            <input
              id="email"
              type={showPassword ? "text" : "password"}
              name="password"
              value={formik.values.password}
              // onChange={handleChange}
              className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="Password"
              autoComplete={false}
              readonly="readonly"
              onMouseDown={(e) => {
                e.target.removeAttribute("readonly");
              }}
              onChange={(e) => {
                handleChange(e);
                formik.handleChange(e);
              }}
            />
            {formik.errors.password &&
              formik.touched.password &&
              formik.errors.password && (
                <p className="text-red-700 text-sm">{formik.errors.password}</p>
              )}
            <button
              type="button"
              className="absolute justify-center inset-y-0 right-0 flex items-center px-3 text-gray-700  flex-col"
            >
              {showPassword ? (
                <MdVisibilityOff onClick={handleClickShowPassword} size={25} />
              ) : (
                <MdVisibility onClick={handleClickShowPassword} size={25} />
              )}
              {/* <p className="text-[8px] font-bold">Verify</p> */}
            </button>
          </div>
        </div>
        {/* <div className="mb-2 w-full 900px:w-[80%]">
        <label className="block text-sm text-label  mb-2">OTP</label>
        <OtpInput
          value={otp}
          onChange={setOtp}
          placeholder={true}
          numInputs={4}
          renderInput={(props) => <input placeholder="--" {...props} />}
          inputStyle={{
            width: "4rem",
            height: "3rem",
            margin: "0 0.5rem",
            fontSize: "2rem",
            borderRadius: "4px",
            border: "1px solid rgba(0,0,0,0.3)",
          }}
        />
 
        <div className="flex items-center gap-2 ml-1 pt-1">
          <label className="block text-[15px] text-label text-blue-500">
            Resend
          </label>
          <label className="block text-[15px] text-label ">00:00</label>
        </div>
      </div> */}
        <div className="flex items-center mb-4">
          <input
            id="default-checkbox"
            type="checkbox"
            name="termPolicy"
            value={formik.values.termPolicy}
            onChange={(e) => {
              formik.setFieldValue("termPolicy", e.target.checked);
            }}
            className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
            defaultChecked
          />
          <label
            htmlFor="default-checkbox"
            className="ms-2 text-md text-gray-900"
          >
            By signing up you are agree to our terms and privacy policy{" "}
          </label>
        </div>
        {formik.errors.termPolicy &&
          formik.touched.termPolicy &&
          formik.errors.termPolicy && (
            <p className="text-red-700 text-sm">{formik.errors.termPolicy}</p>
          )}
        <p className="text-[14px] font-semibold text-gray-500  text-center mb-4">
          Already have an account?{" "}
          <Link
            href={"/auth/login"}
            className="font-medium text-[14px] text-blue-600 hover:underline "
          >
            Login!
          </Link>
        </p>
        {loader ? (
          <div className="flex justify-center">
            <CircularProgress />
          </div>
        ) : (
          <button
            type="submit"
            // onClick={() => setStep(3)}
            className="block w-[75%] mx-auto px-4 py-2 mt-4 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-[2rem] active:bg-btnprimary hover:bg-btnhover focus:outline-none focus:shadow-outline-blue"
          >
            Next
          </button>
        )}
      </form>
    </div>
  );
};

export default ServiceDetailForm;
