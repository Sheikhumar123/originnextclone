import React, { useEffect, useState } from "react";
import { Autocomplete, Chip, TextField } from "@mui/material";
import { IoChevronBackSharp } from "react-icons/io5";
import {
  GetProviderCategories,
  GetProviderSkills,
  GetProviderSubCategories,
  GetServiceProviderDetails,
} from "@/redux/Slices/ServiceProviderSlice/ServiceProviderSlice";
import { useDispatch, useSelector } from "react-redux";
import { AllLanguage } from "@/components/Language/Language";
import { toast } from "react-toastify";
const ServiceDetails = ({ data, handleChange, setStep, setData }) => {
  const { ProviderCategories, ProviderSubCategories, ProviderSkills } =
    useSelector(GetServiceProviderDetails);
  const dispatch = useDispatch();
  console.log(ProviderSubCategories);
  const [errors, seterrors] = useState(null);
  const [selectedCategories, setSelectedCategories] = useState([]);
  const [selectedSubCategories, setSelectedSubCategories] = useState([]);
  const [selectedLanguages, setselectedLanguages] = useState([]);
  const [selectedSkills, setSelectedSkills] = useState([]);
  console.log(selectedCategories);
  useEffect(() => {
    dispatch(GetProviderCategories());
    dispatch(GetProviderSkills());
  }, [dispatch]);
  useEffect(() => {
    if (selectedCategories?.length) {
      let arr = [];
      selectedCategories?.map((item) => {
        arr.push(item?.id);
      });
      dispatch(GetProviderSubCategories(arr));
    }
  }, [selectedCategories]);
  console.log(selectedCategories);
  useEffect(() => {
    if (data?.providingDetails?.categories) {
      setSelectedCategories(data?.providingDetails?.categories);
    }
    if (data?.providingDetails?.subCategories) {
      setSelectedSubCategories(data?.providingDetails?.subCategories);
    }
    if (data?.providingDetails?.skills) {
      setSelectedSkills(data?.providingDetails?.skills);
    }
    if (data?.providingDetails?.language) {
      setselectedLanguages(data?.providingDetails?.language);
    }
  }, [data?.providingDetails]);
  console.log(data);

  const handleSubmit = () => {
    if (selectedCategories?.length == 0) {
      return seterrors({ ...errors, category: true });
    }
    if (selectedSubCategories?.length == 0) {
      return seterrors({ ...errors, subcategory: true });
    }
    if (selectedSkills?.length == 0) {
      return seterrors({ ...errors, skills: true });
    }
    if (selectedLanguages?.length == 0) {
      return seterrors({ ...errors, language: true });
    }
    setStep(8);
  };
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px] flex flex-col">
      <h1 className="text-xl label mb-2 capitalize">
        Details About the service you are providing
      </h1>
      <div className="flex flex-col gap-2 mb-2">
        <label htmlFor="category" className="text-md label">
          Select Category
        </label>
        <Autocomplete
          multiple
          limitTags={2}
          size="small"
          id="multiple-limit-tags-category"
          options={ProviderCategories || []}
          value={selectedCategories || []}
          getOptionLabel={(option) => option?.title}
          // filterOptions={filterOptions}
          onChange={(event, newValue) => {
            if (newValue?.length > 3) {
              toast.error("Please Enter Maximum 3 Categories");
            } else {
              setSelectedCategories(newValue);
              setData({
                ...data,
                providingDetails: {
                  ...data.providingDetails,
                  categories: newValue,
                },
              });
            }
          }}
          renderOption={(props, option) => <li {...props}>{option?.title}</li>}
          renderTags={(value, getTagProps) =>
            value?.map((option, index) => (
              <Chip
                key={option}
                label={option?.title}
                {...getTagProps({ index })}
                style={{ backgroundColor: "#c7c9f4" }}
              />
            ))
          }
          placeholder="Category"
          renderInput={(params) => (
            <TextField
              {...params}
              sx={{
                "& .MuiOutlinedInput-root": {
                  borderRadius: "8px",
                },
                color: "#9E9E9E",
                width: "100%",
              }}
            />
          )}
        />
        {errors && errors?.category ? (
          <p className="text-red-700 text-sm"> Please Enter Categories</p>
        ) : null}
      </div>
      <div className="flex flex-col gap-2 mb-2">
        <label htmlFor="subcategory" className="text-md label">
          Select Sub-Category
        </label>
        <Autocomplete
          multiple
          limitTags={2}
          size="small"
          id="multiple-limit-tags-subcategory"
          options={ProviderSubCategories || []}
          value={selectedSubCategories}
          onChange={(event, newValue) => {
            if (newValue?.length > 3) {
              return toast.error("Please Enter Maximum 3 Sub Categories");
            } else {
              setSelectedSubCategories(newValue);
              setData({
                ...data,
                providingDetails: {
                  ...data.providingDetails,
                  subCategories: newValue,
                },
              });
            }
          }}
          getOptionLabel={(option) => option?.title}

          renderOption={(props, option) => <li {...props}>{option?.title}</li>}
          renderTags={(value, getTagProps) =>
            value.map((option, index) => (
              <Chip
                key={option}
                label={option?.title}
                {...getTagProps({ index })}
                style={{ backgroundColor: "#c7c9f4" }}
              />
            ))
          }
          renderInput={(params) => (
            <TextField
              {...params}
              sx={{
                "& .MuiOutlinedInput-root": {
                  borderRadius: "8px",
                },
                color: "#9E9E9E",
                width: "100%",
              }}
            />
          )}
        />
        {errors && errors?.subcategory ? (
          <p className="text-red-700 text-sm"> Please Enter Sub Categories</p>
        ) : null}
      </div>
      <div className="flex flex-col gap-2 mb-2">
        <label htmlFor="skills" className="text-md label">
          Select Skills
        </label>
        <Autocomplete
          multiple
          limitTags={2}
          size="small"
          id="multiple-limit-tags-skills"
          options={ProviderSkills || []}
          value={selectedSkills}
          onChange={(event, newValue) => {
            if (newValue?.length > 3) {
              return toast.error("Please Enter Maximum 3 Skills");
            } else {
              setSelectedSkills(newValue);
              setData({
                ...data,
                providingDetails: {
                  ...data.providingDetails,
                  skills: newValue,
                },
              });
            }
          }}
          getOptionLabel={(option) => option?.title}

          renderOption={(props, option) => <li {...props}>{option?.title}</li>}
          renderTags={(value, getTagProps) =>
            value.map((option, index) => (
              <Chip
                key={option}
                label={option?.title}
                {...getTagProps({ index })}
                style={{ backgroundColor: "#c7c9f4" }}
              />
            ))
          }
          renderInput={(params) => (
            <TextField
              {...params}
              sx={{
                "& .MuiOutlinedInput-root": {
                  borderRadius: "8px",
                },
                color: "#9E9E9E",
                width: "100%",
              }}
            />
          )}
        />
        {errors && errors?.skills ? (
          <p className="text-red-700 text-sm"> Please Enter Skills</p>
        ) : null}
      </div>
      <div className="flex items-center justify-between mt-2">
        <label htmlFor="language" className="text-md label capitalize">
          Language You Know
        </label>
        <Autocomplete
          multiple
          limitTags={2}
          size="small"
          fullWidth
          sx={{ maxWidth: "230px" }}
          id="multiple-limit-tags-subcategory"
          options={AllLanguage || []}
          value={selectedLanguages}
          onChange={(event, newValue) => {
            setselectedLanguages(newValue);
            setData({
              ...data,
              providingDetails: {
                ...data.providingDetails,
                language: newValue,
              },
            });
          }}
          renderOption={(props, option) => <li {...props}>{option}</li>}
          renderTags={(value, getTagProps) =>
            value.map((option, index) => (
              <Chip
                key={option}
                label={option}
                {...getTagProps({ index })}
                style={{ backgroundColor: "#c7c9f4" }}
              />
            ))
          }
          renderInput={(params) => (
            <TextField
              {...params}
              sx={{
                "& .MuiOutlinedInput-root": {
                  borderRadius: "8px",
                },
                color: "#9E9E9E",
                width: "100%",
              }}
            />
          )}
        />
        {/* <select
          id="language"
          className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[150px]"
          value={data?.providingDetails?.language || ""}
          name="language"
          onChange={(event) => {
            setData({
              ...data,
              providingDetails: {
                ...data.providingDetails,
                language: event.target.value,
              },
            });
          }}
        >
          <option value={""}>Select</option>
          {AllLanguage?.map((item, i) => {
            return (
              <option key={i} value={item}>
                {item}
              </option>
            );
          })}
        </select> */}
      </div>
      {errors && errors?.language ? (
        <p className="text-red-700 text-sm self-end"> Please Enter Language</p>
      ) : null}
      <div className="flex justify-between items-center">
        <div></div>
        {/* <button
          className=" py-2 rounded-md  text-bgBlue self-end mt-6 "
          onClick={() => setStep(6)}
        >
          <div className="px-1 flex gap-1 items-center border-b border-bgBlue">
            <IoChevronBackSharp />
            <div>Previous</div>
          </div>
        </button> */}
        <button
          type="button"
          className="px-8 py-2 rounded-md bg-bgBlue text-white self-end mt-6"
          onClick={handleSubmit}
        >
          Next
        </button>
      </div>
    </div>
  );
};

export default ServiceDetails;
