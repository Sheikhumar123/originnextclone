"use client";
import { randomProviderBg } from "@/data/serviceProviderData";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import cat1 from "../../../public/ProviderLandingPage/cat1.png";
import cat2 from "../../../public/ProviderLandingPage/cat2.png";
import cat3 from "../../../public/ProviderLandingPage/cat3.png";
import axiousInstance from "@/utils/axiousInstance";
import { toast } from "react-toastify";
function PopolarCategories() {
  const [AllPopularCategories, setAllPopularCategories] = useState([]);
  const [allPopularCategories, setallPopularCategories] = useState([]);
  const [AllPopularSkills, setAllPopularSkills] = useState([]);
  const [allPopularSkills, setallPopularSkills] = useState([]);
  console.log(AllPopularCategories);
  let categoriesData = [
    cat1,
    cat2,
    cat3,

    "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
  ];
  const getCategory = async () => {
    try {
      const response = await axiousInstance.get(
        "landingpage/popularcategories"
      );

      if (response.data.status == "1") {
        setAllPopularCategories(response.data?.categories);
      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      toast.error(error.response?.data?.error);
    }
  };
  const getSkills = async () => {
    try {
      const response = await axiousInstance.get("landingpage/popularskills");

      if (response.data.status == "1") {
        setAllPopularSkills(response.data?.Skillsdata);
      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      toast.error(error.response?.data?.error);
    }
  };
  useEffect(() => {
    getCategory();
    getSkills();
  }, []);
  useEffect(() => {
    if (AllPopularCategories) {
      const updatedItems = AllPopularCategories?.map((item, index) => ({
        ...item,
        icon: categoriesData[index],
      }));
      setallPopularCategories(updatedItems);
    }
  }, [AllPopularCategories]);
  useEffect(() => {
    if (AllPopularSkills) {
      const updatedItems = AllPopularSkills?.map((item, index) => ({
        ...item,
        icon: categoriesData[index],
      }));
      setallPopularSkills(updatedItems);
    }
  }, [AllPopularSkills]);
  let abc = ["bg-[#ffebeb]", "bg-[#fdf6f2]", "bg-[#f1fafe]", "bg-[#ffebeb]"];
  let abc2 = ["bg-[#ffcccc]", "bg-[#f7e1d4]", "bg-[#d0f0fb]", "bg-[#ffcccc]"];
  return (
    <>
      <div className="flex flex-wrap items-center justify-center max-w-7xl mx-auto mt-4 gap-4 900px:gap-6 ">
        <div className="flex flex-col gap-2 300px:w-[100%] 900px:w-[48%] items-center 900px:items-start flex-grow">
          <h1 className="font-bold text-lg text-[#2d2d2d] text-center  w-full py-4">
            Popular Categories
          </h1>
          <div className="flex w-full  items-center justify-center 900px:justify-start flex-wrap gap-2">
            {allPopularCategories?.map((item, index) => {
              return (
                <div
                  key={index * 3 + 3}
                  className={`flex w-full max-w-[300px] border border-border items-center rounded-full gap-2 p-2 text-nowrap  font-semibold 500px:text-sm text-[12px] ${abc[index]}`}
                  // key={subIndex * 2.3 + 1.45}
                >
                  <div
                    className={`w-[40px] h-[40px] p-2 rounded-full flex justify-center items-center ${abc2[index]}`}
                  >
                    <div className="w-[25px] h-[25px]">
                      <Image
                        src={item?.icon}
                        height={25}
                        width={25}
                        style={{ width: "100%", height: "100%" }}
                        alt="icon img"
                      />
                    </div>
                  </div>
                  <h1>{item?.title}</h1>
                </div>
              );
            })}
          </div>
        </div>
        <div className="flex flex-col gap-2 300px:w-[100%] 900px:w-[48%] items-center 900px:items-start ">
          <h1 className="font-bold py-4 text-lg text-[#2d2d2d] text-center ">
            Popular Skill
          </h1>
          <div className="flex flex-row  w-full items-center justify-center  900px:justify-start flex-wrap gap-2">
            {allPopularSkills?.map((item, index) => {
              return (
                <div
                  key={index * 3 + 3}
                  className={`flex w-full max-w-[300px] border border-border items-center rounded-full gap-2 p-2 text-nowrap  font-semibold 500px:text-sm text-[12px] ${abc[index]}`}
                  // key={subIndex * 2.3 + 1.45}
                >
                  <div
                    className={`w-[40px] h-[40px] p-2 rounded-full flex justify-center items-center ${abc2[index]}`}
                  >
                    <div className="w-[25px] h-[25px]">
                      <Image
                        src={item?.icon}
                        height={25}
                        width={25}
                        style={{ width: "100%", height: "100%" }}
                        alt="icon img"
                      />
                    </div>
                  </div>
                  <h1>{item?.title}</h1>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </>
  );
}

export default PopolarCategories;
