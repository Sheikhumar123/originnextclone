import React from 'react'
import { FaChevronLeft } from "react-icons/fa6";

function SlickLeftArrow(props) {
  return (
    <>
        <div
      // className="slick-arrow slick-prev"
      onClick={props.onClick}
      className='bottom-[-60px] 1400px:left-[47%] 500px:left-[42%] 300px:left-[38%] absolute z-10 bg-[#242424] w-[40px] h-[40px] rounded-full flex justify-center items-center cursor-pointer'
      // sx={{ right: "80px", top: "0px", zIndex: 1, position: "absolute" }}
    >
    
        <FaChevronLeft size={20} />
      
    </div>

    </>
  )
}

export default SlickLeftArrow