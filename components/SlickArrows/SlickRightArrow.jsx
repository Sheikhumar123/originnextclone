import React from 'react'
import { FaChevronRight } from "react-icons/fa6";

function SlickRightArrow(props) {
  return (
    <>
      <div
        // className="slick-arrow slick-prev"
        onClick={props.onClick}
        className='bottom-[-60px] 1400px:right-[47%] 500px:right-[42%] 300px:right-[38%] absolute z-10 bg-[#242424] w-[40px] h-[40px] rounded-full flex justify-center items-center cursor-pointer'
        // sx={{ right: "80px", top: "0px", zIndex: 1, position: "absolute" }}
      >
      
          <FaChevronRight size={20} />
        
      </div>
  </>
  )
}

export default SlickRightArrow