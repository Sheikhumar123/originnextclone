import React from "react";

const BgGradient = ({ children }) => {
  return (
    <div className=" bg-gradient-to-l from-[#19FB9B] to-[#8C01FA] text-white px-6 py-4 rounded-2xl flex flex-col gap-4 relative overflow-hidden">
      {children}
    </div>
  );
};

export default BgGradient;
