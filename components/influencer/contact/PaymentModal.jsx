"use client";

import { withdrawalTypeData } from "@/data/dashboardData";

const WithdrawalModal = () => {
  return (
    <dialog id="paymentModal" className="modal modal-bottom sm:modal-middle">
      <div className="modal-box p-6 lg:min-w-[800px] h-full scrollbar scrollbar-w-1 ">
        <div className="w-full p-6 flex items-center justify-between border-dashed border-2 border-sky-500 rounded-md bg-[#004551]/15 mb-6">
          <p className="text-xl">Balance</p>
          <h1 className="text-xl">
            <span className="font-bold text-2xl">$258.65</span> USD
          </h1>
        </div>
        <div className="mb-6">
          <label className="block  text-lg">Amount</label>
          <input
            type="number"
            name="amount"
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="50000"
          />
        </div>
        <div className="flex gap-2 flex-wrap w-full mb-6 ">
          {withdrawalTypeData.map((item, key) => (
            <label
              key={key * 3}
              htmlFor={item.name}
              className="relative flex-1 cursor-pointer"
            >
              <input
                type="radio"
                className="peer/payment hidden"
                id={item.name}
                name="paymentType"
                value={item?.name}
              />

              <div className="flex flex-col items-center justify-center p-8 bg-white border-2 border-gray-200 rounded-md transition peer-checked/payment:border-blue-500  peer-checked/payment:shadow-lg peer-checked/payment:-translate-y-1 ">
                {item.logo}
                {item.name}
              </div>
            </label>
          ))}
        </div>
        <div className="mb-6">
          <label className="block  text-lg">
            USDT TRC20 Address <span className="text-lg text-red-600">*</span>
          </label>
          <input
            type="text"
            name="cryptoAdress"
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="50000"
          />
        </div>
        <div className="mb-6">
          <label className="block  text-lg">
            Name <span className="text-lg text-red-600">*</span>
          </label>
          <input
            type="text"
            name="name"
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="John Doe"
          />
        </div>
        <div className="w-full p-6 border-dashed border-2 border-orange-400 rounded-md bg-[#FF9937]/35 mb-6">
          <p className="text-lg">Thanks For Submitting Your Payment!</p>
          <p className="text-md">Your Payment has been sent successfully</p>
        </div>
        <div className="w-full flex items-center justify-center">
          {" "}
          <button className="bg-[#434CD9] w-full md:w-[200px]   px-12 py-3 rounded-3xl text-white">
            Pay
          </button>
        </div>
      </div>
      <form method="dialog" className="modal-backdrop">
        <button>close</button>
      </form>
    </dialog>
  );
};

export default WithdrawalModal;
