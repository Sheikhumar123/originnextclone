import { pricingPlans } from "@/data/pricingData";
import ModalButton from "@/utils/ModalButton";
import { FaCircleCheck } from "react-icons/fa6";
import PaymentModal from "@/components/influencer/contact/PaymentModal";
const CredentialsForm = ({ data, handleChange, setStep, setData }) => {
  return (
    <div className="w-full h-full p-4">
      <h1 className="font-semibold 800px:text-center 800px:text-[2.3rem] capitalize mx-auto 800px:w-3/4">
        We send customers Email and notification to your related Influencer
      </h1>
      <div className="flex items-center justify-center gap-4 flex-wrap">
        {pricingPlans.map((item, index) => (
          <div
            className="rounded-xl min-w-[300px] relative h-[700px] border flex-grow flex-1 1000px:max-w-[400px]"
            key={index + 0.3 * 9}
          >
            <div className="p-6 bg-pricingBg w-full font-semibold  rounded-t-xl text-white">
              {item.planName}
            </div>
            <div className="p-6">
              <h1>
                <span className="font-semibold text-[2rem]">{item.price}</span>{" "}
                / <span> {item.duaration}</span>
              </h1>
            </div>
            <hr />
            <div className="p-6">
              <h1 className="font-semibold mb-4">
                {item.planName} Plan Includes:
              </h1>{" "}
              <div className="flex flex-col gap-4">
                {item.perks.map((item, subIndex) => (
                  <div
                    className="flex items-center justify-between"
                    key={subIndex + 0.333661 * 9}
                  >
                    <h1 className="w-2/3">{item}</h1>
                    <FaCircleCheck size={25} color="#013220" />
                  </div>
                ))}
              </div>
            </div>{" "}
            <div className="absolute bottom-2 p-4 w-full">
              {item.btnText != "Try For Free" ? (
                <ModalButton
                  classNameString="bg-[#434CD9] w-full  px-12 py-3 rounded-3xl text-white"
                  text={item.btnText}
                  openId={"paymentModal"}
                />
              ) : (
                <button className="bg-[#434CD9] w-full  px-12 py-3 rounded-3xl text-white">
                  Select
                </button>
              )}
            </div>
          </div>
        ))}
      </div>
      <PaymentModal />
    </div>
  );
};

export default CredentialsForm;
