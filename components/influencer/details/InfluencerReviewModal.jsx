"use client";
import { manrope } from "@/utils/fontPoppins";
import React, { useState } from "react";
import { Rating } from "@smastrom/react-rating";
import { GoVerified } from "react-icons/go";

const ReviewModal = () => {
  const [rating, setRating] = useState(0);

  return (
    <>
      <dialog
        id="influencerReviewModal"
        className={`modal ${manrope.className} `}
      >
        <div className="modal-box 800px:min-w-[600px]">
          <h3 className="font-semibold text-2xl">Rate Us</h3>
          <p className="text-lg">
            If you are enjoying using our app. Would you like to rate us.
          </p>

          <Rating
            style={{ maxWidth: 150 }}
            value={rating}
            onChange={setRating}
          />
          <h3 className="font-semibold text-xl mt-2">Write A Review</h3>
          <textarea
            className="border border-gray-200 rounded-2xl w-full p-4"
            name="ratingReview"
            id="ratingReview"
            cols="30"
            rows="6"
            placeholder="Write Review"
          ></textarea>
          <div className="flex items-center gap-4">
            <form method="dialog">
              <button className="px-6 py-2 rounded-md bg-gray-200 text-black">
                Maybe Later
              </button>
            </form>{" "}
            <button
              className="px-6 py-2 rounded-md bg-bgBlue text-white"
              onClick={() => {
                document.getElementById(`closeBtn`).click();
                document.getElementById(`successReviewModal`).showModal();
              }}
            >
              Submit
            </button>
          </div>
        </div>
        <form method="dialog" className="modal-backdrop">
          <button id="closeBtn">close</button>
        </form>
      </dialog>
      <dialog id="successReviewModal" className={`modal ${manrope.className} `}>
        <div className="modal-box 800px:min-w-[600px] flex flex-col items-center justify-center gap-2">
          <GoVerified size={200} color="#444BDB" />
          <h3 className="font-semibold text-2xl">
            Review Sumbitted Successfully
          </h3>
          <p className="text-center">
            Your review has been successfully submitted. Thank you for sharing
            your feedback with us!
          </p>
        </div>
        <form method="dialog" className="modal-backdrop">
          <button>close</button>
        </form>
      </dialog>
    </>
  );
};

export default ReviewModal;
