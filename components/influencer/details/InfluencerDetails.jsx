import Image from "next/image";
import { CiShare2, CiHeart, CiShare1, CiClock1 } from "react-icons/ci";
import { MdOutlineVerifiedUser } from "react-icons/md";
import { VscVerifiedFilled } from "react-icons/vsc";
import { TiTick } from "react-icons/ti";
import { LuPhoneCall } from "react-icons/lu";
import { FaWhatsappSquare } from "react-icons/fa";

import featureImg from "@/images/featureImg.png";
import { skillsData, verificationData } from "@/data/homepageData";
const InfluencerDetails = () => {
  return (
    <>
      <div className="mb-2 flex flex-col gap-3">
        <div className="flex items-center justify-start gap-4 w-full">
          <div className="h-[40px] w-[40px] rounded-full flex items-center justify-center bg-blue-100">
            <CiShare2 size={25} />
          </div>
          <div className="h-[40px] w-[40px] rounded-full flex items-center justify-center bg-red-100">
            <CiHeart size={25} />
          </div>
        </div>
        {/* <button className="p-3 w-full flex-grow gap-2 flex items-center justify-center bg-green-700 text-white rounded-md ">
          Online
        </button> */}
        <Image src={featureImg} alt="seller img" />{" "}
        <div className="flex items-center justify-start gap-2">
          <Image
            src={
              "https://cdn.pixabay.com/photo/2012/04/10/23/03/india-26828_640.png"
            }
            alt="flag img"
            width={40}
            height={40}
          />
          <p>India, New Delhi</p>{" "}
        </div>
        <div className="flex items-center justify-start gap-2">
          <CiClock1 size={25} color="#ff0000" />
          <p className="text-sm text-[#e60000] font-semibold">
            5:42 pm local time{" "}
          </p>
        </div>
        <button className="p-3 w-full flex-grow gap-2 flex items-center justify-center bg-yellow-500 text-green-700 rounded-md font-semibold">
          <MdOutlineVerifiedUser size={25} />
          <p>Trusted</p>
        </button>
        <button className="p-3 w-full flex-grow gap-2 flex items-center justify-center bg-blue-500 text-white rounded-md ">
          <VscVerifiedFilled size={25} />
          <p>Verified</p>
        </button>
      </div>
      <div className="p-2 w-full flex flex-col items-center justify-center border border-gray-200 rounded-2xl gap-2">
        <button className="p-3 w-[80%] flex-grow gap-2 flex items-center justify-center bg-black text-white rounded-3xl ">
          Contact Us <LuPhoneCall size={25} color="#fff" className="ml-2" />
        </button>
        <button className="p-3 w-[80%] flex-grow gap-2 flex items-center justify-center rounded-3xl border border-gray-200 text-[#4E4E4E]">
          Whatsapp Us{" "}
          <FaWhatsappSquare size={30} color="#29A71A" className="" />
        </button>
        <button className="p-3 w-[80%] flex-grow gap-2 flex items-center justify-center rounded-3xl border border-gray-200 text-[#4E4E4E]">
          Visit Website <CiShare1 />
        </button>
      </div>
      <div className="p-4 w-full flex flex-col items-start  border border-gray-200 rounded-2xl gap-2">
        <h1 className="font-semibold text-lg">Verification&apos;s</h1>
        {verificationData.map((item, key) => (
          <div
            className="flex items-center gap-4 text-[#004551]"
            key={key + 4 * 7}
          >
            <TiTick size={20} fill="#008000" />
            <p>{item}</p>
          </div>
        ))}
      </div>
      <div className="p-4 w-full flex flex-col items-start  border border-gray-200 rounded-2xl gap-2">
        <h1 className="font-semibold text-lg">Skills</h1>
        <div className="flex gap-3 flex-wrap">
          {skillsData.map((item, key) => (
            <h1
              className="py-2 px-4 border-2 bg-[#CCE0FF] text-[#444BDB] text-sm rounded-2xl text-nowrap"
              key={key + 22 * 8}
            >
              {item.title}
            </h1>
          ))}
        </div>
      </div>
      <div className="p-4 w-full flex flex-col items-start  border border-gray-200 rounded-2xl gap-2">
        <p className="font-semibold text-sm">
          Have any Suggestions for OOrgin ,or found any bug. Send us and Win
          rewards and prizes.
        </p>
        <button className="p-2 rounded-2xl bg-black text-white text-sm ">
          Give Your Suggestion
        </button>
      </div>
    </>
  );
};

export default InfluencerDetails;
