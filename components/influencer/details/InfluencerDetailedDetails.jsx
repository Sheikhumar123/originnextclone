import { manrope } from "@/utils/fontPoppins";
import { FiThumbsUp } from "react-icons/fi";
import { FaChevronDown, FaLinkedin } from "react-icons/fa";
import { SlFlag } from "react-icons/sl";
import React from "react";
import ratingImg from "../../../images/ratingImg.png";

import Image from "next/image";
import serviceImg from "../../../images/serviceImg.png";
import {
  accordionData,
  listingServiceData,
  reviewListingData,
} from "@/data/homepageData";
import ModalButton from "@/utils/ModalButton";
import ReviewModal from "@/components/influencer/details/InfluencerReviewModal";
import RadialProgress from "@/utils/RadialProgress";
import {
  influencerCardData,
  influencerCountry,
  randomGraphColors,
} from "@/data/influencerPageData";
import { CiShare1 } from "react-icons/ci";

const InfluencerDetailedDetails = () => {
  return (
    <div className={`w-full ${manrope.className}`}>
      <div className=" text-[1.8rem] 400px:text-[2.5rem] font-semibold flex items-center gap-2 500px:gap-6 flex-wrap">
        <h1> Best SEO Service Provider Company</h1>
        <RadialProgress
          size={"3.5rem"}
          colorClass={"text-bgBlue"}
          value={65}
          textSize={"16px"}
        />
      </div>
      <h1 className="text-[1.5rem] 400px:text-[2.2rem] font=[200] text-[#757575]">
        Off Page SEO
      </h1>
      <div className="flex items-center gap-4 p-3 w-full flex-wrap">
        <div className="flex items-center gap-2">
          <Image src={ratingImg} alt="ratingImg" height={30} />
          <h3 className="text-lg text-black font-semibold">4.2</h3>
        </div>
        <span className="p-3 text-[0.7rem] 500px:text-[1rem] text-black font-semibold">
          [3426 Reviews]
        </span>
        <ModalButton
          classNameString={
            "500px:px-4 px-2 500px:py-2 py-1 border border-blue-500 text-blue-500 rounded-3xl font-semibold"
          }
          text={"Rate Us Now"}
          openId={"influencerReviewModal"}
        />
      </div>
      <div className="flex items-center gap-4 p-3 w-full flex-wrap">
        <button className="500px:px-20 500px:py-2 px-10 py-1 border bg-bgBlue text-white rounded-3xl font-semibold">
          Follow
        </button>
        <span className="p-3   font-semibold">102 Followers</span>
        <span className="p-3   font-semibold flex items-center gap-1">
          <div className="bg-bgBlue p-2 rounded-xl text-white">
            <FiThumbsUp size={25} />
          </div>{" "}
          <span>102 Recomendations</span>
        </span>
      </div>
      <div className="flex flex-col gap-4 mb-4">
        <p className="flex-1 500px:text-nowrap flex items-center gap-6">
          <span>Total Reach:</span>{" "}
          <span className="font-semibold">2122 Audience</span>
        </p>

        <div className="flex-1 500px:text-nowrap flex flex-col 500px:flex-row 500px:items-center gap-6">
          <span>Audience Country:</span>{" "}
          <div className="flex items-center gap-4 flex-wrap">
            {influencerCountry.map((item, index) => (
              <div
                className="font-semibold flex gap-2 items-center"
                key={index + 166 * 2}
              >
                <span className="font-semibold">{item.title} -</span>
                <RadialProgress
                  size={"2.1rem"}
                  colorClass={randomGraphColors[Math.floor(Math.random() * 6)]}
                  value={item.value}
                  textSize={"10px"}
                />
              </div>
            ))}
          </div>
        </div>

        <p className="flex-1 500px:text-nowrap">
          Language&apos;s I Know:{" "}
          <span className="font-semibold">English, Hindi</span>
        </p>

        <div className="flex-1 500px:text-nowrap flex items-center gap-1 flex-wrap">
          Starting Price:{" "}
          <div className="flex items-center gap-2 500px:mt-0 mt-2">
            <button className="px-2 500px:px-4 py-1 500px:py-2 border border-blue-500 text-blue-500 rounded-3xl font-semibold">
              Get Bid Price
            </button>
            <p className="font-semibold">5000 Post’s</p>
          </div>
        </div>
      </div>
      <div className="flex items-center gap-4 flex-wrap mt-4">
        <button className="btn flex-grow bg-bgBlue text-white">
          Description
        </button>
        <button className="btn flex-grow bg-bgBlue text-white">Services</button>
        <button className="btn flex-grow bg-bgBlue text-white">Q/A</button>
        <button className="btn flex-grow bg-bgBlue text-white">Reviews</button>
      </div>
      <div className="mt-4">
        <h1 className="text-[1.8rem] font-semibold mb-2">Description </h1>
        <p className="w-[90%] text-[#636363]">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor
          incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
          commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
          velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
          occaecat cupidatat non proident, sunt in culpa qui officia deserunt
          mollit anim id est laborum.
        </p>
      </div>
      {/* INFLUENCER DETAILS CARDS  */}
      {influencerCardData.map((item, index) => (
        <div className="mt-4" key={index + 221 * 2}>
          {/* LABEL  */}
          <div
            htmlFor="card-blogger"
            className={`${item.bgLabel} px-6 py-2 rounded-t-2xl text-white font-semibold inline-block`}
          >
            {item.platform}
          </div>

          <div className="border border-gray-300 rounded-r-2xl rounded-b-2xl p-3">
            {/* PROFILE BOX  */}{" "}
            <div className="flex w-full justify-between 500px:flex-row flex-col">
              <div className="flex items-center gap-2">
                <Image
                  src={item.profileImg}
                  alt="Profile Image"
                  width={75}
                  height={75}
                />
                <div className="flex flex-col 500px:gap-2 font-semibold">
                  <h1 className="text-sm 500px:text-[1rem]">{item.name}</h1>
                  <h1 className="text-sm 500px:text-[1rem]">{item.subtitle}</h1>
                </div>
              </div>
              <button className="px-6 h-fit py-2  gap-2 flex items-center justify-center  rounded-3xl border border-gray-200 text-black font-semibold">
                <span className="flex items-center gap-1">
                  {" "}
                  <span>Visit</span> <CiShare1 />
                </span>
              </button>
            </div>{" "}
            {/* STATS BOX  */}
            <div className="flex items-center justify-between gap-4 flex-wrap 500px:w-3/4 500px:mt-2 mt-6 font-semibold">
              {item.statsData.map((statsItem, index) => (
                <div
                  className="flex flex-col gap-2 items-start"
                  key={index + 99 * 1.5}
                >
                  <h1 className="font-semibold">{statsItem.value}</h1>
                  <p className="text-[#666666]">{statsItem.title}</p>
                </div>
              ))}
            </div>
            {/* LANGUAGE BOX */}
            <div className="flex items-center justify-between gap-4 flex-wrap 500px:w-2/4 500px:mt-2 mt-6 font-semibold">
              {item.profileData.map((subItem, index) => (
                <div
                  className="flex flex-col gap-2 items-start"
                  key={index + 62.3 * 0.5}
                >
                  <h1 className="font-semibold">{subItem.title}</h1>
                  <p className="text-[#666666]">{subItem.value}</p>
                </div>
              ))}
            </div>
            {/* AUDIENCE COUNTRY  */}
            <div className="flex-1  500px:flex-row flex-col 500px:text-nowrap flex 500px:items-center 500px:gap-6 gap-2 500px:mt-2 mt-6">
              <span className="font-semibold text-[#666666]">
                Audience Country
              </span>{" "}
              <div className="flex items-center gap-4 flex-wrap">
                {item.influencerAudienceCountry.map((item, index) => (
                  <div
                    className="font-semibold flex gap-2 items-center"
                    key={index + 166 * 2}
                  >
                    <span className="font-semibold">{item.title} -</span>
                    <RadialProgress
                      size={"2.1rem"}
                      colorClass={
                        randomGraphColors[Math.floor(Math.random() * 6)]
                      }
                      value={item.value}
                      textSize={"10px"}
                    />
                  </div>
                ))}
              </div>
            </div>
            <p className="mt-2 text-[#666666] font-semibold">
              {item.description}
            </p>
            <div className="flex items-center justify-between flex-wrap flex-grow 500px:flex-grow-0 mt-2 gap-2 500px:gap-0">
              <div className="flex gap-3">
                <span className="font-semibold text-[#666666]">Sponsor At</span>{" "}
                <h1 className="text-black font-semibold">
                  {item.price}/- Per Post
                </h1>
              </div>
              <button className="px-6 py-2 bg-bgBlue font-semibold text-white rounded-3xl">
                Contact For Sponsorship
              </button>
            </div>
          </div>
        </div>
      ))}

      <div className="mt-4 mb-4">
        <h1 className="text-[1.8rem] font-semibold mb-2">
          Frequently Asked Questions
        </h1>
        <p className="text-[#767676] mb-2">
          Diam Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Vestibulum dui nisi, porttior sit amet diam ut, congue fermentum dui.
          Aenean gravida p
        </p>
        {accordionData.map((item, index) => (
          <div
            key={index + 1 * 7}
            className="collapse collapse-plus bg-bgBlue text-white border mb-2 rounded-md"
          >
            <input type="radio" name="my-accordion-3" />
            <div className="collapse-title text-lg font-medium">
              {item.title}
            </div>
            <div className="collapse-content bg-gray-100 text-black">
              <p>{item.message}</p>
            </div>
          </div>
        ))}
      </div>
      <div className="flex items-center gap-4  mt-8 flex-wrap">
        <h1 className="text-3xl font-semibold  w-full ">2 Reviews</h1>
        <div className="dropdown dropdown-end   ">
          <div
            tabIndex={0}
            role="button"
            className="btn m-1 bg-white border border-gray-400"
          >
            Most Recent <FaChevronDown size={20} />
          </div>
          <ul
            tabIndex={0}
            className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
          >
            <li>
              <a>Item 1</a>
            </li>
            <li>
              <a>Item 2</a>
            </li>
          </ul>
        </div>
        <div className="dropdown dropdown-end     ">
          <div
            tabIndex={0}
            role="button"
            className="btn m-1 bg-white border border-gray-400"
          >
            Any Rating <FaChevronDown size={20} />
          </div>
          <ul
            tabIndex={0}
            className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
          >
            <li>
              <a>Item 1</a>
            </li>
            <li>
              <a>Item 2</a>
            </li>
          </ul>
        </div>
        <button className="bg-[#434CD9] w-full  px-12 py-3 rounded-3xl text-white">
          Write A Review
        </button>
      </div>
      <div className="w-full flex flex-col gap-3">
        {reviewListingData.map((item, key) => (
          <div key={key + 978 * 2} className="p-6 rounded-md  shadow-xl">
            <div className="flex items-center justify-between">
              <div className="mb-2 flex items-center gap-2 w-auto">
                <div className=" w-[45px] h-[45px] rounded-full overflow-hidden">
                  <Image
                    src={item.image}
                    style={{ width: "100%", height: "100%" }}
                    alt="profile img"
                    className=" object-cover "
                    width={500}
                    height={500}
                  />
                </div>
                <div className="flex flex-col ">
                  <h1 className="font-semibold">{item.name}</h1>
                  <p className="text-sm">{item.postition}</p>
                </div>
              </div>
              <div className="flex items-center gap-2">
                <FaLinkedin size={25} fill="#0E76A8" />{" "}
                <h1 className="font-semibold">Verified</h1>
              </div>
            </div>
            <Image src={ratingImg} alt="ratingImg" height={18} />
            <p className="mt-2 text-[#676767] mb-2">{item.review}</p>
            <hr />
            <div className="flex items-center justify-between gap-2 600px:gap-0 mt-4 flex-wrap 600px:flex-nowrap text-nowrap">
              <div className="flex items-center gap-2 500px:gap-6 font-[600] flex-wrap 600px:flex-nowrap ">
                <h1>{item.date}</h1>
                <h1>Helpful (1)</h1>
                <h1>Share</h1>
              </div>
              <div className="flex items-center gap-2">
                <SlFlag fill="#f21a1a" size={20} />
                <h1 className="text-[#f21a1a] font-semibold">Report</h1>
              </div>
            </div>
          </div>
        ))}
      </div>
      {/* HIDDEN DIV FOR RANDOM COLOR RENDERING  */}
      <div className="hidden  text-lime-600 text-yellow-600 text-blue-600  text-red-600 text-green-40 text-orange-600 text-purple-600 text-violet-600 bg-gradient-to-r from-[#833AB4] via-[#C13584] to-[#E1306C] via-[#FD1D1D] to-[#F77737] bg-red-500 bg-orange-500">
        <div
          className={`radial-progress `}
          style={{ "--value": 70, "--size": "1rem" }}
          role="progressbar"
        >
          <p className="font-semibold"> {70}%</p>
        </div>
      </div>
      <ReviewModal />
    </div>
  );
};

export default InfluencerDetailedDetails;
