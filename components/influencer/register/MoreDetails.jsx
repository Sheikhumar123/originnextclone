const MoreDetailsForm = ({ data, handleChange, setStep }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px] flex flex-col">
      <h1 className=" text-xl mb-2">
        Need To Know Little About You, Without Displaying In Public, It Is For
        Our Record Only
      </h1>
      <div className="mb-2">
        <label htmlFor="name" className="text-black text-md ">
          Your Name
        </label>
        <input
          id="name"
          type="text"
          name="name"
          defaultValue={data?.name || ""}
          onChange={handleChange}
          className="w-full bg-white h-[40px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="John Doe"
        />
      </div>
      <div className="flex items-center justify-between mb-6">
        <label htmlFor="designation" className="text-black text-md ">
          Your Designation
        </label>

        <select
          id="desigination"
          className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[180px]"
          defaultValue={data?.desigination || ""}
          name="desigination"
          onChange={handleChange}
        >
          <option value={""}>Select</option>
          <option value="Brand Ambassador">Brand Ambassador</option>
          <option value="Sponsor">Sponsor</option>
          <option value="Item">Item</option>
          <option value="Item">Item</option>
        </select>
      </div>

      <div className="flex items-center justify-between mb-6">
        <label htmlFor="country" className="text-black text-md ">
          Your Country
        </label>

        <select
          id="country"
          className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[180px]"
          defaultValue={data?.country || ""}
          name="country"
          onChange={handleChange}
        >
          <option value={""}>Select</option>
          <option value="India">India</option>
          <option value="Pakistan">Pakistan</option>
          <option value="United States">United States</option>
          <option value="United Kingdom">United Kingdom</option>
        </select>
      </div>

      <div className="flex items-center justify-between mb-6">
        <label htmlFor="city" className="text-black text-md ">
          Your City
        </label>

        <select
          id="city"
          className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[180px]"
          defaultValue={data?.city || ""}
          name="city"
          onChange={handleChange}
        >
          <option value={""}>Select</option>
          <option value="India">India</option>
          <option value="Pakistan">Pakistan</option>
          <option value="US">United States</option>
          <option value="UK">United Kingdom</option>
        </select>
      </div>

      <div className="flex items-center justify-between mb-6">
        <label htmlFor="language" className="text-black text-md ">
          Your Language
        </label>

        <select
          id="language"
          className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[180px]"
          defaultValue={data?.language || ""}
          name="language"
          onChange={handleChange}
        >
          <option value={""}>Select</option>
          <option value="Hindi">Hindi</option>
          <option value="Urdu">Urdu</option>
          <option value="English">English</option>
          <option value="Arabic">Arabic</option>
        </select>
      </div>

      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end"
        onClick={() => {
          setStep(5);
        }}
      >
        Send
      </button>
    </div>
  );
};

export default MoreDetailsForm;
