const ClientDetailsForm = ({ data, handleChange, setStep }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px]">
      <h1 className=" text-xl mb-5">
        Write details that client invest in your Profile{" "}
      </h1>
      <div className="mb-5 800px:w-[450px]">
        <label htmlFor="clientDetails" className="text-black text-md mb-2 ">
          Describe about your services
        </label>
        <textarea
          id="clientDetails"
          rows={12}
          name="clientDetails"
          defaultValue={data?.clientDetails || ""}
          onChange={handleChange}
          className="w-full bg-white  px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="Write Details"
        />
      </div>

      <div className="flex items-center justify-between">
        <button className="px-4 py-2 rounded-md bg-gray-200 text-black self-end border-black border">
          See Sample
        </button>
        <button
          className="px-8 py-2 rounded-md bg-bgBlue text-white self-end"
          onClick={() => {
            setStep(7);
          }}
        >
          Send
        </button>
      </div>
    </div>
  );
};

export default ClientDetailsForm;
