import React from "react";
const NotificationsForm = ({ data, handleChange, setStep }) => {
  return (
    <div className="border border-gray-200 rounded-xl p-4 flex flex-col items-center justify-center w-full">
      <div className="h-[400px] overflow-auto">
        <div className="p-2 rounded-md shadow-sm flex flex-col items-center justify-center">
          <div className="mb-2 relative 600px:w-[300px]">
            <label className="block text-sm text-label mb-2">Phone</label>
            <div className="relative">
              <input
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="+923XXXXXXXXX"
                type="tel"
                name="phone"
                value={data?.phone || ""}
                onChange={handleChange}
              />
            </div>
          </div>
          <button className="px-8 py-2 rounded-md bg-bgBlue text-white self-end">
            Verify
          </button>
          <div className="mb-2 w-full 600px:w-[300px]">
            <label className="block text-sm text-label  mb-2">OTP</label>
            <input
              type="number"
              name="phoneOtp"
              value={data?.phoneOtp || ""}
              onChange={handleChange}
              className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="XXXXXX"
            />
            <div className="flex items-center gap-2 ml-1">
              <label className="block text-[15px] text-label text-blue-500">
                Resend
              </label>
              <label className="block text-[15px] text-label ">00:00</label>
            </div>
          </div>
        </div>
        <div className="p-2 rounded-md shadow-sm flex flex-col items-center justify-center">
          <div className="mb-2 relative 600px:w-[300px]">
            <label className="block text-sm text-label mb-2">
              Add Whatsapp (Optional)
            </label>
            <div className="relative">
              <input
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="+923XXXXXXXXX"
                type="tel"
                name="whatsappNumber"
                value={data?.whatsappNumber || ""}
                onChange={handleChange}
              />
            </div>
          </div>
          <button className="px-8 py-2 rounded-md bg-bgBlue text-white self-end">
            Verify
          </button>
          <div className="mb-2 w-full 600px:w-[300px]">
            <label className="block text-sm text-label  mb-2">OTP</label>
            <input
              type="number"
              name="whatsappOtp"
              value={data?.whatsappOtp || ""}
              onChange={handleChange}
              className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="XXXXXX"
            />
            <div className="flex items-center gap-2 ml-1">
              <label className="block text-[15px] text-label text-blue-500">
                Resend
              </label>
              <label className="block text-[15px] text-label ">00:00</label>
            </div>
          </div>
        </div>
      </div>{" "}
      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end mt-4"
        onClick={() => {
          setStep(4);
        }}
      >
        Next
      </button>
    </div>
  );
};

export default NotificationsForm;
