import { IconContext } from "react-icons";
import { BiLogoFacebookCircle } from "react-icons/bi";
import { FcGoogle } from "react-icons/fc";
import { FaLinkedin } from "react-icons/fa6";
import Link from "next/link";

const DetailsForm = ({ data, handleChange, setStep }) => {
  return (
    <div className="lg:min-w-[450px] md:mt-6 lg:mt-0">
      <h1 className="mb-4 text-2xl font-[600] text-center    text-black">
        Create Your Account
      </h1>
      <div className="flex items-center justify-center gap-2 mb-4 flex-wrap">
        <button className="px-5 py-3 border border-gray-400 flex items-center gap-2 rounded-md flex-grow justify-center">
          <FcGoogle size={25} className="" />
          <p>Google</p>
        </button>
        <button className="px-5 py-3 border border-gray-400 flex items-center gap-2 rounded-md flex-grow justify-center">
          <IconContext.Provider value={{ color: "blue" }}>
            <BiLogoFacebookCircle size={25} className="" />
          </IconContext.Provider>
          <p>Facebook</p>
        </button>{" "}
        <button className="px-5 py-3 border border-gray-400 flex items-center gap-2 rounded-md flex-grow justify-center">
          <FaLinkedin size={25} className="" color="#0077b5" />
          <p>LinkedIn</p>
        </button>
      </div>
      <div className="mb-4 flex justify-center text-gray-500">
        <pre>_______</pre>
        Or continue with
        <pre>_______</pre>
      </div>
      <div className="mb-2">
        <label htmlFor="email" className="text-gray-500 text-sm">
          Enter Your Email
        </label>
        <input
          id="email"
          type="email"
          name="email"
          value={data?.email || ""}
          onChange={handleChange}
          className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="Email"
        />
      </div>
      <div className="mb-2 w-full 900px:w-[80%]">
        <label className="block text-sm text-label  mb-2">OTP</label>
        <input
          type="number"
          name="emailOtp"
          value={data?.emailOtp || ""}
          onChange={handleChange}
          className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="XXXXXX"
        />
        <div className="flex items-center gap-2 ml-1">
          <label className="block text-[15px] text-label text-blue-500">
            Resend
          </label>
          <label className="block text-[15px] text-label ">00:00</label>
        </div>
      </div>
      <div className="flex items-center mb-4">
        <input
          id="default-checkbox"
          type="checkbox"
          value=""
          className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 rounded focus:ring-blue-500 dark:focus:ring-blue-600 dark:ring-offset-gray-800 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"
          defaultChecked
        />
        <label
          htmlFor="default-checkbox"
          className="ms-2 text-md text-gray-900"
        >
          By signing up you are agree to our terms and privacy policy{" "}
        </label>
      </div>
      <p className="text-[14px] font-semibold text-gray-500  text-center mb-4">
        Already have an account?{" "}
        <Link
          href={"/auth/login"}
          className="font-medium text-[14px] text-blue-600 hover:underline "
        >
          Login!
        </Link>
      </p>
      <button
        onClick={() => setStep(3)}
        className="block w-[75%] mx-auto px-4 py-2 mt-4 mb-4 text-sm font-medium leading-5 text-center h-[55px] text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-[2rem] active:bg-btnprimary hover:bg-btnhover focus:outline-none focus:shadow-outline-blue"
      >
        Next
      </button>
    </div>
  );
};

export default DetailsForm;
