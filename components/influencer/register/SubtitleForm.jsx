const SubtitleForm = ({ data, handleChange, setStep }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 800px:max-w-[600px]">
      <h1 className=" text-xl mb-5">
        Now Write Title And Subtitle Of Your Profile
      </h1>
      <div className="mb-5 800px:w-[450px]">
        <label htmlFor="profileTitle" className="text-black text-md mb-2 ">
          Your Title
        </label>
        <input
          id="profileTitle"
          type="text"
          name="profileTitle"
          defaultValue={data?.profileTitle || ""}
          onChange={handleChange}
          className="w-full bg-white h-[40px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="John Doe"
        />
      </div>
      <div className="mb-5 800px:w-[450px]">
        <label htmlFor="profileSubtitle" className="text-black text-md mb-2 ">
          Your Subtitle
        </label>
        <input
          id="profileSubtitle"
          type="text"
          name="profileSubtitle"
          defaultValue={data?.profileSubtitle || ""}
          onChange={handleChange}
          className="w-full bg-white h-[40px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          placeholder="John Doe"
        />
      </div>

      <div className="flex items-center justify-between">
        <button className="px-4 py-2 rounded-md bg-gray-200 text-black self-end border-black border">
          See Sample
        </button>
        <button
          className="px-8 py-2 rounded-md bg-bgBlue text-white self-end"
          onClick={() => {
            setStep(6);
          }}
        >
          Send
        </button>
      </div>
    </div>
  );
};

export default SubtitleForm;
