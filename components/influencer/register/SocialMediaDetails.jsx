import React from "react";

const SocialMediaDetails = ({ data, handleChange, setStep }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 600px:max-w-[600px] flex flex-col">
      <h1 className=" text-xl mb-5 text-center">
        Add social media links and details to get Sponsorship
      </h1>
      <div className="flex items-center justify-between mb-6">
        <label htmlFor="platform" className="text-black text-md ">
          Select Platform{" "}
        </label>

        <select
          id="platform"
          className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[180px]"
          defaultValue={data?.platform || ""}
          name="platform"
          onChange={handleChange}
        >
          <option value={""}>Select</option>
          <option value="LinkedIn">LinkedIn</option>
          <option value="Google">Google</option>
          <option value="Facebook">Facebook</option>
          <option value="Instagram">Instagram</option>
        </select>
      </div>
      <div className="flex items-center justify-between mb-6 flex-wrap 600px:flex-nowrap gap-2">
        <div className="flex items-center gap-2 w-full 600px:w-1/2">
          <label htmlFor="platformType" className="text-black text-md ">
            Select Type{" "}
          </label>

          <select
            id="platformType"
            className="bg-gray-50 border border-gray-300 text-gray-500 text-sm rounded-lg focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[120px]"
            defaultValue={data?.platformType || ""}
            name="platformType"
            onChange={handleChange}
          >
            <option value={""}>Select</option>
            <option value="LinkedIn">LinkedIn</option>
            <option value="Google">Google</option>
            <option value="Facebook">Facebook</option>
            <option value="Instagram">Instagram</option>
          </select>
        </div>
        <input
          type="text"
          id="platformUrl"
          value={data.platformUrl || ""}
          name="platformUrl"
          onChange={handleChange}
          placeholder="Write Name Here"
          className="border border-gray-200 rounded-md text-black p-1 text-sm w-full 600px:w-1/2 flex-grow"
        />
      </div>

      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end"
        onClick={() => {
          // console.log(data);
          setStep(8);
        }}
      >
        Submit
      </button>
    </div>
  );
};

export default SocialMediaDetails;
