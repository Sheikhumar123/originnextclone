import { useRouter } from "next/navigation";
import { CiShare2 } from "react-icons/ci";

const Completion = ({ data, setData, setStep }) => {
  const router = useRouter();
  return (
    <div className="p-4 max-w-[600px]">
      {/* {console.log(data)} */}
      <h1 className="text-xl mb-2 font-semibold capitalize">
        Your profle is ready{" "}
      </h1>
      <p>
        Now you can share your profile to get some reviews so that new clients
        get confidence to contact with you.
      </p>
      <div className="w-full flex items-center justify-between mt-4">
        <button className="flex items-center gap-2 rounded-xl bg-gray-200 px-4 py-3 ">
          <h1>Share Now</h1>
          <CiShare2 size={25} />
        </button>
        <button
          onClick={() => {
            router.push("/auth/login");
          }}
          className="flex items-center text-white gap-2 rounded-xl bg-bgBlue px-14 py-3"
        >
          <h1>Next</h1>
        </button>
      </div>
    </div>
  );
};

export default Completion;
