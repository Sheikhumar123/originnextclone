const UsernameForm = ({ data, handleChange, setStep }) => {
  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full flex flex-col items-center justify-center">
      <h1 className="font-bold text-xl mb-6 text-center">
        Choose Your Profile Name
      </h1>
      <div className="flex items-center gap-2 flex-wrap mb-8">
        <h3 className="font-semibold text-md">
          http://www.origin.com/username
        </h3>
        <input
          type="text"
          id="profileName"
          value={data.profileName || ""}
          name="profileName"
          onChange={handleChange}
          placeholder="Write Name Here"
          className="border border-gray-200 rounded-md text-black p-1 text-sm max-w-[200px]"
        />
        <h3 className="font-semibold text-md text-green-500">Available</h3>
      </div>
      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end"
        onClick={() => setStep(2)}
      >
        Next
      </button>
    </div>
  );
};

export default UsernameForm;
