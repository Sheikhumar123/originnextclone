import React, { useState } from "react";
import { FaCirclePlus } from "react-icons/fa6";

const FAQForm = ({ data, setData, setStep }) => {
  const [faqFields, setFaqFields] = useState([{ question: "", answer: "" }]);

  const handleInputChange = (index, event, field) => {
    const newFaqFields = [...faqFields];
    newFaqFields[index][field] = event.target.value;
    setFaqFields(newFaqFields);
  };

  const handleAddFAQ = () => {
    setFaqFields([...faqFields, { question: "", answer: "" }]);
  };

  const handleSubmit = () => {
    setData((prevData) => ({
      ...prevData,
      faq: faqFields,
    }));
    setStep(9);
  };

  return (
    <div className="p-4 border border-gray-100 rounded-2xl shadow-md w-full 600px:max-w-[600px] flex flex-col">
      <h1 className="text-xl mb-5 text-center">
        Add FAQ for your client to clear their questions
      </h1>
      <div className="overflow-y-auto h-[300px] 800px:max-h-[160px]">
        {faqFields.map((faq, index) => (
          <div key={index} className="mb-2">
            <label
              htmlFor={`faqQuestion-${index}`}
              className="text-black text-md mb-2 block"
            >
              Question
            </label>
            <input
              id={`faqQuestion-${index}`}
              type="text"
              value={faq.question}
              onChange={(event) => handleInputChange(index, event, "question")}
              className="w-full bg-white h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="Enter your question"
            />
            <label
              htmlFor={`faqAnswer-${index}`}
              className="text-black text-md mb-2 mt-2 block"
            >
              Answer
            </label>
            <input
              id={`faqAnswer-${index}`}
              type="text"
              value={faq.answer}
              onChange={(event) => handleInputChange(index, event, "answer")}
              className="w-full bg-white h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="Enter the answer to the question"
            />
          </div>
        ))}
      </div>
      <button
        className="px-4 py-2 rounded-md mt-2  text-black self-start flex items-center gap-1"
        onClick={handleAddFAQ}
      >
        <FaCirclePlus />
        <h1> Add More</h1>
      </button>
      <button
        className="px-8 py-2 rounded-md bg-bgBlue text-white self-end mt-4"
        onClick={handleSubmit}
      >
        Submit
      </button>
    </div>
  );
};

export default FAQForm;
