"use client";

import Image from "next/image";

const ProfileForm = () => {
  return (
    <div className="w-full">
      <form>
        <div className="mb-2 flex items-center gap-4 w-auto">
          <div className=" w-[65px] h-[65px]  500px:w-[100px] 500px:h-[100px] rounded-full overflow-hidden">
            <Image
              src={
                "https://images.unsplash.com/photo-1438761681033-6461ffad8d80?q=80&w=1470&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
              }
              style={{ width: "100%", height: "100%" }}
              alt="profile img"
              className=" object-cover "
              width={500}
              height={500}
            />
          </div>
          <div className="flex flex-col 500px:gap-2">
            <h1 className="font-semibold text-xl">Ava Robert</h1>
            <p>Lorem ipsum dolor sit amet.</p>
          </div>
        </div>
        <div className="mb-2">
          <label className="block text-sm text-label mb-2">Name</label>
          <div className="flex items-center flex-wrap gap-2">
            <input
              type="text"
              name="firstName"
              className="400px:w-[45%] flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="John"
            />
            <input
              type="text"
              name="lastName"
              className="400px:w-[45%] flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="Doe"
            />
          </div>
        </div>
        <div className="mb-2">
          <label className="block text-sm text-label mb-2">Email</label>
          <input
            type="email"
            name="email"
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="John Doe"
          />
        </div>
        <div className="mb-2">
          <label className="block text-sm text-label mb-2">Address</label>
          <input
            type="text"
            name="address"
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="Apt,Suite etc"
          />
        </div>
        <div className="mb-2">
          <div className="flex items-center flex-wrap gap-2">
            <div className="400px:w-[45%] flex-grow">
              <label className="block text-sm text-label mb-2">City</label>
              <input
                type="text"
                name="city"
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Enter Here"
              />
            </div>
            <div className="400px:w-[45%] flex-grow">
              <label className="block text-sm text-label mb-2">State</label>

              <input
                type="text"
                name="lastName"
                className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Doe"
              />
            </div>
          </div>
        </div>
        <div className="mb-2">
          <label className="block text-sm text-label mb-2">Password</label>
          <input
            type="password"
            name="password"
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="********"
          />
        </div>
        <div className="flex items-center justify-start flex-wrap 700px:w-[50%] gap-2">
          <button
            type="submit"
            className="bg-white flex-grow w-full 700px:w-auto px-8 py-3 rounded-3xl text-[#434CD9] border border-[#434CD9]"
          >
            Cancel
          </button>{" "}
          <button
            type="submit"
            className="bg-[#434CD9] flex-grow w-full 700px:w-auto px-12 py-3 rounded-3xl text-white"
          >
            Save
          </button>
        </div>
      </form>
    </div>
  );
};

export default ProfileForm;
