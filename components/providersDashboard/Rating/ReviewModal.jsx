"use client";
import { manrope } from "@/utils/fontPoppins";
import React, { useEffect, useState } from "react";
import { Rating } from "@smastrom/react-rating";
import { GoVerified } from "react-icons/go";
import { RxCross2 } from "react-icons/rx";
import { AddRatingApi } from "@/Apis/ServiceProviderApis/DashboardApi/RatingApi";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { GetServiceProviderRating } from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import Cookies from "js-cookie";
import { CircularProgress } from "@mui/material";

const ReviewModal = ({ showModal, setShowModal, detail }) => {
  const dispatch = useDispatch();
  const [rating, setRating] = useState(4);
  const [review, setReview] = useState("");
  const [error, setError] = useState("");
  const [loader, setloader] = useState(false);

  useEffect(() => {
    const modalElement = document.getElementById("reviewModal");
    if (modalElement) {
      if (showModal) {
        modalElement.showModal();
      } else {
        modalElement.close();
      }
    }
  }, [showModal]);

  const handleSubmit = async () => {
    setloader(true);
    if (review) {
      let data = {
        user_id: detail.to,
        rating: rating,
        review: review,
        review_from: detail.from,
      };
      console.log(data);
      let res = await AddRatingApi(data);
      setloader(false);

      console.log(res?.response?.data);
      if (res?.data?.status == "1") {
        document.getElementById(`successReviewModal`).showModal();
        dispatch(
          GetServiceProviderRating({
            user_id: Cookies.get("user_id"),
            type: "",
            rating: "",
            page: 1,
          })
        );
      } else if (res?.data?.status == "0") {
        toast.error(res?.data?.message);
      }
    } else {
      setloader(false);

      setError("Error");
    }
  };

  return (
    <>
      <dialog id="reviewModal" className={`modal ${manrope.className} `}>
        <div className="modal-box 800px:min-w-[600px]">
          <div
            className="flex justify-end cursor-pointer"
            onClick={() => setShowModal(false)}
          >
            <RxCross2 size={30} />
          </div>
          <h3 className="font-bold text-4xl text-center">Rate Origin</h3>
          <p className="text-lg mt-8 text-center">
            If you are enjoying using our app. Would you like to rate us.
          </p>
          <div className="flex justify-center mt-6">
            <Rating
              style={{ maxWidth: 150 }}
              value={rating}
              onChange={setRating}
            />
          </div>

          <textarea
            className="border border-gray-200 rounded-2xl w-full p-4 mt-6"
            name="ratingReview"
            id="ratingReview"
            cols="30"
            rows="6"
            placeholder="Give your Comments"
            onChange={(e) => {
              setReview(e.target.value);
              setError("");
            }}
          ></textarea>

          {error && (
            <div className="ml-3 text-red-400">This field is required</div>
          )}
          <div className="flex justify-center mt-6">
            {loader ? (
              <CircularProgress />
            ) : (
              <button
                className="px-9 py-3 rounded-md bg-bgBlue text-white"
                onClick={() => handleSubmit()}
              >
                Submit
              </button>
            )}
          </div>
        </div>
        <form method="dialog" className="modal-backdrop">
          <button
            id="closeBtn"
            onClick={() => {
              setShowModal(false);
            }}
          >
            close
          </button>
        </form>
      </dialog>

      <dialog id="successReviewModal" className={`modal ${manrope.className} `}>
        <div className="modal-box 800px:min-w-[600px] flex flex-col items-center justify-center gap-2">
          <GoVerified size={200} color="#444BDB" />
          <h3 className="font-semibold text-2xl">
            Review Sumbitted Successfully
          </h3>
          <p className="text-center">
            Your review has been successfully submitted. Thank you for sharing
            your feedback with us!
          </p>
        </div>
        <form method="dialog" className="modal-backdrop">
          <button
            onClick={() => {
              setShowModal(false);
            }}
          >
            close
          </button>
        </form>
      </dialog>
    </>
  );
};

export default ReviewModal;
