import ReviewAvatar from "../../../images/dashboard/reviewAvatar.png";
import LinkdlnIcon from "../../../images/dashboard/linkdlnIcon.png";
import reportIcon from "../../../images/dashboard/reportIcon.png";
import Image from "next/image";
import moment from "moment";
import { Rating } from "@smastrom/react-rating";
import { useState } from "react";
import { MdDelete, MdOutlineCancel } from "react-icons/md";
import { ReplyToReview } from "@/Apis/ServiceProviderApis/DashboardApi/RatingApi";
import { toast } from "react-toastify";
import { useDispatch } from "react-redux";
import { GetServiceProviderRating } from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import Cookies from "js-cookie";
import { FiEdit } from "react-icons/fi";
import DeleteDialog from "@/components/DeleteDialog/DeleteDialog";
import { MdDone } from "react-icons/md";
import { RxCross2 } from "react-icons/rx";
import { CircularProgress } from "@mui/material";
import profile from '../../../images/dashboard/profile.png'
const RatingCard = ({ details, index }) => {
  const dispatch = useDispatch();
  const [showReply, setShowReply] = useState(-1);
  const [reply, setReply] = useState("");
  const [deleteDialog, setdeleteDialog] = useState(false);
  const [deleteData, setdeleteData] = useState(null);
  const [loader, setloader] = useState(false);
  const [ImageUrl, setImageUrl] = useState(details?.image);
  const saveReply = async (id) => {
    setloader(true);
    console.log(id, reply);
    try {
      let data = {
        id: id,
        review_reply: reply,
      };
      let res = await ReplyToReview(data);
      setloader(false);

      if (res?.data?.status == "1") {
        dispatch(
          GetServiceProviderRating({
            user_id: Cookies.get("user_id"),
            type: "",
            rating: "",
            page: 1,
          })
        );
        toast.success("Reply added Successfully");
        setShowModal(false);
      } else if (res?.data?.status == "0") {
        toast.error(res?.data?.message);
      }
    } catch (error) {
      setloader(false);

      toast.error(error?.response?.data?.message);
    }
    setReply("");
    setShowReply(-1);
  };
  const handleDelete = async (id) => {
    try {
      setloader(true);
      let data = {
        id: deleteData.id,
        review_reply: "",
      };
      let res = await ReplyToReview(data);
      if (res?.data?.status == "1") {
        dispatch(
          GetServiceProviderRating({
            user_id: Cookies.get("user_id"),
            type: "",
            rating: "",
            page: 1,
          })
        );
        toast.success("Reply Deleted Successfully");
      } else {
        toast.error(res?.response?.data?.message);
        setShowModal(false);
      }
    } catch (error) {
      console.log(error);
      toast.error(error);
    }
    setloader(false);
    setdeleteDialog(false);

    setShowReply(-1);
  };
  return (
    <>
      {deleteDialog && (
        <DeleteDialog
          setdeleteData={setdeleteData}
          loader={loader}
          handleDelete={handleDelete}
          type={"Service"}
          name={deleteData?.title}
          deleteData={deleteData}
          setdeleteDialog={setdeleteDialog}
        />
      )}
      <div className="shadow-lg rounded-lg p-4">
        <div className="flex justify-between">
          <div className="flex items-end gap-4">
            <div className="w-[55px] h-[55px] rounded-full ">
              <Image
                className="w-12 h-12 object-contain"
                // src={ReviewAvatar}
                src={ImageUrl}
                onError={() => {
                  setImageUrl(profile);
                }}
                // src={details?.image}
                alt="Avatar"
                width={120}
                height={120}
                style={{borderRadius:"50%",width:"100%",height:"100%"}}
              />
            </div>

            <div>
              <h5 className="text-lg font-semibold">{details?.review_from}</h5>
              <h6 className="text-[#7D7D7D] text-sm">{details?.title}</h6>
            </div>
          </div>

          <div className="flex items-end gap-4 p-6">
            <div>
              <Image
                className="w-6 h-6 object-contain"
                src={LinkdlnIcon}
                alt="Avatar"
              />
            </div>
            <h5 className="text-lg font-semibold">Verified</h5>
          </div>
        </div>
        <div className="p-3">
          <Rating style={{ maxWidth: 150 }} value={Number(details?.rating)} />
        </div>

        <div className="p-3 border-b-[1px]">
          <h5>{details?.review}</h5>
        </div>

        {showReply != index && (
          <div className="p-3 flex justify-between flex-wrap gap-2">
            <div className="flex gap-8 font-bold">
              <h6>
                {details?.createdAt
                  ? moment(details?.createdAt).format("MMMM D, YYYY")
                  : ""}
              </h6>
              {/* <h6>{"Helpful (1)"}</h6>
          <h6 className="cursor-pointer">Share</h6> */}
            </div>
            {/* <div className="flex gap-2">
          <Image src={reportIcon} />

          <h6 className="font-bold text-[#F44336]">Report</h6>
        </div> */}
            {!details.review_reply && (
              <div>
                <h4
                  className="text-[#0027B0] text-xl font-semibold cursor-pointer"
                  onClick={() => {
                    setShowReply(index);
                  }}
                >
                  Reply
                </h4>
              </div>
            )}
          </div>
        )}

        <div>
          {showReply == index && (
            <div className="flex gap-3 items-center">
              <textarea
                value={reply}
                className="border-2 w-full p-1"
                name=""
                id=""
                rows="1"
                onChange={(e) => {
                  setReply(e.target.value);
                }}
              ></textarea>

              <RxCross2
                size={22}
                color="#ef2502"
                className="font-bold cursor-pointer"
                onClick={() => {
                  setShowReply(-1);
                  setReply("");
                }}
              />
              {loader ? (
                <CircularProgress />
              ) : (
                <MdDone
                  size={22}
                  color="#2aa70e"
                  className="font-bold cursor-pointer"
                  onClick={() => {
                    saveReply(details.id);
                  }}
                />
              )}
            </div>
          )}

          {details.review_reply && (
            <div className="p-6 flex gap-3">
              <div className="bg-gray-400 w-1"></div>
              <div className="flex justify-between w-full">
                <div>
                  <p className="font-semibold text-sm">Response From Owner</p>
                  <p className="text-sm">{details.review_reply}</p>
                </div>

                <div className="flex gap-2">
                  <FiEdit
                    size={18}
                    className="cursor-pointer"
                    color="#6b7280"
                    onClick={() => {
                      setShowReply(index);
                      setReply(details.review_reply);
                    }}
                  />
                  <MdDelete
                    className="cursor-pointer"
                    onClick={() => {
                      setdeleteDialog(true);
                      setdeleteData(details);
                    }}
                    size={18}
                    color="#6b7280"
                  />
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </>
  );
};
export default RatingCard;
