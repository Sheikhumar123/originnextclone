"use client";
import BgGradient from "../../BgGradient/BgGradient";
import backgroundImg from "../../../images/dashboard/reviewBg.png";

import Image from "next/image";

import { useEffect, useState } from "react";
import ReviewModal from "./ReviewModal";
import RatingCard from "./RatingCard";
import { useDispatch, useSelector } from "react-redux";
import {
  GetProviderdashboardDetails,
  GetServiceProviderRating,
} from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import Cookies from "js-cookie";
import Pagination from "@/components/Pagination/Pagination";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
const Rating = () => {
  const { ProvoderDashRating, status, error } = useSelector(
    GetProviderdashboardDetails
  );
  const [allRating, setallrating] = useState([]);
  const [count, setcount] = useState(0);
  const [page, setpage] = useState(1);
  const [limit, setlimit] = useState(1);
  useEffect(() => {
    if (ProvoderDashRating) {
      setallrating(ProvoderDashRating?.data);
      setcount(ProvoderDashRating?.count);
      setlimit(ProvoderDashRating?.limit);
    }
  }, [ProvoderDashRating]);
  console.log(ProvoderDashRating);
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  const [rating, setrating] = useState("");
  const [type, settype] = useState("");
  useEffect(() => {
    dispatch(
      GetServiceProviderRating({
        user_id: Cookies.get("user_id"),
        type: type,
        rating: Number(rating),
        page: page,
      })
    );
  }, [dispatch, rating, type,page]);
  return (
    <>
    {
        status=="pending" && <ScreenLoader/>
    }
      {showModal && (
        <ReviewModal
          showModal={showModal}
          setShowModal={setShowModal}
          detail={{ from: Cookies.get("user_id"), to: Cookies.get("user_id") }}
        />
      )}
      <div className="w-full flex gap-8 flex-col">
        <BgGradient>
          <div className="700px:h-56 300px:h-[150px] flex items-center overflow-hidden">
            <div className="absolute left-0">
              <Image src={backgroundImg} />
            </div>
            <div className="max-w-[570px] relative w-full flex flex-col 700px:gap-8 300px:gap-3 justify-center">
              <h1 className="font-bold 700px:text-4xl 300px:text-xl text-white">
                Rating And Reviews
              </h1>
              <h2 className="700px:text-lg 300px:text-md text-white">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim x
              </h2>
            </div>
          </div>
        </BgGradient>

        <div className="1300px:w-5/6 300px:w-full">
          <div className="flex justify-between flex-wrap gap-2">
            <h2 className="font-bold text-3xl">{count} Reviews</h2>

            <div className="w-9/12 flex justify-between flex-wrap gap-2">
              <select
                id="professionalType"
                className="bg-gray-50 border border-gray-300 text-sm rounded-md focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[150px] font-semibold"
                name="professional_type"
                value={type}
                onChange={(e) => {
                  settype(e.target.value);
                }}
              >
                <option value={""}>Type</option>
                <option value={"Most Recent"}>Most Recent</option>
                <option value="Today">Today</option>
                <option value="Last Month">Last Month</option>
              </select>
              <select
                id="professionalType"
                className="bg-gray-50 border border-gray-300 text-sm rounded-md focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[150px] font-semibold"
                name="professional_type"
                value={rating}
                onChange={(e) => {
                  setrating(e.target.value);
                }}
              >
                <option value={""}>Any Rating</option>
                <option value="1">1 Star</option>
                <option value="2">2 Star</option>
                <option value="3">3 Star</option>
                <option value="4">4 Star</option>
                <option value="5">5 Star</option>
              </select>

              <button
                className="bg-[#4756DF] text-white p-3 rounded-3xl"
                onClick={() => {
                  setShowModal(!showModal);
                }}
              >
                Write a review for Origin
              </button>
            </div>
          </div>

          {/* Rating Cards  */}

          {allRating?.length ? (
            <div className="mt-3">
              {allRating?.map((detail, index) => {
                return (
                  <div>
                    <RatingCard details={detail} index={index} />
                  </div>
                );
              })}
            </div>
          ) : null}
        </div>
      </div>
      {count > allRating?.length ? (
        <div className="pt-4 pb-[130px] flex justify-end px-2">
          <Pagination
            pageCount={page}
            setPageCount={setpage}
            totalcount={count}
            limit={limit}
            showCount={true}
          />
        </div>
      ) : null}
    </>
  );
};

export default Rating;
