"use client";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import {
  GetProviderTrustPoint,
  GetProviderdashboardDetails,
} from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import axiousInstance from "@/utils/axiousInstance";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { FaFacebook } from "react-icons/fa";
import { FaLinkedin } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
function Verification() {
  const [trustData, settrustData] = useState(null);
  const [active, setActive] = useState(0);
  const { ProvoderTrustPointData, status } = useSelector(
    GetProviderdashboardDetails
  );
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(GetProviderTrustPoint(Cookies.get("user_id")));
  }, [dispatch, Cookies.get("user_id")]);
  console.log(trustData);
  let arr = [10, 20, 30, 40];
  useEffect(() => {
    if (ProvoderTrustPointData) {
      settrustData(ProvoderTrustPointData);
      setActive(ProvoderTrustPointData?.totalpoints)
    }
  }, [ProvoderTrustPointData]);
  return (
    <>
      {status == "pending" && <ScreenLoader />}
      <div className="flex flex-col gap-6">
        <h3 className="text-2xl font-semibold">Trust Point</h3>
        <div className="border-t-2 border-l-2 border-r-2 p-2">
          <h4 className="font-semibold">What are Trust Points?</h4>
          <p className="inline-block">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente,
            quo tempore ipsum vero ullam assumenda officia rem repellendus
            delectus possimus. Sint non labore deserunt tenetur ullam laudantium
            nisi quasi perferendis! orem ipsum dolor sit amet consectetur
            adipisicing elit. Sapiente, quo tempore ipsum vero ullam assumenda
            officia rem repellendus delectus possimus. Sint non labore deserunt
            tenetur ullam laudantium nisi quasi perferendis!.{" "}
            <p className="text-primary inline-block cursor-pointer">
              Learn More
            </p>
          </p>
        </div>
        <div className="border-t-2 border-l-2 border-r-2 p-3  flex items-center">
          <ol className="flex items-center w-full mb-12 gap-2   ">
            {arr.map((number) => {
              return number == 1 ? (
                <li className="flex w-40 h-30 items-center bg-red" key={number}>
                  <div className="bg-gradient-to-l from-[#19FB9B] to-[#8C01FA] p-0.5 rounded-full w-full h-max">
                    <span className="flex bg-gradient-to-l from-[#19FB9B] to-[#8C01FA] text-white items-center justify-center w-10 h-10  rounded-full lg:h-12 lg:w-12  shrink-0 border-white border-4">
                      {number}
                    </span>
                  </div>
                </li>
              ) : (
                <>
                  <li
                    className={`w-full h-2 ${active >= number ? "bg-bgBlue" : "bg-gray-300"
                      } rounded-md`}
                    key={number}
                  ></li>
                  <li className="flex w-40 h-30 items-center" key={number}>
                    <div
                      className={`${active >= number
                          ? "bg-gradient-to-l from-[#19FB9B] to-[#8C01FA]"
                          : "bg-gray-300"
                        }  p-0.5 rounded-full`}
                    >
                      <span
                        className={`flex items-center justify-center w-10 h-10  rounded-full lg:h-12 lg:w-12  shrink-0 border-4 border-white  ${active >= number
                            ? "bg-gradient-to-l from-[#19FB9B] to-[#8C01FA] text-white"
                            : "bg-gray-300"
                          }`}
                      >
                        {number}
                      </span>
                    </div>
                  </li>
                </>
              );
            })}
          </ol>
        </div>
        <div>
          <div className="border-[1px] px-4 py-6  border-l-lightBlack flex justify-between items-center">
            <p>Phone</p>
            <div className="flex gap-8 items-center">
              <p className="text-[#00AD31]">
                {trustData?.phone_verified ? "verified" : ""}
              </p>
              <div className="bg-[#0479D3] rounded-3xl px-4 py-2  text-white">
                <p>10 points</p>
              </div>
            </div>
          </div>
          <div className="border-[1px] px-4 py-6  border-l-lightBlack flex justify-between items-center">
            <p>Email</p>
            <div className="flex gap-8 items-center">
              <p className="text-[#00AD31]">
                {trustData?.email_verified ? "verified" : ""}
              </p>
              <div className="bg-[#0479D3] rounded-3xl px-4 py-2  text-white">
                <p>10 points</p>
              </div>
            </div>
          </div>
          <div className="border-[1px] px-4 py-6  border-l-lightBlack flex justify-between items-center">
            <p>Facebook</p>
            <div className="flex gap-8 items-center">
              {!trustData?.linkedin_verified && <div className="flex gap-2 items-center bg-[#0C5AE5] p-3 py-2 rounded-md text-white">
                <FaFacebook size={18} />
                <p>connect +</p>
              </div>}
              {
                trustData?.fb_verified ? (
                  <p className="text-[#00AD31]">verified</p>
                ) : null
                //    <p className="text-[#00AD31]">verified</p>
              }
              {trustData?.fb_verified ? (
                <div className="bg-[#0479D3] rounded-3xl px-4 py-2  text-white">
                  <p>10 points</p>
                </div>
              ) : null}
            </div>
          </div>
          <div className="border-[1px] px-4 py-6  border-l-lightBlack flex justify-between items-center">
            <p>LinkedIn</p>
            <div className="flex gap-8 items-center">
              {!trustData?.linkedin_verified && <div className="flex gap-2 items-center bg-[#3AA6F5] p-3 py-2 rounded-md text-white">
                <FaLinkedin size={18} />
                <p>connect +</p>
              </div>}
              {
                trustData?.linkedin_verified ? (
                  <p className="text-[#00AD31]">verified</p>
                ) : null
                //    <p className="text-[#00AD31]">verified</p>
              }
              {trustData?.linkedin_verified ? (
                <div className="bg-[#0479D3] rounded-3xl px-4 py-2  text-white">
                  <p>10 points</p>
                </div>
              ) : null}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Verification;
