"use client";

import { useFormik } from "formik";
import Link from "next/link";
import { useEffect, useState } from "react";
import { MdVisibility } from "react-icons/md";
import * as Yup from "yup";
import { MdVisibilityOff } from "react-icons/md";
import { changePasswordSecurity } from "@/Apis/ServiceProviderApis/DashboardApi/ChangePassword";
import Cookies from "js-cookie";
import { toast } from "react-toastify";
import { CircularProgress } from "@mui/material";
import { AuthFactorApi } from "@/Apis/ServiceProviderApis/DashboardApi/AuthFactorApi";
import axiousInstance from "@/utils/axiousInstance";
const Security = () => {
  const [twofa, settwofa] = useState(false);
  const [resetPassword, setresetPassword] = useState(false);
  const [loader, setloader] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);
  const [showPassword2, setShowPassword2] = useState(false);
  const handleClickShowPassword2 = () => setShowPassword2((show) => !show);
  const [showPassword3, setShowPassword3] = useState(false);
  const handleClickShowPassword3 = () => setShowPassword3((show) => !show);
  const initialValues = {
    oldpassword: "",
    newpassword: "",
    confirmpassword: "",
  };
  const ValidationSchema = Yup.object().shape({
    oldpassword: Yup.string().required("Please enter old password."),
    newpassword: Yup.string()
      .min(8, "Password is too short")
      .required("Password is required"),
    confirmpassword: Yup.string()
      .oneOf([Yup.ref("newpassword"), null], "Passwords must match")
      .required("Confirm Password is required"),
  });
  const handleSubmit = async (values, resetForm) => {
    delete values.confirmpassword;
    values.user_id = Cookies.get("user_id");
    // console.log(values);
    setloader(true);
    try {
      const resp = await changePasswordSecurity(values);
      console.log(resp);
      setloader(false);

      if (resp.data.status == "1") {
        toast.success(resp.data.message);
        resetForm();
      }
    } catch (error) {
      setloader(false);
      toast.error(error.response.data.message);
    }
  };
  const handleAuthFactor = async (data) => {
    const resp = await AuthFactorApi({
      two_fa: data,
      user_id: Cookies.get("user_id"),
    });
    console.log(resp);
    setloader(false);

    if (resp.data.status == "1") {
      getAuthFactor();
      toast.success(resp.data.message);
    }
  };
  const getAuthFactor = async () => {
    try {
      const response = await axiousInstance.get(
        `dashboard/getgeneralsetting?user_id=${Cookies.get("user_id")}`
      );
      console.log(response.data?.userdata?.two_fa);
      if (response.data.status == "1") {
        settwofa(response.data?.userdata?.two_fa);
      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      toast.error(error.response?.data?.error);
    }
  };
  useEffect(() => {
    getAuthFactor();
  }, []);
  const formik = useFormik({
    initialValues: initialValues,
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  return (
    <div className="flex flex-col gap-9">
      <div className="flex justify-between sm:w-2/3 w-full">
        <div>Password</div>
        <div
          onClick={() => {
            setresetPassword(true);
          }}
          className="hover:text-blue-600 hover:cursor-pointer hover:underline text-blue-400"
        >
          Click here to reset
        </div>
      </div>
      {resetPassword ? (
        <form onSubmit={formik.handleSubmit}>
          <div className="sm:w-2/3 w-full">
            <div className="flex gap-3 mb-3">
              <div className="w-full">
                <div className="relative w-full">
                  <label className="block text-sm text-label mb-2">
                    Old Password
                  </label>
                  <input
                    type={showPassword ? "text" : "password"}
                    name="oldpassword"
                    className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                    placeholder="********"
                    value={formik.values.oldpassword}
                    onChange={formik.handleChange}
                  />
                  <button
                    type="button"
                    className="absolute  right-0 flex bottom-[20%] px-3 text-gray-700"
                  >
                    {showPassword ? (
                      <MdVisibilityOff
                        onClick={handleClickShowPassword}
                        size={25}
                      />
                    ) : (
                      <MdVisibility onClick={handleClickShowPassword} size={25} />
                    )}
                  </button>

                </div>
                {formik.errors.oldpassword &&
                  formik.touched.oldpassword &&
                  formik.errors.oldpassword && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.oldpassword}
                    </p>
                  )}
              </div>

              <div className="w-full">
                <div className="relative w-full">
                  <label className="block text-sm text-label mb-2">
                    New Password
                  </label>
                  <input
                    type={showPassword2 ? "text" : "password"}
                    name="newpassword"
                    className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                    placeholder="********"
                    value={formik.values.newpassword}
                    onChange={formik.handleChange}
                  />
                  <button
                    type="button"
                    className="absolute  right-0 flex bottom-[20%] px-3 text-gray-700"
                  >
                    {showPassword2 ? (
                      <MdVisibilityOff
                        onClick={handleClickShowPassword2}
                        size={25}
                      />
                    ) : (
                      <MdVisibility
                        onClick={handleClickShowPassword2}
                        size={25}
                      />
                    )}
                  </button>

                </div>
                {formik.errors.newpassword &&
                  formik.touched.newpassword &&
                  formik.errors.newpassword && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.newpassword}
                    </p>
                  )}
              </div>
            </div>
            <div className="flex gap-3 mb-3">
              <div className="w-full">
                <div className="relative w-[49%]">
                  <label className="block text-sm text-label mb-2">
                    Confirm Password
                  </label>
                  <input
                    type={showPassword3 ? "text" : "password"}
                    name="confirmpassword"
                    className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                    placeholder="********"
                    //   type={showPassword ? "text" : "password"}
                    //   name="password"
                    value={formik.values.confirmpassword}
                    onChange={formik.handleChange}
                  //   autoComplete={false}
                  //   readonly="readonly"
                  //   onMouseDown={(e) => {
                  //     e.target.removeAttribute("readonly");
                  //   }}
                  />
                  <button
                    type="button"
                    className="absolute  right-0 flex bottom-[20%] px-3 text-gray-700"
                  >
                    {showPassword3 ? (
                      <MdVisibilityOff
                        onClick={handleClickShowPassword3}
                        size={25}
                      />
                    ) : (
                      <MdVisibility
                        onClick={handleClickShowPassword3}
                        size={25}
                      />
                    )}
                  </button>

                </div>
                {formik.errors.confirmpassword &&
                  formik.touched.confirmpassword &&
                  formik.errors.confirmpassword && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.confirmpassword}
                    </p>
                  )}
              </div>
            </div>
            <div className="flex items-center justify-end flex-wrap  gap-2">
              {loader ? (
                <CircularProgress />
              ) : (
                <>
                  <button
                    type="button"
                    onClick={() => {
                      setresetPassword(false);
                    }}
                    className="bg-white w-[fit-content] px-6 py-2 rounded-3xl text-[#434CD9] border border-[#434CD9]"
                  >
                    Cancel
                  </button>{" "}
                  <button
                    type="submit"
                    className="bg-[#434CD9] w-[fit-content] px-6 py-2 rounded-3xl text-white border border-[#434CD9]"
                  >
                    Save
                  </button>
                </>
              )}
            </div>
          </div>
        </form>
      ) : null}
      <div className="flex justify-between items-center sm:w-2/3 w-full">
        <div>
          <p className="text-lg font-[600] text-[#404040]">
            2-Factor Authentication
          </p>
          <p className="text-[#6a6a6a]  w-full pt-2">
            Enables 2-factors Authentication for better security{" "}
          </p>
        </div>
        <label className="inline-flex items-center cursor-pointer">
          <input
            type="checkbox"
            defaultValue=""
            checked={twofa}
            className="sr-only peer"
            onChange={(e) => {
              handleAuthFactor(e.target.checked);
            }}
          />
          <div className="relative w-14 h-7 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-6 after:w-6 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />
        </label>
      </div>

      <div className="flex justify-between sm:w-2/3 w-full">
        <div className="text-lg font-[600] text-[#404040]">Email</div>
        <div className="hover:text-blue-600 hover:cursor-pointer hover:underline text-blue-400">
          <Link href={"/providersdashboard/profile"}>change email</Link>
        </div>
      </div>
      <div className="flex justify-between sm:w-2/3 w-full">
        <div className="text-lg font-[600] text-[#404040]">
          <div className="text-lg font-[600] text-[#404040]">Phone</div>
        </div>
        <div className="hover:text-blue-600 hover:cursor-pointer hover:underline text-blue-400">
          <Link href={"/providersdashboard/profile"}>change phone number</Link>
        </div>
      </div>
    </div>
  );
};
export default Security;
