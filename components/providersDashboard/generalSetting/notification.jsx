'use client'
import { getNotificationApi, createNotificationApi } from "@/Apis/ServiceProviderApis/DashboardApi/NotificationApi";
import Cookies from "js-cookie";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
const Notification = () => {
    const userId = Cookies.get("user_id")

    const [notification, setNotiffication] = useState({
        "task_invitation": false,
        "bid_status": false,
        "deadline_reminder": false,
        "payment_alerts": false,
        "message_alerts": false,
        "user_id": userId
    })

    const getNotificationCheck = async (userId) => {
        try {
            let resp = await getNotificationApi(userId)
            console.log(resp);
            let notificationData = resp?.data?.NotificationCheckdata
            if (notificationData) {
                notificationData.user_id = userId
                setNotiffication({ ...notificationData })
            }
        } catch (error) {
            console.log(error);
        }

    }

    useEffect(() => {
        getNotificationCheck(userId)
    },[])
    const handleChange = async (key) => {
        console.log(notification[key]);
        let obj = notification
        if (obj[key]) {
            obj[key] = false
        } else {
            obj[key] = true
        }
        setNotiffication({ ...obj })

        try {
            let resp = await createNotificationApi(obj)
            console.log(resp);
            if (resp.data.status = "1") {
                toast.success(resp.data.message)
            }else{
                toast.error(resp.data.message)
            }
        } catch (error) {
            toast.error(resp.data.message)
            console.log(error);
        }

    }

    console.log(notification);
    return (
        <div className="flex flex-col gap-9">

            <div className="flex justify-between items-center sm:w-2/3 w-full">
                <div>
                    <p className="text-lg font-[700] text-[#404040]">Set Up Your Notification Settings</p>
                    <p className="text-[#6a6a6a]  w-full pt-2">
                        Lorem Ipsum is simply dummy text.{" "}
                    </p>
                </div>
                {/* <label className="inline-flex items-center cursor-pointer">
                    <input type="checkbox" defaultValue="" className="sr-only peer" />
                    <div className="relative w-14 h-7 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-6 after:w-6 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />

                </label> */}

            </div>

            <div className="flex justify-between items-center">
                <div className="w-11/12">
                    <p className="text-lg font-[600] text-[#404040]">Task Invitations</p>
                    <p className="text-[#6a6a6a]  w-full pt-2">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus {" "}
                    </p>
                </div>
                <label className="inline-flex items-center cursor-pointer">
                    <input type="checkbox" defaultValue="" checked={notification.task_invitation} className="sr-only peer" onChange={() => { handleChange('task_invitation') }} />
                    <div className="relative w-14 h-7 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-6 after:w-6 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />

                </label>

            </div>



            <div className="flex justify-between items-center">
                <div className="w-11/12">
                    <p className="text-lg font-[600] text-[#404040]">Bid Status</p>
                    <p className="text-[#6a6a6a]  w-full pt-2">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, {" "}
                    </p>
                </div>
                <label className="inline-flex items-center cursor-pointer">
                    <input type="checkbox" defaultValue="" checked={notification.bid_status} className="sr-only peer" onChange={() => { handleChange('bid_status') }} />
                    <div className="relative w-14 h-7 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-6 after:w-6 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />

                </label>

            </div>

            <div className="flex justify-between items-center">
                <div className="w-11/12">
                    <p className="text-lg font-[600] text-[#404040]">Deadline Reminders</p>
                    <p className="text-[#6a6a6a]  w-full pt-2">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus{" "}
                    </p>
                </div>
                <label className="inline-flex items-center cursor-pointer">
                    <input type="checkbox" defaultValue="" checked={notification.deadline_reminder} className="sr-only peer" onChange={() => { handleChange('deadline_reminder') }} />
                    <div className="relative w-14 h-7 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-6 after:w-6 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />

                </label>

            </div>


            <div className="flex justify-between items-center">
                <div className="w-11/12">
                    <p className="text-lg font-[600] text-[#404040]">Payment Alerts</p>
                    <p className="text-[#6a6a6a]  w-full pt-2">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus {" "}
                    </p>
                </div>
                <label className="inline-flex items-center cursor-pointer">
                    <input type="checkbox" checked={notification.payment_alerts} defaultValue="" className="sr-only peer" onChange={() => { handleChange('payment_alerts') }} />
                    <div className="relative w-14 h-7 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-6 after:w-6 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />

                </label>

            </div>

            <div className="flex justify-between items-center">
                <div className="w-11/12">
                    <p className="text-lg font-[600] text-[#404040]">New Message Alerts</p>
                    <p className="text-[#6a6a6a]  w-full pt-2">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit amet luctus venenatis, lectus magna fringilla urna, {" "}
                    </p>
                </div>
                <label className="inline-flex items-center cursor-pointer">
                    <input type="checkbox" checked={notification.message_alerts} defaultValue="" className="sr-only peer" onChange={() => { handleChange('message_alerts') }} />
                    <div className="relative w-14 h-7 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-6 after:w-6 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />

                </label>

            </div>





        </div>
    )
}
export default Notification