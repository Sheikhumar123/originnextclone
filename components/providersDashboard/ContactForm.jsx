"use client";
import { GetProviderdashboardDetails } from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
const ContactForm = () => {


  

  return (
    <div className="w-full">
      <form>
        <div className="mb-2">
          <label className="block text-sm text-label mb-2">Name</label>
          <div className="flex items-center flex-wrap gap-2">
            <input
              type="text"
              name="firstName"
              className="400px:w-[45%] flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="John"
            />
            <input
              type="text"
              name="lastName"
              className="400px:w-[45%] flex-grow bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              placeholder="Doe"
            />
          </div>
        </div>
        <div className="mb-2">
          <label className="block text-sm text-label mb-2">Email</label>
          <input
            type="email"
            name="email"
            className="w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
            placeholder="John Doe"
          />
        </div>
        <div className="mb-2">
          <label className="block text-sm text-label mb-2">Message</label>
          <textarea
            name="message"
            cols="30"
            rows="5"
            placeholder="Your message here..."
            className="w-full border p-2 rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
          ></textarea>
        </div>
        <button
          type="submit"
          className="bg-[#434CD9] flex-grow w-full md:w-auto px-12 py-3 rounded-3xl text-white"
        >
          Send Message
        </button>
      </form>
    </div>
  );
};

export default ContactForm;
