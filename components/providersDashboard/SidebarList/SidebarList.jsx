"use client";
import Image from "next/image";
import { providerDashboardSidebarData } from "@/data/providerDashboardData";
import Link from "next/link";
import { usePathname } from "next/navigation";
import AvtarImage from "../../../images/dashboard/Avatar.png";
import { MdKeyboardArrowDown, MdKeyboardArrowUp } from "react-icons/md";
import { useEffect, useState } from "react";
import Cookies from "js-cookie";
import profile from "../../../public/ServiceProvider/profile.png";

const SidebarList = () => {
  const [menu, setMenu] = useState(null);
  const path = usePathname();
  const [ImageUrl, setImageUrl] = useState(Cookies?.get("profile_image"));

  console.log("path", path);
  return (
    <>
      {/* Header Title  */}
      <div className="flex items-end gap-4 p-6 border-b-[1px] border-[#d1d5db80] ">
        <div>
          <Image
            className="w-12 h-12 object-contain"
            src={
              ImageUrl
              // Cookies?.get("profile_image")
              //   ? Cookies.get("profile_image")
              //   : "https://images.unsplash.com/photo-1494790108377-be9c29b29330"
            }
            onError={() => {
              setImageUrl(profile);
            }}
            // src={
            //   Cookies.get("profile_image")
            //     ? Cookies.get("profile_image")
            //     : AvtarImage
            // }
            style={{
              width: "50px",
              height: "50px",
              objectFit: "cover",
            }}
            width={50}
            height={50}
            // src={AvtarImage}
            alt="Avatar"
          />
        </div>

        <div>
          <h5 className="text-[#d1d5db80] uppercase text-base">
            {Cookies.get("title") ? Cookies.get("title") : ""}
            {/* {ProviderProfileData?.title?ProviderProfileData?.title:""} */}
          </h5>
          <h6 className="text-base">
            {Cookies.get("username") ? Cookies.get("username") : ""}
          </h6>
        </div>
      </div>

      <div className="p-4">
        <div className="font-light p-3">MAIN</div>
        {providerDashboardSidebarData.map((item, index) => (
          <div>
            <div
              className={`p-4 flex items-center w-full justify-between gap-1 mb-4 hover:bg-[#c7c6c64d] rounded-xl hover:font-semibold ${
                path === item.link ||
                item?.subItems?.some((subMenu) => subMenu?.link === path)
                  ? "bg-[#c7c6c64d] font-semibold"
                  : ""
              } transition-all`}
              // className={`flex justify-between gap-1 w-full ${
              //       path === item.link ||
              //       item?.subItems?.some((subMenu) => subMenu?.link === path)
              //         ? "bg-[#c7c6c64d] font-semibold"
              //         : ""
              //     }`}
            >
              <Link
                key={item.id}
                href={item.link}
                scroll={false}
                // className={`p-4 flex items-center w-full justify-start gap-4 mb-4 hover:bg-[#c7c6c64d] rounded-xl hover:font-semibold ${
                //   path === item.link ||
                //   item?.subItems?.some((subMenu) => subMenu?.link === path)
                //     ? "bg-[#c7c6c64d] font-semibold"
                //     : ""
                // } transition-all`}
              >
                <div className="flex gap-4 items-center w-full">
                  {item.icon}
                  <p>{item.title}</p>
                </div>
              </Link>
              {item?.subItems?.length ? (
                <div>
                  {index != menu ? (
                    <MdKeyboardArrowDown
                      size={25}
                      color="#ffffffb3"
                      className="cursor-pointer"
                      onClick={() => setMenu(index)}
                    />
                  ) : (
                    <MdKeyboardArrowUp
                      size={25}
                      color="#ffffffb3"
                      className="cursor-pointer"
                      onClick={() => setMenu(null)}
                    />
                  )}
                </div>
              ) : null}
            </div>

            {index == menu && (
              <div className="px-6">
                {item?.subItems?.map((item) => (
                  <Link
                    key={item.id}
                    href={item.link}
                    scroll={false}
                    className={`p-3 flex items-center justify-start gap-4 mb-[10px] hover:!text-bgBlue hover:bg-white rounded-xl hover:font-semibold ${
                      path === item.link
                        ? "bg-white !text-bgBlue font-semibold"
                        : ""
                    } transition-all`}
                  >
                    {item.icon}
                    <p>{item.title}</p>
                  </Link>
                ))}
              </div>
            )}
          </div>
        ))}
      </div>
    </>
  );
};

export default SidebarList;
