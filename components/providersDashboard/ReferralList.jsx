"use client";
import { refferalData } from "@/data/dashboardData";
const RefferalList = () => {
  return (
    <div className="w-full p-4">
      <div className="relative overflow-auto scrollbar-thin max-h-[600px] rounded-xl w-full">
        <table className="w-full text-sm text-left rtl:text-right text-gray-500 ">
          <thead className="text-xs text-black uppercase bg-gray-200  ">
            <tr>
              <th scope="col" className="px-6 py-3">
                Sr
              </th>
              <th scope="col" className="px-6 py-3">
                Name
              </th>
              <th scope="col" className="px-6 py-3">
                Earned
              </th>
              <th scope="col" className="px-6 py-3">
                Date
              </th>
            </tr>
          </thead>
          <tbody>
            {refferalData.map((item, index) => (
              <tr className="bg-white text-black text-nowrap " key={index}>
                <td className="px-6 py-4">{item.sr}</td>
                <td className="px-6 py-4">{item.name}</td>
                <td className="px-6 py-4">{item.earned}</td>
                <td className="px-6 py-4">{item.date}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default RefferalList;
