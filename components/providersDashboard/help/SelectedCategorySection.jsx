import React from "react";
import { MdArrowBack } from "react-icons/md";

const SelectedCategorySection = ({
  setstep,
  selectedCategory,
  setselectedQuestion,
}) => {
  return (
    <div>
      <div className="py-6">
        <div
          className="cursor-pointer"
          onClick={() => {
            setstep((prev) => prev - 1);
          }}
        >
          <MdArrowBack fontSize={22} />
        </div>
        <p className="text-black text-2xl py-2 font-semibold">
          {selectedCategory?.title}
        </p>
        <p className="text-[12px] w-full max-w-[500px]">
          {selectedCategory?.description}
        </p>
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-2   lg:grid-cols-3  gap-4 ">
        {selectedCategory?.subCategories?.map((subcategory, index) => {
          return (
            <div
              className=" flex  rounded-lg px-4 py-4 gap-4 min-h-[150px] "
              key={index}
            >
              <div className="w-full flex items-start justify-center flex-col gap-2">
                <p className="font-semibold">{subcategory?.title}</p>
                {subcategory?.questions?.map((question, idx) => {
                  return (
                    <p
                      className="text-[14px] underline cursor-pointer hover:underline-offset-4"
                      key={question?.question + idx}
                      onClick={() => {
                        setselectedQuestion(question);
                        setstep((prev) => prev + 1);
                      }}
                    >
                      {question?.question}
                    </p>
                  );
                })}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default SelectedCategorySection;
