import Image from "next/image";
import React from "react";
import { MdArrowBack } from "react-icons/md";
import { TiSocialLinkedinCircular } from "react-icons/ti";
import { AiFillTwitterCircle } from "react-icons/ai";
import { BiLogoGooglePlusCircle } from "react-icons/bi";
import { MdEmail } from "react-icons/md";

const QuestionDetail = ({ setstep, selectedQuestion }) => {
  return (
    <div>
      <div className="py-6">
        <div
          className="cursor-pointer"
          onClick={() => {
            setstep((prev) => prev - 1);
          }}
        >
          <MdArrowBack fontSize={22} />
        </div>
        <p className="text-black text-2xl py-2 font-semibold">
          {selectedQuestion?.question}
        </p>
        <p className="text-[14px] w-full ">
          {selectedQuestion?.shortDescription}
        </p>
      </div>
      <hr />
      <div className="flex justify-between items-center py-4 flex-wrap ">
        <div className="flex gap-4 items-center">
          <Image
            src={selectedQuestion?.user?.imagePath}
            width={50}
            height={50}
            className="rounded-full"
          />
          <div>
            <p className="text-[14px]">
              Written by{" "}
              <span className="font-semibold">
                {selectedQuestion?.user?.userName}
              </span>
            </p>
            <p className="text-[14px]">Updated over a week ago</p>
          </div>
        </div>
        <div className="flex gap-2 items-center justify-end">
          <p className="text-[14px]"> Share: </p>
          <MdEmail size={22} color={"#ACABAB"} className="cursor-pointer" />
          <AiFillTwitterCircle
            size={22}
            color="#1DA1F2"
            className="cursor-pointer"
          />
          <BiLogoGooglePlusCircle
            size={22}
            color={"#DB4A39"}
            className="cursor-pointer"
          />
          <TiSocialLinkedinCircular
            size={22}
            color={"#0072B1"}
            className="cursor-pointer"
          />
        </div>
      </div>
      <hr />
      <div className="py-4">
        <p className="text-[14px]">{selectedQuestion?.longDescription}</p>
      </div>
      <ul className="list-disc pl-4">
        {selectedQuestion?.points?.map((point, index) => {
          return (
            <li className=" w-full  rounded-lg  py-2" key={index}>
              <p className="font-semibold inline text-[14px]">
                {point?.title}:{" "}
              </p>
              <p className="inline text-justify text-[14px]">
                {" "}
                {point?.description}:
              </p>
            </li>
          );
        })}
      </ul>
      <hr />
      <div className="flex flex-col gap-6 py-6">
        <p className="text-[14px] font-semibold">Do You Still Need Help?</p>
        <div className="flex gap-2 items-center">
          <button className="py-3 bg-bgBlue text-white rounded-md w-44 text-[14px]">
            Contact Support
          </button>
          <button className="py-3 bg-[#DFEAEB] text-[#004551] rounded-md w-44 text-[14px]">
            No Thanks
          </button>
        </div>
      </div>
    </div>
  );
};

export default QuestionDetail;
