import React from "react";
import Image from "next/image";

const CategoriesSection = ({ Categories, setselectedCategory, setstep }) => {
  return (
    <div>
      <div className="py-6">
        <p className="text-black text-2xl py-2 font-semibold">
          Select a Category
        </p>
        <p className="text-[12px] w-full max-w-[500px]">
          Lorem ipsum dolor sit amet consectetur, adipisicing elit. Omnis
          tempore voluptate provident beatae laboriosam quaerat
        </p>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-4 ">
        {Categories?.map((category, index) => {
          return (
            <div
              className="bg-lightbg flex  rounded-lg px-4 py-4 gap-4 min-h-[150px] cursor-pointer"
              key={index}
              onClick={() => {
                setselectedCategory(category);
                setstep((prev) => prev + 1);
              }}
            >
              <div className="w-[100px] h-full flex items-center justify-center">
                <Image
                  src={category?.imagePath}
                  width={50}
                  height={50}
                  alt="user-icon"
                />
              </div>
              <div className="w-full flex items-start justify-center flex-col gap-2">
                <p className="font-semibold">{category?.title}</p>
                <p className="text-[14px]">{category?.description}</p>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default CategoriesSection;
