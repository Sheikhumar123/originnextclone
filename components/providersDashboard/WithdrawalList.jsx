"use client";

import { withdrawalData } from "@/data/dashboardData";
const WithdrawalList = () => {
  return (
    <div className="w-full p-4">
      <div className="relative overflow-auto scrollbar-thin max-h-[600px] rounded-xl w-full">
        <table className="w-full text-sm text-left rtl:text-right text-gray-500 ">
          <thead className="text-xs text-black uppercase bg-gray-200  ">
            <tr>
              <th scope="col" className="px-6 py-3">
                Sr
              </th>
              <th scope="col" className="px-6 py-3">
                Date
              </th>
              <th scope="col" className="px-6 py-3">
                Type
              </th>
              <th scope="col" className="px-6 py-3">
                Amount
              </th>
              <th scope="col" className="px-6 py-3">
                Tax
              </th>
              <th scope="col" className="px-6 py-3">
                Mode
              </th>

              <th scope="col" className="px-6 py-3">
                Status
              </th>
            </tr>
          </thead>
          <tbody>
            {withdrawalData.map((item, index) => (
              <tr className="bg-white text-black text-nowrap " key={index}>
                <td className="px-6 py-4">{item.sr}</td>
                <td className="px-6 py-4">{item.date}</td>
                <td className="px-6 py-4">{item.type}</td>
                <td className="px-6 py-4">{item.amount}</td>
                <td className="px-6 py-4">{item.tax}</td>
                <td className="px-6 py-4">{item.mode}</td>
                <td className="px-3 py-2 ">
                  <span
                    className={`px-3 py-2 font-[500]  rounded-md ${
                      item.status === "Received"
                        ? "text-black bg-[#E0FFE9]"
                        : "text-red-600 bg-[#F7B0B069]"
                    }`}
                  >
                    {item.status}
                  </span>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default WithdrawalList;
