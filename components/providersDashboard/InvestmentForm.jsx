"use client";

import { paymentTypeData } from "@/data/dashboardData";

const InvestmentForm = () => {
  return (
    <>
      <div className="mb-6">
        <h1 className="text-2xl font-semibold mb-3 text-gray-500">Amount:</h1>
        <input
          type="number"
          id="investmentAmount"
          className="w-full lg:w-[50%] h-[55px]  px-4 py-2 text-sm  border  focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600 bg-white rounded-3xl"
          placeholder="Enter Amount"
          name="productName"
        />
      </div>
      <div className="mb-4">
        <h1 className="text-2xl font-semibold mb-4 text-gray-600">Modes</h1>
        <div className="flex gap-2 flex-wrap w-full ">
          {paymentTypeData.map((item, key) => (
            <label
              key={key * 3}
              htmlFor={item.name}
              className="relative flex-1 cursor-pointer"
            >
              <input
                type="radio"
                className="peer hidden"
                id={item.name}
                name="paymentType"
                value={item?.name}
              />
              <div className="flex  items-center justify-center p-8 gap-5 h-28   bg-white border-2 border-gray-200 rounded-md transition peer-checked:border-blue-500  peer-checked:shadow-lg peer-checked:-translate-y-1 ">
                {item.name}
              </div>
            </label>
          ))}
          <button className="bg-[#434CD9] flex-1 h-16 self-center text-nowrap px-6 py-3 rounded-[999px] text-white">
            Invest Now
          </button>
        </div>
      </div>
    </>
  );
};

export default InvestmentForm;
