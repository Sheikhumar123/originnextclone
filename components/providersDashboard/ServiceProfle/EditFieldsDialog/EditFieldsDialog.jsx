"use client";
import AppDialog from "@/components/CustomComponents/AppDialog/AppDialog";
import { Form, Formik } from "formik";
import * as yup from "yup";
import { useEffect, useState } from "react";
import {
  GetProviderServiceProfileDetails,
  GetServiceProfileDetailData,
} from "@/redux/Slices/ProviderdashboardSlice/ServiceProfileSlice/ServiceProfileSlice";
import { useSelector, useDispatch } from "react-redux";
import { toast } from "react-toastify";
import { ServiceProfileEditApi } from "@/Apis/ServiceProviderApis/DashboardApi/ServiceProfileApis/ServiceProfileApis";
import Cookies from "js-cookie";
import { AllLanguage } from "@/components/Language/Language";
import { Autocomplete, Chip, CircularProgress, TextField } from "@mui/material";
import dynamic from "next/dynamic";

const Editor = dynamic(
  () => {
    return import("react-draft-wysiwyg").then((mod) => mod.Editor);
  },
  { ssr: false }
);
// import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { ContentState, EditorState, Modifier, convertToRaw } from "draft-js";
const validationSchema = yup.object().shape({
  title: yup.string().when("editFor", {
    is: (value) => value == "Title",
    then: () => yup.string().required("Field Required!"),
    otherwise: () => yup.string(),
  }),
  username: yup.string().when("editFor", {
    is: (value) => value == "User Name",
    then: () => yup.string().required("Field Required!"),
    otherwise: () => yup.string(),
  }),
  starting_price: yup.string().when("editFor", {
    is: (value) => value == "Information",
    then: () => yup.string().required("Field Required!"),
    otherwise: () => yup.string(),
  }),
  professional_type: yup.string().when("editFor", {
    is: (value) => value == "Information",
    then: () => yup.string().required("Field Required!"),
    otherwise: () => yup.string(),
  }),
  language: yup.string().when("editFor", {
    is: (value) => value == "Information",
    then: () =>
      yup
        .array()
        .min(1, "Please Select Atleast one Language")
        .required("Field Required!"),
    otherwise: () => yup.array(),
  }),
  description: yup.string().when("editFor", {
    is: (value) => value == "Description",
    then: () =>
      yup
        .mixed()
        .test(
          "is-string-or-editorstate",
          "Description is required",
          (value) => {
            return typeof value === "string" || value instanceof EditorState;
          }
        )
        .required("Description is required"),
    otherwise: () => yup.string(),
  }),
});

function EditFieldsDialog({ open, onClose, editDialogFor }) {
  const [loader, setLoader] = useState(false);
  const [DescriptionData, setDescriptionData] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  const dispatch = useDispatch();
  const { ServiceProfileDetailData } = useSelector(
    GetProviderServiceProfileDetails
  );

  const handleSubmi = async (values, { resetForm }) => {

    setLoader(true);
    const formData = new FormData();
    formData.append(`user_id`, Cookies.get("user_id"));

    if (editDialogFor == "Title") {
      formData.append(`title`, values.title);
    }

    if (editDialogFor == "User Name") {
      formData.append(`username`, values.username);
    }

    if (editDialogFor == "Information") {
      formData.append(`starting_price`, parseFloat(values.starting_price));
      formData.append(`professional_type`, values.professional_type);
      formData.append(`language`, JSON.stringify(values?.language));
      // values?.language?.forEach((element) => {
      // });
    }

    if (editDialogFor == "Description") {
    const { convertToHTML } = await import("draft-convert");

      formData.append(`description`, convertToHTML(values.description.getCurrentContent()));
    }

    const response = await ServiceProfileEditApi(formData);
    setLoader(false);

    if (response?.data?.status === "1") {
      toast.success(response.data.message);

      dispatch(GetServiceProfileDetailData(Cookies.get("user_id")));
      resetForm();
      onClose();
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };
  useEffect(() => {
    if (ServiceProfileDetailData?.description) {
      PrefillData();
    }
  }, [ServiceProfileDetailData?.description]);
  const PrefillData = async () => {
    const htmlToDraft = (await import("html-to-draftjs")).default;
    let body = htmlToDraft(
      ServiceProfileDetailData?.description
        ? ServiceProfileDetailData?.description
        : ""
    );
    console.log(body);
    if (body?.contentBlocks) {
      const contentState = ContentState.createFromBlockArray(
        body?.contentBlocks
      );
      console.log(contentState);
      const editorState = EditorState.createWithContent(contentState);
      console.log(editorState);
      setDescriptionData(editorState);
    }
  };
  console.log("ServiceProfileDetailData", ServiceProfileDetailData);
  // const handleOnChange = (editorState) => {
  //   let contentState = editorState.getCurrentContent();
  //   let newContentState = contentState;

  //   // Remove phone numbers and email addresses
  //   const phoneRegex =
  //     /\+\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}|\(\d{1,3}\)\s*\d{3,4}[-.\s]??\d{4}|\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}/g;
  //   const emailRegex = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/gi;

  //   let match;
  //   let containsInvalidContent = false;
  //   let containsInvalidContent2 = false;

  //   // Remove phone numbers
  //   contentState.getBlockMap().forEach((contentBlock) => {
  //     const blockText = contentBlock.getText();
  //     while ((match = phoneRegex.exec(blockText)) !== null) {
  //       const start = match.index;
  //       const end = start + match[0].length;
  //       newContentState = Modifier.replaceText(
  //         newContentState,
  //         editorState
  //           .getSelection()
  //           .merge({ anchorOffset: start, focusOffset: end }),
  //         ""
  //       );
  //       containsInvalidContent = true;
  //     }
  //   });

  //   // Remove email addresses
  //   contentState.getBlockMap().forEach((contentBlock) => {
  //     const blockText = contentBlock.getText();
  //     while ((match = emailRegex.exec(blockText)) !== null) {
  //       const start = match.index;
  //       const end = start + match[0].length;
  //       newContentState = Modifier.replaceText(
  //         newContentState,
  //         editorState
  //           .getSelection()
  //           .merge({ anchorOffset: start, focusOffset: end }),
  //         ""
  //       );
  //       containsInvalidContent2 = true;
  //     }
  //   });

  //   const newEditorState = EditorState.push(
  //     editorState,
  //     newContentState,
  //     "remove-range"
  //   );

  //   setFieldValue("description", newEditorState);

  //   if (containsInvalidContent) {
  //     setErrorMessage("Please remove phone numbers before entering text.");
  //   } else if (containsInvalidContent2) {
  //     setErrorMessage("Please remove email addresses before entering text.");
  //   } else {
  //     setErrorMessage("");
  //   }
  // };
  return (
    <AppDialog open={open} onClose={onClose} title={`Edit ${editDialogFor}`}>
      <Formik
        validateOnChange={true}
        initialValues={{
          editFor: editDialogFor,
          title: ServiceProfileDetailData?.title || "",
          username: ServiceProfileDetailData?.username || "",
          starting_price: ServiceProfileDetailData?.starting_price || "",
          professional_type: ServiceProfileDetailData?.professional_type || "",
          language: ServiceProfileDetailData?.language || [],
          description: DescriptionData || "",
        }}
        validationSchema={validationSchema}
        onSubmit={handleSubmi}
      >
        {({
          values,
          handleChange,
          handleBlur,
          setFieldValue,
          errors,
          touched,
        }) => {
          return (
            <Form>
              {editDialogFor === "Title" && (
                <div className="mb-2">
                  <label className="block text-sm text-label mb-2">Title</label>
                  <input
                    type="text"
                    placeholder="Title *"
                    className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md ${
                      touched?.title && errors?.title
                        ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                        : "border focus:border-blue-400 focus:ring-blue-600"
                    } focus:outline-none focus:ring-1 `}
                    name="title"
                    value={values.title}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />

                  {touched?.title && errors?.title && (
                    <span className="text-xs text-[#cc0000]">
                      {errors?.title}
                    </span>
                  )}
                </div>
              )}

              {editDialogFor === "User Name" && (
                <div className="mb-2">
                  <label className="block text-sm text-label mb-2">
                    User Name
                  </label>
                  <input
                    type="text"
                    placeholder="User Name *"
                    className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md ${
                      touched?.username && errors?.username
                        ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                        : "border focus:border-blue-400 focus:ring-blue-600"
                    } focus:outline-none focus:ring-1 `}
                    name="username"
                    value={values.username}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />

                  {touched?.username && errors?.username && (
                    <span className="text-xs text-[#cc0000]">
                      {errors?.username}
                    </span>
                  )}
                </div>
              )}

              {editDialogFor === "Information" && (
                <>
                  <div className="flex sm:flex-col md:flex-row flex-wrap  gap-3 mb-2">
                    <div className="flex-1">
                      <label className="block text-sm text-label mb-2">
                        Starting Price
                      </label>
                      <input
                        type="number"
                        placeholder="Starting Price *"
                        className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md ${
                          touched?.starting_price && errors?.starting_price
                            ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                            : "border focus:border-blue-400 focus:ring-blue-600"
                        } focus:outline-none focus:ring-1 `}
                        name="starting_price"
                        value={values.starting_price}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      />

                      {touched?.starting_price && errors?.starting_price && (
                        <span className="text-xs text-[#cc0000]">
                          {errors?.starting_price}
                        </span>
                      )}
                    </div>

                    <div className="flex-1">
                      <label className="block text-sm text-label mb-2">
                        Professional Type
                      </label>
                      <select
                        id="professionalType"
                        className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md ${
                          touched?.professional_type &&
                          errors?.professional_type
                            ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                            : "border focus:border-blue-400 focus:ring-blue-600"
                        } focus:outline-none focus:ring-1 `}
                        name="professional_type"
                        value={values.professional_type}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      >
                        <option value={""}>Select</option>
                        <option value="Dev">Dev</option>
                        <option value="Programmer">Programmer</option>
                      </select>
                      {/* <input
                        type="text"
                        placeholder="Professional Type *"
                        className={`w-full bg-white h-[55px] px-4 py-2 text-sm border rounded-md ${
                          touched?.professional_type &&
                          errors?.professional_type
                            ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                            : "border focus:border-blue-400 focus:ring-blue-600"
                        } focus:outline-none focus:ring-1 `}
                        name="professional_type"
                        value={values.professional_type}
                        onChange={handleChange}
                        onBlur={handleBlur}
                      /> */}

                      {touched?.professional_type &&
                        errors?.professional_type && (
                          <span className="text-xs text-[#cc0000]">
                            {errors?.professional_type}
                          </span>
                        )}
                    </div>
                  </div>

                  <div className="mb-2">
                    <label className="block text-sm text-label mb-2">
                      Language
                    </label>
                    <Autocomplete
                      multiple
                      limitTags={2}
                      size="small"
                      fullWidth
                      name="language"
                      autoComplete={false}
                      options={AllLanguage || []}
                      value={values.language}
                      onChange={(event, newValue) => {
                        setFieldValue("language", newValue);
                      }}
                      renderOption={(props, option) => (
                        <li {...props}>{option}</li>
                      )}
                      renderTags={(value, getTagProps) =>
                        value.map((option, index) => (
                          <Chip
                            key={option}
                            label={option}
                            {...getTagProps({ index })}
                            style={{ backgroundColor: "#c7c9f4" }}
                          />
                        ))
                      }
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          sx={{
                            "& .MuiOutlinedInput-root": {
                              // borderRadius: "8px",
                              minHeight: "53px",
                            },
                            color: "#9E9E9E",
                            width: "100%",
                          }}
                          onBlur={handleBlur}
                          error={errors?.language && touched?.language}
                        />
                      )}
                    />

                    {touched?.language && errors?.language && (
                      <span className="text-xs text-[#cc0000]">
                        {errors?.language}
                      </span>
                    )}
                  </div>
                </>
              )}

              {editDialogFor === "Description" && (
                <div className="mb-2">
                  <label className="block text-sm text-label mb-2">
                    Description
                  </label>
                  <Editor
                    placeholder="Enter description"
                    name="description"
                    editorState={values.description}
              onEditorStateChange={(editorState)=>{
                let contentState = editorState.getCurrentContent();
                let newContentState = contentState;
            
                // Remove phone numbers and email addresses
                const phoneRegex =
                  /\+\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}|\(\d{1,3}\)\s*\d{3,4}[-.\s]??\d{4}|\d{1,3}[-.\s]??\d{3,4}[-.\s]??\d{4}/g;
                const emailRegex = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}\b/gi;
            
                let match;
                let containsInvalidContent = false;
                let containsInvalidContent2 = false;
            
                // Remove phone numbers
                contentState.getBlockMap().forEach((contentBlock) => {
                  const blockText = contentBlock.getText();
                  while ((match = phoneRegex.exec(blockText)) !== null) {
                    const start = match.index;
                    const end = start + match[0].length;
                    newContentState = Modifier.replaceText(
                      newContentState,
                      editorState
                        .getSelection()
                        .merge({ anchorOffset: start, focusOffset: end }),
                      ""
                    );
                    containsInvalidContent = true;
                  }
                });
            
                // Remove email addresses
                contentState.getBlockMap().forEach((contentBlock) => {
                  const blockText = contentBlock.getText();
                  while ((match = emailRegex.exec(blockText)) !== null) {
                    const start = match.index;
                    const end = start + match[0].length;
                    newContentState = Modifier.replaceText(
                      newContentState,
                      editorState
                        .getSelection()
                        .merge({ anchorOffset: start, focusOffset: end }),
                      ""
                    );
                    containsInvalidContent2 = true;
                  }
                });
            
                const newEditorState = EditorState.push(
                  editorState,
                  newContentState,
                  "remove-range"
                );
            
                setFieldValue("description", newEditorState);
            
                if (containsInvalidContent) {
                  setErrorMessage("Please remove phone numbers before entering text.");
                } else if (containsInvalidContent2) {
                  setErrorMessage("Please remove email addresses before entering text.");
                } else {
                  setErrorMessage("");
                }
              }}

                    // value={feature.description}
                    
                    // onEditorStateChange={(editorState) => {
                    //   setFieldValue("description", editorState);
                    //   // onEditorStateChange(index, editorState)
                    // }}
                    editorStyle={{
                      backgroundColor: "#ffffff",
                      // height:"100px"
                    }}
                    className="w-full px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  />
                  {/* <textarea
                    className={`w-full bg-white px-4 py-2 text-sm border rounded-md ${
                      touched?.description && errors?.description
                        ? "border-[#cc0000] focus:border-[#cc0000] focus:ring-[#cc0000]"
                        : "border focus:border-blue-400 focus:ring-blue-600"
                    } focus:outline-none focus:ring-1 `}
                    rows={5}
                    name="description"
                    value={values.description}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    Description *
                  </textarea> */}
            {errorMessage && <div style={{ color: "red" }}>{errorMessage}</div>}

                  {touched?.description && errors?.description && (
                    <span className="text-xs text-[#cc0000]">
                      {errors?.description}
                    </span>
                  )}
                </div>
              )}

              <div className="flex justify-end mt-4">
                {loader ? (
                  <div className="flex justify-center">
                    <CircularProgress />
                  </div>
                ) : (
                  <button
                    className="px-4 py-2 text-sm font-medium leading-5 text-center text-white transition-colors duration-150 bg-blue-600 border border-transparent rounded-md active:bg-btnprimary hover:bg-bgBlue/95 focus:outline-none focus:shadow-outline-blue uppercase"
                    type="submit"
                  >
                    Update
                  </button>
                )}
              </div>
            </Form>
          );
        }}
      </Formik>
    </AppDialog>
  );
}

export default EditFieldsDialog;
