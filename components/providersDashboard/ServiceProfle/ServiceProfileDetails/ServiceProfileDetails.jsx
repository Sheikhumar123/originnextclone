import { manrope } from "@/utils/fontPoppins";
import { FiThumbsUp } from "react-icons/fi";
import { FaChevronDown, FaLinkedin } from "react-icons/fa";
import { SlFlag } from "react-icons/sl";
import React, { useEffect, useState } from "react";
import ratingImg from "../../../../images/ratingImg.png";
import Image from "next/image";
import ModalButton from "@/utils/ModalButton";
import { useDispatch, useSelector } from "react-redux";
import { Rating } from "@smastrom/react-rating";
import ServiceProfileReviewModal from "./ServiceProfileReviewModal/ServiceProfileReviewModal";
import {
  GetProviderServiceProfileDetails,
  GetServiceProfileDetailData,
  GetServiceProfileReviews,
} from "@/redux/Slices/ProviderdashboardSlice/ServiceProfileSlice/ServiceProfileSlice";
import Pagination from "@/components/Pagination/Pagination";
import uploadIcon from "../../../../images/uploadIcon.png";
import axios from "axios";
import { FiEdit } from "react-icons/fi";
import { useRouter } from "next/navigation";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import { toast } from "react-toastify";
import { ServiceProfileEditApi } from "@/Apis/ServiceProviderApis/DashboardApi/ServiceProfileApis/ServiceProfileApis";
import { CircularProgress } from "@mui/material";
import Cookies from "js-cookie";
import {
  GetProviderdashboardDetails,
  GetServiceProviderRating,
} from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import Link from "next/link";
// import { animateScroll as scroll } from "react-scroll";
import { Element, scroller } from "react-scroll";
const ServiceProfileDetails = ({
  moreServicesApiLimit,
  setMoreServicesApiLimit,
  faqsApiLimit,
  setFaqsApiLimit,
  page,
  setPage,
  setShowEditDialog,
  setEditDialogFor,
}) => {
  const [readmore, setReadmore] = useState(-1);
  const [isImgValid, setIsImgValid] = useState(false);
  const [loader, setLoader] = useState(false);
  const [loader2, setLoader2] = useState(false);
  const [bannerImage, setbannerImage] = useState(null);
  const [rating, setrating] = useState("");
  const [type, settype] = useState("");
  const dispatch = useDispatch();
  const router = useRouter();

  const {
    ServiceProfileDetailData,
    ServiceProfileFaqsData,
    ServiceProfileMoreServicesData,
    // ServiceProfileMoreReviewsData,
  } = useSelector(GetProviderServiceProfileDetails);
  const { ProvoderDashRating, status, error } = useSelector(
    GetProviderdashboardDetails
  );

  useEffect(() => {
    if (ServiceProfileDetailData) {
      checkIsImageURlValid(ServiceProfileDetailData?.display_image);
    }
  }, [ServiceProfileDetailData]);

  const checkIsImageURlValid = async (imagesurl) => {
    try {
      if (imagesurl != null || imagesurl != undefined || imagesurl != "") {
        let response = await axios.get(imagesurl);
        if (response) {
          setIsImgValid(true);
        }
      } else {
        setIsImgValid(false);
      }
    } catch (error) {
      setIsImgValid(false);
    }
  };
  const handleSubmi = async () => {
    setLoader(true);
    setLoader2(true);

    const formData = new FormData();
    formData.append(`user_id`, Cookies.get("user_id"));
    formData.append(`display_image`, bannerImage?.file);
    const response = await ServiceProfileEditApi(formData);
    setLoader(false);
    setLoader2(false);

    if (response?.data?.status === "1") {
      toast.success(response.data.message);
      setbannerImage(null);
      dispatch(GetServiceProfileDetailData(Cookies.get("user_id")));
    } else if (response?.data?.status === "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };
  const scrollToSection = () => {
    scroller.scrollTo("description", {
      duration: 1500,
      delay: 0,
      smooth: "easeInOutQuart",
    });
  };
  return (
    <div>
      {loader && <ScreenLoader />}
      <div className={`w-full ${manrope.className}`}>
        <div className="flex items-center gap-5">
          <h1 className=" text-[1.8rem] 400px:text-[2.5rem] font-semibold">
            {ServiceProfileDetailData?.title || ""}
          </h1>

          <FiEdit
            title="Edit Title"
            className="cursor-pointer hover:opacity-70 transition-opacity"
            size={18}
            onClick={() => {
              setEditDialogFor("Title");
              setShowEditDialog(true);
            }}
          />
        </div>

        <div className="flex items-center gap-5">
          <h1 className="text-[1.5rem] 400px:text-[2.2rem] font=[200] text-[#757575]">
            {ServiceProfileDetailData?.username || ""}
          </h1>

          <FiEdit
            title="Edit User Name"
            className="cursor-pointer hover:opacity-70 transition-opacity"
            size={18}
            onClick={() => {
              setEditDialogFor("User Name");
              setShowEditDialog(true);
            }}
          />
        </div>

        <div className="flex items-center gap-4 p-3 w-full flex-wrap">
          <div className="flex items-center gap-2">
            <Rating
              style={{ maxWidth: 150 }}
              isDisabled={true}
              value={ServiceProfileDetailData?.avgrating || 0}
            />
            <h3 className="text-lg text-black font-semibold">
              {ServiceProfileDetailData?.avgrating || 0}
            </h3>
          </div>
          <span className="p-3  text-black font-semibold">
            [{ServiceProfileDetailData?.reviews || ""}]
          </span>
          <button
            className="px-4 py-2 border border-blue-500 text-blue-500 rounded-3xl font-semibold"
            // onClick={() => document.getElementById(`${openId}`).showModal()}
          >
            Rate Us Now
          </button>
          {/* <ModalButton
            classNameString={
              "px-4 py-2 border border-blue-500 text-blue-500 rounded-3xl font-semibold"
            }
            text={"Rate Us Now"}
            // openId={"reviewModal"}
          /> */}
        </div>

        <div className="flex items-center gap-4 p-3 w-full flex-wrap">
          <button className="px-20 py-2 bg-bgBlue text-white rounded-3xl font-semibold">
            Follow
          </button>
          <span className="p-3  font-bold">102 Followers</span>
          <span className="p-3 font-bold flex items-center gap-1">
            <div className="p-2 bg-bgBlue rounded-full">
              <FiThumbsUp className="text-white" size={25} />{" "}
            </div>
            <span>102 Recomendations</span>
          </span>
        </div>

        <div>
          <div className="flex justify-end mb-1 w-full max-w-[800px]">
            <FiEdit
              title="Edit Information"
              className="cursor-pointer hover:opacity-70 transition-opacity"
              size={18}
              onClick={() => {
                setEditDialogFor("Information");
                setShowEditDialog(true);
              }}
            />
          </div>
          <div className="flex gap-2 justify-between flex-wrap w-full max-w-[800px] items-center mt-1">
            <div className="flex flex-col gap-2 ">
              <div className="flex 500px:gap-12 300px:gap-3 ">
                <p className="w-[150px] text-[#5c5c5c] font-[500]">
                  Starting Price
                </p>
                <p className="font-bold">
                  {ServiceProfileDetailData?.starting_price || ""}
                </p>
              </div>
              <div className="flex 500px:gap-12 300px:gap-3">
                <p className="w-[150px] text-[#5c5c5c] font-[500]">Type</p>
                <p className="font-bold">
                  {ServiceProfileDetailData?.professional_type}
                </p>
              </div>
            </div>
            <div className="flex flex-col gap-2 ">
              <div className="flex 500px:gap-12 300px:gap-3">
                <p className="w-[150px] text-[#5c5c5c] font-[500]">
                  In services
                </p>
                <p className="font-bold">
                  {ServiceProfileDetailData?.in_expereince} years
                </p>
              </div>
              <div className="flex 500px:gap-12 300px:gap-3 ">
                <p className="w-[150px] text-[#5c5c5c] font-[500]">
                  Response time
                </p>
                <p className="font-bold">40 mints</p>
              </div>
            </div>
          </div>
          <div className="flex flex-col gap-2 mt-2 mb-2">
            <div className="flex 500px:gap-12 300px:gap-3 ">
              <p className="w-[150px] text-[#5c5c5c] font-[500]">
                Languages I know
              </p>
              <p className="font-bold">
                {ServiceProfileDetailData?.language?.join(", ") || ""}
              </p>
            </div>
          </div>
          {/* <div className="flex w-full bg-[red] max-w-[800px] justify-between gap-2 items-center ">
            <div className="flex gap-12 bg-[yellow]">
              <p className="w-[150px]">Starting Price</p>
              <p className="font-semibold">
                {ServiceProfileDetailData?.starting_price || ""}
              </p>
            </div>
            <div className="flex gap-12 bg-[yellow]">
              <p className="w-[150px]">In services</p>
              <p className="font-semibold">
                {ServiceProfileDetailData?.in_expereince} years
              </p>
            </div>
          </div>
          <div className="flex bg-[red] w-full max-w-[800px] justify-between gap-2 items-center mt-3">
            <div className="flex gap-12 bg-[yellow]">
              <p className="w-[150px]">Type</p>
              <p className="font-semibold">
                {ServiceProfileDetailData?.professional_type || ""}
              </p>
            </div>
            <div className="flex gap-12 bg-[yellow]">
              <p className="w-[150px] ">Response time</p>
              <p className="font-semibold">40 mints</p>
            </div>
          </div>
          <div className="flex w-full max-w-[800px] justify-between gap-2 items-center mt-3">
            <div className="flex gap-12">
              <p className="w-[150px]">Languages I know</p>
              <p className="font-semibold">
                {" "}
                {ServiceProfileDetailData?.language?.join(", ") || ""}
              </p>
            </div>
          </div> */}
          {/* <div className="flex items-start 600px:items-center gap-8 flex-wrap w-full mb-4">
            <p className="flex-1 500px:text-nowrap">
              Starting Price:{" "}
              <span className="font-semibold">
                {ServiceProfileDetailData?.starting_price || ""}
              </span>
            </p>

            <p className="flex-1 500px:text-nowrap">
              In Services:{" "}
              <span className="font-semibold">
                {ServiceProfileDetailData?.in_expereince} years
              </span>
            </p>

            <p className="flex-1 500px:text-nowrap">
              Language&apos;s I Know:{" "}
              <span className="font-semibold">
                {ServiceProfileDetailData?.language?.join(", ") || ""}
              </span>
            </p>

            <p className="flex-1 500px:text-nowrap">
              Response Time: <span className="font-semibold">40 mints</span>
            </p>

            <p className="flex-1 500px:text-nowrap">
              Type:{" "}
              <span className="font-semibold">
                {ServiceProfileDetailData?.professional_type || ""}
              </span>
            </p>
          </div> */}
        </div>
        <div className="w-[fit-content] ">
          <div className="w-full flex justify-end py-2">
            <label htmlFor={`providerBannerImage`} className="cursor-pointer">
              <FiEdit
                title="Edit Banner"
                className="cursor-pointer hover:opacity-70 transition-opacity"
                size={18}
              />
              <input
                id={`providerBannerImage`}
                type="file"
                className="cursor-pointer w-[fit-content] mb-2 hidden "
                accept="image/*"
                name={`providerBannerImage`}
                onChange={(event) => {
                  const file = event.target.files[0];
                  const reader = new FileReader();
                  reader.onloadend = () => {
                    setbannerImage({
                      file: file,
                      imageUrl: reader.result,
                    });
                  };
                  if (file) {
                    reader.readAsDataURL(file);
                  }
                }}
              />
            </label>
          </div>
          {bannerImage? (
            // <label htmlFor={`providerBannerImage`} className="cursor-pointer">
            //   <input
            //     id={`providerBannerImage`}
            //     type="file"
            //     className="cursor-pointer w-[fit-content] mb-2 hidden "
            //     accept="image/*"
            //     name={`providerBannerImage`}
            //     onChange={(event) => {
            //       const file = event.target.files[0];
            //       const reader = new FileReader();
            //       reader.onloadend = () => {
            //         setbannerImage({
            //           file: file,
            //           imageUrl: reader.result,
            //         });
            //       };
            //       if (file) {
            //         reader.readAsDataURL(file);
            //       }
            //     }}
            //   />
            <div className="w-[fit-content] ">
            <Image
              src={bannerImage?.imageUrl ? bannerImage?.imageUrl : uploadIcon}
              alt="service img"
              width={500}
              height={500}
              style={{
                width: "100%",
                maxWidth: "500px",
                maxHeight: "500px",
              }}
            />
          </div>
 
            // </label>
          ) : (
            <div className="w-[fit-content] ">
            <Image
              src={
                isImgValid
                  ? ServiceProfileDetailData?.display_image
                  : bannerImage?.imageUrl
                  ? bannerImage?.imageUrl
                  : uploadIcon
              }
              alt="service img"
              width={500}
              height={500}
              style={{
                width: "100%",
                maxWidth: "500px",
                maxHeight: "500px",
              }}
            />
          </div>
            // <div className="w-[fit-content] ">
            //   <Image
            //     src={
            //       bannerImage?.imageUrl ? bannerImage?.imageUrl : uploadIcon
            //     }
            //     alt="service img"
            //     width={500}
            //     height={500}
            //     style={{
            //       width: "100%",
            //       maxWidth: "500px",
            //       maxHeight: "500px",
            //     }}
            //   />
            // </div>

            // <div className="w-[fit-content] ">
            //   <Image
            //     src={bannerImage?.imageUrl ? bannerImage?.imageUrl : uploadIcon}
            //     alt="service img"
            //     width={500}
            //     height={500}
            //     style={{
            //       width: "100%",
            //       maxWidth: "500px",
            //       maxHeight: "500px",
            //     }}
            //   />
            // </div>
            // <label htmlFor={`providerBannerImage`} className="cursor-pointer">
            //   <input
            //     id={`providerBannerImage`}
            //     type="file"
            //     className="cursor-pointer w-[fit-content] mb-2 hidden "
            //     accept="image/*"
            //     name={`providerBannerImage`}
            //     onChange={(event) => {
            //       const file = event.target.files[0];
            //       const reader = new FileReader();
            //       reader.onloadend = () => {
            //         setbannerImage({
            //           file: file,
            //           imageUrl: reader.result,
            //         });
            //       };
            //       if (file) {
            //         reader.readAsDataURL(file);
            //       }
            //     }}
            //   />
            //   <div className="w-[fit-content] ">
            //     <Image
            //       src={
            //         isImgValid
            //           ? ServiceProfileDetailData?.display_image
            //           : bannerImage?.imageUrl
            //           ? bannerImage?.imageUrl
            //           : uploadIcon
            //       }
            //       alt="service img"
            //       width={500}
            //       height={500}
            //       style={{
            //         width: "100%",
            //         maxWidth: "500px",
            //         maxHeight: "500px",
            //       }}
            //     />
            //   </div>
            // </label>
          )}
          <div>
            {bannerImage ? (
              <div className="flex justify-end items-center">
                <div className="flex gap-2 items-center">
                  <button
                    type="button"
                    className="px-6 py-2 rounded-md bg-red-600 text-white self-end mt-4"
                    onClick={() => {
                      setbannerImage(null);
                    }}
                  >
                    Cancel
                  </button>
                  {loader2 ? (
                    <div className="self-end">
                      <CircularProgress />
                    </div>
                  ) : (
                    <button
                      type="button"
                      onClick={handleSubmi}
                      className="px-8 py-2 rounded-md bg-blue-500 text-white self-end mt-4"
                      // onClick={handleSubmit}
                    >
                      Save
                    </button>
                  )}
                </div>
              </div>
            ) : null}
          </div>
        </div>
        <div className="flex items-center gap-4 flex-wrap mt-4">
          {ServiceProfileDetailData?.description && (
            <button
              onClick={scrollToSection}
              // onClick={() => scroll.scrollTo(400)}
              className="btn flex-grow bg-bgBlue text-white"
            >
              Description
            </button>
          )}

          {ServiceProfileMoreServicesData?.provider_services?.length && (
            <button
              onClick={() => {
                scroller.scrollTo("services", {
                  duration: 1500,
                  delay: 0,
                  smooth: "easeInOutQuart",
                });
              }}
              className="btn flex-grow bg-bgBlue text-white"
            >
              Services
            </button>
          )}

          {ServiceProfileFaqsData?.FAQsdata?.length && (
            <button
              onClick={() => {
                scroller.scrollTo("faq", {
                  duration: 1500,
                  delay: 0,
                  smooth: "easeInOutQuart",
                });
              }}
              className="btn flex-grow bg-bgBlue text-white"
            >
              Q/A
            </button>
          )}

          {ProvoderDashRating?.data?.length && (
            <button
              onClick={() => {
                scroller.scrollTo("review", {
                  duration: 1500,
                  delay: 0,
                  smooth: "easeInOutQuart",
                });
              }}
              className="btn flex-grow bg-bgBlue text-white"
            >
              Reviews
            </button>
          )}
        </div>
        <Element name="description" className="element">
          {ServiceProfileDetailData?.description && (
            <div className="mt-4">
              <div className="flex items-center gap-5">
                <h1 className="text-[1.8rem] font-semibold mb-2">
                  Description
                </h1>

                <FiEdit
                  title="Edit Description"
                  className="cursor-pointer hover:opacity-70 transition-opacity"
                  size={18}
                  onClick={() => {
                    setEditDialogFor("Description");
                    setShowEditDialog(true);
                  }}
                />
              </div>
              <p
                dangerouslySetInnerHTML={{
                  __html: ServiceProfileDetailData?.description || "",
                }}
                className="w-[90%] text-[#636363]"
              >
                {/* {ServiceProfileDetailData?.description || ""} */}
              </p>
            </div>
          )}
        </Element>
        <Element name="services" className="element">
          {ServiceProfileMoreServicesData?.provider_services?.length ? (
            <div className="mt-4">
              <div className="flex items-center gap-5">
                <h1 className="text-[1.8rem] font-semibold mb-2">Services</h1>

                <FiEdit
                  title="Edit Services"
                  className="cursor-pointer hover:opacity-70 transition-opacity"
                  size={18}
                  onClick={() => {
                    router.push(`/providersdashboard/services`);
                  }}
                />
              </div>
              <div className="p-4 flex flex-col gap-8">
                {ServiceProfileMoreServicesData?.provider_services?.map(
                  (item, key) => (
                    <div
                      className="w-full flex items-start gap-4 p-8 shadow-md bg-white rounded-md flex-wrap 800px:flex-nowrap"
                      key={key + 98 * 2}
                    >
                      <Image
                        src={item?.image_url}
                        alt="img service"
                        width={100}
                        height={100}
                      />
                      <div>
                        <h1 className="text-xl font-semibold">{item?.title}</h1>
                        <p
                          className={`text-md font-[500] text-[#636363] ${
                            readmore == key ? "" : "line-clamp-3"
                          } leading-12 ${manrope.className}`}
                          dangerouslySetInnerHTML={{
                            __html: item?.description || "",
                          }}
                        >
                          {/* {item?.description} */}
                        </p>
                        {item?.description?.length > 200 && readmore != key && (
                          <div>
                            <span
                              className="text-blue-600 hover:underline hover:text-blue-700 cursor-pointer"
                              onClick={() => setReadmore(key)}
                            >
                              {" "}
                              Read More
                            </span>
                          </div>
                        )}
                        {readmore == key && (
                          <div>
                            <span
                              className="text-blue-600 hover:underline hover:text-blue-700 cursor-pointer"
                              onClick={() => setReadmore(-1)}
                            >
                              {" "}
                              Read less
                            </span>
                          </div>
                        )}
                        <span className=" text-green-600 font-semibold">
                          Starting From:{" "}
                        </span>
                        <span className="font-semibold">
                          ${item?.starting_price} USD
                        </span>
                      </div>
                    </div>
                  )
                )}
              </div>

              {ServiceProfileMoreServicesData?.provider_services?.length <
                ServiceProfileMoreServicesData?.totalCount && (
                <div className="mt-2 flex justify-center">
                  <button
                    className="px-3 py-1 border border-blue-500  text-blue-500 rounded-xl font-semibold hover:opacity-80  transition-opacity"
                    onClick={() => {
                      let newLimit =
                        moreServicesApiLimit + 3 >
                        ServiceProfileMoreServicesData?.totalCount
                          ? ServiceProfileMoreServicesData?.totalCount
                          : moreServicesApiLimit + 3;
                      setMoreServicesApiLimit(newLimit);
                    }}
                  >
                    Load More
                  </button>
                </div>
              )}
            </div>
          ) : null}
        </Element>
        <Element name="faq" className="element">
          {ServiceProfileFaqsData?.FAQsdata?.length ? (
            <div className="mt-4 mb-4">
              <div className="flex items-center gap-5">
                <h1 className="text-[1.8rem] font-semibold mb-2">
                  Frequently Asked Questions
                </h1>

                <FiEdit
                  title="Edit FAQS"
                  className="cursor-pointer hover:opacity-70 transition-opacity"
                  size={18}
                  onClick={() => {
                    router.push(`/providersdashboard/faq`);
                  }}
                />
              </div>
              <p className="text-[#767676] mb-2">
                Diam Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                Vestibulum dui nisi, porttior sit amet diam ut, congue fermentum
                dui. Aenean gravida p
              </p>
              {ServiceProfileFaqsData?.FAQsdata?.map((item, index) => (
                <div
                  key={index + 1 * 7}
                  className="collapse collapse-plus bg-[#CCCCCC] text-black border mb-2 rounded-md"
                >
                  <input type="radio" name="my-accordion-3" />
                  <div className="collapse-title text-lg font-medium">
                    {item?.question}
                  </div>
                  <div className="collapse-content bg-gray-100 text-black">
                    <p>{item?.answer}</p>
                  </div>
                </div>
              ))}

              {ServiceProfileFaqsData?.FAQsdata?.length <
                ServiceProfileFaqsData?.totalCount && (
                <div className="mt-2 flex justify-center">
                  <button
                    className="px-3 py-1 border border-blue-500  text-blue-500 rounded-xl font-semibold hover:opacity-80  transition-opacity"
                    onClick={() => {
                      let newLimit =
                        faqsApiLimit + 3 > ServiceProfileFaqsData?.totalCount
                          ? ServiceProfileFaqsData?.totalCount
                          : moreServicesApiLimit + 3;
                      setFaqsApiLimit(newLimit);
                    }}
                  >
                    Load More
                  </button>
                </div>
              )}
            </div>
          ) : null}
        </Element>
        <Element name="review" className="element">
          <div className="flex items-center justify-between gap-4  mt-8 flex-wrap">
            <h1 className="text-3xl font-semibold">
              {ProvoderDashRating?.count} Reviews
            </h1>
            <div className="flex items-center gap-4 flex-wrap">
              {/* <div className="dropdown dropdown-end   ">
              <div
                tabIndex={0}
                role="button"
                className="btn m-1 bg-white border border-gray-400"
              >
                Most Recent <FaChevronDown size={20} />
              </div>
              <ul
                tabIndex={0}
                className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
              >
                <li>
                  <a>Item 1</a>
                </li>
                <li>
                  <a>Item 2</a>
                </li>
              </ul>
            </div> */}
              <div>
                <select
                  id="professionalType"
                  className="bg-gray-50 border border-gray-300 text-sm rounded-md focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[150px] font-semibold"
                  name="professional_type"
                  value={type}
                  onChange={(e) => {
                    dispatch(
                      GetServiceProviderRating({
                        user_id: Cookies.get("user_id"),
                        page: 1,
                        type: e.target.value,
                        rating: rating ? rating : "",
                      })
                    );
                    settype(e.target.value);
                  }}
                >
                  <option value={""}>Type</option>
                  <option value={"Most Recent"}>Most Recent</option>
                  <option value="Today">Today</option>
                  <option value="Last Month">Last Month</option>
                </select>
              </div>
              {/* <div className="dropdown dropdown-end     ">
              <div
                tabIndex={0}
                role="button"
                className="btn m-1 bg-white border border-gray-400"
              >
                Any Rating <FaChevronDown size={20} />
              </div>
              <ul
                tabIndex={0}
                className="dropdown-content z-[1] menu p-2 shadow bg-base-100 rounded-box w-52"
              >
                <li>
                  <a>Item 1</a>
                </li>
                <li>
                  <a>Item 2</a>
                </li>
              </ul>
            </div> */}
              <div>
                <select
                  id="professionalType"
                  className="bg-gray-50 border border-gray-300 text-sm rounded-md focus:ring-gray-200 focus:border-gray-200 block w-full p-2.5 max-w-[150px] font-semibold"
                  name="professional_type"
                  value={rating}
                  onChange={(e) => {
                    setrating(e.target.value);
                    dispatch(
                      GetServiceProviderRating({
                        user_id: Cookies.get("user_id"),
                        page: 1,
                        type: type ? type : "",
                        rating: Number(e.target.value),
                      })
                    );
                  }}
                >
                  <option value={""}>Any Rating</option>
                  <option value="1">1 Star</option>
                  <option value="2">2 Star</option>
                  <option value="3">3 Star</option>
                  <option value="4">4 Star</option>
                  <option value="5">5 Star</option>
                </select>
              </div>
              <Link href={"/providersdashboard/rating"}>
                <button className="bg-[#434CD9] px-12 py-3 rounded-3xl text-white">
                  Reply to reviews
                </button>
              </Link>
            </div>
          </div>
        </Element>
        {ProvoderDashRating?.data?.length ? (
          <div className="w-full flex flex-col gap-3">
            {ProvoderDashRating?.data?.map((item, key) => (
              <div key={key + 978 * 2} className="p-6 rounded-md  shadow-xl">
                <div className="flex items-center justify-between">
                  <div className="mb-2 flex items-center gap-2 w-auto">
                    <div className=" w-[45px] h-[45px] rounded-full overflow-hidden">
                      <Image
                        src={item?.image}
                        style={{ width: "100%", height: "100%" }}
                        alt="profile img"
                        className=" object-cover "
                        width={500}
                        height={500}
                      />
                    </div>
                    <div className="flex flex-col ">
                      <h1 className="font-semibold">{item.review_from}</h1>
                      <p className="text-sm">{item?.title}</p>
                    </div>
                  </div>
                  <div className="flex items-center gap-2">
                    <FaLinkedin size={25} fill="#0E76A8" />{" "}
                    <h1 className="font-semibold">Verified</h1>
                  </div>
                </div>
                <Rating
                  style={{ maxWidth: 150 }}
                  readOnly
                  orientation="horizontal"
                  value={Number(item?.rating)}
                />

                {/* <Image src={ratingImg} alt="ratingImg" width={18} height={18} /> */}
                <p className="mt-2 text-[#676767] mb-2">{item?.review}</p>
                <hr />
                <div className="flex items-center justify-between gap-2 600px:gap-0 mt-4 flex-wrap 600px:flex-nowrap text-nowrap">
                  <div className="flex items-center gap-2 500px:gap-6 font-[600] flex-wrap 600px:flex-nowrap ">
                    <h1>Helpful ({item?.rating})</h1>
                    <h1>Share</h1>
                  </div>
                  <div className="flex items-center gap-2">
                    <SlFlag fill="#f21a1a" size={20} />
                    <h1 className="text-[#f21a1a] font-semibold">Report</h1>
                  </div>
                </div>
              </div>
            ))}

            <div className="pt-4 pb-[130px] flex justify-end px-2">
              <Pagination
                pageCount={page}
                setPageCount={setPage}
                limit={ProvoderDashRating?.limit}
                totalcount={ProvoderDashRating?.count}
                showCount={true}
              />
            </div>
          </div>
        ) : null}

        <ServiceProfileReviewModal />
      </div>
    </div>
  );
};

export default ServiceProfileDetails;
