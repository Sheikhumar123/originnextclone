"use client";
import React, { useEffect, useState } from "react";
import { manrope } from "@/utils/fontPoppins";
import Cookies from "js-cookie";
import { useDispatch } from "react-redux";
import {
  GetServiceProfileDetailData,
  GetServiceProfileFaqs,
  GetServiceProfileMoreServices,
  GetServiceProfileReviews,
} from "@/redux/Slices/ProviderdashboardSlice/ServiceProfileSlice/ServiceProfileSlice";
import ServiceProfileDetails from "./ServiceProfileDetails/ServiceProfileDetails";
import ServiceProfileSellerDetails from "./ServiceProfileSellerDetails/ServiceProfileSellerDetails";
import EditFieldsDialog from "./EditFieldsDialog/EditFieldsDialog";
import { GetServiceProviderRating } from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";

function ServiceProfle() {
  const [moreServicesApiLimit, setMoreServicesApiLimit] = useState(3);
  const [faqsApiLimit, setFaqsApiLimit] = useState(4);
  const [page, setPage] = useState(1);
  const [showEditDialog, setShowEditDialog] = useState(false);
  const [editDialogFor, setEditDialogFor] = useState("");

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(GetServiceProfileDetailData(Cookies.get("user_id")));
  }, [dispatch]);

  useEffect(() => {
    callMoreServicesApi();
  }, [moreServicesApiLimit]);

  useEffect(() => {
    callFaqsApi();
  }, [faqsApiLimit]);

  useEffect(() => {
    dispatch(
      GetServiceProviderRating({
        user_id: Cookies.get("user_id"),
        page: page,
      })
    );
    // dispatch(
    //   GetServiceProfileReviews({
    //     user_id: Cookies.get("user_id"),
    //     page: page,
    //   })
    // );
  }, [page]);

  const callMoreServicesApi = () => {
    dispatch(
      GetServiceProfileMoreServices({
        user_id: Cookies.get("user_id"),
        limit: moreServicesApiLimit,
      })
    );
  };

  const callFaqsApi = () => {
    dispatch(
      GetServiceProfileFaqs({
        user_id: Cookies.get("user_id"),
        limit: faqsApiLimit,
      })
    );
  };

  return (
    <>
      <div className="w-full">
        <div className={`w-full  mb-4 ${manrope.className}`}>
          <div className="w-full flex flex-col 1100px:flex-row gap-2 p-8">
            <div className="w-full 1100px:w-[70%]">
              <ServiceProfileDetails
                moreServicesApiLimit={moreServicesApiLimit}
                setMoreServicesApiLimit={setMoreServicesApiLimit}
                faqsApiLimit={faqsApiLimit}
                setFaqsApiLimit={setFaqsApiLimit}
                page={page}
                setPage={setPage}
                setShowEditDialog={setShowEditDialog}
                setEditDialogFor={setEditDialogFor}
              />
            </div>
            <div className="flex flex-col gap-4 items-start w-full 1100px:w-[30%]">
              <div className="1100px:hidden mt-4 p-2">
                <h1 className="text-xl font-semibold">Seller Details</h1>
              </div>
              <ServiceProfileSellerDetails />
            </div>
          </div>
        </div>
        <div className="hidden bg-recommendedBlue bg-recommendedRed"></div>
      </div>

      <EditFieldsDialog
        open={showEditDialog}
        onClose={() => {
          setEditDialogFor("");
          setShowEditDialog(false);
        }}
        editDialogFor={editDialogFor}
      />
    </>
  );
}

export default ServiceProfle;
