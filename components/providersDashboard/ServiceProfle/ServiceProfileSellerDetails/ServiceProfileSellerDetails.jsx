import Image from "next/image";
import { CiShare2, CiHeart, CiClock1 } from "react-icons/ci";
import { MdOutlineVerifiedUser, MdOutlineCancel } from "react-icons/md";
import { VscVerifiedFilled } from "react-icons/vsc";
import { TiTick } from "react-icons/ti";
import { useDispatch, useSelector } from "react-redux";
import {
  GetProviderServiceProfileDetails,
  GetServiceProfileDetailData,
} from "@/redux/Slices/ProviderdashboardSlice/ServiceProfileSlice/ServiceProfileSlice";
import moment from "moment";
import { findFlagUrlByCountryName } from "country-flags-svg";
import { useEffect, useState } from "react";
import uploadIcon from "../../../../images/uploadIcon.png";
import axios from "axios";
import { CircularProgress } from "@mui/material";
import Cookies from "js-cookie";
import { ServiceProfileEditApi } from "@/Apis/ServiceProviderApis/DashboardApi/ServiceProfileApis/ServiceProfileApis";
import { toast } from "react-toastify";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import { FiEdit } from "react-icons/fi";
import ProviderCategoriesEditModal from "./ProviderCategoriesEditModal";

const ServiceProfileSellerDetails = () => {
  const [isImgValid, setIsImgValid] = useState(false);
  const [loader, setLoader] = useState(false);
  const [loader2, setLoader2] = useState(false);
  const [profileImage, setprofileImage] = useState(null);
  const { ServiceProfileDetailData } = useSelector(
    GetProviderServiceProfileDetails
  );
  const [showEditDialog, setShowEditDialog] = useState(false);

  const dispatch = useDispatch();
  const [allCategories, setallCategories] = useState([]);
  const [allSubCategories, setallSubCategories] = useState([]);
  console.log(allCategories);
  console.log(allSubCategories);
  console.log(ServiceProfileDetailData);
  useEffect(() => {
    if (ServiceProfileDetailData) {
      let arr = [];
      let arr2 = [];

      ServiceProfileDetailData?.categories?.map((item) => {
        arr.push({
          title: item?.category?.title,
          id: item?.category?.id,
        });
        arr2?.push(...item?.subcategory);
      });
      setallCategories(arr);
      setallSubCategories(arr2);
    }
  }, [ServiceProfileDetailData]);
  console.log(ServiceProfileDetailData);
  useEffect(() => {
    checkIsImageURlValid(ServiceProfileDetailData?.image_url);
  }, [ServiceProfileDetailData]);

  const checkIsImageURlValid = async (imagesurl) => {
    try {
      if (imagesurl != null || imagesurl != undefined || imagesurl != "") {
        let response = await axios.get(imagesurl);
        if (response) {
          setIsImgValid(true);
        }
      } else {
        setIsImgValid(false);
      }
    } catch (error) {
      setIsImgValid(false);
    }
  };
  const handleSubmi = async () => {
    setLoader(true);
    setLoader2(true);

    const formData = new FormData();
    formData.append(`user_id`, Cookies.get("user_id"));
    formData.append(`profile_image`, profileImage?.file);
    const response = await ServiceProfileEditApi(formData);
    setLoader(false);
    setLoader2(false);

    if (response?.data?.status === "1") {
      toast.success(response.data.message);
      setprofileImage(null);
      dispatch(GetServiceProfileDetailData(Cookies.get("user_id")));
    } else if (response?.data?.status === "0") {
      toast.error(response.data?.message);
    } else {
      toast.error(response.response?.data?.message);
    }
  };
  return (
    <>
      {loader && <ScreenLoader />}
      <div className="mb-2 flex flex-col gap-3">
        <div className="flex items-center justify-start gap-4 w-full">
          <div className="h-[40px] w-[40px] rounded-full flex items-center justify-center bg-blue-100">
            <CiShare2 size={25} />
          </div>
          <div className="h-[40px] w-[40px] rounded-full flex items-center justify-center bg-red-100">
            <CiHeart size={25} />
          </div>
        </div>

        <button className="p-3 w-full flex-grow gap-2 flex items-center justify-center bg-gradient-to-t from-[#C4FF78] to-[#FFE816] text-green-700 rounded-md font-semibold">
          <MdOutlineVerifiedUser size={25} />
          <p>Trusted</p>
        </button>
        <button className="p-3 w-full flex-grow gap-2 flex items-center justify-center bg-blue-500 text-white rounded-md ">
          <VscVerifiedFilled size={25} />
          <p>Verified</p>
        </button>

        {/* <Image
          src={isImgValid ? ServiceProfileDetailData?.image_url : uploadIcon}
          alt="service img"
          width={800}
          height={800}
          style={{ maxWidth: "100%", maxHeight: "100%", objectFit: "fill" }}
        /> */}
        <div className="w-[fit-content] ">
          <div className="w-full flex justify-end py-2">
            <label htmlFor={`providerProfImage`} className="cursor-pointer">
              <FiEdit
                title="Edit Image"
                className="cursor-pointer hover:opacity-70 transition-opacity"
                size={18}
              />
              <input
                id={`providerProfImage`}
                type="file"
                className="cursor-pointer w-[fit-content] mb-2 hidden "
                accept="image/*"
                name={`providerProfImage`}
                onChange={(event) => {
                  const file = event.target.files[0];
                  const reader = new FileReader();
                  reader.onloadend = () => {
                    setprofileImage({
                      file: file,
                      imageUrl: reader.result,
                    });
                  };
                  if (file) {
                    reader.readAsDataURL(file);
                  }
                }}
              />
            </label>
          </div>
          {profileImage ? (
            // <label htmlFor={`providerProfImage`} className="cursor-pointer">
            //   <input
            //     id={`providerProfImage`}
            //     type="file"
            //     className="cursor-pointer w-[fit-content] mb-2 hidden "
            //     accept="image/*"
            //     name={`providerProfImage`}
            //     onChange={(event) => {
            //       const file = event.target.files[0];
            //       const reader = new FileReader();
            //       reader.onloadend = () => {
            //         setprofileImage({
            //           file: file,
            //           imageUrl: reader.result,
            //         });
            //       };
            //       if (file) {
            //         reader.readAsDataURL(file);
            //       }
            //     }}
            //   />
            <div className="w-[fit-content] ">
              <Image
                src={
                  profileImage?.imageUrl ? profileImage?.imageUrl : uploadIcon
                }
                alt="service img"
                width={500}
                height={500}
                style={{
                  width: "100%",
                  maxWidth: "600px",
                  maxHeight: "400px",
                }}
              />
            </div>
          ) : (
            // </label>
            // <label htmlFor={`providerProfImage`} className="cursor-pointer">
            //   <input
            //     id={`providerProfImage`}
            //     type="file"
            //     className="cursor-pointer w-[fit-content] mb-2 hidden "
            //     accept="image/*"
            //     name={`providerProfImage`}
            //     onChange={(event) => {
            //       const file = event.target.files[0];
            //       const reader = new FileReader();
            //       reader.onloadend = () => {
            //         setprofileImage({
            //           file: file,
            //           imageUrl: reader.result,
            //         });
            //       };
            //       if (file) {
            //         reader.readAsDataURL(file);
            //       }
            //     }}
            //   />
            <div className="w-[fit-content] ">
              <Image
                src={
                  isImgValid
                    ? ServiceProfileDetailData?.image_url
                    : profileImage?.imageUrl
                    ? profileImage?.imageUrl
                    : uploadIcon
                }
                alt="service img"
                width={500}
                height={500}
                style={{
                  width: "100%",
                  maxWidth: "600px",
                  maxHeight: "400px",
                }}
              />
            </div>
            // </label>
          )}
          <div>
            {profileImage ? (
              <div className="flex justify-end items-center">
                <div className="flex gap-2 items-center">
                  <button
                    type="button"
                    className="px-6 py-2 rounded-md bg-red-600 text-white self-end mt-4"
                    onClick={() => {
                      setprofileImage(null);
                    }}
                  >
                    Cancel
                  </button>
                  {loader2 ? (
                    <div className="self-end">
                      <CircularProgress />
                    </div>
                  ) : (
                    <button
                      type="button"
                      onClick={handleSubmi}
                      className="px-8 py-2 rounded-md bg-blue-500 text-white self-end mt-4"
                      // onClick={handleSubmit}
                    >
                      Save
                    </button>
                  )}
                </div>
              </div>
            ) : null}
          </div>
        </div>
        {ServiceProfileDetailData?.country ? (
          <div className="flex items-center justify-start gap-2">
            <Image
              src={
                ServiceProfileDetailData?.country
                  ? findFlagUrlByCountryName(ServiceProfileDetailData?.country)
                  : ""
              }
              alt="flag img"
              width={40}
              height={40}
            />
            <p>{ServiceProfileDetailData?.country || ""}</p>{" "}
          </div>
        ) : null}
        <div className="flex items-center justify-start gap-2">
          <CiClock1 size={25} color="#ff0000" />
          <p className="text-sm text-[#e60000] font-semibold">
            {moment(ServiceProfileDetailData?.country?.time_zone).format(
              "h:mm a"
            )}{" "}
            local time{" "}
          </p>
        </div>
      </div>

      {/* <div className="p-2 w-full flex flex-col items-center justify-center border border-gray-200 rounded-2xl gap-2">
        <Link
          href={"/service-provider/contact/id"}
          className="p-3 w-[80%] flex-grow gap-2 flex items-center justify-center bg-black text-white rounded-3xl "
        >
          Contact Us <LuPhoneCall size={25} color="#fff" className="ml-2" />
        </Link>
        <Link
          href={"/service-provider/contact/id"}
          className="p-3 w-[80%] flex-grow gap-2 flex items-center justify-center rounded-3xl border border-gray-200 text-[#4E4E4E]"
        >
          Whatsapp Us{" "}
          <FaWhatsappSquare size={30} color="#29A71A" className="" />
        </Link>
        <Link
          href={"/service-provider/contact/id"}
          className="p-3 w-[80%] flex-grow gap-2 flex items-center justify-center rounded-3xl border border-gray-200 text-[#4E4E4E]"
        >
          Visit Website <CiShare1 />
        </Link>
      </div> */}

      <div className="p-4 w-full flex flex-col items-start  border border-gray-200 rounded-2xl gap-2">
        <h1 className="font-semibold text-lg">Verification&apos;s</h1>
        <div className="flex items-center gap-4 text-[#004551]">
          {ServiceProfileDetailData?.phone_verified ? (
            <TiTick size={20} fill="#008000" />
          ) : (
            <MdOutlineCancel size={20} color="red" />
          )}
          <p>Phone Verified</p>
        </div>

        <div className="flex items-center gap-4 text-[#004551]">
          {ServiceProfileDetailData?.fb_verified ? (
            <TiTick size={20} fill="#008000" />
          ) : (
            <MdOutlineCancel size={20} color="red" />
          )}
          <p>Facebook Verified</p>
        </div>

        <div className="flex items-center gap-4 text-[#004551]">
          {ServiceProfileDetailData?.linkedin_verified ? (
            <TiTick size={20} fill="#008000" />
          ) : (
            <MdOutlineCancel size={20} color="red" />
          )}
          <p>LinkedIn Verified</p>
        </div>
      </div>

      {/* {ServiceProfileDetailData?.categories?.length ? (
        <div className="p-4 w-full flex flex-col items-start  border border-gray-200 rounded-2xl gap-2">
          <h1 className="font-semibold text-lg">Categories</h1>
          <div className="flex gap-3 flex-wrap">
            {ServiceProfileDetailData?.categories?.map((item, key) => (
              <h1
                className="py-2 px-4 border-2 bg-[#CCE0FF] text-[#444BDB] text-sm rounded-2xl text-nowrap"
                key={key + 22 * 8}
              >
                {item.title}
              </h1>
            ))}
          </div>
        </div>
      ) : null} */}
      <div className="flex justify-end w-full">
        <FiEdit
          title="Edit Categories or Skills"
          className="cursor-pointer hover:opacity-70 transition-opacity"
          size={18}
          onClick={() => {
            setShowEditDialog(true);
          }}
        />
      </div>
      <div>
        {allCategories?.length ? (
          <div className="p-4 w-full flex flex-col items-start  border border-gray-200 rounded-2xl gap-2 mb-4">
            <h1 className="font-semibold text-lg">Categories</h1>
            <div className="flex gap-3 flex-wrap">
              {allCategories?.map((item, key) => (
                <h1
                  className="py-2 px-4 border-2 bg-[#CCE0FF] text-[#444BDB] text-sm rounded-2xl text-nowrap"
                  key={key + 22 * 8}
                >
                  {item?.title}
                </h1>
              ))}
            </div>
          </div>
        ) : null}
        {allSubCategories?.length ? (
          <div className="p-4 w-full flex flex-col items-start  border border-gray-200 rounded-2xl gap-2">
            <h1 className="font-semibold text-lg">Sub Categories</h1>
            <div className="flex gap-3 flex-wrap">
              {allSubCategories?.map((item, key) => (
                <h1
                  className="py-2 px-4 border-2 bg-[#CCE0FF] text-[#444BDB] text-sm rounded-2xl text-nowrap"
                  key={key + 22 * 8}
                >
                  {item?.title}
                </h1>
              ))}
            </div>
          </div>
        ) : null}
      </div>
      {ServiceProfileDetailData?.skills?.length ? (
        <div className="p-4 w-full flex flex-col items-start  border border-gray-200 rounded-2xl gap-2">
          <h1 className="font-semibold text-lg">Skills</h1>
          <div className="flex gap-3 flex-wrap">
            {ServiceProfileDetailData?.skills?.map((item, key) => (
              <h1
                className="py-2 px-4 border-2 bg-[#CCE0FF] text-[#444BDB] text-sm rounded-2xl text-nowrap"
                key={key + 22 * 8}
              >
                {item.title}
              </h1>
            ))}
          </div>
        </div>
      ) : null}
      <ProviderCategoriesEditModal
        open={showEditDialog}
        onClose={() => {
          setShowEditDialog(false);
        }}
        allCategories={allCategories}
        allSubCategories={allSubCategories}
        skills={ServiceProfileDetailData?.skills}
        showEditDialog={showEditDialog}
        setShowEditDialog={setShowEditDialog}
      />
    </>
  );
};

export default ServiceProfileSellerDetails;
