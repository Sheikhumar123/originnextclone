"use client";
import AppDialog from "@/components/CustomComponents/AppDialog/AppDialog";
import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Cookies from "js-cookie";
import { useFormik } from "formik";
import * as Yup from "yup";
import axiousInstance from "@/utils/axiousInstance";
import { toast } from "react-toastify";
import { Autocomplete, Chip, CircularProgress, TextField } from "@mui/material";
import {
  GetProviderCategories,
  GetProviderSkills,
  GetProviderSubCategories,
  GetServiceProviderDetails,
} from "@/redux/Slices/ServiceProviderSlice/ServiceProviderSlice";
import { ServiceProfileEditApi } from "@/Apis/ServiceProviderApis/DashboardApi/ServiceProfileApis/ServiceProfileApis";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import { GetServiceProfileDetailData } from "@/redux/Slices/ProviderdashboardSlice/ServiceProfileSlice/ServiceProfileSlice";
function ProviderCategoriesEditModal({
  open,
  onClose,
  skills,
  allSubCategories,
  allCategories,
  showEditDialog,
  setShowEditDialog,
}) {
  const [loader, setLoader] = useState(false);
  const [loader2, setLoader2] = useState(false);
  const { ProviderCategories, ProviderSubCategories, ProviderSkills } =
    useSelector(GetServiceProviderDetails);
  const dispatch = useDispatch();
  useEffect(() => {
    if(showEditDialog){
    dispatch(GetProviderCategories());
    dispatch(GetProviderSkills());
  }
  }, [dispatch,showEditDialog]);
  const validationSchema = Yup.object().shape({
    categories: Yup.array()
      .min(1, "At least 1 Category is required")
      .max(3, "Please Select maximum 3 Categories")
      .required("Please Select Categories"),
    subcategories: Yup.array()
      .min(1, "At least 1 Category is required")
      .max(3, "Please Select maximum 3 Sub Categories")
      .required("Please Select Sub Categories"),
    skills: Yup.array()
      .min(1, "At least 1 Skill is required")
      .max(3, "Please Select maximum 3 Skills")
      .required("Please Select Skills"),
  });
  const formik = useFormik({
    initialValues: {
      categories: [],
      subcategories: [],
      skills: [],
    },
    validationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  useEffect(() => {
    if (showEditDialog) {
      let arr = [];
      allSubCategories?.map((item) => {
        arr.push({
          ...item,
          _id: item?.id,
          title: item?.title,
        });
      });
      formik.setFieldValue("categories", allCategories);
      formik.setFieldValue("subcategories", arr);
      formik.setFieldValue("skills", skills);
    }
  }, [showEditDialog]);
  useEffect(() => {
    if (formik.values.categories?.length) {
      let arr = [];
      formik.values.categories?.map((item) => {
        arr.push(item?.id);
      });
      dispatch(GetProviderSubCategories(arr));
    }
  }, [formik.values.categories]);
  const handleSubmit = async (values,) => {
    console.log(values);
    let arr = [];
    let arr3 = [];

    values?.categories?.map((item) => {
      let arr2 = [];
      values?.subcategories?.map((item2) => {
        if (
          item?.id == item2?.categoryid 
        ) {
          arr2.push(item2?._id);
        }
      });
      arr.push({ category: item?.id, subcategory: arr2 });
    });
    values?.skills?.map((item) => {
      arr3.push(item?.id);
    });
    setLoader(true);
    setLoader2(true);
    const formData = new FormData();
    formData.append(`user_id`, Cookies.get("user_id"));
    formData.append(`categories`, JSON.stringify(arr));
    formData.append(`skills`, JSON.stringify(arr3));
    const response = await ServiceProfileEditApi(formData);
    setLoader(false);
    setLoader2(false);

    if (response?.data?.status === "1") {
      toast.success(response.data.message);
      dispatch(GetServiceProfileDetailData(Cookies.get("user_id")));
      setShowEditDialog(false);
    } else if (response?.data?.status === "0") {
      toast.error(response.data?.message);
    } else {
      toast.error(response.response?.data?.message);
    }
  };
  console.log(formik.values);
  return (
    <AppDialog
      open={open}
      onClose={onClose}
      title={`Edit Categories or Skills`}
    >
      {loader && <ScreenLoader />}
      <form onSubmit={formik.handleSubmit}>
        <div>
          <div className="flex flex-col gap-2 mb-2">
            <label htmlFor="category" className="text-md label">
              Select Category
            </label>
            <Autocomplete
              multiple
              limitTags={2}
              size="small"
              id="multiple-limit-tags-category"
              options={ProviderCategories || []}
              sx={{
                "& .MuiAutocomplete-inputRoot": {},
                width: "100%",
              }}
              name={`categories`}
              value={formik.values.categories || null}
              onChange={(event, newValue) => {
                formik.setFieldValue(`categories`, newValue);
  
                formik.setFieldValue(`subcategories`, []);
              }}
              getOptionLabel={(option) => option?.title}

              renderOption={(props, option) => (
                <li {...props}>{option?.title}</li>
              )}
              renderTags={(value, getTagProps) =>
                value?.map((option, index) => (
                  <Chip
                    key={option}
                    label={option?.title}
                    {...getTagProps({ index })}
                    style={{ backgroundColor: "#c7c9f4" }}
                  />
                ))
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      borderRadius: "8px",
                    },
                    color: "#9E9E9E",
                    width: "100%",
                  }}
                />
              )}
              isOptionEqualToValue={(option, value) => option?.id === value?.id}
              fullWidth
            />
            {formik.errors.categories &&
              formik.touched.categories &&
              formik.errors.categories && (
                <p className="text-red-700 text-sm">
                  {formik.errors.categories}
                </p>
              )}
          </div>
          <div className="flex flex-col gap-2 mb-2">
            <label htmlFor="subcategory" className="text-md label">
              Select Sub-Category
            </label>
            <Autocomplete
              multiple
              limitTags={2}
              size="small"
              id="multiple-limit-tags-subcategory"
              options={ProviderSubCategories || []}
              name={`subcategories`}
              value={formik.values.subcategories || []}
              onChange={(event, newValue) => {
    
                if (newValue?.length > 3) {
                  toast.error("Please Enter Maximum 3 Sub Categories");
               }else{

                 formik.setFieldValue(`subcategories`, newValue);
               }
              }}
              getOptionLabel={(option) => option?.title}

              renderOption={(props, option) => (
                <li {...props}>{option?.title}</li>
              )}
              renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                  <Chip
                    key={option}
                    label={option?.title}
                    {...getTagProps({ index })}
                    style={{ backgroundColor: "#c7c9f4" }}
                  />
                ))
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      borderRadius: "8px",
                    },
                    color: "#9E9E9E",
                    width: "100%",
                  }}
                />
              )}
              isOptionEqualToValue={(option, value) =>
                option?._id === value?._id
              }
            />
            {formik.errors.subcategories &&
              formik.touched.subcategories &&
              formik.errors.subcategories && (
                <p className="text-red-700 text-sm">
                  {formik.errors.subcategories}
                </p>
              )}
          </div>
          <div className="flex flex-col gap-2 mb-2">
            <label htmlFor="skills" className="text-md label">
              Select Skills
            </label>
            <Autocomplete
              multiple
              limitTags={2}
              size="small"
              id="multiple-limit-tags-skills"
              options={ProviderSkills || []}
              name={`skills`}
              value={formik.values.skills || []}
              onChange={(event, newValue) => {
                if (newValue?.length > 3) {
                  toast.error("Please Enter Maximum 3 Skills");
               }else{

                formik.setFieldValue(`skills`, newValue);
               }
              }}
              getOptionLabel={(option) => option?.title}

              renderOption={(props, option) => (
                <li {...props}>{option?.title}</li>
              )}
              renderTags={(value, getTagProps) =>
                value.map((option, index) => (
                  <Chip
                    key={option}
                    label={option?.title}
                    {...getTagProps({ index })}
                    style={{ backgroundColor: "#c7c9f4" }}
                  />
                ))
              }
              renderInput={(params) => (
                <TextField
                  {...params}
                  sx={{
                    "& .MuiOutlinedInput-root": {
                      borderRadius: "8px",
                    },
                    color: "#9E9E9E",
                    width: "100%",
                  }}
                />
              )}
              isOptionEqualToValue={(option, value) => option?.id === value?.id}
            />
            {formik.errors.skills &&
              formik.touched.skills &&
              formik.errors.skills && (
                <p className="text-red-700 text-sm">{formik.errors.skills}</p>
              )}
          </div>
        </div>
        <div className="flex justify-end items-center">
          <div className="flex gap-2 items-center">
            {loader2 ? (
              <div className="self-end">
                <CircularProgress />
              </div>
            ) : (
              <button
                // type="button"
                type="submit"
                className="px-8 py-2 rounded-md bg-blue-500 text-white self-end mt-4"
                // onClick={handleSubmit}
              >
                Update
              </button>
            )}
          </div>
        </div>
      </form>
    </AppDialog>
  );
}

export default ProviderCategoriesEditModal;
