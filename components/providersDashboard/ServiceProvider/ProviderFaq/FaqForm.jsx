"use client";
import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { useFormik } from "formik";
import * as Yup from "yup";
import { FaCirclePlus } from "react-icons/fa6";
import axiousInstance from "@/utils/axiousInstance";
import { CircularProgress } from "@mui/material";
import Cookies from "js-cookie";
import { MdOutlineKeyboardBackspace } from "react-icons/md";
import { AddFAQ, GetFAQCout, UpdateFAQ } from "@/Apis/ServiceProviderApis/DashboardApi/FAQApi/FAQApi";
function FaqForm({ setaddMore, editData, setEditData }) {
  const [render, setrender] = useState(false);
  const [loader, setloader] = useState(false);
  const [count, setCount] = useState(0)
  const [totalCount, setTotalCount] = useState(0)
  const handleSubmit = async (values, resetForm) => {
    setloader(true);
    let obj = {
      faqs: values?.faqs,
      user_id: Cookies.get("user_id"),
      type: "ServiceProvider",
      // user_id:"65fbe408df2dca09c5cf1551",
      // formData.append(`id`, "65fbe408df2dca09c5cf1551");
    };
    try {
      let response = ''

      if (editData.user_id) {
        response = await UpdateFAQ(values?.faqs[0])
      } else {

        response = await AddFAQ(obj)
      }
      setloader(false);
      if (response.data.status === "1") {
        // setStep(11);
        toast.success(response.data.message);
        resetForm();
        setaddMore(false);
      } else {
        setloader(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error.response?.data?.error);
    }
    // setData((prevData) => ({
    //   ...prevData,
    //   faq: faqFields,
    // }));
    // setStep(11);
  };
  const validationSchema = Yup.object().shape({
    faqs: Yup.array().of(
      Yup.object().shape({
        question: Yup.string().required("Question is required"),
        answer: Yup.string().required("Answer is required"),
      })
    ),
  });
  const formik = useFormik({
    initialValues: {
      faqs: [{ question: "", answer: "" }],
    },
    validationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  const getFAQCount = async () => {
    let resp = await GetFAQCout(Cookies.get("user_id"))
    console.log(resp);
    setCount(resp.data.count)

  }
  useEffect(() => {
    getFAQCount()
    if (editData?.user_id) {
      formik.setFieldValue("faqs", [editData])
    }
  }, [])

  useEffect(()=>{
    const total=formik?.values?.faqs?.length+count
    setTotalCount(total)
  },[formik?.values?.faqs?.length])

  console.log(totalCount);
  console.log(count);
  return (
    <div className="pt-4">
      <MdOutlineKeyboardBackspace
        onClick={() => {
          setaddMore(false);
          setEditData({})
        }}
        className="cursor-pointer"
        size={28}
      />
      <form onSubmit={formik.handleSubmit}>
        <div className="overflow-y-auto ">
          {formik.values.faqs?.map((item, index) => (
            <div key={index} className="mb-2">
              <label className="label text-md block">Question</label>
              <input
                name={`faqs.${index}.question`}
                type="text"
                value={item.question}
                onChange={formik.handleChange}
                className="w-full bg-white h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Enter your question"
              />

              {formik.touched.faqs &&
                formik.errors.faqs &&
                formik.errors.faqs[index] && (
                  <p className="text-red-700 text-sm">
                    {" "}
                    {formik.errors.faqs[index].question}
                  </p>
                )}
              <label className="label text-md  block">Answer</label>
              <textarea
                name={`faqs.${index}.answer`}
                rows="3"
                value={item.answer}
                onChange={formik.handleChange}
                placeholder="Enter the answer to the question"
                className="w-full border p-2 rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              ></textarea>

              {formik.touched.faqs &&
                formik.errors.faqs &&
                formik.errors.faqs[index] && (
                  <p className="text-red-700 text-sm">
                    {" "}
                    {formik.errors.faqs[index].answer}
                  </p>
                )}
            </div>
          ))}
        </div>
        {!editData?.user_id &&totalCount!=8&& <div className="self-end flex justify-end">
          <button
            type="button"
            onClick={() => {
              setrender(!render);
              formik.values.faqs?.push({ question: "", answer: "" });
            }}
            className="px-4 py-2 rounded-md mt-2  text-black self-end flex  items-center gap-1"
          >
            <FaCirclePlus />
            <h1> Add More</h1>
          </button>
        </div>}
        <div className="flex justify-between items-center">
          {loader ? (
            <CircularProgress className="self-end" />
          ) : (
            <button
              type="submit"
              className="px-8 py-2 rounded-md bg-bgBlue text-white self-end mt-4"
            >
              Submit your question
            </button>
          )}
        </div>
      </form>
    </div>
  );
}

export default FaqForm;
