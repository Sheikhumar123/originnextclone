import ChangeStatusDialoge from "@/components/ChangeStatusDialoge/ChangeStatusDisloge";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";
import { accordionData } from "@/data/homepageData";
import {
  GetFAQ,
  GetGlobalDetails,
} from "@/redux/Slices/GlobalSlice/GlobalSlice";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { FiEdit } from "react-icons/fi";
import { MdDelete } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { DeleteFAQ, UpdateFAQ } from "@/Apis/ServiceProviderApis/DashboardApi/FAQApi/FAQApi";
import DeleteDialog from "@/components/DeleteDialog/DeleteDialog";
function AllFaqs({ setaddMore, setEditData }) {
  const { AllFaq, status, error } = useSelector(GetGlobalDetails);
  const dispatch = useDispatch();
  const [statusDialoge, setstatusDialoge] = useState(false);
  const [loader, setLoader] = useState(false)
  const [statusItem, setStatusItem] = useState("")
  const [deleteDialog, setdeleteDialog] = useState(false);
  const [deleteData, setdeleteData] = useState(null);
  // const [loader, setLoader] = useState(false);
  // const [statusItem, setStatusItem] = useState("");
  useEffect(() => {
    dispatch(GetFAQ({ id: Cookies.get("user_id") }));
  }, [dispatch]);
  const handleStatusChange = async () => {
    console.log(statusItem);
    setLoader(true);
    const response = await UpdateFAQ({
      id: statusItem?.id,
      status: statusItem?.status === "Active" ? "Inactive" : "Active",
    });
    setLoader(false);
    if (response?.data?.status == "1") {
      toast.success(response.data.message);
      dispatch(GetFAQ({ id: Cookies.get("user_id") }));
      setstatusDialoge(false);
    }     if (response?.data?.status == "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };
  const handleDelete = async (id) => {
    setLoader(true)
    try {
      const response = await DeleteFAQ(deleteData.id);

      if (response?.data?.status == "1") {
        toast.success(response.data.message);
        dispatch(GetFAQ({ id: Cookies.get("user_id") }));

      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      console.log(error);
    }
    setLoader(false)
    setdeleteDialog(false)

  }
  const handleCancel = () => {
    console.log("Cancel");
  };
  console.log(AllFaq);
  return (
    <div>
      {status == "pending" && <ScreenLoader />}
      <div className="w-full p-4  flex flex-col gap-4  ">
        {statusDialoge && <ChangeStatusDialoge
          setrowData={handleCancel}
          loader={loader}
          handleStatusChange={handleStatusChange}
          type={"FAQs"}
          rowTitle={'status'}
          status={statusItem.status}
          setstatusDialoge={setstatusDialoge}
        />}
        {deleteDialog && (
          <DeleteDialog
            setdeleteData={setdeleteData}
            loader={loader}
            handleDelete={handleDelete}
            type={"Service"}
            name={deleteData?.title}
            deleteData={deleteData}
            setdeleteDialog={setdeleteDialog}
          />
        )}
        {AllFaq?.map((item, index) => (
          <div className="flex flex-col gap-4 bg-white rounded-md shadow-loginShadow  p-4 1400px:p-6 ">
            <div className="flex gap-3 justify-end">
              <label
                className="inline-flex items-center cursor-pointer"
                data-twe-toggle="tooltip"
                title={"Active"}
              >
                <input
                  type="checkbox"
                  defaultValue=""
                  checked={item.status === "Active"}
                  className="sr-only peer"
                  onChange={() => {
                    setstatusDialoge(true);
                    setStatusItem(item);
                  }}
                />
                <div className="relative w-10 h-5 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />
              </label>

              <FiEdit
                size={18}
                className="cursor-pointer"
                color="#6b7280"
                onClick={() => {
                  setaddMore(true);

                setEditData(item)
              }} />
              <MdDelete className="cursor-pointer" onClick={() => {
                setdeleteData(item)
                setdeleteDialog(true)
              }} size={18} color="#6b7280" />
            </div>
            <div
              key={index + 1 * 7}
              className="collapse collapse-plus bg-[#f2f2f2] rounded-none"
            >
              <input type="radio" name="my-accordion-3" />
              <div className="collapse-title text-lg font-medium">
                {item.question}
              </div>
              <div className="collapse-content bg-white">
                <p>{item.answer}</p>
              </div>
            </div>
          </div>
        ))}
      </div>
      {AllFaq?.length != 8 && (
        <div className="pt-4">
          <div className="border w-full h-[150px] bg-[#d4d6f7] flex justify-center items-center border-dashed rounded-lg border-mainblue">
            <div className="flex justify-center items-center flex-col gap-3">
              {/* <FaUpload color="grey" className="text-red" size={26}/> */}
              <p className="text-[#434CD9] font-semibold">
                Still have more Questions?
              </p>
              <button
                type="button"
                onClick={() => {
                  setaddMore(true);
                }}
                className="bg-mainpurple text-sm text-white py-2 px-6 rounded-xl"
              >
                Add More
              </button>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default AllFaqs;
