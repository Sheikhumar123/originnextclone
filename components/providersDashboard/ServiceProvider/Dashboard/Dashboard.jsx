import React from "react";
import DesignPart from "./DesignPart";
import LeadSettingPart from "./LeadSettingPart";
import LeadsPart from "./LeadsPart";
import backgroundImg from "../../../../images/dashboard/layer.png";
import backgroundImg2 from "../../../../images/dashboard/layer2.png";
import user from "../../../../images/dashboard/user.png";
import Image from "next/image";

function Dashboard() {
  return (
    <>
      <div
        className="w-full 700px:h-[200px] 300px:h-[140px] bg-gradient-to-r from-[#7d22ed] to-green-400 flex items-center pl-12 rounded-3xl relative overflow-hidden"
        style={{
          backgroundImage: `url(${backgroundImg})`,
          backgroundSize: "cover",
          backgroundPosition: "center",
        }}
      >
        <div className="absolute right-0 top-0 w-full 700px:max-w-[50%] 300px:max-w-[70%]">
          <Image
            src={backgroundImg}
            width={1000}
            height={1000}
            style={{ width: "100%", height: "100%" }}
          />
        </div>
        <div className="absolute right-0 top-0 w-full 700px:max-w-[70%] 300px:max-w-[100%]">
          <Image
            src={backgroundImg2}
            width={1000}
            height={1000}
            style={{ width: "100%", height: "100%" }}
          />
        </div>
        <div className="w-full flex flex-col gap-4 justify-center">
          <h1 className="font-bold 700px:text-2xl 300px:text-lg text-white">
            Welcome to Bark, Sadia
          </h1>
          <h2 className="font-bold 700px:text-2xl 300px:text-lg text-white">
            We're excited to help you grow your business
          </h2>
        </div>
      </div>
      {/* 1200px:flex-row 300px:flex-col
      flex-wrap  */}
      <div
        className="flex gap-5 w-full pt-12 
      1200px:flex-row 300px:flex-col
      "
      >
        <DesignPart />
        <LeadSettingPart />
        <LeadsPart />
      </div>
      <div className="w-full flex gap-4 pt-[5rem]">
        <div className="700px:w-[49%] 300px:w-[100%]">
          <p className="text-4xl text-[#404040] capitalize">
            We're excited to help you grow your business.
          </p>
          <div className="flex flex-col gap-6 pt-6">
            <div className="flex gap-5">
              <div className="w-[35px] h-[35px] rounded-[50%] bg-[#0479d3] flex justify-center items-center">
                <p className="text-white font-[500] text-lg">1</p>
              </div>
              <div>
                <p className="text-lg capitalize text-[#404040] font-[500]">
                  Customers tell us what they need
                </p>
                <p className="text-sm capitalize text-[#9194ac] font-[500]">
                  {" "}
                  Customers answer specific questions about their requirements.
                </p>
              </div>
            </div>
            <div className="flex gap-5">
              <div className="w-[35px] h-[35px] rounded-[50%] bg-[#0479d3] flex justify-center items-center">
                <p className="text-white font-[500] text-lg">2</p>
              </div>
              <div>
                <p className="text-lg capitalize text-[#404040] font-[500]">
                  We send you matching leads
                </p>
                <p className="text-sm capitalize text-[#9194ac] font-[500]">
                  {" "}
                  You receive leads that match your preferences instantly by
                  email and on the app.
                </p>
              </div>
            </div>
            <div className="flex gap-5">
              <div className="w-[35px] h-[35px] rounded-[50%] bg-[#0479d3] flex justify-center items-center">
                <p className="text-white font-[500] text-lg">3</p>
              </div>
              <div>
                <p className="text-lg capitalize text-[#404040] font-[500]">
                  You choose leads you like
                </p>
                <p className="text-sm capitalize text-[#9194ac] font-[500]">
                  {" "}
                  Get customer contact details right away.
                </p>
              </div>
            </div>
            <div className="flex gap-5">
              <div className="w-[35px] h-[35px] rounded-[50%] bg-[#0479d3] flex justify-center items-center">
                <p className="text-white font-[500] text-lg">4</p>
              </div>
              <div>
                <p className="text-lg capitalize text-[#404040] font-[500]">
                  You contact the customer
                </p>
                <p className="text-sm capitalize text-[#9194ac] font-[500]">
                  {" "}
                  Call or email the customer and sell your services.
                </p>
              </div>
            </div>
            <div className="flex gap-5">
              <div className="w-[35px] h-[35px] rounded-[50%] bg-[#0479d3] flex justify-center items-center">
                <p className="text-white font-[500] text-lg">5</p>
              </div>
              <div>
                <p className="text-lg capitalize text-[#404040] font-[500]">
                  You contact the customer
                </p>
                <p className="text-sm capitalize text-[#9194ac] font-[500]">
                  {" "}
                  Call or email the customer and sell your services.
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="w-[49%] 700px:block 300px:hidden">
          <div style={{ width: "100%", height: "100%" }}>
            <Image
              src={user}
              // width={1000}
              // height={1000}
              // style={{ width: "100%", height: "100%" }}
            />
          </div>
        </div>
      </div>
      <div className="flex flex-col gap-3 pt-2">
        <p className="text-4xl text-[#404040] capitalize">
          How much does Bark cost?
        </p>
        <p className="text-sm capitalize text-[#9194ac] font-[500] w-full 700px:max-w-[55%] 300px:max-w-[100%]">
          {" "}
          It's free to receive leads and you only pay to contact those you like.
          Leads are priced in credits, based on the value of the job. We offer a
          discounted starter pack with enough credits for about 10 responses,
          backed by our Get Hired Guarantee. We're so confident you'll get hired
          at least once from this pack, that if you don't we'll give you all
          your credits back.
        </p>
        <div className="pt-4">
          <button className="bg-mainpurple text-sm text-white py-2 px-4 rounded-xl w-[fit-content]">
            View new 234 leads
          </button>
        </div>
      </div>
    </>
  );
}

export default Dashboard;
