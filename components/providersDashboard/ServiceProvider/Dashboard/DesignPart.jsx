import React from "react";
import help from "../../../../images/help.png";
import user from "../../../../images/user.png";
import more from "../../../../images/more.png";
import Image from "next/image";
import { FaLongArrowAltDown } from "react-icons/fa";
import { IoIosArrowForward } from "react-icons/io";
function DesignPart() {
  return (
    <div className="flex flex-col gap-6  1200px:w-[32%] 300px:w-[100%]">
      <div className="border border-border p-4 bg-mainblue rounded-2xl w-[100%] ">
        <div className="flex justify-between items-start p-2">
          <div className="flex gap-2 items-center">
            <div className="flex justify-center items-center w-[50px] h-[45px] rounded-md bg-[#99C2FF]">
              <p className="font-[500] text-2xl">M</p>
            </div>
            <p className="text-white">Design</p>
          </div>
          <button className="bg-white text-sm text-mainpurple py-2 px-4 rounded-xl">
            Edit Profile
          </button>
        </div>
        <p className="text-white py-2">your profile is 30% complete</p>
        <div className="py-2">
          <div class="w-full bg-lightblue rounded-full h-2 ">
            <div
              class="bg-white h-2 rounded-full"
              style={{ width: "45%" }}
            ></div>
          </div>
        </div>
        <div className="py-2 px-3 w-[80%]">
          <p className="text-textgrey text-sm">
            Completing your profile is a great way to appeal to customers
          </p>
        </div>
      </div>
      <div className="border border-border p-4 bg-lightbg rounded-2xl w-[100%] ">
        <p className="text-lg font-[500] py-2">Help</p>
        <div className="w-[100%] border-t-2"></div>
        <div className="flex gap-2 items-center py-4">
          <div className="w-[25px]">
            <Image
              src={help}
              alt="Image"
              width={25}
              height={25}
              style={{ width: "100%", height: "100%" }}
            />
          </div>
          <p>visit help center for tips & advice</p>
        </div>

        <p className="text-textdarkgrey py-2 text-sm">Email team@orgihn.com</p>
        <p className="  text-lg">Call +91 12345678</p>
        <p className="text-textdarkgrey py-2 text-sm">
          open 24 hours a day, 7 days a week
        </p>
      </div>
      <div className="border border-border p-4 bg-lightbg rounded-2xl w-[100%]  relative ">
        <div className="flex justify-between items-center">
          <p className="text-lg font-[500] py-2">Inbox</p>
          <div className="flex gap-1 items-center">
            <p className="text-textdarkgrey">View All</p>
            <IoIosArrowForward className="text-textdarkgrey" />
          </div>
        </div>
        <div className="w-[100%] border-t-2"></div>
        <div className="py-3 flex flex-col gap-4 px-2">
          <div className="flex gap-3">
            <div className="w-[60px]">
              <Image
                src={user}
                alt="Image"
                width={60}
                height={40}
                style={{ width: "100%", height: "100%" }}
              />
            </div>
            <div>
              <p className="w-[100%] max-w-[230px] text-textdarkgrey text-md">
                You have a new message from John Doe
              </p>
              <p className="text-[#AEAEAE]">12h ago</p>
            </div>
          </div>
          <div className="flex gap-3">
            <div className="w-[60px]">
              <Image
                src={user}
                alt="Image"
                width={60}
                height={40}
                style={{ width: "100%", height: "100%" }}
              />
            </div>
            <div>
              <p className="w-[100%] max-w-[230px] text-textdarkgrey text-md">
                You have a new message from John Doe
              </p>
              <p className="text-[#AEAEAE]">12h ago</p>
            </div>
          </div>
        </div>
        <div className="w-[60px] h-[60px] rounded-[50%] shadow-loadmore flex justify-center items-center absolute bottom-[-10px] right-[50%] ">
<FaLongArrowAltDown size={22}/>
            </div>
      </div>
    </div>
  );
}

export default DesignPart;
