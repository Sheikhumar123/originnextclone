import React from "react";
import { HiMapPin } from "react-icons/hi2";
function LeadSettingPart() {
  return (
    <div className="flex flex-col gap-6 1000px:w-[40%] 300px:w-[100%]">
      <div className="border border-border p-4 bg-lightbg rounded-2xl w-[100%] ">
        <p className="text-lg font-[500] py-2">Lead settings</p>
        <div className="flex justify-between items-center">
          <p className="text-md font-[500] py-2">Services</p>

          <p className="text-textdarkgrey underline">Edit</p>
        </div>
        <div className="w-[100%] border-t-2"></div>

        <p className="text-textdarkgrey py-4 text-sm">
          you'll receive settings in these categories
        </p>
        <div className="flex gap-2 pb-3 items-center flex-wrap">
          <div className="bg-[#f5f5f5] flex justify-center items-center py-2 px-4 rounded-[20px] w-[fit-content]">
            <p>Graphic Design</p>
          </div>
          <div className="bg-[#f5f5f5] flex justify-center items-center py-2 px-4 rounded-[20px] w-[fit-content]">
            <p>Web Design</p>
          </div>

          <div className="bg-[#f5f5f5] flex justify-center items-center py-2 px-4 rounded-[20px] w-[fit-content]">
            <p>Logo Design</p>
          </div>
        </div>
      </div>

      <div className="border border-border p-4 bg-lightbg rounded-2xl w-[100%] ">
        <div className="flex justify-between items-center">
          <p className="text-lg font-[500] py-2">Location</p>

          <p className="text-textdarkgrey">Edit</p>
        </div>
        <div className="flex gap-1 pb-4 items-center">
          <HiMapPin className="text-[#a3a3a3]" size={22} />
          <p className="text-[#a3a3a3] text-sm">New York, USA</p>
        </div>
        <div className="w-[100%] border-t-2"></div>

        <p className="text-[#0E0B33] py-4 text-sm">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna
        </p>
      </div>

      <div className="border border-border p-4 bg-lightbg rounded-2xl w-[100%] ">
        <div className="flex justify-between items-center">
          <p className="text-lg font-[500] py-2">Estimated 101 leads per day</p>

          <p className="text-textdarkgrey">Edit</p>
        </div>
        {/* <div className="flex gap-1 pb-4 items-center">
          <HiMapPin className="text-[#a3a3a3]" size={22} />
          <p className="text-[#a3a3a3] text-sm">New York, USA</p>
        </div> */}
        <div className="w-[100%] border-t-2"></div>
        <div className="flex gap-2 pb-2 pt-4 items-center flex-wrap">
          <div className="bg-[#f5f5f5] flex justify-center items-center py-2 px-4 rounded-[20px] w-[fit-content]">
            <p>Graphic Design</p>
          </div>
          <div className="bg-[#f5f5f5] flex justify-center items-center py-2 px-4 rounded-[20px] w-[fit-content]">
            <p>Photoshop</p>
          </div>

          <div className="bg-[#f5f5f5] flex justify-center items-center py-2 px-4 rounded-[20px] w-[fit-content]">
            <p>HTML/CSS</p>
          </div>
        </div>
        <p className="text-[#0E0B33] py-2 text-sm">
        Sending new leads to
        </p>
        <p className="text-[#0E0B33] pb-2 font-semibold">
        sadiamehwish85@gmail.com
        </p>
        <button className="bg-mainpurple text-sm text-white py-2 px-6 rounded-xl">
            Update Email
          </button>
      </div>
    </div>
  );
}

export default LeadSettingPart;
