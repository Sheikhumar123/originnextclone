import React from "react";

function LeadsPart() {
  return (
    <div className="flex flex-col gap-6 1000px:w-[25%] 300px:w-[100%]">
      <div className="border border-border p-4 bg-lightbg rounded-2xl w-[100%] ">
        <p className="text-lg font-[500] py-2">Leads</p>
        <div className="w-[100%] border-t-2"></div>
        <div className="flex flex-col gap-2 justify-center items-center py-4">
          <div className="bg-[#d4d6f7] flex justify-center items-center py-2 px-4 rounded-[20px] w-[fit-content]">
            <p className="text-mainpurple font-semibold text-md">786</p>
          </div>
          <p className="font-semibold">Unread Leads</p>
          <div className="pt-2">
          <button className="bg-mainpurple text-sm text-white py-2 px-6 rounded-xl">
            View All
          </button>
          </div>
        </div>
      </div>
      <div className="border border-border p-4 bg-lightbg rounded-2xl w-[100%] ">
        <p className="text-lg font-[500] py-2">Done Leads</p>
        <div className="w-[100%] border-t-2"></div>
        <div className="flex flex-col gap-2 justify-start items-start py-4">
          <div className="bg-[#d4d6f7] flex justify-center items-center py-2 px-4 rounded-[20px] w-[fit-content]">
            <p className="text-mainpurple font-semibold text-md">10</p>
          </div>
          <p className="font-semibold">Leads are done</p>
          <div className="pt-2">
          <button className="bg-mainpurple text-sm text-white py-2 px-6 rounded-xl">
            View All
          </button>
          </div>
        </div>
      </div>
      <div className="border border-border p-4 bg-lightbg rounded-2xl w-[100%] ">
        <p className="text-lg font-[500] py-2">Get started</p>
        <div className="w-[100%] border-t-2"></div>
        <div className="flex flex-col gap-2 justify-start items-start py-4">
 
          <p className="font-[500]">20% OFFSTARTER PACK OFFER</p>
          <div className="pt-2">
          <button className="bg-mainpurple text-sm text-white py-2 px-6 rounded-xl">
            View All
          </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LeadsPart;
