import Table from "@/components/Table/Table";
import {
  GetProviderDashServices,
  GetProviderdashboardDetails,
} from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import { manrope } from "@/utils/fontPoppins";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FiEdit } from "react-icons/fi";
import Image from "next/image";
import { MdDelete } from "react-icons/md";
import DeleteDialog from "@/components/DeleteDialog/DeleteDialog";
import { toast } from "react-toastify";
import { DeleteServiceApi } from "@/Apis/ServiceProviderApis/DashboardApi/ServicesApis/DeleteServiceApi";
import Pagination from "@/components/Pagination/Pagination";
import { BsFillLightningChargeFill } from "react-icons/bs";
import { FiThumbsUp } from "react-icons/fi";
import { HiOutlineUserGroup } from "react-icons/hi2";
import { IoIosHeartEmpty } from "react-icons/io";
import { LiaPhoneVolumeSolid } from "react-icons/lia";
import ratingImgCard from "../../../../images/ratingImgCard.png";
import ChangeStatusDialoge from "@/components/ChangeStatusDialoge/ChangeStatusDisloge";
import { UpdateFAQ } from "@/Apis/ServiceProviderApis/DashboardApi/FAQApi/FAQApi";
import { ServiceStatusChangeApi } from "@/Apis/ServiceProviderApis/DashboardApi/ServicesApis/ServiceStatusChangeApi";
import ScreenLoader from "@/components/ScreenLoader/ScreenLoader";

function ProviderServicesTable({ setshowForm, setisEdit, setrowData,settotal }) {
  const { ProviderDashServices ,status} = useSelector(GetProviderdashboardDetails);
  const [allServices, setallServices] = useState([]);
  const [deleteDialog, setdeleteDialog] = useState(false);
  const [loader, setloader] = useState(false);
  const [loader2, setloader2] = useState(false);
  const [deleteData, setdeleteData] = useState(null);
  const [page, setpage] = useState(1);
  const [count, setcount] = useState(0);
  const [limit, setlimit] = useState(1);
  const [readmore, setReadmore] = useState(-1)
const [statusItem,setStatusItem]=useState(null)
const [statusDialoge,setstatusDialoge  ]=useState(false)
  const dispatch = useDispatch();
  console.log(ProviderDashServices);
  useEffect(() => {
    dispatch(
      GetProviderDashServices({ user_id: Cookies.get("user_id"), page: page })
    );
  }, [dispatch, page]);
  useEffect(() => {
    if (ProviderDashServices) {
      setallServices(ProviderDashServices?.data);
      setcount(ProviderDashServices?.count);
      setlimit(ProviderDashServices?.limit);
      settotal(ProviderDashServices?.data?.length);
    }
  }, [ProviderDashServices]);
  console.log(ProviderDashServices);
  const columns = [
    {
      label: "Image",
      value: "image_url",
    },
    { label: "title", value: "title" },
    { label: "Duration", value: "duration" },
    { label: "Starting Price", value: "starting_price" },
    { label: "Description", value: "description" },
  ]; // Define table columns
  const renderBodyContent = (column, item) => {
    if (column?.label === "Image") {
      return (
        <div className="w-[60px] h-[60px] rounded-full">
          <Image
            width={60}
            height={60}
            style={{ width: "100%", height: "100%" }}
            src={item?.image_url}
          />
          {/* {item[column?.image_url]} */}
        </div>
      );
    }
    return item[column?.value];
  };
  const actions = [
    {
      type: "icon",
      label: "Edit",
      buttonClass: "bg-red-500 text-white px-4 py-2 rounded",
      icon: <FiEdit size={18} />,
      onClick: (item) => {
        setisEdit(true);
        setrowData(item);
        setshowForm(true);
      },
    },
    // {
    //   type: "icon",
    //   label: "View",
    //   buttonClass: "bg-red-500 text-white px-4 py-2 rounded",
    //   icon: <MdVisibility size={18} />,
    //   onClick: (item) => {
    //     setshowDetailModal(true);
    //     setdetailData(item);
    //   },
    // },
    {
      type: "icon",
      label: "Delete",
      buttonClass: "bg-red-500 text-white px-4 py-2 rounded",
      icon: <MdDelete size={18} />,
      onClick: (item) => {
        setdeleteDialog(true);
        setdeleteData(item);
      },
    },
  ];
  const handleDelete = async () => {
    setloader(true);
    const response = await DeleteServiceApi({ id: deleteData?._id,user_id:deleteData?.user_id });
    setloader(false);
    if (response?.data?.status == "1") {
      toast.success(response.data.message);
      dispatch(
        GetProviderDashServices({ user_id: Cookies.get("user_id"), page: page })
      );
      setdeleteDialog(false);
      setdeleteData(null)
    }     if (response?.data?.status == "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };
  const handleStatusChange = async () => {
    console.log(statusItem);
    setloader2(true);
    const response = await ServiceStatusChangeApi({
      id: statusItem?._id,
      status: statusItem?.status === "Active" ? "Inactive" : "Active",
    });
    setloader2(false);
    console.log("wese",response);

    if (response?.data?.status == "1") {
      toast.success(response.data.message);
      dispatch(
        GetProviderDashServices({ user_id: Cookies.get("user_id"), page: page })
      );
      setstatusDialoge(false);
    }     if (response?.data?.status == "0") {
      console.log("0",response);

      toast.error(response.data.message);
    } else {
      console.log("error",response);
      toast.error(response.response?.data.message);
    }
  };
  return (
    <>
      {(loader2 || status=="pending") && <ScreenLoader />}
      {statusDialoge && (
        <ChangeStatusDialoge
          setrowData={setStatusItem}
          loader={loader}
          handleStatusChange={handleStatusChange}
          type={"Service"}
          rowTitle={"status"}
          status={statusItem?.status}
          setstatusDialoge={setstatusDialoge}
        />
      )}
      {deleteDialog && (
        <DeleteDialog
          setdeleteData={setdeleteData}
          loader={loader}
          handleDelete={handleDelete}
          type={"Service"}
          name={deleteData?.title}
          deleteData={deleteData}
          setdeleteDialog={setdeleteDialog}
        />
      )}
      <div className={`w-full px-3 ${manrope.className}`}>
        <div className="mb-3">
          <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
            Services
          </h1>
          <p className="text-[#525252">
            Get Connected To Grow Better business.
          </p>
        </div>
      </div>
      {
        ProviderDashServices?.data?.length>=5?null:
      
      <div className="w-full flex justify-end items-end py-4">
        <button
          type="button"
          onClick={() => {
            setshowForm(true);
          }}
          className="bg-[#434CD9] w-[fit-content] px-6 py-2 rounded-md text-white"
        >
          Add Services
        </button>
      </div>
}
      <div className="flex flex-col gap-4">
        {allServices?.map((item, index) => {
          return (
            <div className="flex flex-col gap-2 bg-white rounded-md shadow-loginShadow w-full  p-4 1400px:p-6 ">
              <div className="flex gap-2 self-end items-center">
                <label
                  className="inline-flex items-center cursor-pointer"
                  data-twe-toggle="tooltip"
                  title={"Active"}
                >
                  <input
                    type="checkbox"
                    defaultValue=""
                    checked={item.status === "Active"}
                    className="sr-only peer"
                    onChange={() => {
                      setStatusItem(item);
                      setstatusDialoge(true);
                    }}
                  />
                  <div className="relative w-10 h-5 bg-gray-200 peer-focus:outline-none peer-focus:ring-4 peer-focus:ring-blue-300 dark:peer-focus:ring-blue-800 rounded-full peer dark:bg-gray-700 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-white after:content-[''] after:absolute after:top-0.5 after:start-[4px] after:bg-white after:border-gray-300 after:border after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-gray-600 peer-checked:bg-blue-600" />
                </label>
                <FiEdit
                  className="cursor-pointer" onClick={() => {
                    setisEdit(true);
                    setrowData(item);
                    setshowForm(true);
                  }} size={18} color="#6b7280" />
                <MdDelete className="cursor-pointer" onClick={() => {
                  setdeleteDialog(true);
                  setdeleteData(item);
                }} size={18} color="#6b7280" />
              </div>
              <div className="flex gap-4  " key={index + 33 * 8}>
                <div className="w-[150px]">
                  <div className="w-[150px]">
                    <Image
                      src={item?.image_url}
                      alt="img service"
                      width={150}
                      height={70}
                      style={{
                        width: "100%",
                        height: "100%",
                        maxHeight: "150px",
                      }}
                    />
                  </div>
                </div>

                <div>
                  <h1 className="text-xl font-semibold">{item?.title}</h1>
                  <p
                  dangerouslySetInnerHTML={{ __html: item?.description }}
                    className={`text-md font-[500] text-[#636363] ${readmore == index ? "" : "line-clamp-3"} leading-12 ${manrope.className}`}
                  >
                    {/* {item?.description} */}

                  </p>
                  {/* {console.log(readmore==index)} */}
                  {item?.description.length > 200 && readmore != index && <div><span className="text-blue-600 hover:underline hover:text-blue-700 cursor-pointer" onClick={() => setReadmore(index)}> Read More</span></div>}
                  {readmore == index && <div><span className="text-blue-600 hover:underline hover:text-blue-700 cursor-pointer" onClick={() => setReadmore(-1)}> Read less</span></div>}
                  <span className=" text-green-600 font-semibold">
                    Starting From:{" "}
                  </span>
                  <span className="font-semibold">
                    ${item?.starting_price} USD
                  </span>
                </div>
              </div>
            </div>
          );
        })}
      </div>
      {/* <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
        <Table
          columns={columns}
          data={allServices}
          actions={actions}
          renderBody={renderBodyContent}
        />
      </div> */}
      {count > limit ? (
        <div className="pt-4 pb-[130px] flex justify-end px-2">
          <Pagination
            pageCount={page}
            setPageCount={setpage}
            totalcount={count}
            limit={limit}
            showCount={true}
          />
        </div>
      ) : null}
    </>
  );
}

export default ProviderServicesTable;
