"use client";
import React, { useState } from "react";
import ProviderServicesForm from "./ProviderServicesForm";
import ProviderServicesTable from "./ProviderServicesTable";

function ProviderServicesMain() {
  const [showForm, setshowForm] = useState(false);
  const [isEdit, setisEdit] = useState(false);
  const [rowData, setrowData] = useState(null);
  const [total, settotal] = useState(0);
  return (
    <>
      {showForm ? (
        <ProviderServicesForm total={total} setisEdit={setisEdit} isEdit={isEdit} setrowData={setrowData} rowData={rowData} setshowForm={setshowForm} />
      ) : (
        <ProviderServicesTable settotal={settotal} setisEdit={setisEdit} setrowData={setrowData} setshowForm={setshowForm} />
      )}
    </>
  );
}

export default ProviderServicesMain;
