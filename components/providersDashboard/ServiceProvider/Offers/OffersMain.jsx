"use client";
import React, { useState } from "react";
import OffersForm from "./OffersForm";
import OffersTable from "./OffersTable";

function OffersMain() {
  const [showForm, setshowForm] = useState(false);
  const [isEdit, setisEdit] = useState(false);
  const [rowData, setrowData] = useState(null);
  return (
    <>
      {showForm ? (
        <OffersForm
          setisEdit={setisEdit}
          isEdit={isEdit}
          setrowData={setrowData}
          rowData={rowData}
          setshowForm={setshowForm}
        />
      ) : (
        <OffersTable
          setisEdit={setisEdit}
          setrowData={setrowData}
          setshowForm={setshowForm}
          rowData={rowData}
        />
      )}
    </>
  );
}

export default OffersMain;
