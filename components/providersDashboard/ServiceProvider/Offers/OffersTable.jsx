import Table from "@/components/Table/Table";
import {
  GetProviderdashboardDetails,
  GetServiceProviderOffers,
} from "@/redux/Slices/ProviderdashboardSlice/ProviderdashboardSlice";
import { manrope } from "@/utils/fontPoppins";
import Cookies from "js-cookie";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FiEdit } from "react-icons/fi";
import { MdDelete } from "react-icons/md";
import DeleteDialog from "@/components/DeleteDialog/DeleteDialog";
import {
  DeleteOffer,
  UpdateOffer,
} from "@/Apis/ServiceProviderApis/DashboardApi/OffersApi/OffersApi";
import { toast } from "react-toastify";
import { FaRegEye } from "react-icons/fa";
import ChangeStatusDialoge from "@/components/ChangeStatusDialoge/ChangeStatusDisloge";
import OfferDetailModal from "./OfferDetailModal";
import { MdVisibility } from "react-icons/md";
function OffersTable({ setshowForm, setisEdit, setrowData, rowData }) {
  const { ProviderDashOffers } = useSelector(GetProviderdashboardDetails);
  const [allOffers, setallOffers] = useState([]);
  const [deleteDialog, setdeleteDialog] = useState(false);
  const [loader, setloader] = useState(false);
  const [showDetailModal, setshowDetailModal] = useState(false);
  const [statusDialoge, setstatusDialoge] = useState(false);

  const dispatch = useDispatch();
  const GetProviderOffers = () => {
    dispatch(
      GetServiceProviderOffers({ user_id: Cookies.get("user_id"), page: 1 })
    );
  };
  useEffect(() => {
    GetProviderOffers();
  }, [dispatch]);
  useEffect(() => {
    if (ProviderDashOffers) {
      setallOffers(ProviderDashOffers);
    }
  }, [ProviderDashOffers]);

  const columns = [
    { label: "title", value: "title" },
    { label: "Discount Percentage", value: "discount_percentage" },
    { label: "Start Date", value: "start_date" },
    { label: "End Date", value: "end_date" },
    // { label: "Description", value: "description" },
    { label: "Status", value: "status" },
  ]; // Define table columns

  const actions = [
    {
      type: "toogle",
      onClick: (item) => {
        console.log(item);
        setrowData(item);
        setstatusDialoge(true);
      },
    },
    {
      type: "icon",
      label: "View",
      buttonClass: "bg-red-500 text-white px-4 py-2 rounded",
      icon: <MdVisibility size={18} />,
      onClick: (item) => {
        setshowDetailModal(true);
        setrowData(item);
      },
    },
    {
      type: "icon",
      label: "Edit",
      buttonClass: "bg-red-500 text-white px-4 py-2 rounded",
      icon: <FiEdit size={18} />,
      onClick: (item) => {
        setisEdit(true);
        setrowData(item);
        setshowForm(true);
      },
    },
    {
      type: "icon",
      label: "Delete",
      buttonClass: "bg-red-500 text-white px-4 py-2 rounded",
      icon: <MdDelete size={18} />,
      onClick: (item) => {
        setdeleteDialog(true);
        setrowData(item);
      },
    },
  ];
  // const renderBodyContent = (column, item) => {
  //   console.log(item?.description?.length);
  //   if (column?.label === "Description") {
  //     return (
  //       <div className={`${item?.description?.length>100?"w-full max-w-[100px] text-ellipsis overflow-hidden":""}`} dangerouslySetInnerHTML={{ __html: item?.description }}></div>
  //     );
  //   }
  //   return item[column?.value];
  // };
  const handleDelete = async () => {
    setloader(true);
    const response = await DeleteOffer({ id: rowData?.id });
    setloader(false);
    if (response?.data?.status == "1") {
      toast.success(response.data.message);
      GetProviderOffers();
      setdeleteDialog(false);
    }
    if (response?.data?.status == "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };
  const handleStatusChange = async () => {
    setloader(true);
    const response = await UpdateOffer({
      id: rowData?.id,
      status: rowData?.status === "Active" ? "Inactive" : "Active",
    });
    setloader(false);
    if (response?.data?.status == "1") {
      toast.success(response.data.message);
      GetProviderOffers();
      setstatusDialoge(false);
    }     if (response?.data?.status == "0") {
      toast.error(response.data.message);
    } else {
      toast.error(response.response?.data.message);
    }
  };

  console.log(rowData);
  return (
    <>
      {deleteDialog && (
        <DeleteDialog
          setdeleteData={setrowData}
          loader={loader}
          handleDelete={handleDelete}
          type={"Offer"}
          name={rowData?.title}
          setdeleteDialog={setdeleteDialog}
        />
      )}
      {statusDialoge ? (
        <ChangeStatusDialoge
          setrowData={setrowData}
          loader={loader}
          handleStatusChange={handleStatusChange}
          type={"Offer"}
          rowTitle={rowData?.title}
          status={rowData?.status}
          setstatusDialoge={setstatusDialoge}
        />
      ) : null}
      {showDetailModal && rowData ? (
        <OfferDetailModal
          id={rowData?.id}
          setshowDetailModal={setshowDetailModal}
          showDetailModal={showDetailModal}
          setrowData={setrowData}
        />
      ) : null}
      <div className={`w-full px-3 ${manrope.className}`}>
        <div className="mb-3">
          <h1 className="text-[1.6rem] font-semibold mb-2 text-black">
            Offers
          </h1>
          <p className="text-[#525252">
            Get Connected To Grow Better business.
          </p>
        </div>
      </div>
      <div className="w-full flex justify-end items-end py-4">
        <button
          type="button"
          onClick={() => {
            setshowForm(true);
          }}
          className="bg-[#434CD9] w-[fit-content] px-6 py-2 rounded-md text-white"
        >
          Add Offer
        </button>
      </div>
      <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
        <Table
          columns={columns}
          data={allOffers}
          actions={actions}
          // renderBody={renderBodyContent}
        />
      </div>
    </>
  );
}

export default OffersTable;
