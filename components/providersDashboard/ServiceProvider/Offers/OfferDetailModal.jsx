import { GetSingleOffer } from "@/Apis/ServiceProviderApis/DashboardApi/OffersApi/OffersApi";
import { CircularProgress } from "@mui/material";
import moment from "moment";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import { BsFillLightningChargeFill } from "react-icons/bs";
import { RxCrossCircled } from "react-icons/rx";
function OfferDetailModal({ id, setshowDetailModal, setrowData }) {
  const [loader, setloader] = useState(false);
  const [OfferDetail, setOfferDetail] = useState(null);
  const GetDetails = async () => {
    setloader(true);
    const response = await GetSingleOffer(id);
    setloader(false);
    if (response?.data?.status == "1") {
      setOfferDetail(response.data.Offersdata);
    }
  };
  console.log(OfferDetail);
  useEffect(() => {
    if (id) {
      GetDetails();
    }
  }, [id]);

  return (
    <div
      className="relative z-10"
      aria-labelledby="modal-title"
      role="dialog"
      aria-modal="true"
      onClick={(e) => {
        e.stopPropagation();
        setshowDetailModal(false);
      }}
    >
      <div className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"></div>

      <div className="fixed inset-0 z-10 w-screen overflow-y-auto">

        <div className="flex min-h-full items-center justify-center p-4 text-center ">
          <div
            className="relative transform overflow-hidden rounded-lg bg-white text-left shadow-xl transition-all  w-full max-w-[900px]"
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            <RxCrossCircled onClick={()=>{
              setshowDetailModal(false)
            }} className="absolute right-1 top-1 cursor-pointer text-red-600" size={22}/>
            {loader ? (
              <div className="flex justify-center p-10">
                <CircularProgress />
              </div>
            ) : (
              <div className="bg-white px-4 py-4 ">
                <div className="sm:flex sm:items-start">
                  <div className="font-semibold text-2xl py-2">
                    Offer Detail
                  </div>
                </div>
                <div className="grid grid-cols-1 md:grid-cols-2">
                  <div className="flex items-center  ">
                    <p className="font-semibold w-56">Offer Name: </p>
                    <p className="text-[14px]">{OfferDetail?.title}</p>
                  </div>
                  <div className="flex items-center  ">
                    <p className="font-semibold w-56">Discount Percentage: </p>
                    <p className="text-[14px]">
                      {OfferDetail?.discount_percentage}
                    </p>
                  </div>
                  <div className="flex items-center  ">
                    <p className="font-semibold w-56">Start Date: </p>
                    <p className="text-[14px]">
                      {moment(OfferDetail?.start_date).format("DD-MMM-YY")}
                    </p>
                  </div>
                  <div className="flex items-center  ">
                    <p className="font-semibold w-56">End Date: </p>
                    <p className="text-[14px]">
                      {moment(OfferDetail?.end_date).format("DD-MMM-YY")}
                    </p>
                  </div>
                </div>
                <div className="flex flex-col items-start">
                  <p className="font-semibold w-56">Description: </p>
                  <p className="text-[14px]" dangerouslySetInnerHTML={{ __html: OfferDetail?.description }}></p>
                  {/* {OfferDetail?.description} */}
                </div>
                <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-2 w-full py-2">
                  {OfferDetail?.services_id?.map((item, index) => (
                    <div
                      className="flex flex-col gap-4 rounded-md items-center justify-center p-3 1400px:p-3 border-dashed border border-bgBlue flex-grow "
                      key={index + 33 * 8}
                    >
                      <div
                        className={`${
                          index % 2 ? `bg-[#D8F8FF]` : "bg-[#FAD8FF]"
                        } pl-3  w-full   rounded-md shadow-lg hover:text-white hover:bg-black`}
                     
                      >
                        <div
                          className={`flex flex-col gap-2 p-2 bg-${item?.bg} `}
                        >
                          <div className="flex items-center justify-between gap-2 flex-wrap">
                            <div className="flex gap-2"></div>
                            {item?.verified && (
                              <p className="px-2 py-1 text-sm rounded-md bg-blue-500 text-white">
                                Verified
                              </p>
                            )}
                            {item?.online && (
                              <p className="px-2 py-1 text-sm rounded-md bg-green-800 text-white">
                                Online
                              </p>
                            )}
                          </div>
                          <div className="w-full flex flex-wrap 500px:flex-nowrap gap-4 relative overflow-hidden ">
                            {item?.featured && (
                              <div className="ribbon absolute -bottom-[6.5rem] -right-[5rem] h-[10.5rem] w-40 overflow-hidden before:absolute before:top-0 before:right-0 before:-z-[10000] before:border-4 before:border-blue-500 after:absolute after:left-0 after:bottom-0 after:-z-[1] after:border-4 after:border-blue-500">
                                <div className="absolute -left-[4rem] top-[43px] w-60 -rotate-[40deg] bg-gradient-to-br from-yellow-600 via-yellow-400 to-yellow-500 py-2.5 text-center text-white shadow-md flex items-center justify-center">
                                  <BsFillLightningChargeFill className="ml-7 mb-2" />
                                </div>
                              </div>
                            )}
                            <Image
                              src={item?.image_url}
                              alt="img service"
                              width={100}
                              height={70}
                              className="w-16 h-28"
                            />
                            <div>
                              <h1 className="text-xl font-semibold">
                                {item?.title}
                              </h1>
                              <p dangerouslySetInnerHTML={{ __html: item?.description }} className="text-sm z-50 max-w-[700px] line-clamp-2">
                                {/* {item?.description} */}
                                {/* Lorem ipsum, dolor sit amet consectetur
                                adipisicing elit. Tenetur, aut! Fugit totam ab
                                odio modi quos exercitationem, ipsam itaque
                                optio ex aut nam accusamus error minus,
                                architecto quasi, reprehenderit eum? */}
                              </p>
                              <span className=" text-green-600 font-semibold">
                                Starting From:{" "}
                              </span>
                              <span className="font-semibold">
                                ${item?.starting_price} USD
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default OfferDetailModal;
