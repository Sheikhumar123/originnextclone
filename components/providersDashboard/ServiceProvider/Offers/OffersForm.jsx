import React, { useEffect, useState } from "react";
import { Autocomplete, Chip, CircularProgress, TextField } from "@mui/material";
import { toast } from "react-toastify";
import { useDispatch, useSelector } from "react-redux";
import {
  GetProviderServicesPopulate,
  GetServiceProviderDetails,
} from "@/redux/Slices/ServiceProviderSlice/ServiceProviderSlice";
import Cookies from "js-cookie";
import { useFormik } from "formik";
import * as Yup from "yup";
import {
  AddOffer,
  UpdateOffer,
} from "@/Apis/ServiceProviderApis/DashboardApi/OffersApi/OffersApi";
import moment from "moment";

import dynamic from "next/dynamic";

const Editor = dynamic(
  () => {
    return import("react-draft-wysiwyg").then((mod) => mod.Editor);
  },
  { ssr: false }
);
// import { EditorState } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { ContentState, EditorState, convertToRaw } from "draft-js";

const ValidationSchema = Yup.object().shape({
  title: Yup.string().required("Please Enter Title"),
  // description: Yup.string().required("Please Enter Description"),
  description: Yup.mixed()
    .test("is-string-or-editorstate", "Description is required", (value) => {
      return typeof value === "string" || value instanceof EditorState;
    })
    .required("Description is required"),
  start_date: Yup.date().required("Please Select Start Date"),

  end_date: Yup.date()
    .required("Please Select End Date")
    .min(Yup.ref("start_date"), "End date must be greater than start date"),

  discount_percentage: Yup.string()
    .required("Please Enter Offer Percentage")
    .max(100),
  services_id: Yup.array()
    .min(1, "Please Select Atleast one service")
    .required("test"),
});

function OffersForm({ setshowForm, rowData, isEdit, setrowData, setisEdit }) {
  const [loader, setloader] = useState(false);
  const { ProviderServices } = useSelector(GetServiceProviderDetails);
  const formik = useFormik({
    initialValues: {
      title: "",
      start_date: "",
      end_date: "",
      description: EditorState.createEmpty(),
      discount_percentage: 0,
      user_id: Cookies.get("user_id"),
      type: "ServiceProvider",
      services_id: [],
      selectedServices: [],
    },
    validationSchema: ValidationSchema,
    onSubmit: (values, { resetForm, setSubmitting }) => {
      handleSubmit(values, resetForm, setSubmitting);
    },
  });
  const dispatch = useDispatch();

  const handleInputChange = (value) => {
    formik.setFieldValue(
      "services_id",
      value.map((sevice) => sevice.id)
    );
    formik.setFieldValue("selectedServices", value);
  };
  useEffect(() => {
    dispatch(
      GetProviderServicesPopulate({
        user_id: Cookies.get("user_id"),
        type: "ServiceProvider",
      })
    );
  }, [dispatch]);
  const PrefillData = async () => {
    if (rowData && isEdit) {
      formik.setFieldValue("id", rowData?.id);
      formik.setFieldValue("title", rowData?.title);
      formik.setFieldValue("start_date", rowData?.start_date);
      formik.setFieldValue("end_date", rowData?.end_date);
      // formik.setFieldValue("description", rowData?.description);
      const htmlToDraft = (await import("html-to-draftjs")).default;

      let body = htmlToDraft(rowData?.description ? rowData?.description : "");
      console.log(body);
      if (body?.contentBlocks) {
        const contentState = ContentState.createFromBlockArray(
          body?.contentBlocks
        );
        console.log(contentState);
        const editorState = EditorState.createWithContent(contentState);
        console.log(editorState);
        formik.setFieldValue(`description`, editorState);
        // setEditorState(editorState);
      }
      formik.setFieldValue("discount_percentage", rowData?.discount_percentage);
      formik.setFieldValue(
        "services_id",
        rowData?.services_id.map((service) => service?.id)
      );
      formik.setFieldValue("selectedServices", rowData?.services_id);
    }
  };

  useEffect(() => {
    PrefillData();
  }, [rowData]);

  const handleSubmit = async (values) => {
    const { convertToHTML } = await import("draft-convert");

    let obj = null;
    obj = {
      ...values,
      description: convertToHTML(values?.description.getCurrentContent()),
    };
    setloader(true);
    let response = null;
    if (isEdit) {
      response = await UpdateOffer(obj);
    } else {
      response = await AddOffer(obj);
    }
    setloader(false);

    if (response?.data?.status === "1") {
      toast.success(response?.data?.message);
      setrowData(null);
      setisEdit(false);
      setshowForm(false);
    } else {
      toast.error(response?.data?.message);
    }
  };

  return (
    <div className="p-4 border border-gray-100  rounded-2xl shadow-md w-full 800px:min-w-[600px] flex flex-col">
      <form onSubmit={formik.handleSubmit}>
        <h1 className="text-xl label font-semibold capitalize">Add Offer</h1>
        <div className="overflow-y-auto h-[37rem] p-4">
          <div className="mb-8 p-2 rounded-xl shadow-md relative">
            <div className="grid grid-cols-1 md:grid-cols-2  md:gap-4">
              <div>
                <label htmlFor={`offerTitle`} className="label text-md  block">
                  Title
                </label>
                <input
                  id={`offerTitle`}
                  type="text"
                  value={formik.values.title}
                  onChange={formik.handleChange}
                  name="title"
                  className="w-full h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="Enter Offer title"
                />
                {formik.errors.title &&
                  formik.touched.title &&
                  formik.errors.title && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.title}
                    </p>
                  )}
              </div>
              <div>
                <label
                  htmlFor={`discount_percentage`}
                  className="label text-md  block"
                >
                  Discount Percentage
                </label>
                <input
                  id={`discount_percentage`}
                  type="number"
                  value={formik.values.discount_percentage}
                  onChange={formik.handleChange}
                  name="discount_percentage"
                  className="w-full h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                  placeholder="percentage"
                />
                {formik.errors.discount_percentage &&
                  formik.touched.discount_percentage &&
                  formik.errors.discount_percentage && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.discount_percentage}
                    </p>
                  )}
              </div>
            </div>
            <div className="grid grid-cols-1 md:grid-cols-2  md:gap-4">
              <div>
                <label htmlFor={`start_date`} className="label text-md  block">
                  Start Date
                </label>
                <input
                  id={`start_date`}
                  type="date"
                  value={formik.values.start_date}
                  onChange={formik.handleChange}
                  name="start_date"
                  className="w-full h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                />
                {formik.errors.start_date &&
                  formik.touched.start_date &&
                  formik.errors.start_date && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.start_date}
                    </p>
                  )}
              </div>
              <div>
                <label htmlFor={`end_date`} className="label text-md  block">
                  End Date
                </label>
                <input
                  id={`end_date`}
                  type="date"
                  value={formik.values.end_date}
                  onChange={formik.handleChange}
                  name="end_date"
                  className="w-full h-10 px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                />
                {formik.errors.end_date &&
                  formik.touched.end_date &&
                  formik.errors.end_date && (
                    <p className="text-red-700 text-sm">
                      {formik.errors.end_date}
                    </p>
                  )}
              </div>
            </div>

            <div className="flex flex-col gap-2 mb-2">
              <label htmlFor="services" className="text-md label">
                Select Services
              </label>
              <Autocomplete
                multiple
                limitTags={2}
                size="small"
                id="multiple-limit-tags-services"
                options={ProviderServices || []}
                value={formik.values.selectedServices}
                onChange={(event, newValue) => {
                  console.log(newValue);
                  handleInputChange(newValue, "subcategories");
                }}
                renderOption={(props, option) => (
                  <li {...props}>{option?.title}</li>
                )}
                renderTags={(value, getTagProps) =>
                  value.map((option, index) => (
                    <Chip
                      key={option.title + index}
                      label={option?.title}
                      {...getTagProps({ index })}
                      style={{ backgroundColor: "#c7c9f4" }}
                    />
                  ))
                }
                renderInput={(params) => (
                  <TextField
                    {...params}
                    sx={{
                      "& .MuiOutlinedInput-root": {
                        borderRadius: "8px",
                      },
                      color: "#9E9E9E",
                      width: "100%",
                    }}
                  />
                )}
                isOptionEqualToValue={(option, value) =>
                  option?.id === value?.id
                }
              />
              {formik.errors.services_id &&
                formik.touched.services_id &&
                formik.errors.services_id && (
                  <p className="text-red-700 text-sm">
                    {formik.errors.services_id}
                  </p>
                )}
            </div>
            <div>
              <label htmlFor={`description`} className="label text-md block">
                Description
              </label>
              <Editor
                placeholder="Enter description"
                name={`description`}
                editorState={formik.values.description}
                onEditorStateChange={(editorState) => {
                  formik.setFieldValue("description", editorState);
                }}
                editorStyle={{
                  backgroundColor: "#ffffff",
                  // height:"100px"
                }}
                className="w-full px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
              />
              {/* <textarea
                id={`description`}
                rows={8}
                cols={20}
                value={formik.values.description}
                onChange={formik.handleChange}
                name="description"
                className="w-full px-4 py-2 text-sm border rounded-md focus:border-blue-400 focus:outline-none focus:ring-1 focus:ring-blue-600"
                placeholder="Enter description"
              /> */}
              {formik.errors.description &&
                formik.touched.description &&
                formik.errors.description && (
                  <p className="text-red-700 text-sm">
                    {formik.errors.description}
                  </p>
                )}
            </div>
          </div>
        </div>

        <div className="flex justify-end items-center">
          <div className="flex gap-2 items-center">
            <button
              type="button"
              className="px-6 py-2 rounded-md bg-red-600 text-white self-end mt-4"
              onClick={() => {
                setrowData(null);
                setisEdit(null);
                setshowForm(false);
              }}
            >
              Cancel
            </button>
            {loader ? (
              <div className="self-end">
                <CircularProgress />
              </div>
            ) : (
              <button
                type="submit"
                className="px-8 py-2 rounded-md bg-blue-500 text-white self-end mt-4"
              >
                {"Save"}
              </button>
            )}
          </div>
        </div>
      </form>
    </div>
  );
}

export default OffersForm;
