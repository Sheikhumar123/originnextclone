"use client";
import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import Image from "next/image";
import { FaLinkedin } from "react-icons/fa6";
import { SlFlag } from "react-icons/sl";
import { Rating } from "@smastrom/react-rating";
import SlickLeftArrow from "../SlickArrows/SlickLeftArrow";
import SlickRightArrow from "../SlickArrows/SlickRightArrow";

function ClientsReviewSlider({ categoryReviews }) {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: categoryReviews?.length < 6 ? categoryReviews?.length : 1.65,
    slidesToScroll: 1,
    prevArrow: <SlickLeftArrow />,
    nextArrow: <SlickRightArrow />,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 700,
        settings: {
          slidesToShow:
            categoryReviews?.length < 2 ? categoryReviews?.length : 1,
          slidesToScroll: 1,
        },
      }, // 1 item on small screens
      {
        breakpoint: 750,
        settings: {
          slidesToShow:
            categoryReviews?.length < 3 ? categoryReviews?.length : 1.65,
          slidesToScroll: 1,
        },
      }, // 2 items on screens with breakpoint > 500px

    ],
  };
//   800px:min-w-[550px] 400px:min-w-[350px] min-w-[300px]
  return (
    <>
      <Slider style={{}} {...settings}>
        {categoryReviews?.map((item, i) => {
          return (
            <div key={i + 978 * 2} className="700px:px-6 300px:px-1">
              <div className="p-6 rounded-md  shadow-xl w-[full]   bg-white text-black   ">
                <div className="flex 300px:flex-row flex-col items-start 400px:items-center 300px:justify-between">
                  <div className="mb-2 flex items-center gap-2  w-auto 400px:flex-row flex-col">
                    <div className=" w-[45px] h-[45px] rounded-full overflow-hidden">
                      <Image
                        src={item.image}
                        style={{ width: "100%", height: "100%" }}
                        alt="profile img"
                        className=" object-cover "
                        width={500}
                        height={500}
                      />
                    </div>
                    <div className="flex flex-col ">
                      <h1 className="font-semibold">{item.review_from}</h1>
                      <p className="text-sm">{item?.postition || "Position"}</p>
                    </div>
                  </div>
                  <div className="flex items-center gap-2">
                    <FaLinkedin size={25} fill="#0E76A8" />{" "}
                    <h1 className="font-semibold">Verified</h1>
                  </div>
                </div>
                <Rating
                  style={{ maxWidth: 150 }}
                  value={Math.round(Number(item?.rating))}
                />
                <p className="mt-2 text-[#676767] mb-2">{item?.review}</p>
                <hr />
                <div className="flex items-center justify-between gap-2 600px:gap-0 mt-4 flex-wrap 600px:flex-nowrap text-nowrap">
                  <div className="flex items-center gap-2 500px:gap-6 font-[600] flex-wrap 600px:flex-nowrap ">
                    <h1>{item.date}</h1>
                    <h1>Helpful (1)</h1>
                    <h1>Share</h1>
                  </div>
                  <div className=" items-center gap-2 hidden 800px:flex">
                    <SlFlag fill="#f21a1a" size={20} />
                    <h1 className="text-[#f21a1a] font-semibold">Report</h1>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </Slider>
    </>
  );
}

export default ClientsReviewSlider;
