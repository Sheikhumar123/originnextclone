// "use client";
import { Rating } from "@smastrom/react-rating";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import { FaLinkedin } from "react-icons/fa6";
import { IoIosArrowDropright } from "react-icons/io";
const CategoryUser = ({
  categoryUsers,
  params,
  categoryUsersLimit,
  categoryUsersCount,
}) => {
  console.log("headersList", categoryUsers);
  const abc =
    "One might say that descriptive writing is the art of painting a picture with words. But descriptive writing goes beyond visuals. Descriptive writing hits all the senses; we describe how things look, sound, smell, taste, and feel (their tactile quality). The term descriptive writingcan mean a few different things: 1. The act of writing descriptioa picture with words. But descriptive writing a picture with words. But descriptive writing a picture with words. But descriptive writing";
  return (
    <div>
      <div
        // className="w-full flex gap-3 flex-wrap"
        className="grid 2000px:grid-cols-5 1300px:grid-cols-4 900px:grid-cols-3 700px:grid-cols-2 300px:grid-cols-1 gap-4 w-full"
      >
        {categoryUsers?.map((item, key) => (
          // w-[400px] flex-grow max-w-[450px]
          <div key={key + 978 * 2} className="p-6 rounded-xl  shadow-xl ">
            <div className="flex items-center justify-between">
              <div className="mb-2 flex items-center gap-2 w-auto">
                <div className=" w-[45px] h-[45px] rounded-full overflow-hidden">
                  <Image
                    src={item.profile_image}
                    style={{ width: "100%", height: "100%" }}
                    alt="profile img"
                    className=" object-cover "
                    width={500}
                    height={500}
                  />
                </div>
                <div className="flex flex-col ">
                  <h1 className="font-semibold">{item.name}</h1>
                  <p className="text-sm">{item.title}</p>
                </div>
              </div>
              <div className="flex items-center gap-2">
                <FaLinkedin size={25} fill="#0E76A8" />{" "}
                <h1 className="font-semibold">Verified</h1>
              </div>
            </div>
            <Rating
              style={{ maxWidth: 150 }}
              value={Math.round(Number(item?.average_rating))}
            />
            <p
              className={`mt-2 text-[#676767] mb-2 line-clamp-4 h-[100px] overflow-hidden`}
              dangerouslySetInnerHTML={{ __html: item?.description }}
            >
              {/* {item?.description} */}
            </p>
            <hr />
            <div className="flex items-center justify-between gap-2 600px:gap-0 mt-4 flex-wrap 600px:flex-nowrap text-nowrap">
              <Link href={`/home/listings/listingdetail/${item?.user_id}`}>
                <button className="px-4 py-2 rounded-lg bg-bgBlue text-white font-semibold">
                  View Profile
                </button>
              </Link>
              <span className="text-green-600 font-bold">
                {item?.starting_price
                  ? item?.starting_price?.toLocaleString()
                  : 0}
                $ {item?.duration}
              </span>
            </div>
          </div>
        ))}
      </div>
      {categoryUsersCount > categoryUsersLimit ? (
        <div className="w-[fit-content]">
          <Link href={`/categories/allprovider?id=${params?.category}`}>
            <button className="flex items-center w-[fit-content] gap-2 text-bgBlue mt-4 800px:mt-8">
              <h1 className="font-bold">View All</h1>
              <IoIosArrowDropright size={20} />
            </button>
          </Link>
        </div>
      ) : null}
    </div>
  );
};

export default CategoryUser;
