
import React from "react";
import ClientsReviewSlider from "./ClientsReviewSlider";
function ClientsReview({ categoryReviews }) {

  return (
    <div className="w-full bg-bg-gradient-slider bg-no-repeat text-white bg-cover 700px:p-8 300px:p-2">
      <h1 className="text-[2rem] font-bold text-center mb-8">
        Client Reviews For Our Service Provider
      </h1>
      <div className="relative pb-16 ">

    <ClientsReviewSlider categoryReviews={categoryReviews}/>
      </div>
      {/* <div className="overflow-x-auto !scrollbar-none flex items-center gap-12">
        {categoryReviews.map((item, key) => (
          <div
            key={key + 978 * 2}
            className="p-6 rounded-md  shadow-xl 800px:min-w-[550px] 400px:min-w-[350px] min-w-[300px]  bg-white text-black   "
          >
            <div className="flex 300px:flex-row flex-col items-start 400px:items-center 300px:justify-between">
              <div className="mb-2 flex items-center gap-2  w-auto 400px:flex-row flex-col">
                <div className=" w-[45px] h-[45px] rounded-full overflow-hidden">
                  <Image
                    src={item.image}
                    style={{ width: "100%", height: "100%" }}
                    alt="profile img"
                    className=" object-cover "
                    width={500}
                    height={500}
                  />
                </div>
                <div className="flex flex-col ">
                  <h1 className="font-semibold">{item.review_from}</h1>
                  <p className="text-sm">{item?.postition || "Position"}</p>
                </div>
              </div>
              <div className="flex items-center gap-2">
                <FaLinkedin size={25} fill="#0E76A8" />{" "}
                <h1 className="font-semibold">Verified</h1>
              </div>
            </div>
            <Rating
              style={{ maxWidth: 150 }}
              value={Math.round(Number(item?.rating))}
            />
            <p className="mt-2 text-[#676767] mb-2">{item?.review}</p>
            <hr />
            <div className="flex items-center justify-between gap-2 600px:gap-0 mt-4 flex-wrap 600px:flex-nowrap text-nowrap">
              <div className="flex items-center gap-2 500px:gap-6 font-[600] flex-wrap 600px:flex-nowrap ">
                <h1>{item.date}</h1>
                <h1>Helpful (1)</h1>
                <h1>Share</h1>
              </div>
              <div className=" items-center gap-2 hidden 800px:flex">
                <SlFlag fill="#f21a1a" size={20} />
                <h1 className="text-[#f21a1a] font-semibold">Report</h1>
              </div>
            </div>
          </div>
        ))}
      </div> */}
    </div>
  );
}

export default ClientsReview;
