"use client";
import React, { useEffect, useState } from "react";
import { Rating } from "@smastrom/react-rating";
import Image from "next/image";
import { FaLinkedin } from "react-icons/fa6";
import { IoIosArrowDropright } from "react-icons/io";
import axiousInstance from "@/utils/axiousInstance";
import { toast } from "react-toastify";
import ScreenLoader from "../ScreenLoader/ScreenLoader";
import Link from "next/link";
function CategoryAllProviders({ categoryUsers, id }) {
  const [allProvidersData, setallProvidersData] = useState([]);
  const [loader, setloader] = useState(false);
  const [page, setpage] = useState(1);
  const [count, setcount] = useState(0);
  const [limit, setlimit] = useState(1);
  console.log(count);
  const handleLoadMore = async () => {
    setloader(true);

    try {
      const response = await axiousInstance.get(
        `landingpage/categoryusers?id=${id}&page=${page + 1}`
      );
      setloader(false);
      if (response.data.status === "1") {
        setallProvidersData((prevData) => [
          ...prevData,
          ...response?.data?.allusers,
        ]);

        setcount(response?.data?.count);
        setlimit(response?.data?.limit);
      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error?.response?.data?.error);
    }
  };
  const getAllCategories = async () => {
    setloader(true);

    try {
      const response = await axiousInstance.get(
        `landingpage/categoryusers?id=${id}&page=${page}`
      );
      setloader(false);
      if (response.data.status === "1") {
        setallProvidersData(response?.data?.allusers);
        // setallProvidersData(response?.data?.categories);
        setcount(response?.data?.count);
        setlimit(response?.data?.limit);
      } else {
        toast.error(response.data.message);
      }
    } catch (error) {
      setloader(false);
      toast.error(error?.response?.data?.error);
    }
  };
  useEffect(() => {
    getAllCategories();
  }, []);
  return (
    <>
      {loader && <ScreenLoader />}
      <div className="bg-white">
        <h1 className="text-[2rem] text-center font-bold my-4">
          {/* Hire Website Designer */}
          Hire Service Provider
        </h1>

        <div
          // className="w-full flex gap-3 flex-wrap"
          className="grid 2000px:grid-cols-5 1300px:grid-cols-4 900px:grid-cols-3 700px:grid-cols-2 300px:grid-cols-1 gap-4 w-full"
        >
          {allProvidersData?.map((item, key) => (
            // w-[400px] flex-grow max-w-[450px]
            <div key={key + 978 * 2} className="p-6 rounded-xl  shadow-xl ">
              <div className="flex items-center justify-between">
                <div className="mb-2 flex items-center gap-2 w-auto">
                  <div className=" w-[45px] h-[45px] rounded-full overflow-hidden">
                    <Image
                      src={item.profile_image}
                      style={{ width: "100%", height: "100%" }}
                      alt="profile img"
                      className=" object-cover "
                      width={500}
                      height={500}
                    />
                  </div>
                  <div className="flex flex-col ">
                    <h1 className="font-semibold">{item?.name}</h1>
                    <p className="text-sm">{item?.title}</p>
                  </div>
                </div>
                <div className="flex items-center gap-2">
                  <FaLinkedin size={25} fill="#0E76A8" />{" "}
                  <h1 className="font-semibold">Verified</h1>
                </div>
              </div>
              <Rating
                style={{ maxWidth: 150 }}
                value={Math.round(Number(item?.average_rating))}
              />
              <p className="mt-2 text-[#676767] mb-2">{item?.average_rating}</p>
              <hr />
              <div className="flex items-center justify-between gap-2 600px:gap-0 mt-4 flex-wrap 600px:flex-nowrap text-nowrap">
                <Link href={`/home/listings/listingdetail/${item?.user_id}`}>
                  <button className="px-4 py-2 rounded-lg bg-bgBlue text-white font-semibold">
                    View Profile
                  </button>
                </Link>
                <span className="text-green-600 font-bold">
                  {item?.starting_price
                    ? item?.starting_price?.toLocaleString()
                    : 0}
                  $ Per Hour
                </span>
              </div>
            </div>
          ))}
        </div>
        {count > allProvidersData?.length ? (
          <div className="w-full flex justify-center">
            <button
              type="button"
              onClick={handleLoadMore}
              className=" bg-[#434CD9] w-auto  px-12 text-nowrap py-2 mt-4 rounded-3xl text-white"
            >
              Load More
            </button>
          </div>
        ) : null}
        {/* <div className="w-full flex justify-center" >
        <button
          onClick={handleLoadMore}
          className="flex items-center gap-2 self-center text-bgBlue mt-4 800px:mt-8"
        >
          <h1 className="font-bold">Load More</h1>
          <IoIosArrowDropright size={20} />
        </button>
        </div> */}
      </div>
    </>
  );
}

export default CategoryAllProviders;
