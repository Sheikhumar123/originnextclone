import React from "react";
import Image from "next/image";
import { featuredImgDataorLandingPages } from "@/data/imagesData";
import { FaLinkedin } from "react-icons/fa6";
import { hiringProcessData } from "@/data/categoriesLandingPageData";
import { SlFlag } from "react-icons/sl";
import { IoIosArrowRoundForward } from "react-icons/io";
import { Rating } from "@smastrom/react-rating";
import CategoryUser from "./CategoryUser";
import ClientsReview from "./ClientsReview";
const CategoryDetailPage = ({
  categorydata,
  categoryUsers,
  categoryReviews,
  params,
  categoryUsersLimit,
  categoryUsersCount
}) => {
  const { title, coverImg, description1, description2 } = categorydata;
  return (
    <div className="w-full">
      {/* HERO SECTION  */}
      <div className="w-full bg-no-repeat   text-white  flex flex-col justify-between relative">
        <div className="absolute w-full h-full">
          <Image
            src={coverImg?coverImg:""}
            alt="brand image"
            layout="fill"
            className="object-fill h-full"
          />
        </div>
        <div className="flex w-full 900px:w-[58%] flex-col gap-4 px-12 pt-12 pb-4 z-50">
          <h1 className="uppercase text-[2rem] 500px:text-[4.1rem] font-semibold leading-[1.2]">
            Hire expert {title?title:""} for any jobssss
          </h1>
          <p className="text-lg w-[90%]">
            Lorem ipsum dolor, sit amet consectetur adipisicing elit.
            Necessitatibus, cum. Dolores, commodi minima esse explicabo,
            corrupti aliquid magni consectetur, blanditiis rerum temporibus qui
            officiis odit.
          </p>
          <button className="500px:px-6 px-3 500px:py-4 py-2 rounded-md bg-[#6366F1] max-w-[300px] text-sm 500px:text-md">
            Hire Top {title} Now
          </button>
        </div>
        <div className="hidden 700px:block w-full bg-white/5 px-8 py-2 1200px:mb-8 mt-8 z-50">
          <h1 className="mb-2">Trusted by 180,000+ customers worldwide</h1>
          <div className="flex items-center justify-start gap-2 700px:justify-between flex-wrap">
            {featuredImgDataorLandingPages.map((item, index) => (
              <Image
                src={item}
                alt="brand image"
                key={index + 0.03 * 9}
                width={150}
                height={150}
              />
            ))}
          </div>
        </div>
      </div>
      {/* INTRO SECTION  */}
      <div className="w-full p-4 500px:p-12">
        <div className="pb-2 border border-white rounded-[20px] border-b  bg-gradient-to-br from-indigo-500 to-teal-400 max-w-[800px] mx-auto">
          <div className=" bg-white rounded-xl p-8 border border-white">
            <div className="mb-4">
              <p className="text-md">{description1?description1:""}</p>
              {/* <h1 className="text-[1.3rem] font-semibold rounded-3xl">
                Types Of Website Design?
              </h1>
              <p className="text-md">
                When considering the term website design more often consumers
                reference the process of creating visual elements of a website
                for distribution to end-users, via the internet. The color
                schemes, typography or fonts, imagery, layout and overall
                graphical appearance are typically forefront of one’s mind as a
                visible demonstration of careful planning and design formation.{" "}
              </p> */}
            </div>
            {/* <div className="mb-4">
              <h1 className="text-[1.3rem] font-semibold rounded-3xl">
                What Is Website Design?
              </h1>
              <p className="text-sm">
                When considering the term website design more often consumers
                reference the process of creating visual elements of a website
                for distribution to end-users, via the internet. The color
                schemes, typography or fonts, imagery, layout and overall
                graphical appearance are typically forefront of one’s mind as a
                visible demonstration of careful planning and design formation.
              </p>
            </div> */}
          </div>
        </div>
      </div>
      {/* HIRE WEBSITE DESIGNER SECTION  */}
      <div className="w-full p-4 500px:p-12 ">
        <h1 className="text-[2rem] font-bold mb-4">
          {/* Hire Website Designer */}
          Hire Service Provider

          </h1>
        <CategoryUser categoryUsersCount={categoryUsersCount} categoryUsersLimit={categoryUsersLimit} params={params} categoryUsers={categoryUsers} />
      </div>
      {/* REVIEWS SECTION  */}
  <div>
    <ClientsReview categoryReviews={categoryReviews} />
    </div>
      {/* DETAILS OF PAGE  */}
      <div className="w-full p-4 500px:p-12">
        <h1 className="text-[2rem] font-bold mb-4 900px:w-1/2 uppercase">
          How to hire a great freelance {title}
        </h1>
        <p>{description2?description2:""}</p>
      </div>
      {/* HIRING PROCESS DATA  */}
      <div className="w-full p-4 500px:p-12">
        <h1 className="text-[2rem] font-bold mb-4  uppercase text-center">
          Process is of hiring
        </h1>
        <div className="flex items-center justify-between flex-wrap w-full">
          {hiringProcessData.map((item, index) => (
            <div
              className={`flex flex-col  mx-auto w-[250px] 400px:p-2 ${
                index !== hiringProcessData.length - 1 &&
                "1100px:[border-image:linear-gradient(to_top_right,#6D48E5,#6D48E55E)_30]  border-r-[3px] border-solid border-transparent "
              } shadow-sm gap-6 p-4 rounded-xl 400px:shadow-none 400px:gap-0  400px:rounded-none`}
              key={index + 0.639 * 2}
            >
              <Image
                src={item.imgUrl}
                alt="hiring process step image"
                width={75}
                height={75}
              />
              <div>
                <h1 className="font-semibold capitalize text-lg">
                  {item.title}
                </h1>
                <p>{item.text}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
      {/* GET STARTED SECTION  */}
      <div className="w-full p-4 500px:p-12">
        <div className="max-w-[900px] mx-auto rounded-3xl bg-bg-gradient-started bg-cover bg-no-repeat p-12 text-white">
          <h1 className="font-semibold text-[2rem]">
            So what are you waiting for?
          </h1>
          <p className="mb-8">
            Post a project today and get bids from talented freelancers
          </p>
          <button className="px-8 py-3 rounded-3xl text-black bg-white flex items-center gap-2">
            <p>Get Started</p>
            <IoIosArrowRoundForward size={25} />
          </button>
        </div>
      </div>
    </div>
  );
};

export default CategoryDetailPage;
