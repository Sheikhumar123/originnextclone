// import usePagination from "@/hooks/paginationHook";
import usePagination from "@/hook/paginationHook";
import React, { useEffect } from "react";

function Pagination({ pageCount, setPageCount, limit, totalcount, showCount }) {
  const [FirstPage, LastPage, paginationArr, SetpaginationData] =
    usePagination();

  useEffect(() => {
    if (limit && totalcount) {
      SetpaginationData(limit, totalcount, pageCount);
    }
  }, [pageCount, limit, totalcount]);

  return (
    <nav
      className="flex gap-2 flex-column md:flex-row flex-wrap justify-between items-center p-2"
      aria-label="Table navigation"
    >
      {showCount ? (
        <span className="text-sm font-normal text-gray-500 dark:text-gray-400 mb-4 md:mb-0 block w-full md:inline md:w-auto">
          Showing{" "}
          <span className="font-semibold text-gray-500 dark:text-white">
            {FirstPage} - {LastPage}
          </span>{" "}
          of{" "}
          <span className="font-semibold text-gray-500 dark:text-white">
            {totalcount}
          </span>
        </span>
      ) : (
        <></>
      )}
      <ul className="inline-flex -space-x-px rtl:space-x-reverse text-sm h-8">
        <li>
          <span
            className={`flex justify-center items-center px-3 h-8 ms-0 leading-tight  text-gray-500 bg-white border border-gray-300 rounded-s-lg hover:bg-gray-100 hover:text-gray-700 ${
              pageCount === 1 ? "cursor-not-allowed" : "cursor-pointer"
            } transition-colors`}
            onClick={() => {
              if (pageCount > 1) {
                setPageCount((count) => count - 1);
              }
            }}
          >
            Previous
          </span>
        </li>
        {paginationArr?.map((page, idx) => {
          if (
            idx == 0 ||
            idx == paginationArr?.length - 1 ||
            page == pageCount - 1 ||
            page == pageCount ||
            page == pageCount + 1
          ) {
            return (
              <li key={page}>
                <span
                  className={`flex items-center justify-center px-3 h-8 leading-tight ${
                    pageCount === page
                      ? "text-blue-600 bg-blue-50 hover:text-blue-700 hover:bg-blue-100"
                      : "text-gray-500 hover:text-gray-700 hover:bg-gray-100"
                  }  border border-gray-300 bg-white cursor-pointer transition-colors`}
                  onClick={() => {
                    setPageCount(page);
                  }}
                >
                  {page}
                </span>
              </li>
            );
          }
        })}
        <li>
          <span
            className={`flex items-center justify-center px-3 h-8 leading-tight text-gray-500 bg-white border border-gray-300 rounded-e-lg hover:bg-gray-100 hover:text-gray-700 ${
              pageCount == paginationArr?.length
                ? "cursor-not-allowed"
                : "cursor-pointer"
            } transition-colors`}
            onClick={() => {
              if (pageCount != paginationArr?.length) {
                setPageCount((count) => count + 1);
              }
            }}
          >
            Next
          </span>
        </li>
      </ul>
    </nav>
  );
}

export default Pagination;
