const RadialProgress = ({ colorClass, value, size, textSize }) => {
  return (
    <div
      className={`radial-progress ${colorClass} text-[${textSize}]`}
      style={{ "--value": 70, "--size": size }}
      role="progressbar"
    >
      <p className="font-semibold"> {value}%</p>
    </div>
  );
};

export default RadialProgress;
