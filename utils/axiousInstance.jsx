import axios from "axios";
import { baseUrl } from "./BaseURL";
import Cookies from "js-cookie";

const axiousInstance = axios.create({
  baseURL: `${baseUrl}`,
  headers: {
    "Content-Type": "application/json",
    Authorization: Cookies?.get("orignToken"),
  },
});

export default axiousInstance;
