"use client";
const WithdrawButton = ({ classNameString, text }) => {
  return (
    <button
      className={classNameString}
      onClick={() => document.getElementById("withdrawModal").showModal()}
    >
      {text}
    </button>
  );
};

export default WithdrawButton;
