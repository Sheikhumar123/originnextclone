"use client";
const ModalButton = ({ classNameString, openId, text }) => {
  return (
    <button
      className={classNameString}
      onClick={() => document.getElementById(`${openId}`).showModal()}
    >
      {text}
    </button>
  );
};

export default ModalButton;
