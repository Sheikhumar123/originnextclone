import { Poppins, Manrope } from "next/font/google";

export const poppins = Poppins({
  subsets: ["latin"],
  display: "swap",
  variable: "poppins",
  weight: ["100", "200", "300", "400", "500", "600", "700", "800", "900"],
});

export const manrope = Manrope({
  subsets: ["latin"],
  display: "swap",
  variable: "manrope",
  weight: ["variable"],
});
