import { MdOutlineDashboard } from "react-icons/md";
import { PiUsersFourThin, PiCurrencyCircleDollarBold } from "react-icons/pi";
import { IoMailUnreadOutline, IoCashOutline } from "react-icons/io5";
import { TbUserSquareRounded } from "react-icons/tb";
import { BiLogoPaypal } from "react-icons/bi";
import { FaBitcoin } from "react-icons/fa6";
import { TbCurrencyRupee } from "react-icons/tb";
import { TiVendorAndroid } from "react-icons/ti";
import { LuCodesandbox } from "react-icons/lu";
export const dashboardSidebarData = [
  {
    title: "Dashboard",
    icon: <MdOutlineDashboard size={25} />,
    id: 1,
    link: "/dashboard",
  },
  {
    title: "Invest Page",
    icon: <PiCurrencyCircleDollarBold size={25} />,
    id: 2,
    link: "/dashboard/investment",
  },
  {
    title: "Referral Page",
    icon: <PiUsersFourThin size={25} />,
    id: 3,
    link: "/dashboard/referrals",
  },
  {
    title: "Withdrawal",
    icon: <IoCashOutline size={25} />,
    id: 4,
    link: "/dashboard/withdrawal",
  },
  {
    title: "Become a Vendor",
    icon: <TiVendorAndroid size={25} />,
    id: 4,
    link: "/dashboard/vendor",
  },
  {
    title: "Codes",
    icon: <LuCodesandbox size={25} />,
    id: 4,
    link: "/dashboard/code",
  },
  {
    title: "Contact Us",
    icon: <IoMailUnreadOutline size={25} />,
    id: 5,
    link: "/dashboard/contact",
  },
  {
    title: "Profile",
    icon: <TbUserSquareRounded size={25} />,
    id: 6,
    link: "/dashboard/profile",
  },
];
export const dashboardMiniStats = [
  {
    title: "Invest",
    value: "1500",
    data: {
      chart: {
        height: "100%",
        maxWidth: "100%",
        type: "area",
        fontFamily: "Inter, sans-serif",
        dropShadow: {
          enabled: false,
        },
        toolbar: {
          show: false,
        },
      },
      tooltip: {
        enabled: true,
        x: {
          show: false,
        },
      },
      fill: {
        type: "gradient",
        gradient: {
          opacityFrom: 0.55,
          opacityTo: 0,
          shade: "#00A389",
          gradientToColors: ["#00A389"],
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        width: 3,
      },
      grid: {
        show: false,
        strokeDashArray: 4,
        padding: {
          left: 2,
          right: 2,
          top: 0,
        },
      },

      xaxis: {
        categories: [
          "01 February",
          "02 February",
          "03 February",
          "04 February",
          "05 February",
          "06 February",
          "07 February",
        ],
        labels: {
          show: false,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
      },
      yaxis: {
        show: false,
      },
    },
    series: [
      {
        name: "New users",
        data: [0, 200, 400, 250, 600, 700, 800, 1300, 850, 2000],
        color: "#00A389",
      },
    ],
  },
  {
    title: "Profit",
    value: "4.5 INR",
    data: {
      chart: {
        height: "100%",
        maxWidth: "100%",
        type: "area",
        fontFamily: "Inter, sans-serif",
        dropShadow: {
          enabled: false,
        },
        toolbar: {
          show: false,
        },
      },
      tooltip: {
        enabled: true,
        x: {
          show: false,
        },
      },
      fill: {
        type: "gradient",
        gradient: {
          opacityFrom: 0.55,
          opacityTo: 0,
          shade: "#FF5B5B",
          gradientToColors: ["#FF5B5B"],
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        width: 3,
      },
      grid: {
        show: false,
        strokeDashArray: 4,
        padding: {
          left: 2,
          right: 2,
          top: 0,
        },
      },

      xaxis: {
        categories: [
          "01 February",
          "02 February",
          "03 February",
          "04 February",
          "05 February",
          "06 February",
          "07 February",
        ],
        labels: {
          show: false,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
      },
      yaxis: {
        show: false,
      },
    },
    series: [
      {
        name: "Profit",
        data: [0, 200, 400, 250, 600, 700, 800, 1300, 850, 2000],
        color: "#FF5B5B",
      },
    ],
  },
  {
    title: "Total",
    value: "$8792",
    data: {
      chart: {
        height: "100%",
        maxWidth: "100%",
        type: "area",
        fontFamily: "Inter, sans-serif",
        dropShadow: {
          enabled: false,
        },
        toolbar: {
          show: false,
        },
      },
      tooltip: {
        enabled: true,
        x: {
          show: false,
        },
      },
      fill: {
        type: "gradient",
        gradient: {
          opacityFrom: 0.55,
          opacityTo: 0,
          shade: "#AB54DB",
          gradientToColors: ["#AB54DB"],
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        width: 3,
      },
      grid: {
        show: false,
        strokeDashArray: 4,
        padding: {
          left: 2,
          right: 2,
          top: 0,
        },
      },

      xaxis: {
        categories: [
          "01 February",
          "02 February",
          "03 February",
          "04 February",
          "05 February",
          "06 February",
          "07 February",
        ],
        labels: {
          show: false,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
      },
      yaxis: {
        show: false,
      },
    },
    series: [
      {
        name: "New users",
        data: [0, 200, 400, 250, 600, 700, 800, 1300, 850, 2000],
        color: "#AB54DB",
      },
    ],
  },
  {
    title: "Withdrawal Locked",
    value: "354",
    data: {
      chart: {
        height: "100%",
        maxWidth: "100%",
        type: "area",
        fontFamily: "Inter, sans-serif",
        dropShadow: {
          enabled: false,
        },
        toolbar: {
          show: false,
        },
      },
      tooltip: {
        enabled: true,
        x: {
          show: false,
        },
      },
      fill: {
        type: "gradient",
        gradient: {
          opacityFrom: 0.55,
          opacityTo: 0,
          shade: "#FF9937",
          gradientToColors: ["#FF9937"],
        },
      },
      dataLabels: {
        enabled: false,
      },
      stroke: {
        width: 3,
      },
      grid: {
        show: false,
        strokeDashArray: 4,
        padding: {
          left: 2,
          right: 2,
          top: 0,
        },
      },

      xaxis: {
        categories: [
          "01 February",
          "02 February",
          "03 February",
          "04 February",
          "05 February",
          "06 February",
          "07 February",
        ],
        labels: {
          show: false,
        },
        axisBorder: {
          show: false,
        },
        axisTicks: {
          show: false,
        },
      },
      yaxis: {
        show: false,
      },
    },
    series: [
      {
        name: "New users",
        data: [0, 200, 400, 250, 600, 700, 800, 1300, 850, 2000],
        color: "#FF9937",
      },
    ],
  },
];

export const statsChartData = {
  series: [
    {
      name: "Investor Edition",
      data: [0, 500, 1000, 750, 2000, 1500, 2200, 3000, 4500],
      color: "#1A56DB",
    },
    {
      name: "Buyer Edition",
      data: [0, 200, 1000, 360, 2500, 1250, 2700, 2650, 1800],
      color: "#AB54DB",
    },
  ],
  chart: {
    height: "100%",
    maxWidth: "100%",
    type: "area",
    fontFamily: "Inter, sans-serif",
    dropShadow: {
      enabled: false,
    },
    toolbar: {
      show: false,
    },
  },
  tooltip: {
    enabled: true,
    x: {
      show: false,
    },
  },
  legend: {
    show: false,
  },
  fill: {
    type: "gradient",
    gradient: {
      opacityFrom: 0.55,
      opacityTo: 0,
      shade: "#1C64F2",
      gradientToColors: ["#1C64F2"],
    },
  },
  dataLabels: {
    enabled: false,
  },
  stroke: {
    width: 6,
  },
  grid: {
    show: false,
    strokeDashArray: 4,
    padding: {
      left: 2,
      right: 2,
      top: 0,
    },
  },
  xaxis: {
    categories: [
      "01 February",
      "02 February",
      "03 February",
      "04 February",
      "05 February",
      "06 February",
      "07 February",
    ],
    labels: {
      show: false,
    },
    axisBorder: {
      show: false,
    },
    axisTicks: {
      show: false,
    },
  },
  yaxis: {
    show: false,
    labels: {
      formatter: function (value) {
        return "$" + value;
      },
    },
  },
};

export const paymentTypeData = [
  {
    id: 1,
    name: "UPI",
  },
  {
    id: 1,
    name: "Card",
  },
  {
    id: 1,
    name: "Cash",
  },
  {
    id: 1,
    name: "Crypto",
  },
];

export const investmentData = [
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Verified",
  },
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Verified",
  },
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Verified",
  },
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Verified",
  },
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Verified",
  },
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Verified",
  },
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Verified",
  },
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Verified",
  },
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Verified",
  },
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Verified",
  },
];

export const refferalData = [
  {
    sr: "1",
    date: "12/12/2024",
    earned: "$1500",
    name: "John Doe",
  },
  {
    sr: "1",
    date: "12/12/2024",
    earned: "$1500",
    name: "John Doe",
  },
  {
    sr: "1",
    date: "12/12/2024",
    earned: "$1500",
    name: "John Doe",
  },
  {
    sr: "1",
    date: "12/12/2024",
    earned: "$1500",
    name: "John Doe",
  },
  {
    sr: "1",
    date: "12/12/2024",
    earned: "$1500",
    name: "John Doe",
  },
  {
    sr: "1",
    date: "12/12/2024",
    earned: "$1500",
    name: "John Doe",
  },
  {
    sr: "1",
    date: "12/12/2024",
    earned: "$1500",
    name: "John Doe",
  },
  {
    sr: "1",
    date: "12/12/2024",
    earned: "$1500",
    name: "John Doe",
  },
  {
    sr: "1",
    date: "12/12/2024",
    earned: "$1500",
    name: "John Doe",
  },
  {
    sr: "1",
    date: "12/12/2024",
    earned: "$1500",
    name: "John Doe",
  },
];

export const withdrawalData = [
  {
    sr: "1",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Received",
    type: "Refferal Income",
  },
  {
    sr: "2",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Received",
    type: "Investment Income",
  },
  {
    sr: "3",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Received",
    type: "Refferal Income",
  },
  {
    sr: "4",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Received",
    type: "Investment Income",
  },
  {
    sr: "5",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Received",
    type: "Refferal Income",
  },
  {
    sr: "6",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Received",
    type: "Investment Income",
  },
  {
    sr: "7",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Received",
    type: "Refferal Income",
  },
  {
    sr: "8",
    date: "12/12/2024",
    amount: "$1500",
    tax: "5%",
    mode: "Card",
    status: "Received",
    type: "Investment Income",
  },
];

export const withdrawalTypeData = [
  {
    id: 1,
    name: "UPI",
    logo: <TbCurrencyRupee size={40}/>,
  },
  {
    id: 3,
    name: "Crypto",
    logo: <FaBitcoin color="#F7931A" size={40}/>,
  },

  {
    id: 2,
    name: "PayPal",
    logo: <BiLogoPaypal color="#253b80" size={40}/>,
  },
];
