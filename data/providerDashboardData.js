import { MdOutlineDashboard } from "react-icons/md";
import { PiCurrencyCircleDollarBold } from "react-icons/pi";
import { TfiWorld } from "react-icons/tfi";
import { MdOutlineEmojiPeople, MdOutlineHelpOutline } from "react-icons/md";
import { MdCleaningServices } from "react-icons/md";
import { BiSolidOffer } from "react-icons/bi";
import { MdOutlineReviews, MdMiscellaneousServices } from "react-icons/md";

export const providerDashboardSidebarData = [
  {
    id: 1,
    title: "Service Provider",
    icon: <PiCurrencyCircleDollarBold size={25} />,
    link: "/providersdashboard",
    subItems: [
      {
        id: 1,
        title: "Leads",
        icon: <MdOutlineEmojiPeople size={20} />,
        link: "/providersdashboard/leads",
      },
      {
        id: 2,
        title: "Services",
        icon: <MdCleaningServices size={20} />,
        link: "/providersdashboard/services",
      },
      {
        id: 3,
        title: "Offers",
        icon: <BiSolidOffer size={20} />,
        link: "/providersdashboard/offers",
      },
      {
        id: 4,
        title: "Service Profile",
        icon: <MdMiscellaneousServices size={20} />,
        link: "/providersdashboard/service-profile",
      },
      {
        id: 5,
        title: "Rating",
        icon: <MdOutlineReviews size={20} />,
        link: "/providersdashboard/rating",
      },
      {
        id: 6,
        title: "FAQs",
        icon: <MdOutlineReviews size={20} />,
        link: "/providersdashboard/faq",
      },
    ],
  },
  {
    id: 2,
    title: "Influencer Dashboard",
    icon: <MdOutlineDashboard size={25} />,
    // link: "/providersdashboard/inf",
    link: "/home",

    subItems: [],
  },
  {
    id: 3,
    title: "Website and online tools",
    icon: <TfiWorld size={25} />,
    // link: "/providersdashboard/tools",
    link: "/home",
    subItems: [],
  },
  {
    id: 2,
    title: "Contact Us",
    icon: <MdOutlineDashboard size={25} />,
    link: "/providersdashboard/contact",
    subItems: [],
  },
  {
    id: 5,
    title: "Help",
    icon: <MdOutlineHelpOutline size={25} />,
    link: "/providersdashboard/help",
    subItems: [],
  },
];
