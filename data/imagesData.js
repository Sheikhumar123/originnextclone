import airbus from "@/images/logos/airbus.png";
import amazon from "@/images/logos/amazon.png";
import deloitte from "@/images/logos/deloitte.png";
import nasa from "@/images/logos/nasa.png";
import facebook from "@/images/logos/facebook.png";
export const featuredImgDataorLandingPages = [
  airbus,
  amazon,
  deloitte,
  nasa,
  facebook,
];
