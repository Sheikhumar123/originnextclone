export const influencerCountry = [
  { title: "USA", value: 86 },
  { title: "India", value: 56 },
  { title: "Other", value: 41 },
];
export const randomGraphColors = [
  "text-lime-600",
  "text-yellow-600",
  "text-blue-600",
  "text-red-600",
  "text-green-600",
  "text-orange-600",
  "text-purple-600",
  "text-violet-600",
];
export const influencerCardData = [
  {
    bgLabel: "bg-orange-500",
    profileImg: "https://i.ibb.co/PmSDLcx/Rectangle-34624903.png",
    platform: "Blogger",
    name: "Ava Robert",
    subtitle:
      "I am a skilled blogger with lots of experience in the releavnt niche",
    statsData: [
      {
        title: "Email Sub",
        value: 300,
      },
      {
        title: "Indeed Days",
        value: 58,
      },
      {
        title: "Posts",
        value: 60,
      },
      {
        title: "DA",
        value: 565,
      },
      {
        title: "DR",
        value: 55,
      },
      {
        title: "PA",
        value: 145,
      },
      {
        title: "Monthly Trafic",
        value: 565,
      },
    ],
    profileData: [
      {
        title: "Language",
        value: "English",
      },
      {
        title: "Type",
        value: "Profile",
      },
      {
        title: "Niche",
        value: "Entertainment",
      },
    ],
    influencerAudienceCountry: [
      { title: "USA", value: 86 },
      { title: "India", value: 56 },
      { title: "Other", value: 41 },
    ],
    description:
      "Sarah, a fashion and lifestyle blogger influencer, captivates her audience with her impeccable style and authentic personality. Through her engaging content, she shares fashion tips, beauty secrets, and insights into her daily life. With a keen eye for trends and a genuine connection with her followers, Sarah inspires her audience to embrace their individuality and live confidently. Her vibrant social media presence and relatable approach make her a trusted voice in the influencer community. Sarah's passion for creativity and self-expression shines through her curated posts, fostering a loyal and growing following.",
    price: 5000,
  },
  {
    bgLabel: "bg-red-500",
    profileImg: "https://i.ibb.co/PmSDLcx/Rectangle-34624903.png",
    platform: "Youtube",
    name: "Lucas Martinez",
    subtitle: "Lifestyle enthusiast sharing moments of joy and inspiration",
    statsData: [
      {
        title: "Followers",
        value: 200000,
      },
      {
        title: "Posts",
        value: 500,
      },
      {
        title: "Average Likes",
        value: 10000,
      },
      {
        title: "Engagement Rate",
        value: "6%",
      },
      {
        title: "Average Comments",
        value: 500,
      },
    ],
    profileData: [
      {
        title: "Language",
        value: "English and Spanish",
      },
      {
        title: "Type",
        value: "Profile",
      },
      {
        title: "Niche",
        value: "Travel and Photography",
      },
    ],
    influencerAudienceCountry: [
      {
        title: "USA",
        value: "30",
      },
      {
        title: "Brazil",
        value: "15",
      },
      {
        title: "Spain",
        value: "10",
      },
      {
        title: "Other",
        value: "45",
      },
    ],
    description:
      "Lucas Martinez, an avid traveler and photography enthusiast, shares his adventures and insights with a diverse Instagram audience. With stunning visuals and heartfelt captions, Lucas transports his followers to breathtaking destinations around the world. His authentic storytelling and genuine connections resonate deeply with his community, inspiring them to explore new horizons and cherish life's moments. Lucas' Instagram feed is a testament to his passion for exploration and appreciation of diverse cultures, fostering a sense of wanderlust and connection among his followers.",
    price: 2000,
  },
  {
    bgLabel:
      "bg-gradient-to-r from-[#833AB4] via-[#C13584] to-[#E1306C] via-[#FD1D1D] to-[#F77737]",
    profileImg: "https://i.ibb.co/PmSDLcx/Rectangle-34624903.png",
    platform: "Instagram",
    name: "Lucas Martinez",
    subtitle: "Lifestyle enthusiast sharing moments of joy and inspiration",
    statsData: [
      {
        title: "Followers",
        value: 200000,
      },
      {
        title: "Posts",
        value: 500,
      },
      {
        title: "Average Likes",
        value: 10000,
      },
      {
        title: "Engagement Rate",
        value: "6%",
      },
      {
        title: "Average Comments",
        value: 500,
      },
    ],
    profileData: [
      {
        title: "Language",
        value: "English and Spanish",
      },
      {
        title: "Type",
        value: "Profile",
      },
      {
        title: "Niche",
        value: "Travel and Photography",
      },
    ],
    influencerAudienceCountry: [
      {
        title: "USA",
        value: "30",
      },
      {
        title: "Brazil",
        value: "15",
      },
      {
        title: "Spain",
        value: "10",
      },
      {
        title: "Other",
        value: "45",
      },
    ],
    description:
      "Lucas Martinez, an avid traveler and photography enthusiast, shares his adventures and insights with a diverse Instagram audience. With stunning visuals and heartfelt captions, Lucas transports his followers to breathtaking destinations around the world. His authentic storytelling and genuine connections resonate deeply with his community, inspiring them to explore new horizons and cherish life's moments. Lucas' Instagram feed is a testament to his passion for exploration and appreciation of diverse cultures, fostering a sense of wanderlust and connection among his followers.",
    price: 2000,
  },
];
export const influencerLandingPageData = [
  {
    title: "Popular Categories",
    categories: [
      {
        name: "Website Developer",
        icon: "https://i.ibb.co/dWzx627/Vivid-JS.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
    ],
  },
  {
    title: "Popular Skills",
    categories: [
      {
        name: "Website Developer",
        icon: "https://i.ibb.co/dWzx627/Vivid-JS.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
    ],
  },
];
export const randomInfluencerBg = [
  "bg-red-100",
  "bg-yellow-100",
  "bg-green-100",
  "bg-blue-100",
];

export const onBoardData = [
  {
    title: "Select your category",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic odio a eius possimus accusantium. Eaque at iusto quae dolor et fuga eum totam, sunt nisi!",
    imageUrl: "https://i.postimg.cc/pLnN9B8g/Influencer-Form-step-1-1.png",
  },
  {
    title: "Register for free",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic odio a eius possimus accusantium. Eaque at iusto quae dolor et fuga eum totam, sunt nisi!",
    imageUrl: "https://i.postimg.cc/t4pzdymd/Influencer-Form-step-2-1.jpg",
  },
  {
    title: "Add details about your services",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic odio a eius possimus accusantium. Eaque at iusto quae dolor et fuga eum totam, sunt nisi!",
    imageUrl: "https://i.postimg.cc/V64Vbgww/Influencer-Form-step-11-1.png",
  },
  {
    title: "Done, Start Getting Clients..",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic odio a eius possimus accusantium. Eaque at iusto quae dolor et fuga eum totam, sunt nisi!",
    imageUrl: "https://i.postimg.cc/zGVHmGvF/Influencer-Form-step-12-1.png",
  },
];
export const benefitsData = [
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
];
export const serviceProviderData = [
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
];
export const influencerContactPlatformData = [
  { id: 1, name: "Facebook" },
  { id: 2, name: "Instagram" },
  { id: 3, name: "Blogger" },
  { id: 4, name: "Youtube" },
];
