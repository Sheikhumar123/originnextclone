export const WebsiteToolsLandingPageData = [
  {
    title: "Popular Categories",
    categories: [
      {
        name: "Website Developer",
        icon: "https://i.ibb.co/dWzx627/Vivid-JS.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
    ],
  },
  {
    title: "Popular Skills",
    categories: [
      {
        name: "Website Developer",
        icon: "https://i.ibb.co/dWzx627/Vivid-JS.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
      {
        name: "Graphic Designer",
        icon: "https://i.ibb.co/vk2R8Ym/Group-1597885717.png",
      },
    ],
  },
];
export const randomWebsiteToolsBg = [
  "bg-red-100",
  "bg-yellow-100",
  "bg-green-100",
  "bg-blue-100",
];

export const onBoardData = [
  {
    title: "Select your category",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic odio a eius possimus accusantium. Eaque at iusto quae dolor et fuga eum totam, sunt nisi!",
    imageUrl: "https://i.postimg.cc/pLnN9B8g/Influencer-Form-step-1-1.png",
  },
  {
    title: "Register for free",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic odio a eius possimus accusantium. Eaque at iusto quae dolor et fuga eum totam, sunt nisi!",
    imageUrl: "https://i.postimg.cc/t4pzdymd/Influencer-Form-step-2-1.jpg",
  },
  {
    title: "Add details about your services",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic odio a eius possimus accusantium. Eaque at iusto quae dolor et fuga eum totam, sunt nisi!",
    imageUrl: "https://i.postimg.cc/V64Vbgww/Influencer-Form-step-11-1.png",
  },
  {
    title: "Done, Start Getting Clients..",
    text: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic odio a eius possimus accusantium. Eaque at iusto quae dolor et fuga eum totam, sunt nisi!",
    imageUrl: "https://i.postimg.cc/zGVHmGvF/Influencer-Form-step-12-1.png",
  },
];
export const benefitsData = [
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
];
export const serviceProviderData = [
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
  "benefits to become service provider in our website",
];
