/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      boxShadow: {
        loginShadow: "rgba(0, 0, 0, 0.24) 0px 3px 8px",
        loadmore: "rgba(0, 0, 0, 0.35) 0px 5px 15px",
      },
      backgroundImage: {
        "repeat-x-6":
          "repeating-linear-gradient(90deg, #808080, #808080 6px, transparent 6px, transparent 12px)",
        "bg-designer-img": "url('/designers-page-bg.webp')",
        "bg-gradient-slider": "url('/gradient-bg-slider.webp')",
        "bg-gradient-started": "url('/get-started-bg.webp')",
        "bg-gradient-research": "url('/bg-writer-img.webp')",
        pricingBg: "url('/pricingBg.webp')",
      },

      keyframes: {
        spinWords: {
          "10%": { transform: "translateY(-112%)" },
          "25%": { transform: "translateY(-100%)" },
          "35%": { transform: "translateY(-212%)" },
          "50%": { transform: "translateY(-200%)" },
          "60%": { transform: "translateY(-312%)" },
          "75%": { transform: "translateY(-300%)" },
          "85%": { transform: "translateY(-412%)" },
          "100%": { transform: "translateY(-400%)" },
        },
      },
      animation: {
        spinwords: "spinWords 6s linear infinite ",
      },
      fontFamily: {
        poppins: ["Poppins", "sans-serif"],
      },
      colors: {
        bgBlue: "#444BDB",
        recommendedBlue: "#D8F8FF",
        recommendedRed: "#FAD8FF",
        recommendedBlack: "#000",
        headingBlack: "#585858",
        lightBlack: "#ACADAC",
        border: "#d5d5d5",
        mainblue: "#317ff4",
        lightblue: "#5a99f6",
        lightyellow: "#D8F8FF",
        lightpink: "#fad8ff",
        white: "#FFFFFF",
        mainpurple: "#434cd9",
        textgrey: "#dddddd",
        textdarkgrey: "#666666",
        lightbg: "#FAFAFA",
      },
      screens: {
        "100px": "100px",
        "200px": "200px",
        "300px": "300px",
        "400px": "400px",
        "500px": "500px",
        "600px": "600px",
        "700px": "700px",
        "800px": "800px",
        "900px": "900px",
        "1000px": "1000px",
        "1100px": "1100px",
        "1200px": "1200px",
        "1300px": "1300px",
        "1400px": "1400px",
        "1500px": "1500px",
        "1600px": "1600px",
        "1700px": "1700px",
        "1800px": "1800px",
        "1900px": "1900px",
        "2000px": "2000px",
      },
    },
  },

  plugins: [
    require("daisyui"),
    require("tailwind-scrollbar")({ nocompatible: true }),
    require("tailwind-scrollbar-hide"),
    require("tailwindcss-animation-delay"),
  ],
  daisyui: {
    themes: ["light"],
    darkMode: false,
  },

  darkMode: "class",
};
