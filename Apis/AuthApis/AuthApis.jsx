import axiousInstance from "@/utils/axiousInstance";

export const forgotPassowordApi = async (data) => {
  try {
    let response = await axiousInstance.post("auth/forgetpassword", data);
    return response;
  } catch (error) {
    return error;
  }
};

export const verifyPinApi = async (data) => {
  try {
    let response = await axiousInstance.post("auth/verifypin", data);
    return response;
  } catch (error) {
    return error;
  }
};

export const changePasswordApi = async (data) => {
  try {
    let response = await axiousInstance.post("auth/changepassword", data);
    return response;
  } catch (error) {
    return error;
  }
};
