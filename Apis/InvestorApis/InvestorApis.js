import axiousInstance from "@/utils/axiousInstance";

export const registerInvestorApi = async (data) => {
  try {
    let response = await axiousInstance.post(
      "/auth/registerinvestor",
      data
    );
    return response;
  } catch (error) {
    return error;
  }
};
export const registerInvestmentAddApi = async (data) => {
  try {
    let response = await axiousInstance.post(
      "auth/investortransactionsadd",
      data,{
        headers:{
          'Content-Type': 'multipart/form-data',
        }
      }
    );
    return response;
  } catch (error) {
    return error;
  }
};
export const registerInvestorRefrelApi = async (data) => {
  try {
    let response = await axiousInstance.post(
      "auth/referaluserapi",
      data
    );
    return response;
  } catch (error) {
    return error;
  }
};
export const withdrawInvestorRefrelApi = async (data) => {
  try {
    let response = await axiousInstance?.post(
      "investor/investorwithdrawl",
      data
    );
    return response;
  } catch (error) {
    return error;
  }
};
export const InvestorGenereteCodeApi = async (data) => {
  try {
    let response = await axiousInstance.post(
      "investor/codegeneratingapi",
      data
    );
    return response;
  } catch (error) {
    return error;
  }
};