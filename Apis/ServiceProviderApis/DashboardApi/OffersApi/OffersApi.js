import axiousInstance from "@/utils/axiousInstance";

export const AddOffer = async (data) => {
  try {
    let response = await axiousInstance.post("dashboard/addOffers", data);
    return response;
  } catch (error) {
    return error;
  }
};
export const UpdateOffer = async (data) => {
  try {
    let response = await axiousInstance.post("dashboard/updateOffers", data);
    return response;
  } catch (error) {
    return error;
  }
};

export const DeleteOffer = async (data) => {
  try {
    let response = await axiousInstance.post("dashboard/OffersDelete", data);
    return response;
  } catch (error) {
    return error;
  }
};

export const GetSingleOffer = async (id) => {
  try {
    let response = await axiousInstance.get(
      `dashboard/getsingleofferdetail?id=${id}`
    );
    return response;
  } catch (error) {
    return error;
  }
};
