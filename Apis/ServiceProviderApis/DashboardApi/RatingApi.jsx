import axiousInstance from "@/utils/axiousInstance";

export const AddRatingApi = async (data) => {
    try {
      let response = await axiousInstance.post("landingpage/addReview", data);
      return response;
    } catch (error) {
      return error;
    }
  };
  export const UpdateRatingApi = async (data) => {
    try {
      let response = await axiousInstance.post("userdata/updateReview", data);
      return response;
    } catch (error) {
      return error;
    }
  };

  export const ReplyToReview = async (data) => {
    try {
      let response = await axiousInstance.post("landingpage/replytoReview", data);
      return response;
    } catch (error) {
      return error;
    }
  };



  