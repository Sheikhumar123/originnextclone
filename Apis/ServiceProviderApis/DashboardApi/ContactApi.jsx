import axiousInstance from "@/utils/axiousInstance";

export const addContactApi = async (data) => {
    try {
      let response = await axiousInstance.post("dashboard/addcontactusinfo", data);
      return response;
    } catch (error) {
      return error;
    }
  };
  