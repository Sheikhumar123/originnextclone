import axiousInstance from "@/utils/axiousInstance";

export const changePasswordSecurity = async (data) => {
    try {
      let response = await axiousInstance.post("dashboard/changepassword", data);
      return response;
    } catch (error) {
      return error;
    }
  };
  