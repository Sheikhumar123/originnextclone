import axiousInstance from "@/utils/axiousInstance";

export const getNotificationApi = async (userId) => {
    try {
      let response = await axiousInstance.get(`dashboard/getuserNotificationCheck?user_id=${userId}`);
      return response;
    } catch (error) {
      return error;
    }
  };
  export const createNotificationApi = async (data) => {
    try {
      let response = await axiousInstance.post(`dashboard/addNotificationCheck`,data);
      return response;
    } catch (error) {
      return error;
    }
  };