import axiousInstance from "@/utils/axiousInstance";
import Cookies from "js-cookie";

export const DeleteServiceApi = async (data) => {
    try {
      let response = await axiousInstance.post("dashboard/deleteservice", data,{
        headers:{
        Authorization:`${Cookies.get("orignToken")}`,
          'Content-Type': 'application/json',
        }
      }
    );
      return response;
    } catch (error) {
      return error;
    }
  };
  