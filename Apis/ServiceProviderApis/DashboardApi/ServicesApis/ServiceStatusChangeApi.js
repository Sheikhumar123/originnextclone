import axiousInstance from "@/utils/axiousInstance";

export const ServiceStatusChangeApi = async (data) => {
  try {
    let response = await axiousInstance.post(
      "dashboard/statuschangeservice",
      data,
    );
    return response;
  } catch (error) {
    return error;
  }
};
