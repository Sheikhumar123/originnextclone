import axiousInstance from "@/utils/axiousInstance";
import Cookies from "js-cookie";

export const EditProfile = async (data) => {
  try {
    let response = await axiousInstance.post(
      "investor/investorprofileupt",
      data,
      {
        headers: {
          Authorization: Cookies.get("orignToken"),
          "Content-Type": "multipart/form-data",
        },
      }
    );
    return response;
  } catch (error) {
    return error;
  }
};

export const ChangePassword = async (data) => {
  try {
    let response = await axiousInstance.post("auth/updatenewpassword", data, {
      headers: {
        Authorization: Cookies.get("orignToken"),
        "Content-Type": "application/json",
      },
    });
    console.log(response);
    return response;
  } catch (error) {
    return error;
  }
};
