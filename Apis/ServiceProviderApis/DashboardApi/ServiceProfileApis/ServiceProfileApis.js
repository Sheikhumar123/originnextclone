import axiousInstance from "@/utils/axiousInstance";
import Cookies from "js-cookie";

export const ServiceProfileEditApi = async (data) => {
  try {
    let response = await axiousInstance.post(
      "dashboard/profiledashboardedit",
      data,
      {
        headers: {
          Authorization: Cookies.get("orignToken"),
          "Content-Type": "multipart/form-data",
        },
      }
    );
    return response;
  } catch (error) {
    return error;
  }
};
