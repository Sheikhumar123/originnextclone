import axiousInstance from "@/utils/axiousInstance";

export const AuthFactorApi = async (data) => {
  try {
    let response = await axiousInstance.post("dashboard/generalsetting", data, {
      headers: {
        "Content-Type": "application/json",
      },
    });
    return response;
  } catch (error) {
    return error;
  }
};
