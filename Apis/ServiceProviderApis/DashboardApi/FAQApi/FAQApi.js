import axiousInstance from "@/utils/axiousInstance";

export const AddFAQ = async (data) => {
  try {
    let response = await axiousInstance.post("userdata/addproviderfaqs", data);
    return response;
  } catch (error) {
    return error;
  }
};
export const UpdateFAQ = async (data) => {
  try {
    let response = await axiousInstance.post("userdata/updateFAQ", data);
    return response;
  } catch (error) {
    return error;
  }
};

export const DeleteFAQ = async (data) => {
  try {
    console.log(data);
    let response = await axiousInstance.delete(`userdata/deleteFAQ/${data}`);
    return response;
  } catch (error) {
    return error;
  }
};

export const GetFAQCout = async (id) => {
  try {
    let response = await axiousInstance.get(
      `dashboard/faqscountbyid?user_id=${id}`
      
    );
    return response;
  } catch (error) {
    return error;
  }
};
