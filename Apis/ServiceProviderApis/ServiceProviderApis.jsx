import axiousInstance from "@/utils/axiousInstance";

export const verifyEmailApi = async (data) => {
  try {
    let response = await axiousInstance.post("auth/verifyemail", data);
    return response;
  } catch (error) {
    return error;
  }
};

export const verifyNumberApi = async (data) => {
  try {
    let response = await axiousInstance.post("auth/verifynumber", data);
    return response;
  } catch (error) {
    return error;
  }
};

export const addServiceProviderCategoryData = async (data) => {
  try {
    let response = await axiousInstance.post(
      "userdata/serviceproviderforms",
      data
    );
    return response;
  } catch (error) {
    return error;
  }
};
