import { useState } from "react";

const usePagination = () => {
  const [LastPage, setLastPage] = useState(0);
  const [FirstPage, setFirstPage] = useState(0);
  const [paginationArr, setPaginationArr] = useState([]);

  const SetpaginationData = (limit, totalcount, page) => {
    let from = page * limit - limit + 1;
    let to = page * limit > totalcount ? totalcount : page * limit;
    let pageNumbers = Math.ceil(totalcount / limit);
    let arr = [];
    for (let i = 0; i < pageNumbers; i++) {
      arr.push(i + 1);
    }
    setLastPage(to);
    setFirstPage(from);
    setPaginationArr(arr);
  };

  return [FirstPage, LastPage, paginationArr, SetpaginationData];
};

export default usePagination;
