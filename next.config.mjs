/** @type {import('next').NextConfig} */
const nextConfig = {
  async redirects() {
    return [
      {
        source: "/",
        destination: "/home",
        permanent: true,
      },
    ];
  },
  images: {
    domains: [
      "images.unsplash.com",
      "i.ibb.co",
      "source.unsplash.com",
      "s3-alpha-sig.figma.com",
      "cdn.pixabay.com",
      "i.postimg.cc",
      '192.168.0.119'
    ],
  },
};

export default nextConfig;
